#pragma once

//模拟实现string类：
//先实现非模板版本：
 
//包含头文件：
#include <assert.h> //断言头文件


//自定义命名空间：
namespace ggdpz
{
	//模拟实现非模板string类：
	class string
	{
		//公有成员函数：
	public:

		//简易迭代器的实现：：
		//非const版本：可读可写，指针本身可以修改，指向内容不能修改
		typedef char* iterator; //使用原生指针
		//将char*重命名为iterator（迭代器）
		//typedef因此能够屏蔽底层的一些细节，实现封装的作用

		//创建迭代器的begin()方法：
		iterator begin()
			//iterator即char*（字符指针）
		{
			//返回同为char*的_str
			return _str; //即字符串首字符指针
		}

		//创建迭代器的end()方法：
		iterator end()
			//iterator即char*（字符指针）
		{
			return _str + _size;
			/*
			* 返回字符串结束符‘\0’的指针，
			* _str + size 即 字符串结束符指针
			*/
		}
		/*
		* 数据结构的底层空间是连续的才可以
		* 通过这种简单的方式来实现该数据结构的迭代器
		* （string字符串底层是数组则可以，如果是链表则不可以了）
		*/


		//const版本：只读，指针本身可以修改，指向内容不能修改
		typedef const char* const_iterator; //const版本char指针

		//创建迭代器的begin()方法：const版本
		const_iterator begin() const
			//const_iterator即const char*（字符指针）
		{
			//返回同为char*的_str
			return _str; //即字符串首字符指针
		}

		//创建迭代器的end()方法：const版本
		const_iterator end() const
			//iterator即char*（字符指针）
		{
			return _str + _size;
			/*
			* 返回字符串结束符‘\0’的指针，
			* _str + size 即 字符串结束符指针
			*/
		}


		////string类无参构造函数：
		//string()
		//	:_str(new char[1])
		//	,_size(0)
		//	,_capacity(0)
		//{
		//	_str[0] = '\0';
		//	/*
		//	* string类无参构造函数中:
		//	* char* _str = new char[1];
		//	* 字符串指针指向的空间虽然没有存储数据，
		//	* 但是也应该为其开辟一个空间，用于存储'\0'，
		//	* 即字符串结尾，不能直接将指针置为空指针，
		//	* 不然cout识别时会崩溃（不能解引用空指针）
		//	*/
		//}

		//无参构造函数一般不显示写出来，在有参构造中通过缺省值代替即可：		

		//string类全缺省有参构造函数：
		string(const char* str = "") //接收常量字符串
			/*
			*				const char* str = ""
			* 这里的缺省值应该是"",即空字符串，它会默认以'\0'结尾，
			* 也可以写成："\0"，这样写实际会有两个'\0'，但也是行得通的
			*/
		{
			//_size：长度和参数str的一样
			_size = strlen(str);

			//_capaciyt：
			_capacity = _size;
			/*
			*				capacity：
			* 这里容量初始化和_size一样，即有效字符的个数，
			* 不包括‘\0’。这里容量也可以是 参数str长度+1，
			* 但之后扩容时就需要对应调整（一般不这样初始化）
			*/

			//_str：开辟 参数str长度+1 的空间（1是给‘\0’的）
			_str = new char[_capacity + 1];

			//开辟对应空间后，_str进行初始化：
			strcpy(_str, str); //str拷贝给_str
			/*
			* 如果接收的是常量指针，就没法进行增删改查操作。
			* 所以需要开辟空间把字符串拷贝过来
			*/

			/*
			* string类中：
			* 这里在构造函数体内按照 _size->_capaciyt->_str 的顺序，
			* 依次进行初始化，_size先初始化后，_capacity的初始化就可以复用到_size，
			* 而不是使用"_capacity = strlen(str)"，提高效率（不用再strlen遍历一次str），
			* _str同理复用_capacity进行初始化提高效率，
			* 而不是 “_str = new char[strlen(str) + 1];” （不用再strlen遍历一次str）
			*
			* 如果这里是通过初始化列表来初始化这三个成员变量的话，
			* 要按照 _size->_capaciyt->_str 就会很麻烦，
			* 因为初始化列表的执行顺序是按照private中成员变量写的顺序执行的，
			* 所以如果硬要通过这个顺序执行的话，就要改变private中成员变量的顺序，
			* 这很“奇怪”，所以初始化列表的使用也要看实际情况
			* (内置类型 通过 初始化列表初始化 还是 函数体内初始化 区别不大)
			*/
		}



		//string类析构函数：
		~string()
		{
			//delete释放之前new申请的空间：
			delete[] _str;

			//指向空间的指针置为空：
			_str = nullptr;

			//字符串长度（大小）置为0
			_size = 0;

			//字符串容量置为0
			_capacity = 0;
		}



		//c_str()方法：
		//功能：string类将C++字符串转化为C语言格式
		const char* c_str() const //（只读）
			//第一个const修饰返回值，
			//第二个const修饰this指针（*this）
		{
			return _str;
			/*
			* 私有成员变量_str的类型是char*，
			* C语言中字符串就是这种类型，
			* 所以直接返回_str即可
			*/
		}



		//size()方法：
		//功能：计算string类字符串的长度（大小）并返回
		size_t size() const //（只读）
		{
			return _size;
			/*
			* 我们有设置对应的私有成员变量_size，
			* 直接返回_size即可
			*/
		}



		//operator[] -- “[]”运算符重载方法（第一个版本--只读）
		//功能：让string字符串可以像数组一样使用下标(pos)
		const char& operator[](size_t pos) const
		//对于const对象，只读不能修改（如打印）
			//第一个const修饰返回值，
			//第二个const修饰this指针（*this）  
		{
			//断言：pos下标不能超过_size字符串长度
			assert(pos <= _size);
			/*
			* size如果等于0，即字符串长度为0，
			* 只有一个默认的'\0'，pos为0的字符即'\0'，
			* '\0'也是可以被修改的。
			* 断言后如果下标越界就能第一时间被检测出来
			*/

			return _str[pos];
			/*
			* 返回类型为引用类型：char&
			* 直接返回stirng字符串中pos位置字符的引用（别名）即可，
			* 作为‘别名’，‘别名’被修改，字符串中对应字符也会被修改
			*
			* 如果是传值返回的话，那么返回的就是对应位置字符的拷贝，
			* 这时就只能获取该字符，而不能修改字符串中的对应字符
			*
			* 所以引用返回就有两层意义：
			* 1、减少拷贝，提高效率
			* 2、可以直接修改返回值
			*/
		}

		//operator[] -- “[]”运算符重载方法（第二个版本--可读可写）
		//功能：让string字符串可以像数组一样使用下标(pos)
		char& operator[](size_t pos)
			//对于非const对象，可读可写（如遍历修改）
		{
			//断言：pos下标不能超过_size字符串长度
			assert(pos <= _size);

			return _str[pos];
		}



		//push_back方法：
		//功能：在string字符串后尾插一个字符(ch)
		void push_back(char ch);



		//append方法：
		//功能：在string字符串后尾插一个常量字符串(str)
		void append(const char* str);



		//operator+= -- “[]”运算符重载函数：
		//功能（第一个版本）：在string字符串后"加上"一个字符(ch)
		string& operator+=(char ch);



		//operator+= -- “[]”运算符重载函数：
		//功能（第二个版本）：在string字符串后"加上"一个常量字符串(str)
		string& operator+=(const char* str);





	//私有成员变量：
	private: 
		char* _str; //字符串首字符指针 
		size_t _size; //字符串长度（大小）
		size_t _capacity; //字符串容量（有效字符空间个数）
	};



	//print方法：
	//功能：打印字符串
	void print_str(const string& s)
		//参数s是const修饰的
	{
		//第一种方式：for循环遍历
		for (size_t i = 0; i < s.size(); i++)
		{
			//调用第一个“[]”重载函数：
			cout << s[i] << " ";  //（只读）
			//s[i]：这里参数是const修饰的，打印不需要修改字符串内容
		}
		//换行：
		cout << endl;


		//第二种方式：迭代器遍历(打印--只读--const版本迭代器)
		//const版本迭代器：本身可以修改，指向的内容不能修改
		string::const_iterator it = s.begin();
		while (it != s.end())
		{
			cout << *it << " ";
			++it; //指针本身可以修改，指向内容不能修改
		}
		cout << endl;
	}



	//测试函数1：
	void test_string1()
	{
		//测试：全缺省有参构造函数
		
		//string(const char* str = "")
		string s1("hello world"); //有参调用
		//通过C_str将字符串转化为C语言格式后打印：
		cout << s1.c_str() << endl;

		string s2; //无参调用
		//打印：
		cout << s2.c_str() << endl;
		/*
		* cout 在识别输出类型时，如果是char*，
		* 那么会认为你要输入的是字符串，然后就会去解引用char*，
		* 那么如果无参构造函数中将“char* _str = nullptr；"
		* 就会解引用空指针，之后就会报错
		*/


		//第一种遍历方式：“[]”
		//测试：operator[]重载函数
		for (size_t i = 0; i < s1.size(); i++)
		{
			//调用第二个"[]"重载函数（可读可写）：
			s1[i]++; 
			//使用“[]”遍历修改字符串内容，需要可读可写
		}
		//打印：
		cout << s1.c_str() << endl;


		//第二种遍历方式：“迭代器iterator”
		//测试：迭代器：
		string::iterator it = s1.begin();
		//实际就是 char* it = s1.begin();
		while (it != s1.end())
			//只要it还没指到字符串‘\0’位置：
		{
			//解引用char*指针it打印当前字符：
			cout << *it << " ";
			//调整指针（迭代器）：
			++it;
		}
		//换行：
		cout << endl;


		//第三种遍历方式：auto范围for循环
		//实际还是第二种遍历方式
		for (auto ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;
		/*
		* 范围for循环在预编译时，
		* 会被简单地（傻瓜式地）替换成迭代器（库中的），
		* 也就是说这里的范围for循环和上面的
		* 简易迭代器本质上是一样的，
		* 如果把上面迭代器方法的名字
		* 从 begin(end) 变成 Begin(End) ,
		* 那这里的范围for循环就执行不了了
		* (迭代器名字不按规范取名，范围for循环就用不了了)
		*/


		//测试：打印字符串
		print_str(s1);
	}


	//测试函数2：
	void test_string2()
	{ 
		string s1("hello world");
		cout << s1.c_str() << endl;
	}
}