#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

////一般在传参才会用到const对象：
////(让对象在函数中使用时不会被改变)
//void func(const string& s)
//{
//	//第二种：const对象版本：
//	string::const_reverse_iterator it = s.rbegin();
//	//这里反向迭代器的const版本的类型很长，所以可以用auto进行省略：
//	auto it = s.rbegin();
//	
//	//进行反向遍历：
//	while (it != s.rend())
//	{
//		//"只读"
//		//*it1 = 'x'; //报错，无法‘写’
//
//		//打印当前字符串s1中it指针指向的字符：
//		cout << *it << " ";
//		//调整it指针：
//		++it;
//	}
//	cout << endl; //换行
//}
//
//
//int main()
//{
//	//string类对象：
//	string s1("hello world");
//	/*
//	* string类对象的三种遍历方式：
//	* 1、下标 + [] ：只适用于底层连续的容器
//	* 2、迭代器 ：yyds，能配合算法使用
//	* 3、范围for ：看似好用，实际是依靠迭代器实现的
//	*/
//
//	/*
//	* string的反向迭代器：std::string::rbegin
//	* 
//	* 第一种：reverse_iterator rbegin();
//	* 第二种：const_reverse_iterator rbegin() const;
//	* (rend 和 rbegin 类似)
//	* 
//	* 对于没有下标的情况，如链表，如果要进行倒着遍历，
//	* 就可以使用其反向迭代器，掌握了迭代器，
//	* 就掌握了所有容器的遍历、访问、修改
//	*/
//	  
//	//第一种：非const版本：
//	string::reverse_iterator it1 = s1.rbegin();
//	//进行反向遍历：
//	while (it1 != s1.rend())
//	{
//		//"可读可写"
//		//*it1 = 'x'; //‘写’
//		
//		//打印当前字符串s1中it指针指向的字符：
//		cout << *it1 << " ";
//		//调整it指针：
//		++it1;
//	}
//	cout << endl; //换行
//
//	/*
//	* 打印："d l r o w  o l l e h"，
//	* 即实现了s1："h e l l o  w o r l d"的反向遍历
//	*/
//
//
//	//第二种：const对象版本：
//	func(s1);
//	/*
//	* func函数接收const对象，
//	* s1不是const对象，传过去后权限缩小，
//	* “可读可写” 变成 “只读”
//	*/
//
//	return 0;
//}



//int main()
//{
//	//std::string::max_size
//	//（返回字符串可以达到的最大长度）
//
//	//无参字符串对象：
//	string s1;
//
//	//有参字符串对象：
//	string s2("hello world");
//
//	cout << s1.max_size() << endl;
//	cout << s2.max_size() << endl;
//	/*
//	* 无论是s1的无参，还是s2的有参，
//	* 打印两者的“max_size()”时，
//	* (32位系统)都是“214783647”(2^31)，
//	* 即整型最大值的一半,
//	* 也就是说此时这两个字符串能达到的最大长度为2^31,
//	* 这是在VS2022上，不同编译器可能不同，
//	* 不同系统也不同（x64/x86），
//	* 实际也不一定就开了2^31个字符的空间
//	*/
//
//
//
//	//std::string::reserve
//	/*
//	* （为字符串预留空间，可以进行扩容操作）
//	* 注意和revserve进行区分，
//	* reserve：保留
//	* reverse：反转、逆置
//	*/
//	s1.reserve(s1.max_size());
//	//保留max_size个空间
//
//	//std::string::capacity
//	//（实际能够存储的有效字符个数）
//	cout << s1.capacity() << endl; //“15”
//	cout << s2.capacity() << endl; //“15”
//	/*
//	* string s1;
//	* string s2("hello world");
//	*
//	* s1 和 s2 的capacity打印时都是“15”，
//	* 即实际可存储的有效字符个数为15，
//	* 但实际是有16个空间的，有一个空间给了"\0"，
//	* "\0"不算有效字符，而是标识字符，
//	* 所以capacity打印时为“15”
//	*/
//
//
//
//	//std::string::resize
//	//（将有效字符的个数分割成n个，多出的空间用字符c填充）
//	/*
//	* reserve：只影响容量，不会影响数据；
//	* resize：即影响容量，也影响数据
//	* 
//	* resize有两种实现：
//	* 1、void resize(size_t n);
//	* 2、void resize(size_t n, char c);
//	* 
//	* resize的使用分三种情况：
//	*/
//
//	string s1("hello world");
//	//起始字符串大小（长度）-- 11
//	cout << s1.size() << endl;
//	//起始容量 -- 15
//	cout << s1.capacity() << endl; 
//
//	//第一种情况：resize分割数 > capacity
//	//	（即影响容量，又影响数据）
//	//size为11，capacity为15，分割数为100：
//	s1.resize(100); 
//	
//	//分割后字符串大小 -- 100
//	cout << s1.size() << endl; 
//	//分割后容量 -- 111
//	cout << s1.capacity() << endl; 
//	/*
//	* 当 resize分割数 > capacity 时，
//	* 分割后capacity容量会增容到至少比分割数大，
//	*（这里 capacity--15 就变成至少比 分割数--100 大的111）
//	* 
//	* 而字符串数据 size 从15变成了100，
//	* 那其余“85”的数据是什么数据呢？
//	* 
//	* 这里的resize是第一种实现：void resize(size_t n);
//	* 使用resize这种实现，且 resize分割数 > capacity 时，
//	* 多出的数据就会用 空字符(初识字符)--'/0' 插入，
//	* 这里的'/0'就不是标识字符了，而是有效字符了，
//	* '/0'是哪种字符，取决于'/0'是否在size范围中，
//	* 这里增容后，size从15扩大到100并用'/0'插入充当多余数据，
//	* 此时“数据'/0'”就不是结束符而是有效数据了
//	* 
//	* 如果是第二种实现：void resize(size_t n, char c);
//	* 相同情况下，多出的数据就会用传过来的 字符c 插入
//	* 
//	* resize分割数 > capacity ---- “扩容 + 尾插”
//	*/
//
//
//	string s2("hello world");
//	//起始字符串大小（长度）-- 11
//	cout << s2.size() << endl;
//	//起始容量 -- 15
//	cout << s2.capacity() << endl;
//
//	//第二种情况：size < n < capacity
//	//（只会改变字符串大小size）
//	//size为11，capacity为15，分割数n为12：
//	s2.resize(12);
//
//	//分割后字符串大小 -- 12
//	cout << s2.size() << endl;
//	//分割后容量 -- 15
//	cout << s2.capacity() << endl;
//	/*
//	* 在VS中，reserve不会缩容，resize也不会缩容，
//	* 所以容量还是15，不会改变，
//	* 字符串数据size还是和第一种情况类似，
//	* size < 分割数n，数据size就增加到b，
//	* 并用'/0'或'字符c'插入充当多余数据，
//	*（插入的数据取决于resize是哪种实现）
//	* 
//	*（g++ 和 VS 的resize在这种情况下是一样的）
//	* size < n < capacity ---- “尾插（容量足够）”
//	*/
//
//
//	string s3("hello world");
//	//起始字符串大小（长度）-- 11
//	cout << s3.size() << endl;
//	//起始容量 -- 15
//	cout << s3.capacity() << endl;
//
//	//第三种情况：分割数n < size
//	//（只会改变字符串大小size）
//	//size为11，capacity为15，分割数n为5：
//	s3.resize(5);
//
//	//分割后字符串大小 -- 5
//	cout << s3.size() << endl;
//	//分割后容量 -- 15
//	cout << s3.capacity() << endl;
//	/*
//	* 和第二种情况一样，
//	* 因为capacity容量足够，
//	* 所以不会改变空间大小，
//	* 只对数据进行分割
//	*（g++中和VS也是一样的）
//	* 
//	* 分割数n < size ---- “删除数据，保留分割数n个”
//	*/
//
//	/*
//	* 总结：
//	* resize一定会对数据大小size进行操作，
//	* 对容量capacity可能会进行增容操作
//	*（g++ 和 VS 两个主流平台中）
//	* 在第一种情况中可以 增加数据，
//	* 在第三种情况中可以 删除数据
//	* 所以resize的作用就是：
//	* 1、插入数据（如果空间不够还会扩容）
//	* 2、删除数据
//	* 更多场景下是用于开空间并初始化
//	*/
//}

//int main()
//{
//	//reserve 和 capacity 的使用：
//
//	//无参字符串对象：
//	string s1;
//	//有参字符串对象：
//	string s2("hello world");
//
//	//使用reserve进行空间预留：
//	s1.reserve(500);
//	/*
//	* 如果我们知道大概需要多少空间，
//	* 则可以使用reserve提前开好空间，
//	* 不需要进行后面的扩容操作，
//	* 打印结果："15、511"
//	* 容量直接一次性扩容到足够存储500个有效字符
//	* （实际512个空间）
//	*/
//
//	//通过capacity检车string的扩容机制：
//	size_t old = s1.capacity(); //s1此时容量
//	cout << old << endl; 
//
//	for (size_t i = 0; i < 100; i++)
//	{
//		//循环依次，尾插一个字符：
//		s1.push_back('x');
//
//		if (old != s1.capacity())
//			/*
//			* 如果 old 和当前容量不同，
//			* 说明old容量被扩容了
//			*/
//		{
//			//打印扩容后容量：
//			cout << s1.capacity() << endl;
//			//更新old容量：
//			old = s1.capacity();
//		}
//	}
//	/*
//	* 如果没有使用reserve进行空间预留，则会进行扩容操作：
//	* 
//	* 打印结果：“15、31、47、70、105”
//	* 容量从15变成31在变成47……
//	* 从整体来说，是按当前容量的1.5倍进行扩容
//	* 
//	* 不同编译器容量和扩容操作不同：
//	* VS -- "15、31、47、70、105" -- 1.5倍扩容
//	* gcc -- "0、1、2、4、8、16……" -- 2倍扩容
//	*/
//
//	/*
//	* reserve在VS上只会增容；
//	* 
//	* 在g++上除了增容，还可以缩容，
//	* 但缩容不会影响数据，只会影响空间，
//	* 即最多缩容到现存数据大小的空间
//	*（缩容不会删除数据，最小缩到size）
//	*/
//}





////“[]”和“at”错误时的区别：
//int main()
//{
//	try
//	{
//		string s1("hello world");
//		/*
//		* s1有11个有效字符，
//		* 对应数组下标：0~10，下标11是‘/0’
//		*/
//		cout << s1[11] << endl; //访问下标11
//		/*
//		* 使用 operator[] 访问数据：
//		* 下标11可以访问，但打印不出什么
//		*/
//		cout << s1[12] << endl; //访问下标12（越界访问）
//		/*
//		* 使用 operator[] 访问数据：
//		* 越界访问后报错（assert断言），比较“暴力”
//		*/
//
//
//		//std::string::at ：
//		/*
//		* ‘at’ 和上面的 ‘[]’ 作用是一样的，
//		* 都分const版本和非const版本，
//		* 但‘at’和‘[]’报错时的做法不同，更“委婉一点”，
//		* 平常还是使用 ‘[]’ 更多一点
//		*/
//		cout << s1.at(12) << endl;
//		/*
//		* 使用 at 访问数据：
//		* 越界访问后会抛出异常，
//		* “invalid string position” -- 字符串下标非法
//		* “警告式报错，还有挽回余地”
//		*/
//	}
//	//捕获异常：
//	catch (const exception& e)
//	{
//		//打印捕获的异常：
//		cout << e.what() << endl;
//	}
//
//	return 0;
//}


/*
* 目前了解了的string类各种操作：
* 增：'push_back'
* 删：
* 改：'[]'
* 查：'[]'、'迭代器'
*/

////“append”、“operator+=”、“insert”：
//int main()
//{
//	//创建一个字符串：
//	string s1("hello");
//
//	//“增”操作使用‘push_back’，
//	//每次只能尾插一个字符效率太低了：
//	s1.push_back(' '); //s1后尾插一个空格
//
//	//std::string::append
//	/*
//	* append也是用于“增”操作的，
//	* 有多种实现（和string构造函数的实现类似）：
//	* 1、string& append(const string& str);
//	* 2、string& append(const string& str, size_t subpos, size_t sublen);
//	* 3、string& append(const char* s);
//	* 4、string& append(const char* s, size_t n);
//	* 5、string& append(size_t n, char c);
//	* 6、template<class InputIterator> string& append(InputIterator first, InputIterator last);
//	*
//	* 第1种实现功能：在字符串后追加str字符串
//	* 第2种实现功能：把str字符串中subpos下标后的sublen长度的字符追加到字符串后
//	* 第3种实现功能：在字符串后追加一个常量字符串
//	* 第4种实现功能：把s常量字符串的n个字符追加到字符串后
//	* 第5种实现功能：在字符串后追加n个c字符
//	* 第6种实现功能：使用迭代器操作另一个字符串来追加到字符串后
//	*/
//
//	//第3种实现：string& append(const char* s);
//	s1.append("world"); //直接在s1后加上“world”字符串
//	/*
//	* 使用append来实现“增”功能，
//	* 可以直接一步到位，
//	* 不需要像push_back一个一个字符尾插
//	* 现在s1：“hello world”
//	*/
//
//
//	string s2 = "xxxx"; //再创建一个字符串
//	//第6种实现：template<class InputIterator> 
//	//string& append(InputIterator first, InputIterator last);
//	s2.append(s1.begin(), s1.end());
//	/*
//	* 将s1的begin和end，从头到尾追加到s2后，
//	* 打印结果：“xxxx”
//	*/
//
//	s2.append(++s1.begin(), --s1.end());
//	/*
//	* 也可以使用++或--来调整begin和end的位置，
//	* 此时打印结果：“xxxxello worl”
//	* begin -- 跳过了“hello”的“h”
//	* end -- 跳过了“world”的“d”
//	*/
//
//
//
//	//但C++中进行“增”操作时，
//	//最喜欢使用的是：operator+=
//	/*
//	*		std::string::operator+=
//	* 对应实现：
//	* 1、string& operator+=(const string& str);
//	* 2、string& operator+=(const char* s);
//	* 3、string& operator+=(char c);
//	*
//	* 第1个实现功能：字符串后“+=”一个字符串（对象）
//	* 第2个实现功能：字符串后“+=”一个常量字符串
//	* 第3个实现功能：字符串后“+=”一个字符
//	*/
//
//	//第1种实现：string& operator+=(const string& str);
//	s1 += s2; //s1字符串后追加一个s2字符串
//
//	//第2种实现：string& operator+=(const char* s);
//	s1 += "xxxx"; //s1字符串后追加一个常量字符串"xxxx"
//	//类似“append”
//
//	//第3种实现：string& operator+=(char c);
//	s1 += '!'; //s1字符串后追加一个字符'!'
//	//类似“push_back”
//
//
//
//
//	//上面的“增”操作都是在尾部追加，
//	//还有可能需要其它位置的数据追加
//
//	//std::string::insert
//	/*
//	* (从字符串某个位置追加字符)
//	* 对应实现：
//	* 1、string& insert(size_t pos, const string& str);
//	* 2、string& insert(size_t pos, const string& str, size_t subpos, size_t sublen);
//	* 3、string& insert(size_t pos, const char* s);
//	* 4、string& insert(size_t pos, const char* s, size_t n);
//	* 5、string& insert(size_t pos, size_t n, char c);
//	*	 void insert(iterator p, size_t n, char c);
//	* 6、iterator insert(iterator p, char c);
//	* 7、template <class InputIterator>
//	*	 void insert(iterator p, InputIterator first, InputIterator last);
//	*
//	* 第1个实现功能：在字符串的pos下标位置前插入一个string类对象str
//	* 第2个实现功能：在字符串的pos下标位置前插入一个string类对象str的subpos下标后的sublen长度的字符串
//	* 第3个实现功能：在字符串的pos下标位置前插入一个常量字符串s
//	* 第4个实现功能：在字符串的pos下标位置前插入一个常量字符串s的前n个字符
//	* 第5个实现功能：在字符串的pos下标位置前插入n个字符c（string类对象调用版本）
//	*				（void版本为非string类对象调用版本）
//	* 第6个实现功能：在迭代器p之前插入字符c
//	* 第7个实现功能：定义一个类模板，再配合insert在字符串中插入数据
//	*
//	* 实际使用时，挪动数据是不太好的，所以能不用insert就不用
//	*/
//
//	//定义一个string类对象：
//	string s1("hello world");
//
//	//第3种实现：
//	s1.insert(5, "xxxx");
//	/*
//	* 在s1字符串的下标5位置前插入常量字符串“xxxx”：
//	* 插入后的s1字符串 -- “helloxxxx world”
//	*/
//
//	//如果要插入一个字符，可以使用第5种实现：
//	s1.insert(0, 1, 'x'); //insert实现“头插”
//	/*
//	* 在s1字符串的下标0位置前插入1个‘x’字符（头插）：
//	* 插入后的s1字符串 -- “xhello world”
//	*/
//
//	//第6种实现（迭代器）：
//	s1.insert(s1.begin(), 'y');
//	/*
//	* 通过迭代器在s1字符串头部插入‘y’字符（头插）：
//	* 插入后的s1字符串 -- “yhello world”
//	*/
//
//
//
//	//std::string::erase
//	/*
//	* (从字符串某个位置删除字符)
//	* 对应实现：
//	* 1、string& erase(size_t pos = 0, size_t len = npos);
//	* 2、iterator erase(iterator p);
//	* 3、iterator erase(iterator first, iterator last);
//	*
//	* 第1个实现功能：删除字符串中从pos位置开始len长度的数据(有缺省值)
//	* 第2个实现功能：删除字符串中迭代器p对应的数据
//	* 第3个实现功能：删除字符串中迭代器[first, last]范围对应的数据
//	*/
//
//	//第一种实现（不使用缺省值）：
//	s1.erase(2, 2);
//	/*
//	* 假设s1原字符串为：“hello world”
//	* 删除其下标2后2个长度的数据：
//	* “hello world” --> “hel world”
//	*/
//
//	//第一种实现（使用缺省值）：
//	s1.erase(2);
//	/*
//	* 1、string& erase(size_t pos = 0, size_t len = npos);
//	* 这里会调用len的缺省值：npos，即无符号整型最大值
//	* size_t --> unsigned int的‘-1’ -> 无符号整型最大值
//	* 所以这里会删除s1字符串下标2后的所有数据：
//	* “hello world” --> “hel”
//	*/
//}
//
//
//
////“查找并替换”：
//int main()
//{
//	//s1：被调整字符串
//	string s1("hello world hello");
//
//	//std::string::assign -- 拷贝覆盖（不常用）
//	//std::string::replace -- 替换（不常用）
//	//std::string::find -- 查找
//	//使用find和replace将字符串中的空格替换为"20%"：
//	size_t pos = s1.find(' ', 0); //find从头查找空格
//	while (pos != string::npos)
//		/*
//		* 如果find找到了对应字符，则会返回其对应下标，
//		* 如果没有找到对应字符，则会返回npos（无符号整型最大值）
//		*/
//	{
//		//pos不为npos说明找到了当前空格下标：
//		s1.replace(pos, 1, "20%"); //将当前空格替换
//
//		//调整pos下标，继续找字符串中的下个空格下标：
//		pos = s1.find(' ', pos + 3);
//		//从pos+3再开始查找，防止每次都从头开始找空格
//	}
//	/*
//	* 这里将一个“ ”替换成“20%”时，
//	* 将一个字符替换为三个字符，所以每次替换
//	* 都需要将字符串中的数据进行挪动，
//	* 所以使用replace替换数据效率是很低的，
//	* 能不使用就不使用
//	*/
//
//
//	//另一种更好的方法：
//	//s2：被调整字符串
//	string s2("hello world hello");
//	string s3; //再创建一个字符串
//	//使用范围for循环遍历s2：
//	for (auto ch : s2)
//	{
//		if (ch == ' ')
//			//s2中ch是空格：
//		{
//			//s3中则添加“20%”：
//			s3 += "20%";
//		}
//		else
//			//s2中ch不为空格：
//		{
//			//s3中添加s2对映内容：
//			s3 += ch;
//		}
//	}
//	//最后将替换完成的s3交换到s2：
//	s2.swap(s3); //（string的swap）
//	/*
//	* 不在s2原字符串中进行操作，
//	* 而是遍历s2再在s3字符串中进行操作，
//	* 时间复杂度为O(1)
//	*/
//}



//// c_str :
//int main()
//{
//	/*
//	* C和C++有千丝万缕的关系，
//	* C++兼容C，C++某些库的接口是由C提供的（混编），
//	* C++中用string对字符串进行管理，
//	* 那这时候就需要返回C形式的常量字符串
//	*（C没有string字符串）
//	*/
//
//	//string字符串定义文件名（C++）：
//	string filename("Test.cpp");
//
//	//使用C打开文件的方式：
//	FILE* fout = fopen(filename, "r");
//	/*
//	* 这里C打开文件的方式无法识别
//	* C++中的string字符串文件名。
//	* 
//	* fopen：FILE* fopen(const char* filename, const char* mode);
//	* 第一个参数：const char*，无法接收string类对象，类型不匹配
//	*/
//
//	//这时就可以通过c_str来解决这个问题：
//	FILE* fout = fopen(filename.c_str(), "r");
//	/*
//	*	std::string::c_str
//	*		对应实现：
//	* const char* c_str() const;
//	* 接口返回的是const char*，而不是string，
//	* 即返回 C形式的字符串指针
//	* 
//	* string的length有size代替，
//	* 比起length更喜欢使用size；
//	* 而string的data（std::string::data）则有c_str代替，
//	* 比起data跟喜欢使用c_str 
//	*/
//
//	//获取并处理文件内容：
//	char ch = fgetc(fout); //获取
//	while (ch != EOF) //循环打印
//	{
//		cout << ch;
//		ch = fgetc(fout);
//	}
//
//
//	return 0;
//}



//int main()
//{
//	//std::string::find:
//	/*
//	* 对应实现：
//	* 1、size_t find(const string& str, size_t pos = 0) const;
//	* 2、size_t find(const char* s, size_t pos = 0) const;
//	* 3、size_t find(const char* s, size_t pos, size_t n) const;
//	* 4、size_t find(char c, size_t pos = 0) const;
//	* 
//	* 第1种实现：在字符串中从pos位置开始找string字符串str返回其对应下标
//	* 第2种实现：在字符串中从pos位置开始找常量字符串s返回其对应下标
//	* 第3种实现：在字符串中从pos位置开始找常量字符串s的前n个字符返回其对应下标
//	* 第4种实现：在字符串中从pos位置开始找字符c返回其对应下标
//	*/
//
//	//std::string::substr:
//	/*
//	* 对应实现：string substr(size_t pos = 0, size_t len = npos) const;
//	*			  将字符串中pos下标后len长度的字符构建成一个新的字符串
//	*/
//
//	//取文件名后缀（取字符串子串）：
//	string s1("Test.cpp"); //文件名1
//	string s2("Test.tar.zip"); //文件名2
//
//	//通过find找到字符串中'.'的下标：
//	size_t pos1 = s1.find('.'); //第一个文件名中
//	//如果找到了：
//	if (pos1 != string::npos) //npos是静态变量
//		//pos1 == npos说明没找到
//	{
//		//通过substr从'.'下标开始构建子串：
//		string suff1 = s1.substr(pos1, s1.size() - pos1);
//		/*
//		* s1.size() - pos1 为 后缀长度，
//		* 这里第二个参数不给也可以，npos为无符号整型最大值，
//		* 文件后缀名即'.'后的所有字符，
//		* 直接通过'.'后所有字符构成子串即可
//		*/
//
//		//构成后打印对应后缀：
//		cout << suff1 << endl;
//	}
//
//
//	//但如果是第二个文件名，使用上述方法会获得：tar.zip，不是我们想要的
//	//这时就可以使用 std::string::rfind
//	/*
//	* rfind 和 find 类似，只不过find是从字符串头开始找，
//	* 而rfind是从字符串尾开始找，只需要在上述方法中使用rfind即可：
//	*/
//	//通过find找到字符串中最后一个'.'的下标：
//	size_t pos2 = s2.rfind('.'); //第一个文件名中
//	//如果找到了：
//	if (pos2 != string::npos) //npos是静态变量
//		//pos1 == npos说明没找到
//	{
//		//通过substr从'.'下标开始构建子串：
//		string suff2 = s2.substr(pos2);
//		/*
//		* 获取最后一个'.'的下标后，
//		* 直接将其后的所有字符构成一个子串
//		*/
//
//		//构成后打印对应后缀：
//		cout << suff2 << endl;
//	}
//
//	return 0;
//}


////"分割网址"：
//int main()
//{
//	//被“分割”网址：协议、域名、资源名称
//	string str("https://cplusplus.com/reference/string/string/substr/");
//
//	//定义sub1存储协议、sub2存储存储域名、sub3存储资源名称：
//	string sub1, sub2, sub3;
//
//	//找“协议”：
//	//网址“:”前面的内容就是协议，先获取“:”下标：
//	size_t pos1 = str.find(':');
//	//有了“:”下标后，通过substr构建“协议”子字符串：
//	sub1 = str.substr(0, pos1 - 0);
//	// “pos1 - 0” 即 截取字符个数
//
//	//打印“协议”：
//	cout << sub1 << endl;
//
//
//	//找“域名”：
//	//（pos1+3，之后一个“/”）区间就是“域名”： 
//	size_t pos2 = str.find('/', pos1+3); 
//	/*
//	* 获取pos1 + 3后的下一个“/”下标
//	*/
//	//构建“协议”子串：
//	sub2 = str.substr(pos1 + 3, pos2 - (pos1 + 3));
//	/*
//	* pos1 + 3 -- 起始位置
//	* pos2 - (pos1 + 3) -- 截取字符个数
//	*/
//
//	//打印“域名”：
//	cout << sub2 << endl;
//	
//
//	//找“资源名称”：
//	//“协议”之后的内容就是“资源名称”，直接构建“资源名称”子串：
//	sub3 = str.substr(pos2 + 1);
//
//	//打印“资源名称”：
//	cout << sub3 << endl;
//
//
//	return 0;
//}

//std::string::find_first_of -- 在一个字符串中找另一个字符串中出现的字符（从第二个字符串头开始）
//std::string::find_last_of -- 在一个字符串中找另一个字符串中出现的字符（从第二个字符串尾开始）
//std::string::find_first_not_of -- 在一个字符串中找另一个字符串中没出现的字符（从第二个字符串头开始）
//std::string::find_last_not_of -- 在一个字符串中找另一个字符串中没出现的字符（从第二个字符串尾开始）

////std::operator+(string)
//int main()
//{
//	/*
//	* 对应实现：
//	* 1、string operator+(const string& lhs, const string& rhs);
//	* 2、string operator+(const string& lhs, const char* rhs);
//	*    string operator+(const char* lhs, const string& rhs);
//	* 3、string operator+(const string& lhs, char rhs);
//	*	 string operator+(char lhs, const string& rhs);
//	* 
//	* 第1种实现：string类 + string类
//	* 第2种实现：string类 + 常量字符串 ；常量字符串 + string类
//	* 第3种实现：string类 + 单个字符 ；单个字符 + string类
//	*/
//
//	string s1("hello");
//	string s2("hello");
//
//	//第1种实现：
//	string ret1 = s1 + s2; //string + string
//	cout << ret1 << endl;
//
//	//第2种实现：
//	string ret2 = s1 + "xx"; //string + char*
//	cout << ret2 << endl;
//
//	string ret3 = "xx" + s1; //char* + string
//	cout << ret3 << endl;
//	/*
//	* 实现时写了：string + char* / char* + string
//	* 是为了满足左右操作数对调的情况，第3种实现同理
//	* 
//	* 为了让左右操作数可以分别设置，需要把这类函数设置为非成员函数，
//	* 因为成员函数的第一个参数会被默认的this指针占用
//	*/
//
//	return 0;
//}


////HJ1（力扣） 字符串最后一个单词的长度：
//#include <iostream>
//#include <string>
//using namespace std;
//
//int main() {
//
//	//std::getline(string)
//	/*
//	* 获取输入时整行字符串，包括空格
//	*（遇到空格时不会默认换行）
//	* 
//	* 对应实现：
//	* 1、istream& getline(istream& is, string&str, char delim);
//	* 2、istream& getline(istream& is, string& str);
//	*/
//
//	string str; //操作的字符串
//	//cin >> str; //输入字符串
//	getline(cin, str); //第二种实现
//	/*
//	之后需要对字符串中的空格进行操作，
//	所以不能使用 cin / scanf 进行输入，
//	因为它们在输入遇到空格后会默认进行分割，
//	只能取到空格前的内容，
//	所以需要getline整行输入的内容(包括空格)，
//	因为只有字符串（string）中出现空格合理，
//	所以只有字符串string需要使用getline函数
//	*/
//
//	//rfind找最后一个空格下标：
//	size_t pos = str.rfind(' ');
//	//如果找到了：
//	if (pos != string::npos)
//	{
//		cout << str.size() - pos - 1 << endl;
//		/*
//		最后一个单词长度为：size() - (pos+1)
//		即：size() - pos - 1
//		*/
//	}
//	else
//		//如果没找到：
//	{
//		//没有找到最后一个空格，说明就只用一个单词：
//		cout << str.size() << endl; //其长度即size()
//	}
//
//	return 0;
//}


////力扣：387. 字符串中的第一个唯一字符
//class Solution {
//public:
//	int firstUniqChar(string s) {
//
//		//使用计数排序统计字符出现个数：
//		int count[26] = { 0 };
//		//26个字母各种对应count数组的26个下标
//
//		for (auto ch : s)
//		{
//			//使用相对映射进行统计：
//			count[ch - 'a']++;
//			/*
//			* 假设ch为'b'，
//			* 'b' - 'a' = 1，
//			* 那么count[1]就是统计'b'的出现次数
//			*/
//		}
//
//		//题目要找的是第一个只出现的字符（下标），
//		//它可能有多个只出现一次的字符
//		for (size_t i = 0; i < s.size(); ++i)
//		{
//			if (count[s[i] - 'a'] == 1)
//			{
//				return i;
//			}
//			/*
//			* 如果s字符串中的下标i元素
//			* 在count数组中对应位置只出现了一次，
//			* 则返回该下标（第一个出现一次）
//			*/
//		}
//
//		//如果都不止出现一次，返回-1：
//		return -1;
//	}
//};
//
//
////力扣：415. 字符串相加
//class Solution {
//public:
//	string addStrings(string num1, string num2) {
//		//通过字符0~9对应的ASCII值进行计算：
//
//		//第一个字符串的最后字符下标：
//		int end1 = num1.size() - 1;
//		//第二个字符串的最后字符下标：
//		int end2 = num2.size() - 1;
//
//		//创建一个变量处理进位的情况：
//		int next = 0;
//
//		string retstr; //最终结果字符串
//
//		//进行“相加”：
//		while (end1 >= 0 || end2 >= 0)
//			/*
//			* 可能会有进位的情况，
//			* 所以必须两个字符串都遍历完，
//			* 处理完可能的进位情况
//			*/
//		{
//			//end1对应下标字符ASCII值默认为0：
//			int val1 = 0;
//			//end2对应下标字符ASCII值默认为0：
//			int val2 = 0;
//
//			if (end1 >= 0)
//			{
//				/*
//				* 字符串1中end1下标
//				* 还大于0说明还没遍历完，
//				* 获取end1下标对应字符ASCII值：
//				*/
//				val1 = num1[end1--] - '0';
//				//获取end1当前下标字符后，随便调整end1
//			}
//
//			if (end2 >= 0)
//			{
//				/*
//				* 字符串2中end2下标
//				* 还大于0说明还没遍历完，
//				* 获取end2下标对应字符ASCII值：
//				*/
//				val2 = num2[end2--] - '0';
//				//获取end2当前下标字符后，随便调整end2
//			}
//
//			//两对应ASCII值相加(+next处理进位)：
//			int ret = val1 + val2 + next;
//
//			//如果进位：
//			next = ret / 10; //得到进位值
//			/*ret9变12，得到十位的1*/
//			ret %= 10; //进位后的“个位值”
//			/*ret9变12，得到个位的2*/
//
//			/*
//			//将当前“位数”计算后结果头插入retstr中：
//			retstr.insert(retstr.begin(), '0' + ret);
//			//'0' + ret -- 确保返回的是字符的“数字”
//			//但头插效率不高：O^2
//			*/
//
//			//使用尾插：
//			retstr += ('0' + ret);
//		}
//
//		//最后一次计算可能刚好进位，进位后的next可能还没头插：
//		if (next == 1) //等于1说明最后是一次进位
//		{
//			/*
//			//将进位后的next（字符'1'）头插入retstr：
//			retstr.insert(retstr.begin(), '1');
//			//但头插效率不高：O^2
//			*/
//
//			//使用尾插：
//			retstr += '1';
//		}
//
//		//因为是尾插，所以还需要逆置一下：
//		reverse(retstr.begin(), retstr.end());
//
//		//最后返回计算好的字符串：
//		return retstr;
//	}
//};




#include"string.h"

int main()
{
	//调用自己的string类测试函数：
	ggdpz::test_string1();


	return 0;
}