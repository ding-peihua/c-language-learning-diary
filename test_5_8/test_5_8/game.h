#pragma once

//该文件存放 游戏函数声明 的代码

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


//游玩时显示的数组大小
#define ROW 9
#define COL 9

//实际数组大小
#define ROWS ROW+2
#define COLS COL+2

//简单版本的雷个数
#define EASY_COUNT 10

//1. InitBoard() -- 初始化棋盘 函数声明
void InitBoard(char board[ROWS][COLS], int rows, int cols, char set);

//2. DisplayBoard() -- 打印棋盘
//传过来的数组是 11*11 的数组，但（访问）显示的只是其中的 9*9 
void DisplayBoard(char board[ROWS][COLS], int row, int col);

//3. SetMine() -- 布置雷
void SetMine(char board[ROWS][COLS], int row, int col);

//4. FindMine() -- 排查雷(在mine数组中找信息，信息放到show数组中，操作的也是9*9)
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col);
