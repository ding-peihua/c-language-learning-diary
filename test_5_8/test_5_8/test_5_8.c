#define _CRT_SECURE_NO_WARNINGS 1

//实现扫雷游戏代码
//该文件为 测试游戏 的代码

// 设计要点：
//1. 扫雷游戏要存储布置好的雷的信息，需要一个二维数组

//2. 给两个二维数组： 一个存放雷的信息 ； 另一个存放布置好的雷的信息

//3. 为了防止在统计坐标周围的雷的额个数的时候越界，我们让给数组设计为 11 * 11；

//4. 数组是 11 * 11，并且是字符数组

#include "game.h"
 
void menu()
{
	printf("***********************************\n");
	printf("**********    1.play    ***********\n"); 
	printf("**********    0.exit    ***********\n");
	printf("***********************************\n");
}

void game()
//在自定义game()函数中实现扫雷游戏的执行
{
	char mine[ROWS][COLS]; //存放布置好的雷
	char show[ROWS][COLS]; //存放排查出的雷的信息

	//1. 初始化棋盘
	//（1）. mine数组最开始全是‘0’；
	//（2）. show数组最开始全是‘#’；
	InitBoard(mine, ROWS, COLS, '0');
	InitBoard(show, ROWS, COLS, '*');
	// 第四个参数为想要初始化成的 符号

	//2. 打印棋盘（玩的时候只显示 9*9 ，所以打印 9*9 就可以了）
	//DisplayBoard(mine, ROW, COL); 
	DisplayBoard(show, ROW, COL); //实际显示的棋盘

	//3. 布置雷(布置在 9*9 中)
	SetMine(mine, ROW, COL); //雷是布置在mine数组上的
	//DisplayBoard(mine, ROW, COL);

	//4. 排查雷(在mine数组中找信息，信息放到show数组中，操作的也是9*9)
	FindMine(mine, show, ROW, COL);
	 
	  
}

//int main()
//{
//	int input = 0;
//	
//	//使用rand前要设置srand。设置随机数起点
//	//用time()函数返回时间戳
//	//返回类型为 无符号整型
//	srand((unsigned int)time(NULL));
//	
//	do
//	{
//		menu();
//		printf("请选择：>");
//		scanf("%d", &input);
//
//		switch (input)
//		{
//		case 1:
//			game(); 
//			//在自定义game()函数中实现扫雷游戏的执行
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//
//	} while (input); 
//	//如果选择了0退出游戏，则刚好判断为0退出循环
//	//非0则继续循环
//	return 0;
//}