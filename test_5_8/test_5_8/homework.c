#define _CRT_SECURE_NO_WARNINGS 1


////实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
////如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
//
//#include <stdio.h>
//
//void print_table(int n)
//{
//	int i = 0;
//	for (i = 1; i <= n; i++) //控制n行
//	{
//		int j = 0;
//		for (j = 0; j <= i; j++) //每行打印几行
//		{
//			printf("%2d*%2d=%3d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	print_table(n);
//
//	return 0;
//}



//编写一个函数实现n的k次方，使用递归实现
//#include <stdio.h>
//
//double Pow(int n, int k)
//{
//	if (k == 0)
//	{
//		return 1;
//	}
//	else if (k > 0)
//	{
//		return n * Pow(n, k - 1); //2的4次方 = 2 * 2的3次方
//	}
//	else
//	{
//		return 1.0 / Pow(n, -k);
//	}
//
//}
//
//int main()
//{
//	int n = 0;
//	int k = 0;
//
//	//输入:
//	scanf("%d %d", &n, &k);
//
//	//调用函数
//	double num = Pow(n, k);
//
//	//输出：
//	printf("%lf\n", num);
//
//	return 0;
//}
//
//


//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//输入：1729，输出：19

//DigitSum（1729）-> DigitSum（172）+ 9 -> DigitSum（17）+ 2 + 9 
//-> DigitSum（1）+ 7 + 2 + 9
//#include <stdio.h>
//
//int DigitSum(int n)
//{
//	if (n <= 9) //只有个位数直接返回相加就好
//	{
//		return n;
//	}
//	else //不止个位数进行取尾数，递归
//	{
//		return DigitSum(n / 10) + n % 10;
//	}
//}
//
//int main()
//{
//	unsigned int num = 0; //非负整数,设置成无符号unsigned
//	scanf("%d", &num);
//
//	int ret = DigitSum(num); //递归实现要求
//	
//	printf("%d\n", ret);
//
//
//	return 0;
//}


//编写一个函数 reverse_string(char * string)（递归实现）
//实现：将参数字符串中的字符反向排列，不是逆序打印。
//要求：不能使用C函数库中的字符串操作函数。

//版本1：使用库函数，不使用递归
//#include <stdio.h>
//
//void reverse_string(char* s)
//{
//	int left = 0; //左下标
//	int right = strlen(s) - 1; //右下标
//
//	while (left < right) 
//	//left < right：说明数组中还有元素需要调换
//	{
//		//通过中间值进行调换
//		char tmp = s[left];
//		s[left] = s[right];
//		s[right] = tmp;
//		left++;
//		right--;
//	}
//	
//}

//版本2：不使用库函数，使用递归
#include <stdio.h>

//reverse_string("abcdef") -> 交换a和f + reverse_string("bcde")
// -> 交换b和e + reverse_string("cd") -> 交换c和d + reverse_string("")

int my_strlen(char* s)
{
	int count = 0; //统计元素个数
	while (*s != '\0') //还没到结束符就继续统计
	{
		count++;
		s++;
	}
	return count;
}

void reverse_string(char* s)
{
	//if (*s = '\0')//首字符为结束符，说明是空字符串
	//{
	//	return;//直接返回
	//}
	int len = my_strlen(s);
	char tmp = s[0];
	s[0] = s[len - 1]; //把最后的值赋给第一个值

	s[len - 1] = '\0';
	//赋完后把最后位置换成\0，先别把第一个值赋给最后一个值
	//倒数第二个假设成数组的最后一个值，然后递归

	if (my_strlen(s+1) >= 2) 
	//从第二个元素开始数到新赋值的\0处
	//如果有大于等于2个元素则还需要进行调换
	{
		reverse_string(s + 1);
	}

	s[len - 1] = tmp;
	//最后再赋值给后面的值
}

int main()
{
	char arr[] = "abcdef";

	//调用自定义函数
	reverse_string(arr);
	printf("%s\n", arr);

	return 0;
}
