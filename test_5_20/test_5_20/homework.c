#define _CRT_SECURE_NO_WARNINGS 1

//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水（编程实现）。
//#include <stdio.h>
//int main()
//{
//	int money = 0;
//	int total = 0;
//	int empty = 0;
//
//	//输入：
//	scanf("%d", &money);
//
//	total = money; //一瓶1元，多少元就有多少瓶
//	empty = money; //买了多少瓶，就有多少空瓶
//
//	while (empty > 1)
//	{
//		total += empty / 2; //2空瓶换1汽水
//		//换完后又有空瓶
//		empty = empty / 2 + empty % 2;
//		//空瓶再2换1，加不够换的1空瓶
//	}
//
//	printf("%d", total);
//
//	return 0;
//}



//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。
#include <stdio.h>

void sort(int* arr, int sz)
{
	//指针从1开始
	//偶数位：2 4 6 7 8。。
	int i = 0;
	for (i = 1; i < sz-1; i+=2) //1 3 5 7 9..
	//头尾不动，中间的奇数位往前挪一位，偶数位往后挪一位
	{
		//*(arr + i);偶数位的值，1 + 1 = 2 -> 第1个偶数位
		//*(arr + i + 1);第二个奇数位 
		int tmp = *(arr + i);
		*(arr + i) = *(arr + i + 1);
		*(arr + i + 1) = tmp;
	}
	for (i = 2; i < sz - 1; i += 2) //1 3 5 7 9..
		//头尾不动，中间的奇数位往前挪一位，偶数位往后挪一位
	{
		//*(arr + i);偶数位的值，1 + 1 = 2 -> 第1个偶数位
		//*(arr + i + 1);第二个奇数位 
		int tmp = *(arr + i);
		*(arr + i) = *(arr + i + 1);
		*(arr + i + 1) = tmp;
	}
	for (i = 3; i < sz - 1; i += 2) //1 3 5 7 9..
		//头尾不动，中间的奇数位往前挪一位，偶数位往后挪一位
	{
		//*(arr + i);偶数位的值，1 + 1 = 2 -> 第1个偶数位
		//*(arr + i + 1);第二个奇数位 
		int tmp = *(arr + i);
		*(arr + i) = *(arr + i + 1);
		*(arr + i + 1) = tmp;
	}
	for (i = 4; i < sz - 1; i += 2) //1 3 5 7 9..
		//头尾不动，中间的奇数位往前挪一位，偶数位往后挪一位
	{
		//*(arr + i);偶数位的值，1 + 1 = 2 -> 第1个偶数位
		//*(arr + i + 1);第二个奇数位 
		int tmp = *(arr + i);
		*(arr + i) = *(arr + i + 1);
		*(arr + i + 1) = tmp;
	}
}

int main()
{
	int arr[10] = { 0 };

	//数组长度：
	int sz = sizeof(arr) / sizeof(arr[0]);

	//输入
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		scanf("%d", &arr[i]);
	}

	//使用自定义函数排序
	sort(arr, sz);

	//打印：
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}

	return 0;
}

