#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//自定义函数：
void young_table_search(int arr[3][3], int k, int* px, int* py)
{
	//通过两个变量找出二维数组第一行的最大值：
	//行和列是从0开始的，
	int x = 0; //二维数组的行，从第一行进行查找
	int y = *py - 1; //二维数组的列，从最大列开始查找，

	//使用 while循环 进行查找：
	while (x<=*px-1 && y>=0)
		//x<=*px-1  -- 未查找到最大行数
		//y>=0  --  未调整到最小列数
	{
		if (arr[x][y] < k)
			//第一行最大值 小于 k
		{
			x++; //排除这一行，移到下一行
		}
		else if (arr[x][y] > k)
			//第一行最大值 大于 k
		{
			y--; //k就在这一行，移到列进行查找
		}
		else
			//找到了：把k的行和列赋给指针px和py
		{
			*px = x;
			*py = y;
			return;
		}
	}
	//自定义未找到的情况：
	*px = -1;
	*py = -1;
}

int main() 
{
	//给出一个杨氏矩阵：
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
	//					1 2 3
	//					4 5 6
	//					7 8 9

	//输入要找的数：
	int k = 0;
	scanf("%d", &k);

	//设置矩阵的行和列：
	int x = 3; //矩阵的行
	int y = 3; //矩阵的列

	//使用自定义函数进行查找：
	young_table_search(arr, k, &x ,&y);

	//根据情况大于相应情况：
	if (x==-1 && y==-1)
		//未找到
	{
		printf("未找到");
	}
	else
		//找到了
	{
		printf("找到了，它的下标为：第%d行 第%d列", x, y);
	}

	return 0;
}
