#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	int* p = arr; //使用指针变量存放数组首元素地址
//
//	for ( i = 0; i < sz; i++)
//	{
//		//打印数组地址
//		printf("%p == %p\n", p+i, &arr[i]);
//		// 使用指针变量 访问数组地址 == 使用数组下标 访问数组地址
//	}
//
//	printf("\n");
//
//	for ( i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//		//使用指针访问 数组元素（指针和数组的联系）
//	}
//
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int a = 10;
//
//	int* p = &a; 
//	//p是一级指针变量，指针变量也是变量，变量是在内存中开辟空间的，是变量就有地址
//
//	int** pp = &p;
//	//pp就是二级指针变量，二级指针变量就是用来存放一级指针变量的地址
//
//	*(*pp) = 100; 
//	//(*pp):解引用找到p，对p再解引用找到 a
//	//二级指针解引用找到一级指针，一级指针解引用找到变量内容
//	printf("%d\n", a);
//
//
//	return 0;
//}

////二级指针引用：
//#include <stdio.h>
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "hello world";
//	char arr3[] = "cuihua";
//
//	//使用 一级指针 存放这三个字符数组的首元素
//	char* parr[] = { arr1,arr2,arr3 }; //分别存放三个字符数组的首元素地址
//
//	//使用 二级指针 存放该一级指针
//	char** p = parr; //放的是一级指针的首元素地址
//
//	return 0;
//}

////指针数组的应用：
//#include <stdio.h>
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "hello world";
//	char arr3[] = "cuihua";
//
//	//使用 指针数组 存放这三个字符数组的首元素
//	char* parr[] = { arr1,arr2,arr3 }; //分别存放三个字符数组的首元素地址
//
//	//使用 指针数组 循环打印三个首地址，再通过首地址打印三个字符串
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("%s\n", parr[i]);
//		// %s：通过字符串首元素地址就可以打印字符串，通过元素地址找到字符串
//	}
//
//	return 0;
//}

//指针数组的应用：
//#include <stdio.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//
//	//使用 指针数组 存放这三个整型数组的首元素
//	int* parr[] = { arr1, arr2, arr3 }; //分别存放三个整型数组的首元素地址
//
//	//使用 指针数组 循环打印三个首地址，再通过首地址打印三个整型数组
//	int i = 0;
//	for (i = 0; i < 3; i++) //指针数组大小为3
//	{
//		//第一个循环：找到指针数组的元素
//
//		int j = 0;
//		for (j = 0; j < 5; j++) //整型数组大小为5
//		{
//			//第二个循环：找到对应数组元素地址后，再通过该地址打印整型数组内容
//
//			printf("%d ", parr[i][j]); 
//			//也可以写成 *(parr[i] + j)
//			
//			//第一个【】：找到指针数组里的指针，定位到对应整型数组
//			//第二个【】：定位到对应整型数组后，再用指针访问整型数组元素
//		}
//
//		printf("\n"); //打印完一个数组后换行
//	}
//
//	return 0;
//}

//结构体
//#include <stdio.h>
//
////描述一个学生
////声明结构体类型，还没创建空间，所以成员变量没法初始化
//struct Stu 
////struct：定义结构体的关键字
////tag：根据实际情况给结构体起名
//{
//	//member - list：成员变量列表（1个或者多个
//	//成员变量：用来描述结构体对象的相关属性的
//	char name[20];
//	int age;
//	char sex[5]; //男 女 保密，一个汉字占2个字符
//} s2, s3, s4; 
////variable - list：变量列表,也可以在这创建变量，在创建类型随便创建变量
////因为是在大括号外创建的变量，所以是全局变量，开创了内存空间
//
//
//typedef struct Stu
//{
//	char name[20];
//	int age;
//	char sex[5];
//} Stu;
////使用 typedef 重新将 struct Stu 命名为 Stu
//
//int main()
//{
//	//struct Stu 就是一个类型
//	struct Stu s1; //这里创建的是局部变量
//	//运用该结构体类型创建一个名为是 s1 的学生变量
//	//声明一个 学生变量 后才创建了空间
//	//在C语言中，没有使用 typedef 重命名的话，struct关键字 是不能省略的
//
//	Stu s2; //使用typedef 重新命名后，直接使用重命名的名称就可以
//
//	return 0;
//}

//结构体变量的访问
//#include <stdio.h>
//
//struct A
//{
//	int a; //标量（变量）
//	char arr[5]; //数组
//	int* p; //指针
//}a1 = {100, "lll", NULL}; //初始化方式和数组的类似
//
//struct A a2 = {99, "hhh", NULL};
//
//struct B
//{
//	char ch[10];
//	struct A a; //结构体
//	double d;
//};
//
//int main()
//{
//	struct A a3 = {.arr = "abc", .p = NULL, .a = 1}; 
//	//通过 .操作符 访问成员变量，再进行初始化
//
//	printf("%d %s %p\n", a3.a, a3.arr, a3.p);
//	//使用 .结构成员访问操作符 访问结构成员变量
//
//	struct B b1 = { "hello", {20, "qqq", NULL}, 3.14 };
//
//	printf("%s %d %s %p %lf\n", b1.ch, b1.a.a, b1.a.arr, b1.a.p, b1.d);
//
//	return 0;
//}


////访问
//#include <stdio.h>
//#include <string.h>
//
//struct Stu
//{
//	int age;
//	char name[20];
//};
//
////定义一个函数，对结构体成员进行赋值
//void set_stu(struct Stu t)//t 的类型就是struct Stu
//{
//	t.age = 20;
//
//	//因为成员 name 是数组，数组名是首元素地址
//	//t.name = "张三"; 
//	//所以不能这样写，不能把 张三 放在地址上
//	//应该把 张三 放在地址的空间里
//	//strcpy()函数：字符串拷贝
//	strcpy(t.name, "张三");
//	//strcpy：把字符串拷贝到一个地方
//	//把 张三这个字符串 拷贝到 t.name这个空间去
//}
//
////定义一个函数，打印结构体变量
//void print_stu(struct Stu t)
//{
//	printf("%s %d\n", t.name, t.age);
//}
//
//int main()
//{
//	struct Stu s = { 0 };
//
//	//定义一个函数，对结构体成员进行赋值
//	set_stu(s);
//
//	//定义一个函数，打印结构体变量
//	print_stu(s);
//		
//	return 0;
//}


////访问
//#include <stdio.h>
//#include <string.h>
//
//struct Stu
//{
//	int age;
//	char name[20];
//};
//
////第一种方法：对地址解引用，再使用 .操作符
////定义一个函数，对结构体成员进行赋值
//void set_stu(struct Stu* ps)//t 的类型就是struct Stu
//{
//	(*ps).age = 20;
//
//	//因为成员 name 是数组，数组名是首元素地址
//	//t.name = "张三"; 
//	//所以不能这样写，不能把 张三 放在地址上
//	//应该把 张三 放在地址的空间里
//	//strcpy()函数：字符串拷贝
//	strcpy((*ps).name, "张三");
//	//strcpy：把字符串拷贝到一个地方
//	//把 张三这个字符串 拷贝到 t.name这个空间去
//}
//
////第二种方法：直接使用 ->操作符
////定义一个函数，对结构体成员进行赋值
//void set_stu(struct Stu* ps)//t 的类型就是struct Stu
//{
//	ps->age = 20;
//
//	//因为成员 name 是数组，数组名是首元素地址
//	//t.name = "张三"; 
//	//所以不能这样写，不能把 张三 放在地址上
//	//应该把 张三 放在地址的空间里
//	//strcpy()函数：字符串拷贝
//	strcpy(ps->name, "张三");
//	//strcpy：把字符串拷贝到一个地方
//	//把 张三这个字符串 拷贝到 t.name这个空间去
//}
//
////定义一个函数，打印结构体变量
//void print_stu(struct Stu t)
//{
//	printf("%s %d\n", t.name, t.age);
//}
//
//int main()
//{
//	struct Stu s = { 0 };
//
//	//定义一个函数，对结构体成员进行赋值
//	set_stu(&s);
//
//	//定义一个函数，打印结构体变量
//	print_stu(s);
//
//	return 0;
//}

////传参
//#include <stdio.h>
//
//struct S
//{
//	int data[1000];
//	int num;
//};
//
//struct S s = { {1,2,3,4}, 1000 };
//
////方案1：结构体传参
//void print1(struct S s)
//{
//	printf("%d\n", s.num);
//}
//
////方案2：结构体地址传参
//void print2(struct S* ps)
//{
//	printf("%d\n", ps->num);
//}
//
//
//int main()
//{
//	print1(s); //传结构体（变量）
//	print2(&s); //传地址
//
//	return 0;
//}