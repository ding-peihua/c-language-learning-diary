#define _CRT_SECURE_NO_WARNINGS 1

//写一个函数返回参数二进制中 1 的个数。
//比如： 15    0000 1111    4 个 1
//#include <stdio.h>

//int number_of_1(unsigned int m)//要设置成无符号的，不然无法判断 负数
//{
//	int count = 0; //计数器
//	while (m) //如果 m 不为0，说明二进制还有1
//	{
//		if (m % 2 == 1)//判断最低位
//		{
//			count++; //为1则计数器++
//		}
//		m /= 2; //相当于二进制去了一位
//	}
//	//一直判断直到m等于0，返回统计的1的个数
//	return count;
//}


//#include <stdio.h>
//
//int number_of_1(int m)//要设置成无符号的，不然无法判断 负数
//{
//	int count = 0; //计数器
//	
//	int i = 0;
//	for (i = 0; i < 32; i++) //二进制有32位，判断32次
//	{
//		if ( ( (m >> i) & 1) == 1)
//		//移动 i位 后再 按位与1 ，判断最低位二进制值是否为 1
//		//移动后 m 的值并没有变，所以可以一直移动
//		{
//			count++; //是 1 则计数++
//		}
//	}
//
//	return count;
//}
//
//int main()
//{
//	int n = 0;
//	//输入：
//	scanf("%d", &n);
//
//	//定义一个计算二进制中1个数的函数
//	int ret = number_of_1(n);
//	
//	printf("%d\n", ret);
//
//	return 0;
//}

//#include <stdio.h>
//
//int number_of_1(int m)//要设置成无符号的，不然无法判断 负数
//{
//	int count = 0; //计数器
//	
//	while (m)//如果 m 不为0，说明二进制还有1
//	{
//		m = m & (m - 1);//去掉最右边的1
//		count++; //计数器++
//	}
//
//	return count;
//}
//
//int main()
//{
//	int n = 0;
//	//输入：
//	scanf("%d", &n);
//
//	//定义一个计算二进制中1个数的函数
//	int ret = number_of_1(n);
//
//	printf("%d\n", ret);
//
//	return 0;
//}

//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
// #include <stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	
//	//打印奇数位
//	printf("奇数位：");
//	for (i = 30; i >= 0; i -= 2)
//	//循环时是 偶数位：30 28 26.。。。
//	{
//		printf("%d ", (n >> i) & 1);
//		//这里移位后是奇数位，按位与1 取出最低位 打印
//	}
//
//	printf("\n");
//	
//	printf("偶数位：");
//	//打印偶数位
//	for (i = 31; i >= 1; i -= 2)
//		//循环时是 偶数位：31 29 27.。。。
//	{
//		printf("%d ", (n >> i) & 1);
//		//这里移位后是偶数位，按位与1 取出最低位 打印
//	}
//
//	return 0;
//}


////编程实现：两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？
////输入例子 :
////1999 2299
////输出例子 : 7
//#include <stdio.h>
//
//int number_of_1(int m)
//{
//	int count = 0; //计数器
//
//	while (m)//如果 m 不为0，说明二进制还有1
//	{
//		m = m & (m - 1);//去掉最右边的1
//		count++; //计数器++
//	}
//}
//
//int main()
//{
//	int m = 0;
//	int n = 0;
//	
//	//输入
//	scanf("%d %d", &m, &n);
//
//	//异或：相同为0，相异为1
//	//把 m 异或 n 后，有几个相异就有几个1，再计算下二进制中有几个1即可
//	int ret = number_of_1(m ^ n);
//	printf("%d", ret);
//
//	return 0;
//}


//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水（编程实现）。
#include <stdio.h>
int main()
{
	int money = 20;
	int soda = 1;
	int bottle = 2;

	int drank = (money / soda) / bottle + (money / soda);

	printf("%d", drank);

	return 0;
}