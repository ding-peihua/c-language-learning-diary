#define _CRT_SECURE_NO_WARNINGS 1

//字符指针的一种写法:
//#include <stdio.h>
//int main()
//{
//	char ch = 'w'; //创建字符变量
//
//	char* pc = &ch; //使用字符指针来存放字符变量的地址
//
//	*pc = 'a'; //运用字符指针来调用字符变量的地址
//
//	return 0;
//}

//字符指针的另一种写法：
//#include <stdio.h>
//int main()
//{
//	// 创建一个字符数组并初始化：
//	char arr[] = "abcdef";
//	// 使用 "abcdef" 给arr初始化，相当于：
//	// [a b c d e f \0]
//
//	//字符指针的另一种写法：
//	const char* p = "abcdef";
//	// "abcdef" 可以理解为一个字符串表达式，
//	// 表达式的结果是 首字符a的地址，
//	// 所以本质上是把 首字符a的地址 赋给了字符指针p,
//	// 相当于 p 指向了 这个 字符串"abcdef"。
//	// 但这样写的话，字符串是一个 常量字符串 ，不能被改
//	// 所以要加上 const 给到 常属性
//	// const 写在左边 限制了 *p ，使其不能改变该字符串
//
//	//使用该字符变量：
//	printf("%s\n", p); //从 p地址的位置 向后打印字符串
//
//	printf("%c\n", *p); // 解引用字符变量p，打印一个字符
//
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	// 创建两个字符数组，一个叫str1，一个叫str2，
//	// 只不过两个数组存储的内容相同，两个空间是相互独立的
//	char str1[] = "hello world.";
//	char str2[] = "hello world.";
//	
//	// str1数组 和 str2数组 是两个相互独立的空间
//	// 所以两者指向的 首元素地址 是不同的
//	if (str1 == str2)
//		printf("str1 and str2 are same\n");
//	else
//		printf("str1 and str2 are not same\n");
//
//
//	// 这两个是字符指针，“hello world”是一个常量字符串，不会改变
//	// 所以只会为常量字符串开辟一个空间，指针使用时直接指向该字符串地址进行使用
//	// 那么 str3 和 str4 应该是指向同一个空间（地址）的
//	const char* str3 = "hello world.";
//	const char* str4 = "hello world.";
//	
//	// 只会为常量字符串开辟一个空间，指针使用时直接指向该字符串地址进行使用
//	// 那么 str3 和 str4 应该是指向同一个空间（地址）的
//	if (str3 == str4)
//		printf("str3 and str4 are same\n");
//	else
//		printf("str3 and str4 are not same\n");
//
//	return 0;
//}

////指针数组：
//#include <stdio.h>
//int main()
//{
//	int arr1[10]; //整型数组
//	//一级整型指针数组
//	int* arr2[10]; //存放一级整型指针的数组
//	//二级整型指针数组
//	int** arr3[10]; //存放二级整型指针的数组
//
//	char arr4[10]; //字符数组
//	//一级字符型指针数组
//	char* arr5[10]; //存放一级字符型指针的数组
//	//二级字符型指针数组
//	char** arr6[10]; //存放二级字符型指针的数组
//
//	return 0;
//}

////使用指针数组模拟二维数组：
//#include <stdio.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//	
//	//使用指针数组存放3个数组名（数组首元素地址）
//	int* arr[3] = { arr1,arr2,arr3 };
//
//	//通过指针数组里的指针找到指针内容
//	int i = 0;
//	for (i = 0; i < 3; i++)
//		//循环0-3，指针数组的下标0-3
//	{
//		int j = 0;
//		for ( j = 0; j < 5; j++)
//			//循环0-5，指针数组里指针内容的下标0-5
//		{
//			printf("%d ", arr[i][j]);
//		}
//
//		printf("\n"); //打印完一行后进行换行
//	}
//
//	return 0;
//}


////数组名的理解：
////数组名表示数组首元素地址，但有两个例外
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//
//	//证明数组名是首元素地址：
//	printf("%p\n", arr); //打印arr的地址
//	printf("%p\n", &arr[0]); //打印&arr[0]的地址
//
//	//换行：
//	printf("\n");
//
//	//数组名不表示首元素的两种例外：
//	//1. sizeof(数组名)，这里的数组名表示整个数组
//	//	计算整个数组的大小，单位是字节
//	printf("%d\n", sizeof(arr));
//
//	//换行：
//	printf("\n");
//
//	//2. &数组名，这里的数组名也表示整个数组
//	//	取出整个数组的地址
//	printf("%p\n", arr); //打印arr的地址 -- int* 类型
//	printf("%p\n", arr + 1);
//
//	//换行：
//	printf("\n");
//
//	printf("%p\n", &arr[0]); //打印&arr[0]的地址 -- int* 类型
//	printf("%p\n", &arr[0] + 1);
//
//	//换行：
//	printf("\n");
//
//	printf("%p\n", &arr); //打印&arr的地址 -- int(*)[10]
//	printf("%p\n", &arr + 1);
//
//
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	//使用 数组指针 存放 数组的地址：
//	int(*p)[10] = &arr; //这里数组地址的类型是：int(*)[10]
//	//&arr：整个数组的地址
//
//
//	return 0;
//}


////在二维数组中使用数组指针进行打印
//#include <stdio.h>
//void Print(int (*p)[5], int r, int c)
//	//因为 二维数组数组名 是一个一维数组地址，
//	//所以可以用 数组指针 作为形参
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//		//遍历二维数组的元素
//	{
//		int j = 0;
//		for ( j = 0; j < c; j++)
//			//遍历二维数组中每一行的元素
//		{
//			printf("%d ", *(*(p + i) + j));
//			// *(p + i) -- 解引用当前行的首元素
//			// 再 +j 向后遍历当前行
//		}
//		//打印完一行进行换行：
//		printf("\n");
//	}
//}
//
//int main()
//{
//	//定义一个 三行五列的二维数组：
//	int arr[3][5] = { 1,2,3,4,5, 2,3,4,5,6, 3,4,5,6,7 };
//	
//	//定义一个使用 数组指针 打印二维数组的函数：
//	Print(arr, 3, 5);
//	//参数：(二维数组名，行数，列数)
//
//	return 0;
//}

////一维数组传参：
////形参的部分可以是数组，也可以是指针
//#include <stdio.h>
//void test1(int arr[5], int sz)
//// 形参使用数组接收，但本质是个指针，这样可以方便理解
//{}
//
//void test2(int* p, int sz)
//// 形参使用指针接收
//{}
//
//int main()
//{
//	int arr[5] = { 0 };
//
//	test1(arr, 5);
//	test2(arr, 5);
//
//	return 0;
//}

////二维数组传参：
////形参的部分可以是数组，也可以是指针
//#include <stdio.h>
//void test3(char arr[3][5], int r, int c)
//// 形参使用数组接收，但本质是个指针，这样可以方便理解
//{}
//
//void test4(char (*p)[5], int r, int c)
//// 形参使用指针接收
//{}
//
//int main()
//{
//	//三行五列的二维数组：
//	char arr[3][5] = { 0 };
//
//	test3(arr, 3, 5);
//	test4(arr, 3, 5);
//
//	return 0;
//}

//#include <stdio.h>
//void test(int arr[])//ok?
////可以 -- 整型数组传递，整型数组接收，
////因为形参不会占用空间，所以可以不写元素个数
//{}
//void test(int arr[10])//ok?
////可以 -- 整型数组传递，整型数组接收，
//{}
//void test(int* arr)//ok?
////可以 -- 整型数组首元素是个整型地址，可以用指针接收
//{}
//
//void test2(int* arr[20])//ok?
////可以 -- 整型指针数组传递，整型指针数组接收
//{}
//void test2(int** arr)//ok?
////可以 -- 一级指针地址传递，是 int* 类型，用二级指针接收
//{} 
//
//int main()
//{
//	// 整型数组，存放整数
//	int arr[10] = { 0 };
//	// 指针数组，存放指针
//	int* arr2[20] = { 0 };
//
//	test(arr);
//
//	test2(arr2);
//}

//void test(int arr[3][5])//ok？
////可以 -- 三行五列二维数组传递，三行五列二维数组接收
//{}
//void test(int arr[][])//ok？
////不可以 -- 二维数组 行可以省略，列不可以省略
//{}
//void test(int arr[][5])//ok？
////可以 -- 二维数组 行可以省略，列不可以省略
//{}
////总结：二维数组传参，函数形参的设计只能省略第一个[]的数字。
////因为对一个二维数组，可以不知道有多少行，但是必须知道一行多少元素。
////这样才方便运算，一行有多少个就能知道有多少行。
//
//void test(int* arr)//ok？
////不可以 -- 传递的是一维数组的地址，是一行的地址，应该用数组指针。
//{}
//void test(int* arr[5])//ok？
////不可以 -- 这个是指针数组，不能接收整型一维数组的地址
//{}
//void test(int(*arr)[5])//ok？
////可以 -- 数组地址传递，数组指针接收
//{}
//void test(int** arr)//ok？
////不可以 -- 不能用 二级指针 接收 数组地址
//{}
//
//int main()
//{
//	//三行五列的二维数组：
//	int arr[3][5] = { 0 };
//
//	//二维数组传参：
//	test(arr);
//}

//#include <stdio.h>
//void print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\n", *(p + i));
//	}
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//一级指针p，传给函数
//	print(p, sz);
//	return 0;
//}


//#include <stdio.h>
//void test(int** ptr)
////二级指针 和 一级指针的地址 都可以用二级指针接收
//{
//	printf("num = %d\n", **ptr);
//}
//
//int main()
//{
//	int n = 10; //整型变量
//	int* p = &n; //一级指针
//	int** pp = &p; //二级指针
//
//	test(pp); //传二级指针
//	test(&p); //传一级指针的地址
//	return 0;
//}

#include <stdio.h>
int Add(int x, int y)
{
	return x + y;
}

int main()
{
	//函数名是函数的地址
	printf("%p\n", &Add);

	//&函数名 也是函数的地址
	printf("%p\n", Add);
	
	//使用函数指针：
	int (*p)(int, int) = &Add;
	// int (int, int) 就是函数指针类型

	//使用函数指针调用函数：
	int r = (*p)(4, 5); //解引用函数指针后输入参数调用函数
	int m = p(4, 5); //不加 & 也是可以的，因为 &Add 和 Add 一样
	printf("%d\n", r);
	printf("%d\n", m);

	return 0;
}