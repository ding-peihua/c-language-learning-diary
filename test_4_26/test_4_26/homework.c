#define _CRT_SECURE_NO_WARNINGS 1

//作业3：
//
//int Fun(int n)
//{
//    if (n == 5)
//        return 2;
//    else
//        return 2 * Fun(n + 1);
//}
//
//#include <stdio.h>
//
//int main() 
//{
//    int num = Fun(2);
//    printf("%d\n",num);
//
//	return 0;
//}

////作业5：
////编写一个函数实现n的k次方，使用递归实现。
//// n^k = n * n^(k-1)
//
//#include <stdio.h>
//
////非递归：
////int exp(int n, int k)
////{
////	int result = n;
////	//int count = n;
////	// 使用迭代计算n的k次方
////	int i = 0;
////	for (i = k; i > 1; i--)
////	{
////		result = result * n;
////	}
////	return result;
////}
//
////递归：
//int exp(int n, int k)
//{
//	int result = n;
//	// 迭代一般用循环，递归要有限制条件，所以一般用条件判断
//	if (k > 1)
//	{
//		result = result * exp(n , k-1);
//		k--;
//	}
//
//	return result;
//
//}
//
//int main() 
//{
//	int n = 0;
//	int k = 0; // 次方值
//
//	//输入：
//	scanf("%d %d", &n, &k);
//
//	//调用自定义函数：
//	int num = exp(n, k);
//
//	//打印
//	printf("%d\n", num);
//
//	return 0;
//}



//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//输入：1729，输出：19

//#include <stdio.h>
//
//int DigitSum(int n)
//{
//	int head = 0; // 存放字符串首位
//	int result = 0;
//
//	if (n > 9) // n为两位数
//	{
//		result = DigitSum(n / 10); // 接受返回的 (result + head)
//	}
//	head = n % 10; //要放条件判断外，不然个位数进不去，无法加上
//
//	return (result + head);
//	//递归要设置好 返回值，限制条件，接受返回值，形参处理
//}
//
//int main() 
//{
//	int input = 0;
//
//	
//	//输入：
//	scanf("%d", &input);
//
//	//调用递归函数：
//	int num = DigitSum(input);
//
//	printf("%d", num);
//
//	return 0;
//}


//练习7
//编写一个函数 reverse_string(char * string)（递归实现）
//实现：将参数字符串中的字符反向排列，不是逆序打印。
//要求：不能使用C函数库中的字符串操作函数。
#include <stdio.h>
#include <String.h>

void reverse_string(char* string)
{
	static int flag = 1; // 要设置成静态变量
	int count = strlen(string); // 数组长度
	char tmp = 0; // 交换数值的中间值

	// 改变数组逆序
	if (count > flag) // 数组长度大于1，进行逆序
	{
		tmp = *string; // 数组中的值传给中间值
		*string = *(string + count - flag);
		// *(string + count - flag) --> （当前指针+数组长度-flag）的指针的值 
		// 把 最后一位 赋给 第一位？

		*(string + count - flag) = tmp;
		// 把 第一位 赋给 最后一位？

		flag++;

		reverse_string(++string); // 改变下一位
	}
}

int main() 
{
	char arr[] = "abc";

	//输入：
	scanf("%c", arr);

	//调用自定义函数
	reverse_string(arr); // 使用指针直接改变实参，不需要返回值

	printf("%s", arr);

	return 0;
}