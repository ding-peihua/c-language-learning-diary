#define _CRT_SECURE_NO_WARNINGS 1

//写一个函数，每调用一次这个函数，就会将num的值增加1：1
//改变了实参的值，要使用地址变量
//#include <stdio.h>
//
//void test(int* p)
//{
//	(*p)++; // （复习）*：说明p是指针变量
//	// 此时 *p 去取num的地址，所以相当于num
//}
//
//int main() 
//{
//	int num = 0;
//	test(&num); // 调用一次test()这个函数,将num的值增加1
//	test(&num); // 函数参数为变量地址，用于在函数中改变实参的值 
//	printf("%d\n", num);
//
//	return 0;
//}

//写一个函数，每调用一次这个函数，就会将num的值增加1：2
//#include <stdio.h>
//
//int test(int n)
//{
//	return (n + 1);
//}
//
//int main() 
//{
//	int num = 0;
//	num = test(num);
//	num = test(num);
//	printf("%d\n", num);
//
//	return 0;
//}

//不可以嵌套定义
//#include <stdio.h>
//
//int test(int x, int y) 
//{
//	int a = 0;
//	int b = 0;
//	return a + b;
//}
//
//void fun()
//{
//	int num = test(2,3);
//	printf("%d\n", num);
//}
//
//int main() 
//{
//	
//	return 0;
//}


//链式访问
//#include <stdio.h>
//#include <string.h>
//
//int main() 
//{
//	printf("%zd\n", strlen("abcdef"));
//	return 0;
//}

//链式访问(“4321”+ 空格)
//#include <stdio.h>
//
//int main() 
//{
//	// printf()函数的返回值是 字符串的个数 （重点）
//	printf("%d ", printf("%d ", printf("%d ", 43)));
//
//	return 0;
//}

//函数声明:告诉编译器有一个函数叫什么，参数是什么，返回类型是什么。
//但是具体是不是存在(可能只声明了但未定义)，函数声明决定不了。
//#include <stdio.h>

//函数声明:
//函数的声明一般出现在函数的使用之前。要满足先声明后使用。
//int Add(int x, int y);
//
//int main() 
//{
//	int a = 0;
//	int b = 0;
//	//输入
//	scanf("%d %d", &a, &b);
//	//加法函数：
//	int c = Add(a, b);//函数调用
//	//打印
//	printf("%d\n", c);
//
//	return 0;
//}
//
////函数的定义：
//int Add(int x, int y)
//{
//	return x + y;
//}


//函数声明:告诉编译器有一个函数叫什么，参数是什么，返回类型是什么。
//但是具体是不是存在(可能只声明了但未定义)，函数声明决定不了。
//#include <stdio.h>

////函数的定义：
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	//输入
//	scanf("%d %d", &a, &b);
//	//加法函数：
//	int c = Add(a, b);//函数调用
//	//打印
//	printf("%d\n", c);
//
//	return 0;
//}

// 主程序：
//分模块去编程，方便协作，最后做整合
//可以把代码的实现和声明分离
//#include <stdio.h>
//
//#include "add.h" // 调用对应的头文件，
////相当于对相应的函数进行了声明
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	//输入
//	scanf("%d %d", &a, &b);
//
//	//加法函数：
//	int c = Add(a, b);
//	//其它源文件内定义的函数具有 外部连接属性 ，
//	//可直接调用 ，（之前staic讲过）
//
//	//打印
//	printf("%d\n", c);
//
//	return 0;
//}

//递归
//#include <stdio.h>
//
//int main() 
//{
//	printf("hehe\n");
//
//	// 递归：函数自己调用自己
//	main();
//
//	return 0;
//}

//递归 练习1：
//#include <stdio.h>
//
//void print(unsigned int n) // 1234
//{
//	if (n > 9) // 大于9说明是两位数
//	{
//		print(n / 10); // 1234进来后变为123
//	}
//
//	printf("%d ", n % 10); //  
//}
//
//int main() 
//{
//	unsigned int num = 0;
//	
//	//输入
//	scanf("%d", &num);
//
//	print(num); // 调用自定义函数
//
//	return 0;
//}

// 每一次 函数调用 都要创造 函数栈帧
// 整个函数栈帧的空间都在 栈区  上（函数栈帧的开辟都是在栈区上进行的）


//编写函数不允许创建临时变量，求字符串的长度

//#include <stdio.h>
//#include <string.h>

//int my_strlen(char* s) // 返回字符串长度，所以返回类型是int
//// 因为实参是数组名称，是地址，所以形参使用 char*
//// 数组末尾包括 \0 , 求字符串长度是求 \0 前有多少个字符（注意）
//{
//	int count = 0; // 临时变量
//	while (*s != '\0') // 如果没有到\0（结束标志）,说明数组还有值，就继续循环
//		// 使用 s指针 往后遍历字符串 ，字符串放在数组中是连续的
//	{
//		count++; // 加入循环说明有值，计数加1
//		s++; // 让指针往后偏移，判断下一位
//	}
//	return count;
//}

// 使用递归
// my_strlen("abc") --> 1 + my_strlen("bc")
// --> 1 + 1 + my_strlen("c") --> 1 + 1 + 1 + my_strlen("\0") 
// --> 1 + 1 + 1 + 0 (\0 不计数 ，记为0)
// -> 把字符一个一个剥离下来（大事化小）
//#include <stdio.h>
//
//int my_strlen(char* s)
//{
//	if (*s == '\0')
//	{
//		return 0; // 首地址数为\0,说明为空数组
//	}
//	else
//	{
//		// 进入else说明首地址有值，所以先给个1
//
//		return 1 + my_strlen(s + 1); // 使用递归
//
//		// (s + 1)相当于让指针往后偏移，判断下一位
//	}
//}
//
//int main() 
//{
//	char arr[] = "abc"; 
//	// 这个数组相当于 {a b c \0]
//
//	int len = my_strlen(arr);
//	// 调用自定义函数
//
//	printf("%d\n", len); 
//	// 字符数组的数组名是首元素的地址
//
//	return 0;
//}

////// 求n的阶乘(迭代)
//#include <stdio.h>
//
//// 循环（迭代）
//int Fac(int n)
//{
//	int r = 1;
//	int i = 0;
//	for ( i = 1; i <= n; i++ ) // 产生1-n的数
//	{
//		r = r * i; 
//	}
//	return r;
//}
//
//int main() 
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int ret = Fac(n); // 调用自定义函数
//
//	printf("%d\n", ret);
//
//
//	return 0;
//}

//// 求n的阶乘(递归)
//#include <stdio.h>
//
//// 递归
//int Fac(int n)
//{
//	if (n <= 1)
//	{
//		return 1;
//	}
//	else
//	{
//		return n * Fac(n - 1);
//	}
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int ret = Fac(n); // 调用自定义函数
//
//	printf("%d\n", ret);
//
//
//	return 0;
//}


// 求第n个斐波那契数（递归）
// 1 1 2 3 5 8 13 21 34 55 ...
// 前2个的数的和是第三个数
//#include <stdio.h>
//
//int Fib(int n)
//{
//	if (n <= 2)
//	{
//		return 1; // 前两个斐波那契数都是1
//	}
//	else
//	{
//		return Fib(n - 1) + Fib(n - 2);
//	}
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int ret = Fib(n); // 调用自定义函数
//
//	printf("%d\n", ret);
//	return 0;
//}

// 求第n个斐波那契数(迭代)
// 1 1 2 3 5 8 13 21 34 55 ...
// 前2个的数的和是第三个数
//#include <stdio.h>
//
//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	
//	while (n >= 3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//
//		n--;
//	}
//
//	return c;
//}
//
//int main() 
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int ret = Fib(n); // 调用自定义函数
//
//	printf("%d\n", ret);
//	return 0;
//}