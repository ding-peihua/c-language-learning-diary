#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//struct Stu
//{
//	char name[20];
//	int age;
//	char id[20];
//};
//
//int main()
//{
//	struct Stu s1, s2, s3;
//
//	return 0;
//}


//#include <stdio.h>
//
//struct Stu
//{
//	char name[20];
//	int age;
//	char id[20];
//}s1, s2, s3;
//
//int main()
//{
//
//	return 0;
//}



//#include <stdio.h>
//
//struct
//{
//	int a;
//	char c;
//	float f;
//}x;
//
//struct
//{
//	int a;
//	char c;
//	float f;
//}* p;
//
//
//int main()
//{
//	p = &x;
//
//	return 0;
//}


//#include <stdio.h>
//
//struct Node
//{
//	int data; //4个字节（x86）
//
//	struct Node next; 
//	//无法确定字节 ，会一直next下去（死递归？）
//};
//
//int main()
//{
//	
//	return 0;
//}


//#include <stdio.h>
//
//struct Node
//{
//	//数据域 -- 存放数据
//	int data; //4个字节（x86）
//
//	// 指针域 -- 存放指针（地址）,下个节点
//	struct Node* next;
//	//这里写成指针后，就是4个字节（x86）
//	//通过指针找到指定数据，
//	//实现在结构中包含一个类型为该结构本身的成员
//	//通过同类项的指针找到同类型的数据
//};
//
//int main()
//{
//
//	return 0;
//}


//#include <stdio.h>
//
////1.
//struct SN
//{
//	char c;
//	int i;
//}sn1, sn2; 
////创建结构体时顺带创建结构体变量
////注：创建的是全局变量
//
//int main()
//{
//	//2.
//	struct SN sn3, sn4; 
//	//在主函数中进行创建
//
//	return 0;
//}



//#include <stdio.h>
//
//struct SN
//{
//	char c;
//	int i;
//}sn1 = { 'q', 100 }, sn2 = {.i=200, .c='w'};
////初始化方法1：按照结构体的成员变量的顺序进行赋值初始化 
////初始化方法2：通过 . 操作符访问成员变量进行赋值初始化，打破顺序的束缚
//
//int main()
//{	
//	struct SN sn3, sn4;
//
//	return 0;
//}


//#include <stdio.h>
//
//struct SN
//{
//	char c;
//	int i;
//}sn1 = { 'q', 100 }, sn2 = { .i = 200, .c = 'w' };
//
//struct S
//{
//	double d;
//	struct SN sn; //结构体中包含另一个结构体：
//	int arr[10]; //结构体中包含数组
//};
//
//int main()
//{
//	//初始化：
//	struct S s = { 3.14, {'a', 99}, {1,2,3} };
//
//	//打印结构体信息：
//	printf("%lf %c %d\n", s.d, s.sn.c, s.sn.i);
//
//	//打印结构体里的数组：
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", s.arr[i]);
//	}
//
//	return 0;
//}


////结构体内存对齐(重点)：
//#include <stdio.h>
//#include <stddef.h>
//
//struct S1
//{
//	char c1;
//	int i;
//	char c2;
//};
//
//struct S2
//{
//	int i;
//	char c1;
//	char c2;
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct S1));
//	printf("%d\n", sizeof(struct S2));
//
//	printf("%d\n", offsetof(struct S1, c1));
//	printf("%d\n", offsetof(struct S1, i));
//	printf("%d\n", offsetof(struct S1, c2));
//	//offsetof() -- 是一个宏，可以计算结构体成员相较于结构体起始位置的偏移量
//	//需要头文件 -- <stddef.h>
//	//宏可以接收类型，而函数不能
//
//	return 0;
//}


////结构体内存对齐(重点)：
//#include <stdio.h>
//
//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//
////求嵌套结构体大小
//struct S4
//{
//	char c1;
//	struct S3 s3;
//	double d;
//};
//
//int main()
//{
//
//	printf("%d\n", sizeof(struct S3));
//
//	printf("%d\n", sizeof(struct S4));
//
//	return 0;
//}


//// 修改默认对齐数：
//#include <stdio.h>
//
////使用 #pragma 修改默认对齐数为1：
//#pragma pack(1) //因为都是1的倍数，所以就是按顺序进行存放
//
//struct S
//{
//	char c1; //本身大小：1	默认对齐数：1  最终对齐数：1
//	int a;	 //本身大小：4	默认对齐数：1  最终对齐数：1
//	char c2; //本身大小：1	默认对齐数：1  最终对齐数：1
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct S));
//
//	return 0;
//}



////结构体传参：
//#include <stdio.h>
//
//struct S
//{
//	int data[100];
//	int num;
//};
//
////接收结构体变量：
//void printf1(struct S tmp)
//{
//	printf("%d\n", tmp.num);
//}
//
////接收结构体变量地址：
//void printf2(const struct S* ps)
//{
//	printf("%d\n", ps->num);
//}
//
//int main()
//{
//	//创建结构体变量：
//	struct S s = { {1,2,3}, 100 };
//
//	printf1(s);
//
//	printf2(&s);
//
//	return 0;
//}



//位段：
//#include <stdio.h>
//
//struct A
//{
//	//数字是二进制数(bit)
//	int _a : 2;
//	int _b : 5; 
//	int _c : 10;
//	int _d : 30; 
//	//这里总共是 2 + 5 + 10 + 30 = 47bit，
//	//需要2个整型 8个字节 64个bit 存储
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct A));
//
//	return 0;
//}



//#include <stdio.h>
//
//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;
//};
//
//int main()
//{
//	struct S s = { 0 };
//
//	s.a = 10; //二进制表示：1010
//	s.b = 12; //二进制表示：1100
//	s.c = 3;  //二进制表示：0011
//	s.d = 4;  //二进制表示：0100
//
//	return 0;
//}


//#include <stdio.h>
//
////枚举类型声明：
//enum Color //列举颜色：
//{
//	//枚举常量，没到最后一个，要加逗号：
//	RED, //默认0
//	GREEN, //默认1
//	YELLOW, //默认2，依次递增
//	BLUE = 100 //默认3，也可以设置这些值
//};
//
//int main()
//{
//	//创建枚举类型变量创建和赋值：
//	enum Color R = RED;
//
//	printf("%d\n", RED);
//	printf("%d\n", GREEN);
//	printf("%d\n", YELLOW);
//	printf("%d\n", BLUE);
//
//	return 0;
//}



//#include <stdio.h>
//
////创建联合体：
//union Un
//{
//	//联合体成员：
//	char c; //1个字节
//	int i; //4个字节
//};
//
//int main()
//{
//	//查看联合体大小：
//	printf("%d\n", sizeof(union Un));
//
//	//创建联合体变量：
//	union Un un = { 0 };
//
//	//查看联合体变量un的地址：
//	printf("%p\n", &un);
//
//	//查看联合体变量un中成员char c的地址：
//	printf("%p\n", &(un.c));
//
//	//查看联合体变量un中成员int i的地址：
//	printf("%p\n", &(un.i));
//
//	return 0;
//}


#include <stdio.h>

union Un
{
	short c[7]; //14个字节，
	//每个元素是2个字节，VS默认对齐数8个字节，最终对齐数为2
	int i; //4个字节
	//int是4个字节，VS默认对齐数8个字节，最终对齐数为4
};

int main()
{
	//查看联合体大小：
	printf("%d\n", sizeof(union Un));

	return 0;
}