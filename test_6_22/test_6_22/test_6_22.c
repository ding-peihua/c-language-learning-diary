#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//	//定义一维数组：
//	float arr[] = { 3.14f,99.9f,66.5f,0.0f };
//
//	//定义指针变量：
//	float* p = arr; //把数组名（首地址）放进指针中
//
//	//计算数组元素个数：
//	int sz = sizeof(arr) / sizeof(arr[0]);//元素个数
//
//	//使用 for循环 和 指针变量 打印数组内容：
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		//使用 解引用符号* 获取指针内容
//		printf("%f ", *p);
//		//获取一位后就调整一次指针，把指针移向下一位
//		p++;
//	}
//
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	//定义一维数组：
//	float arr[] = { 3.14f,99.9f,66.5f,0.0f };
//
//	//定义指针变量：
//	float* p = arr; //把数组名（首地址）放进指针中
//
//	//计算数组元素个数：
//	int sz = sizeof(arr) / sizeof(arr[0]);//元素个数
//
//	//使用指针变量表示元素个数：
//	float* q = arr + sz; //此时指针变量在最后一个元素地址
//
//	//使用 while循环 和 指针变量 打印数组内容：
//	while (p < q)
//	//对比地址大小，只要还小于最后一个元素地址就循环
//	{
//		printf("%.2f ", *p++);
//		//*p++ , 先解引用 ，后再p++
//	}
//
//	return 0;
//}

#include <stdio.h>

//自定义函数print：
void print(float* p, int sz)
{
	//使用指针变量表示元素个数：
	float* q = p + sz; //此时指针变量在最后一个元素地址

	//使用 while循环 和 指针变量 打印数组内容：
	while (p < q)
		//对比地址大小，只要还小于最后一个元素地址就循环
	{
		printf("%.2f ", *p++);
		//*p++ , 先解引用 ，后再p++
	}
}

int main()
{
	//定义一维数组：
	float arr[] = { 3.14f,99.9f,66.5f,0.0f };

	//定义指针变量：
	float* p = arr; //把数组名（首地址）放进指针中

	//计算数组元素个数：
	int sz = sizeof(arr) / sizeof(arr[0]);//元素个数

	//调用自定义函数进行打印数组元素：
	print(p, sz);

	return 0;
}