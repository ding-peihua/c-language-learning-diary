#define _CRT_SECURE_NO_WARNINGS 1

//包含IO流：
#include <iostream>;
//完全展开std命名空间：
using namespace std;


//Swap函数 -- 交换两个int类型数据：
void Swap(int& left, int& right)
{
	int temp = left;
	left = right;
	right = temp;
}

//Swap函数 -- 交换两个double类型数据：
void Swap(double& left, double& right)
{
	double temp = left;
	left = right;
	right = temp;
}

//Swap函数 -- 交换两个char类型数据：
void Swap(char& left, char& right)
{
	char temp = left;
	left = right;
	right = temp;
}

/*
* 这里实现了三个Swap函数，
* 分别交换了三种不同的类型，
* 但实现的逻辑都是相同的，就只有交换类型不同，
* 所以就造成了某种程度的“冗余”
* 
* 上面的函数都需要针对具体的类型，
* 那能不能让一个代码能够针对广泛的类型呢，
* C++中就有了泛型编程：
*/

//函数模板：
template<typename T>
//tyename 也可以写成 class
//template<class T> 
//Swap函数 -- 交换两类型数据（泛型编程）：
void Swap(T& left, T& right)
{
	char temp = left;
	left = right;
	right = temp;
}
/*
* 使用函数模板即可实现泛型编程，
* 让函数能够针对广泛的类型，
* 而不只能针对一种类型，
* 
* 通过关键字template即可定义一个模板，
* Swap函数的参数设置为模板参数
*/


//主函数：
int main()
{
	int a = 0; //int类型变量
	int b = 1; //int类型变量

	double c = 1.1; //double类型变量
	double d = 2.2; //double类型变量

	//调用设置了模板参数的Swap函数：
	Swap(a, b); //int类型 -- 模板参数T
	Swap(c, d); //double类型 -- 模板参数T

	/*
	* 这里调用的两个Swap函数实际不是同一个，
	* 两个Swap函数的函数地址不同，
	* 不同类型调用的Swap函数不同是由模板参数导致的
	* 
	*			模板的原理：
	* 模板参数接受参数如果是int类型，
	* 需要调用到int类型的函数，
	* T 模板参数就会推演成 int类型，（模板参数推演）
	* 然后就会实例化出具体的函数：
	* T 是int类型的对应函数。（模板实例化）
	* 
	* 如果接收的是double类型数据，
	* T 模板参数就会推演成 double类型，（模板参数推演）
	* 然后就会示例化出具体的函数：
	* T 是double类型的对应函数。（模板实例化）
	*/

	return 0;
}


//如果一个函数需要接收不同类型的参数：
template<class T1, class T2>
/*
* 如果需要接收不同类型的参数，
* 直接在模板中设置多个模板参数即可，
*（模板参数名可以随便取，但一般会取为T -- type）
* 
* 模板参数 和 函数参数 类似，
* 但是 函数参数 定义的是 形参对象，
* 而 模板参数 定义的则是 类型
*/
void func(const T1& t1, const T2& t2)
//模板参数T1接收一种类型，T2接收另一种类型
{
	cout << t1 << endl;
	cout << t2 << endl;
	/*
	* 设置了模板参数的函数，
	* 如果要进行输入或输出，
	* 就必须使用 cin/cout 进行 输入/输出 了，
	* 因为设置了模板参数，
	* 不知道实际传进来的数据是什么数据，
	* 因为使用 scanf/printf 必须要指定数据类型，
	* 所以这里使用 scanf/printf 来 输入/输出
	*/
}


//通用（泛型）加法函数：
template<class T>
T Add(T left, T right)
//接收 T 模板参数类型
{
	return left + right;
	//返回值也是 T 模板类型
}


template<class T>
T* f()
{
	//开辟T类型的动态空间：
	T* p = new T[10];
	//没设置模板参数T，却使用了T

	//返回T类型指针：
	return p;

	/*
	* 该函数没有设置模板参数T，
	* (设置的参数不是模板参数)
	* 但返回值却返回模板指针类型(T*)，
	* 
	* 没设置模板参数就无法进行类型推演
	*/
}


//主函数：
int main()
{
	/*
	*			推演实例化：
	* 函数参数传递，推演出模板参数的类型，
	* 再生成（实例化）对应的函数
	*/

	//隐式实例化：
	
	//T1推演为int，T2推演为int：
	func(1, 2);

	//T1推演为double，T2推演为double：
	func(1.1, 2.2);

	//T1推演为double，T2推演为int：
	func(1.1, 2);


	//调用通用（泛型）加法函数：
	cout << Add(1, 2.2) << endl;
	/*
	* 该语句不能通过编译，因为在编译期间，
	* 当编译器看到该实例化时，需要推演其实参类型
	* 通过实参a1将T推演为int，通过实参d1将T推演为double类型，
	* 但模板参数列表中只有一个T，
	* 编译器无法确定此处到底该将T确定为int 或者 double类型而报错
	* 
	* 注意：在模板中，编译器一般不会进行类型转换操作，
	* 因为一旦转化出问题，编译器就需要背黑锅
	* 
	* 此时有两种处理方式：1. 用户自己来强制转化 2. 使用显式实例化
	*/	
	cout << Add(1, (int)2.2) << endl;



	//显式实例化：
	
	// 2.2 隐式转换为int类型：
	cout << Add<int>(1, 2.2) << endl; 

	// 1 隐式转换为double类型：
	cout << Add<double>(1, 2.2) << endl;

	/*
	* 直接显式实例化，指定将参数实例化为某种类型，
	* 而不通过模板参数的类型推演
	*/

	//函数没有设置模板参数：
	double* p = f<double>();
	/*
	* 函数参数没设置模板参数，
	* 但却使用了模板参数，
	* 编译器没法进行类型推演，
	* 所以此时就需要显式实例化来确定类型
	* （显式实例化的真正用法）
	*/

	return 0;
}



//使用栈解决多类型问题：
typedef int STDataType;
/*
* 想让栈存储int类型数据，
* 就在这里设置类型为int，
* 想让栈存储double类型数据，
* 就在这里设置类型为double，
* ……
*/

////类模板：
//template<class T>
//
////类模板 -- 栈类：
//class Stack
//{
//public: //公有成员函数：
//
//	//构造函数：
//	Stack(int capacity = 4)
//	{
//		//调用了构造函数则打印：
//		cout << "Stack(int capacity = 4)" << endl;
//
//		//使用new开辟栈容量大小的空间：
//		
//		// typedef 设置多类型：
//		//_a = new STDataType[capacity];
//
//		// 类模板 设置多类型：
//		_a = new T[capacity]; //使用模板T类型
//
//		_top = 0; //栈顶值默认为0
//		_capacity = capacity; //设置栈容量
//	}
//
//	//析构函数：
//	~Stack()
//	{
//		//调用了析构函数则打印：
//		cout << "~Stack()" << endl;
//
//		//使用delete释放new开辟的空间：
//		delete[] _a;
//
//		_a = nullptr; //置为空指针
//		_top = 0; //栈顶值置为0
//		_capacity = 0; //栈容量置为0
//	}
//
//private: //私有成员变量：
//
//	T* _a; //栈指针 -- 使用模板T类型
//	int _top; //栈顶值
//	int _capacity; //栈容量
//
//};


//类模板：
template<class T>

//类模板 -- 栈类：
class Stack
{
public: //公有成员函数：

	//构造函数 -- 类模板成员函数声明和定义分离：
	Stack(int capacity = 4);


	//析构函数：
	~Stack()
	{
		//调用了析构函数则打印：
		cout << "~Stack()" << endl;

		//使用delete释放new开辟的空间：
		delete[] _a;

		_a = nullptr; //置为空指针
		_top = 0; //栈顶值置为0
		_capacity = 0; //栈容量置为0
	}

private: //私有成员变量：

	T* _a; //栈指针 -- 使用模板T类型
	int _top; //栈顶值
	int _capacity; //栈容量 

};

//类模板成员函数的声明和实现分离：
template<class T>
Stack<T>::Stack(int capacity)
/*
* 实现时指定的不是Stack成员函数的类名（类域），
* 而是Stack成员函数的类型，
* (不是Stack::，而是Stack<T>)
* 需要把模板参数写出来
* 
*				注：
* 类模板不允许声明和定义分离到两个文件，
* 分离时都写在一个.h文件中
*/
{
	//调用了构造函数则打印：
	cout << "Stack(int capacity = 4)" << endl;

	//使用new开辟栈容量大小的空间：

	// typedef 设置多类型：
	//_a = new STDataType[capacity];

	// 类模板 设置多类型：
	_a = new T[capacity]; //使用模板T类型

	_top = 0; //栈顶值默认为0
	_capacity = capacity; //设置栈容量
}


//主函数：
int main()
{
	/*
	* typedef 可以解决多类型问题，
	* 那 typedef 可以代替 模板 吗？
	* 
	* 答案是不能，typedef设置一个类的类型后，
	* 该类的类型就只能是typedef设置的那一种了，
	* 
	* 如果用类模板的话设置一个类的话，
	* 该类的一个对象就可以是int类型，
	* 而该类的另一个对象还可以是double类型
	*/

	//显式实例化：
	//让这个栈类型对象存储int类型：
	Stack<int> st1; //int

	//让这个栈类型对象存储double类型：
	Stack<double> st2; //double

	/*
	* 函数模板可以显式实例化，也可以让它自己推演，
	* 函数模板大多数情况让它自己推演出类型，
	* 不进行显式实例化
	* 
	* 类模板能让一个类的对象是不同类型的，
	* （如：都是栈类对象，但一个栈对象存储int类型数据，
	* 另一个栈对象存储double类型数据）
	* 只需要在创建对象时显式实例化需要的类型即可。
	* 这时我们实现的数据结构，
	* 就跟具体的存储类型是无关的，想要哪种类型，
	* 在创建对象时就显式实例化哪种类型
	*/

	return 0;
}



// 专门处理int的加法函数 -- 非模板函数
int Add(int left, int right)
{
	return left + right;
}


// 通用加法函数 -- 模板函数
template<class T>
T Add(T left, T right)
{
	return left + right;
}


//测试函数：
void Test()
{
	// 与非模板函数匹配，编译器不需要特化：
	Add(1, 2); 

	// 调用编译器特化的Add版本：
	Add<int>(1, 2); 
	//函数模板Add被实例化为非模板函数Add
}


//专门处理int的加法函数 -- 非模板函数
int Add(int left, int right)
{
	return left + right;
}

//通用加法函数 -- 模板函数
template<class T1, class T2>
T1 Add(T1 left, T2 right)
{
	return left + right;
}

//测试函数：
void Test()
{
	//Add(int, int)：
	Add(1, 2);
	/*
	* 与非函数模板类型完全匹配，
	* 不需要函数模板实例化
	*/

	//Add(int, double)：
	Add(1, 2.0);
	/*
	* 模板函数可以生成更加匹配的版本，
	* 编译器根据实参生成更加匹配的Add函数
	*/
}