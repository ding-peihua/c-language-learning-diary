#define _CRT_SECURE_NO_WARNINGS 1

////内存开辟方式有:
//#include <stdio.h>
//int main()
//{
//	int val = 20;//在栈空间上开辟四个字节
//
//	char arr[10] = { 0 };
//	//在栈空间上开辟10个字节的连续空间
//	
//	return 0;
//}


////malloc开辟内存空间：
//#include <stdio.h>
//#include <stdlib.h>
//int main()
//{
//	int arr[10]; //int 4个字节，总共40个字节
//
//	//我们也可以用malloc函数来实现：
//	int* p = (int*)malloc(40);
//	
//	//如果申请失败：
//	if (p == NULL)
//	{
//		perror("malloc");
//		//通过perror函数打印错误信息
//		return 1;
//	}
//
//	//到这说明申请成功了：
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d\n", *(p + i));
//	}
//
//	//使用完空间后，释放内存：
//	free(p);
//
//	//释放后为防止p变成野指针，
//	//主动将其置为空指针NULL：
//	p = NULL;
//
//	return 0;
//}

//
//#include <stdio.h>
//#include <stdlib.h>//需要改头文件
//
//int main()
//{
//	//开辟10个int大小字节的内存，并初始化：
//	int* p = (int*)calloc(10, sizeof(int));
//
//	//和malloc一样开辟失败会返回NULL，
//	//所以一样需要检验：
//	if (p == NULL)
//	{
//		perror("calloc");
//		return 1;
//	}
//
//	//开辟成功：
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}
//
//	//释放：
//	free(p);
//	p = NULL;
//
//	return 0;
//}


//#include <stdio.h>
//#include <stdlib.h>
//
//int main()
//{
//	//先开辟空间：
//	int* p = (int*)malloc(40);
//	//检验开辟情况（失败 或 成功）
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//开辟成功则对空间进行初始化：
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		p[i] = i + 1;
//	}
//	
//	//使用realloc函数调整空间：
//	int* ptr = (int*)realloc(p, 800);
//	//这里设置调整空间大小为800字节，空间会不够，
//	//不够则要开辟新空间，那么就要检验开辟情况：
//	if (ptr != NULL)
//	{
//		//不是空指针，则开辟成功，
//		//再将返回空间地址赋给原指针：
//		p = ptr;
//	}
//	else
//	{
//		//开辟失败，打印错误信息：
//		perror("realloc");
//		return 1;
//	}
//	//打印调整后的空间内容：
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d\n", p[i]);
//	}
//	//释放：
//	free(p);
//	p = NULL;
//
//	return 0;
//}


//#include <stdio.h>
//#include <stdlib.h>
//
//int main()
//{
//	int* p = (int*)malloc(INT_MAX / 4);
//	
//	//malloc calloc realloc 函数都可能开辟空间，
//	//开辟空间就有可能会失败，返回 NULL空指针
//	//这时解引用该空指针就可能会出问题，
//	
//	//所以开辟完动态内存后最好要检验一下：
//	if (p != NULL)
//	{
//		//不为空指针再进行使用
//		*p = 20;
//	}
//	else
//	{
//		//开辟失败，打印错误信息：
//		perror("realloc");
//		return 1;
//	}
//
//	free(p);
//	return 0;
//}


//#include <stdio.h>
//#include <stdlib.h>
//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//
//	//对动态开辟空间进行 越界访问：
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		p[i] = i;//越界访问
//	}
//
//	free(p);
//
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	printf("%d\n", *p);
//	
//	//对非动态开辟内存使用free函数释放：
//	free(p);
//	p = NULL;
//
//	return 0;
//}



//#include <stdio.h>
//#include <stdlib.h>
//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//
//	//循环对动态空间赋值：
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*p = i;
//		p++; //改变了指向动态空间的指针
//	}
//	
//	//释放
//	free(p);
//	p = NULL;
//
//	return 0;
//}



//#include <stdio.h>
//#include <stdlib.h>
//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//
//	//释放
//	free(p);
//	//p = NULL;
//
//	free(p);
//
//	return 0;
//}


//#include <stdio.h>
//#include <stdlib.h>
//void test()
//{
//	int* p = (int*)malloc(100);
//	if (p != NULL)
//	{
//		*p = 20;
//	}
//
//	//free(p); -- 忘记释放  
//}
//
//int main()
//{
//	test();
//
//	while (1); //死循环，程序无法结束
//
//	return 0;
//}
//
