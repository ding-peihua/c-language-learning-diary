#define _CRT_SECURE_NO_WARNINGS 1

////1:改前
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//
//void GetMemory(char* p)
//{
//	//开辟动态空间
//	p = (char*)malloc(100);
//}
//
//void Test(void)
//{
//	//创建空指针：
//	char* str = NULL;
//
//	//使用该指针进行动态内存开辟：
//	GetMemory(str);
//
//	//对动态空间赋值并使用：
//	strcpy(str, "hello world");
//	printf(str);
//
//}
//
//int main()
//{
//	Test();
//	return 0;
//}
//
//
////1:改后
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//
//void GetMemory(char** p)
//{
//	//开辟动态空间
//	*p = (char*)malloc(100);
//}
//
//void Test(void)
//{
//	//创建空指针：
//	char* str = NULL;
//
//	//使用该指针进行动态内存开辟：
//	GetMemory(&str);
//
//	//对动态空间赋值并使用：
//	strcpy(str, "hello world");
//	printf(str);
//
//	//使用后进行释放：
//	free(str);
//	str = NULL;
//}
//
//int main()
//{
//	Test();
//	return 0;
//}、




////2:改前
//#include <stdio.h>
//#include <stdlib.h>
//
//char* GetMemory(void)
//{
//	char p[] = "hello world";
//	return p;
//}
//
//void Test(void)
//{
//	//创建空指针：
//	char* str = NULL;
//	//调用上面的函数：
//	str = GetMemory();
//	printf(str);
//}
//
//int main()
//{
//	Test();
//	return 0;
//}
//
//
////2:改后
//#include <stdio.h>
//#include <stdlib.h>
//
//char* GetMemory(void)
//{
//	static char p[] = "hello world";
//	return p;
//}
//
//void Test(void)
//{
//	//创建空指针：
//	char* str = NULL;
//
//	str = GetMemory();
//	printf(str);
//}
//
//int main()
//{
//	Test();
//	return 0;
//}


////3：改前：
//#include <stdio.h>
//#include <stdlib.h>
//
//void GetMemory(char** p, int num)
//{
//	//根据需求创建动态空间：
//	*p = (char*)malloc(num);
//}
//
//void Test(void)
//{
//	//创建空指针变量：
//	char* str = NULL;
//	//调用函数：
//	GetMemory(&str, 100);
//	//使用动态空间：
//	strcpy(str, "hello");
//	printf(str);
//}
//
//int main()
//{
//	Test();
//	return 0;
//}
//
//
////3：改后：
//#include <stdio.h>
//#include <stdlib.h>
//
//void GetMemory(char** p, int num)
//{
//	//根据需求创建动态空间：
//	*p = (char*)malloc(num);
//}
//
//void Test(void)
//{
//	//创建空指针变量：
//	char* str = NULL;
//	//调用函数：
//	GetMemory(&str, 100);
//	//使用动态空间：
//	strcpy(str, "hello");
//	printf(str);
//	//释放：
//	free(str);
//	str = NULL;
//}
//
//int main()
//{
//	Test();
//	return 0;
//}


////4：改前：
//#include <stdio.h>
//#include <stdlib.h>
//
//void Test(void)
//{
//	//创建动态空间并接收：
//	char* str = (char*)malloc(100);
//
//	//使用动态空间：
//	strcpy(str, "hello");
//
//	//释放：
//	free(str);
//
//	if (str != NULL)
//	{
//		strcpy(str, "world");
//		printf(str);
//	}
//}
//
//int main()
//{
//	Test();
//	return 0;
//}
//
//
////4：改后：
//#include <stdio.h>
//#include <stdlib.h>
//
//void Test(void)
//{
//	//创建动态空间并接收：
//	char* str = (char*)malloc(100);
//
//	//使用动态空间：
//	strcpy(str, "hello");
//
//	//释放：
//	free(str);
//	str = NULL;
//
//	if (str != NULL)
//	{
//		strcpy(str, "world");
//		printf(str);
//	}
//}
//
//int main()
//{
//	Test();
//	return 0;
//}


//#include <stdio.h>
//
////全局变量：
//int globalVar = 1;
////全局常量：
//static int staticGlobalVar = 1;
//
//void Test()
//{
//	//局部常量：
//	static int staticVar = 1;
//
//	//局部变量（包括指针变量） 和 数组
//	int localVar	= 1;
//	int num1[10]	= { 1,2,3 };
//					  //常量字符串：
//	char char2[]	= "abcd";
//	char* pChar3[]	= "abcd";
//					  //开辟动态空间：
//	int* ptr1		= (int*)malloc(sizeof(int) * 4);
//	int* ptr2		= (int*)calloc(4, sizeof(int));
//	int* ptr3		= (int*)realloc(ptr2, sizeof(int) * 4);
//
//	free(ptr1);
//	free(ptr3);
//}
//
//int main()
//{
//	
//	return 0;
//}

#include <stdio.h>
struct S
{
	int i;
	int arr[];//柔性数组成员
};

int main()
{
	//通过malloc函数来为柔性数组开辟动态内存空间：
	struct S* ps = (struct S*)malloc(sizeof(struct S) + 40);
	// sizeof(struct S) 为结构体大小，不包括柔性数组
	// sizeof(struct S)+40 ，加上的40就是柔性数组的大小

	//检验开辟情况：
	if (ps == NULL)
	{
		//开辟失败：
		perror("malloc");
		return 1;
	}

	//开辟成功进行使用：
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		ps->arr[i] = i + 1;
	}

	//如果决定开辟的空间不够，可以扩容：
	struct S* ptr = realloc(ps, sizeof(struct S) + 60);
	//检验开辟情况：
	if (ptr == NULL)
	{
		//开辟失败：
		perror("realloc");
		return 1;
	}

	//开辟成功进行使用：
	ps = ptr;

	for (i = 0; i < 15; i++)
	{
		printf("%d\n", ps->arr[i]);
	}

	//释放：
	free(ps);
	ps = NULL;

	return 0;
}