#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>

//逆序自定义函数 reverse：
void reverse(char* left, char* right)
{
	//使用while循环：
	while (left < right)
	//左小于右，说明中间还有值且没有指向一个字符，继续逆序
	{
		//使用临时变量tmp进行逆序：
		char tmp = *left;
		*left = *right;
		*right = tmp;
		//调换后调整左右位置
		left++;
		right--;
	}
}

int main()
{
	//定义字符数组：
	char arr[101];

	//使用 gets(arr) 获取字符串
	gets(arr);

	//求字符串长度：
	int len = strlen(arr);
	//strlen 不会把 \0 计算进去，找到 \0 就停了
	//需要头文件：<string.h>

	//逆序整个字符串：
	reverse(arr, arr + len - 1); //调用自定义逆序函数
	//函数参数：数组首地址（左指针）、最后一个字符的地址（右指针）
	//arr+len-1：首地址 + 数组长度 - 1 --> 最后一个字符的地址

	//逆序每个单词：
	//定义 单词的 起始 和 尾部 位置：
	char* start = arr; //单词起始位置
	char* cur = arr; //单词尾部位置，后面再调整

	//使用 while循环 循环查找并逆序单词：
	while (*cur != '\0')
	//整个字符串还没结束，继续找单词
	{
		//内嵌 while循环 调整单词尾部位置cur：
		while (*cur != ' ' && *cur != '\0')
		//单词尾部未找到空格且还没有结束符就继续往后移一位
		{
			cur++;//往后移一位
		}//直到找到单词，此时在空格位置

		//找到单词后进行单词逆序：
		reverse(start, cur - 1);
		//因为单词尾部 cur 此时在 空格位置，
		//所以要在空格（或\0，最后一个单词）位置前-1

		//找下一个单词：
		//调整start单词起始位置：
		start = cur + 1;
		//cur+1，空格后的下一个位置就是下一个单词的首地址

		//调整单词尾部位置：
		if (*cur == ' ')
		//只有是空格时才能跳过，\0不能再跳过了，不然就跳不出循环了
		{
			cur++;//调整下一个单词尾部位置到起始位置
		}
	}

	//两个逆序都完成后，进行打印：
	printf("%s", arr);

}