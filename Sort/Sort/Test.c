#define _CRT_SECURE_NO_WARNINGS 1

//包含排序头文件：
#include "Sort.h"

//插入排序测试：
void ISTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 3,4,2,1,5 };

	//调用插入排序函数进行排序：
	InsertSort(a, 5);

	//循环打印排序后的数组：
	printf("使用直接插入排序后的数组:> ");
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", a[i]);
	}
}


//希尔排序测试：
void SSTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 9,1,2,5,7,4,8,6,3,5 };

	//调用希尔排序函数进行排序：
	ShellSort(a, sizeof(a) / sizeof(int));

	//使用自定义打印函数打印排序后数组：
	printf("使用希尔排序后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}


//冒泡排序测试：
void BSTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 9,1,2,5,7,4,8,6,3,5 };

	//调用冒泡排序函数进行排序：
	BubbleSort(a, sizeof(a) / sizeof(int));

	//使用自定义打印函数打印排序后数组：
	printf("使用冒泡排序后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}


//直接选择排序测试：
void SlSTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 9,1,2,5,7,4,8,6,3,5 };

	//调用直接选择排序函数进行排序：
	SelectSort(a, sizeof(a) / sizeof(int));

	//使用自定义打印函数打印排序后数组：
	printf("使用直接选择排序后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}


//快速排序（递归版本）方法测试：
void QSTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 9,1,2,5,7,4,8,6,3,5 };

	//调用快速排序（递归版本）进行排序：
	QuickSort(a, 0, (sizeof(a)/sizeof(int))-1);
	//(sizeof(a)/sizeof(int))-1  --  元素个数-1==尾元素下标

	//使用自定义打印函数打印排序后数组：
	printf("使用快速排序(递归版本)后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}


//快速排序（非递归版本）方法测试：
void QSNRTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 9,1,2,5,7,4,8,6,3,5 };

	//调用快速排序（非递归版本）进行排序：
	QuickSortNonR(a, 0, (sizeof(a) / sizeof(int)) - 1);
	//(sizeof(a)/sizeof(int))-1  --  元素个数-1 == 尾元素下标

	//使用自定义打印函数打印排序后数组：
	printf("使用快速排序（非递归版本）后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}


//快速排序（三路划分递归版本）方法测试：
void QS1_0Test()
{
	//创建要进行插入排序的数组：
	int a[] = { 6,1,6,6,8,6,6,4,6,3 }; //快排中有大量重复数据

	//调用快速排序（递归版本）进行排序：
	QuickSort1_0(a, 0, (sizeof(a) / sizeof(int)) - 1);
	//(sizeof(a)/sizeof(int))-1  --  元素个数-1==尾元素下标

	//使用自定义打印函数打印排序后数组：
	printf("使用快速排序(三路划分递归版本)后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}


//归并排序（递归版本）测试：
void MSTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 9,1,2,5,7,4,8,6,3,5 };

	//调用快速排序（递归版本）进行排序：
	MergeSort(a, (sizeof(a) / sizeof(int)));

	//使用自定义打印函数打印排序后数组：
	printf("使用归并排序（递归版本）后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}


//归并排序（非递归版本）测试：
void MSNRTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 9,1,2,5,7,4,8,6,3,5 };

	//调用快速排序（非递归版本）进行排序：
	MergeSortNonR(a, (sizeof(a) / sizeof(int)));

	//使用自定义打印函数打印排序后数组：
	printf("使用归并排序（非递归版本）后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}


//计数排序（鸽巢原理）测试：
void CSTest()
{
	//创建要进行插入排序的数组：
	int a[] = { 100, 135, 122, 199, 111 };

	//调用计数排序（鸽巢原理）进行排序：
	CountSort(a, (sizeof(a) / sizeof(int)));

	//使用自定义打印函数打印排序后数组：
	printf("使用计数排序（鸽巢原理）后的数组:> ");
	PrintArray(a, sizeof(a) / sizeof(int));
}



int main()
{
	//ISTest();
	//SSTest();
	//BSTest();
	//SlSTest();
	//QSTest();
	//QSNRTest();
	//MSTest();
	//MSNRTest();
	//CSTest();
	QS1_0Test();

	return 0;
}