#define  _CRT_SECURE_NO_WARNINGS

//scanf
//printf
//库函数 - C语言的编译器提供的现成的函数，直接可以使用（使用的时候要包含头文件stdio.h）
//scanf的作用 - 输入数据，数据从键盘读到内存中
//printf的作用 - 输出数据，数据从内存打印（输出）到屏幕上
//#include <stdio.h>
//
//int main()
//{
//	int a = 0;
//
//	//scanf("%d", &a);
//		//scanf("输入的格式", 地址);
//		//地址：表示数据存放在哪里，这里&a是取地址a -- 取出a的地址
//	//printf("%d", a);
//		//结果：  输入--w  输出--0
//		//原因：输入字符和字符类型不匹配，得把格式匹配上
//
//	//读个字符：
//	char ch = 0;//字符类型里也是可以放个0的
//	scanf("%c", &ch);
//	printf("%c", ch);
//	return 0;
//}
//		//%c - 字符类型
//		//%d - 整型
//		//%s - 字符串
//		//%f - float类型
//		//%lf - double类型
//		//%p -  地址的打印

//#include <stdio.h>
//int main()
//{
//	{
//		int a = 100;//局部变量a：作用域为a所在的{}内
//		//printf("%d\n", a);//结果：打印100
//	}
//	printf("%d\n", a);//结果：系统报错 -“系统找不到指定文件”; VS报错 -"未定义标识符'a'"
//	return 0;
//}

//#include <stdio.h>
//
//int a = 2023;//全局变量：语法允许的情况下，任何情况下都可以使用
//
////在大括号外定义的变量：全局变量
////在大括号内定义的变量：局部变量
//
//void test()//void: 不用返回数据
//{
//	printf("2---> %d\n", a);
//}
//
//int main() 
//{
//	printf("1---> %d\n", a);
//	test();//调用上面写的函数
//	return 0;//结果1：打印“1---> 2023”,可以调用主函数外的a
//			 //结果2：1---> 2023
//				    //2---> 2023（调用方法后）
//	
//}
////使用add.c文件里的全局变量
//extern int g_val;//(extern: 外部符号)
//
//int main()
//{
//	printf("%d\n", g_val);
//	return 0;//结果：2023，语法允许的情况下，全局变量任何情况下都可以使用
//}


////生命周期（局部变量）
//#include <stdio.h>
//
//int main()
//{
//	{
//		int a = 3;
//		int b = 5;
//		int c = a + b;
//		printf("%d\n", c);
//	}
//	return 0;
//}


////生命周期（全局变量）
//#include <stdio.h>
//
//int a = 0;
//
//int main()
//{
//	//
//	//。。。
//	//写了好多代码。。
//	//。。。
//	//
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	const int a = 3; //const: 赋予变量常属性
//					 //常属性：不能修改的属性
//	//这里的a是具有常属性的，不能改变，但是本质上还是变量
//	printf("%d\n", a);
//	a = 10;//赋予a常属性后再修改a会报错
//	printf("%d\n", a);
//
//	//数组
//	const int n = 10;
//	//int arr[n];
//	int arr[n];
//	//报错：创建数组时[]中需要填常量，这里使用被const修饰的n填入，还是报错
//		  //说明n本质还是变量，只是不能修改而已（常变量）
//	return 0;
//}

//
//#include <stdio.h>
//
//#define SIZE 10//#define 定义标识符常量
//
//int main()
//{
//	int a = SIZE;
//	int arr[SIZE];
//	
//	return 0;
//}


//4.枚举常量
//枚举 --> 一一列举
//来源：生活中有些值是可以一一列举的：
	//性别：男，女
	//三原色：red,green,blue
	//血型：......

//#include <stdio.h>
//enum SEX//枚举关键字
//{	
//	//列出了枚举类型enum SEX的可能取值
//	MALE,	//男（常量间用逗号隔开）
//	FEMALE	//女
//	//枚举常量，要赋值（初始值）只能在这里面赋值，不赋值的话默认从0自增
//};
//int main()
//{
//	enum SEX s = FEMALE;//枚举的可能取值是给枚举类型赋值的
//	printf("%d\n", MALE);
//	printf("%d\n", FEMALE);
//
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	char ch[] = {'a', 'b', 'c', 'd', 'e', 'f'};
//	char ch2[] = "abcdef";
//	//在字符串的末尾隐藏一个\0，这个\0是字符串的结束标志
//	
//	printf("%s\n", ch);
//	printf("%s\n", ch2);
//	//使用%s打印时，遇到\0才会停止
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char ch1[] = { 'a','b','c' };//[a][b][c]
//	char ch2[] = "abc";			 //[a][b][c][\0]
//
//	int len1 = strlen(ch1);
//	//strlen()方法：求字符串长度,
//	//从给定的地址，向后数字符，直到遇到\0结束，\0不统计在内
//	printf("%d\n", len1);
//
//	int len2 = strlen(ch2);
//	printf("%d\n", len2);
//}

//#include <stdio.h>
//
//int main()
//{
//	printf("(are you ok??)");
//	//在有些编译器会打印成”are you ok]“
//	// "??)" 这三个字符会被认为是三字母词 打印时变成 "]"
//	//在 ？？） 三个字符前分别加上转义字符就可以打印 ？？）了
//	printf("(are you ok\?\?\)");
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
////打印文件路径：
//	printf("c:\code\test.c\n");
//	//打印结果：code  est.c
//	
//	//正确方法：在 \t(水平制表符) 前加上 \ ,转义掉转义字符
//	printf("c:\code\\test.c");
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	printf("\a");//触发电脑蜂鸣器
//	printf("abcdef\n");
//	printf("abc\bdef\n");//\b：退格符，这里\b后面的d退到了c的位置
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	printf("%c",'\130');//8进制的130，转换成10进制-->88-->对应ASCII表的 X
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	printf("%c\n", '\x31');//16进制的31，转换成10进制-->49-->对应ASCII表的 1
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>//使用跟字符串相关的函数时使用该头文件
//
//int main()
//{
//	printf("%d\n", strlen("c:\test\x11\328\test.c"));
//	return 0;
//}

#include <stdio.h>

//int main()
//{
//	//以//开头的注释是属于C++的注释风格
//	
//	/*
//	* 以 "/**/" 开头的注释是属于C语言的注释风格（不支持嵌套）
//	*/
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	printf("愿意好好学习吗？（1/0）：");
//	int flag = 0;
//	scanf("%d",&flag);
//
//	//if选择语句
//	if (flag == 1)
//		printf("好offer\n");
//	else if (flag == 0)
//		printf("卖红薯\n");
//
//	return 0;
//}

#include <stdio.h>

int main()
{
	int line = 0;

	//while循环语句
	while (line < 50000)
	{
		printf("敲代码：%d\n", line);
		line++;
	}

	if (line == 50000)
		printf("好offer\n");
	else
		printf("卖红薯\n");

	return 0;
}