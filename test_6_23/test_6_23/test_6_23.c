#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
int main()
{
	//创建 字符数组str :
	char str[10001] = { 0 };

	//输入字符串：
	gets(str); //输入数组数据，gets可以把空格也读入
	
	//求输入的字符串长度：
	int len = strlen(str); //记得包含头文件<string.h>
	
	//创建左右指针：
	char* left = str; //左指针
	char* right = str + len -1; //右指针

	//使用 while循环 配合 左右指针 进行字符串逆序：
	while (left < right)
	//两指针中间还有值就继续逆序
	{
		//使用一个临时变量进行两个元素的逆序
		char tmp = *left; //使用 解引用符号* 获取指针内容
		*left = *right;
		*right = tmp;
		//逆序完一次后就调整一次指针位置
		left++;
		right--;
	}

	//进行打印：
	printf("%s\n", str);

	return 0;
}