#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int main()
{
	int n = 0; //直角三角形边的长度，"*"的数量，输出行数

	//多组输入：
	while (scanf("%d", &n) == 1)
	{
		int i = 0; //行
		int j = 0; //列
		//使用 for循环 打印行
		for (i = 0; i < n; i++)
		{
			//内嵌 for循环 打印列
			for (j = 0; j < n; j++)
			{
				//如果 行数+列数 < 三角形长度 - 1
				//打印 两个空格：
				if (i+j < n-1)
				{
					printf("  ");
				}
				else
				{
					//其它情况打印 *号+空格：
					printf("* ");
				}
				
			}

			//打印完一行后进行换行：
			printf("\n");
		}
	}

	return 0;
}