#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <windows.h>
int main()
{
	//生成第一个字符串arr1:
	char arr1[] = "welcome to school!!!!!";
	//生成第二个字符串arr2：
	char arr2[] = "######################";

	//定义左右小标：left 和 right
	int left = 0;
	int right = strlen(arr1) - 1;
	//strlen（数组名称）：求数组的长度，计算字符数组中\0前有多少个字符
	//数组长度 - 1 = 最右端下标 

	//使用while循环结合左右下标进行循环替换
	while (left <= right)
	//如果 left > right 说明数组中左右下标之间已经没有值了
	//有值就一直循环到没有值
	{
		//利用左右下标将 arr1 左右两端的值赋给 arr2 左右两端的值
		arr2[left] = arr1[left];//将arr1左边的值 赋给 arr2左边的值
		arr2[right] = arr1[right];//将arr2右边的值 赋给 arr2右边的值

		//打印重新赋值后的arr2：
		printf("%s\n", arr2);
		
		//使用Sleep()函数进行优化：
		Sleep(1000);//休眠1000毫秒，即1秒，再执行后面的语句，需要头文件：<windows.h>

		//使用system()函数进行优化：
		system("cls");//清理屏幕

		//调整左右下标，以便下次循环改变下一对左右两端的值：
		left++; //调整左下标
		right--; //调整右下标
	}

	printf("%s\n", arr2);

	return 0;
}