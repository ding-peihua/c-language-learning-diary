#define _CRT_SECURE_NO_WARNINGS 1

////memcpy()函数：
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	//源数据：
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//目标空间：
//	int arr2[20] = { 0 };
//	
//	//将arr1的内容拷贝到arr2中，因为是int类型不能使用strcpy函数：
//	memcpy(arr2, arr1, 40);
//
//	//结果：
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//
//	return 0;
//}


////模拟memcpy()函数：
//#include <stdio.h>
//#include <assert.h>
//
//void* my_memcpy(void* dest, const void* src, size_t num)
////返回值为 拷贝结束后的目标空间起始地址
////size_t num：参数接收的是 要拷贝的字节数，因为不同类型的数据字节数是不同的
//{
//	//保存目标空间的原始地址,方便拷贝完后返回原地址：
//	void* ret = dest;
//
//	//使用断言，确保两指针不是空指针：
//	assert(dest && src);
//
//	while (num--)
//	//将两个void*指针，转化为char*指针，一个字节一个字节拷贝
//	//所以有几个字节就拷贝几次
//	{
//		//转化为char*指针，一个字节一个字节拷贝：
//		//强制类型转化只是临时的：
//		*(char*)dest = *(char*)src;
//
//		//移动指针拷贝下一位：
//		//因为void*指针不能++和*，
//		//所以要转化为char*后+1再赋给dest：
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//
//	//返回原地址：
//	return ret;
//}
//
//int main()
//{
//	//源数据：
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//目标空间：
//	int arr2[20] = { 0 };
//
//	//将arr1的内容拷贝到arr2中，因为是int类型不能使用strcpy函数：
//	//使用模拟的自定义函数：
//	my_memcpy(arr2, arr1, 20);
//
//	//结果：
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//
//	return 0;
//}

//int main()
//{
//	//源数据：
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	memcpy(arr1+2, arr1, 20);
//
//	//结果：
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//
//	return 0;
//}


////模拟实现memmove函数：
//#include <stdio.h>
//#include <assert.h>
//
//void* my_memmove(void* dest, const void* src, size_t num)
////				  目标空间		原数据空间    （首元素地址）
//{
//	//保存起始位置：
//	void* ret = dest;
//
//	//断言：
//	assert(dest && src);
//
//	//如果源数据空间在目标空间有重叠，则有2种情况：
//	//1. 目标空间首地址(dest) 在 源数据空间首地址左边(src) ，
//	//   那就需要 从前往后 进行移动，才不会有源数据被覆盖的情况
//	//2. 目标空间首地址(dest) 在 源数据空间首地址右边(src) ，
//	//   那就需要 从后往前 进行移动，才不会有源数据被覆盖的情况
//	//如果两个空间没有重叠的情况，则 从前往后 还是 从后往前 移动都可以
//	//使用分两种情况即可：
//	//void* 不能解引用和+1-1操作，但可以比（地址）大小
//	if (dest < src) //第一种情况
//	{
//		//从前往后 移动：
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else //第二种情况
//	{
//		//从后往前 移动：
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//
//	return ret;
//}
//
//int main()
//{
//	//源数据：
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	my_memmove(arr1, arr1+2, 20);
//
//	//结果：
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//
//	return 0;
//}


//memcmp函数：
//#include <stdio.h>
//#include <string.h>
//int main()
//{	
//	int arr1[20] = { 1,2,1,3,4 };
//
//	int arr2[20] = { 1,2,257 };
//
//	//使用memcmp函数进行内存大小的比较：
//	int ret = memcmp(arr1, arr2, 9);
//
//	printf("%d", ret);
//
//	return 0;
//}