#define _CRT_SECURE_NO_WARNINGS 1

//杨氏矩阵：
//有一个数字矩阵，矩阵的每行从左到右是递增的，
//矩阵从上到下是递增的，
//请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N);

////正常写法，时间复杂度为O(N)：
//#include <stdio.h>
//int main()
//{
//	//给出符合杨氏矩阵的二维数组：
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//
//	//输入要找的数：
//	int k = 0;
//	scanf("%d", &k);
//
//	//在二维数组中一个一个遍历：时间复杂度为O(N)
//	int i = 0; 
//	for (i = 0; i < 3; i++)//遍历行
//	{
//		int j = 0;
//		for (j = 0; j < 3; j++)//遍历列
//		{
//			if (arr[i][j] == k)//如果找到了
//			{
//				//进行打印：
//				printf("找到了,它的下标为：%d %d", i, j);
//				
//				return 0; 
//				//直接使用return结束程序，
//				//break只能跳出一层循环
//			}
//		}
//	}
//
//	//未找到则打印未找到：
//	printf("未找到");
//
//	return 0;
//}

//
//#include <stdio.h>
//
//void young_tableau_search(int arr[3][3], int k, int* px, int* py)
//{
//	//开始查找，思路：
//	//因为是杨氏矩阵，所以一行中最右边的数是最大的
//	//这个最大值如果比要找的值都小的话，那就可以排除这一行
//	//列也是同理
//
//	// 坐标(0,2)，第一行的最大值
//	int x = 0; //二维数组的行
//	int y = *py-1; //二维数组的列
//	while (x <= *px-1 && y >= 0)
//		//行最大调整到第二行，列最多调整到第0行
//	{
//		if (arr[x][y] < k)
//			//如果第一行的最大值都小于k，
//		{
//			x++; //排除这一行，移到下一行
//		}
//		else if (arr[x][y] > k)
//			//如果第一行的最大值大于k，
//		{
//			y--; //说明k可能就在这一行，移动列进行查找
//		}
//		else
//		{
//			//找到了就把k的行和列赋给指针px和指针py：
//			*px = x;
//			*py = y;
//			return;
//		}
//	}
//
//	//自定义未找到的情况：
//	*px = -1;
//	*py = -1;
//}
//
//int main()
//{
//	//给出符合杨氏矩阵的二维数组：
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//	//		1 2 3
//	//		4 5 6
//	//		7 8 9
//
//	//输入要找的数：
//	int k = 0;
//	scanf("%d", &k);
//
//	int x = 3; //k的行
//	int y = 3; //k的列
//
//	//自定义一个函数进行查找：
//	young_tableau_search(arr, k, &x, &y);
//	//参数：	二维数组名，要找的值，行数，列数
//
//	//通过函数的使用情况打印相应情况：
//	if (x==-1 && y==-1)
//		//未找到：
//	{
//		printf("未找到");
//	}
//	else
//		//找到了：
//	{
//		printf("找到了,它的下标为：第%d行 第%d列", x, y);
//	}
//
//	return 0;
//}


////方法1：左旋一个字符就移动一次剩余的字符
////实现一个函数，可以左旋字符串中的k个字符。
////例如：
////ABCD左旋一个字符得到BCDA
////ABCD左旋两个字符得到CDAB
//#include <stdio.h>
//#include <string.h>
//#include <assert.h>
//
//void left_move(char* str, int k)
//{
//	//断言：str不为空指针
//	assert(str);
//
//	//左旋k次：
//	int j = 0;
//	for (j = 0; j < k; j++)
//	{
//		//存放被左旋字符的地址，从第一个字符开始
//		char tmp = *str;
//
//		//求字符串长度：
//		int len = strlen(str);
//
//		//左旋后一个字符后，后面len-1个字符要往前挪一位
//		int i = 0;
//		for (i = 0; i < len - 1; i++)
//		{
//			//把后一个字符赋给前一个
//			*(str + i) = *(str + i + 1);
//			//这里就覆盖掉了前面一个字符，末尾空出一个字符
//		}
//
//		//将左旋的字符移动到尾部
//		*(str + len - 1) = tmp;
//	}
//}
//
//int main()
//{
//	char arr[] = "ABCD";
//
//	//输入左旋次数：
//	int k = 0;
//	scanf("%d", &k);
//
//	//使用自定义函数进行左旋：
//	left_move(arr, k);
//	//参数： 字符串首地址，左旋次数
//	
//	//打印左旋后结果：
//	printf("%s\n", arr);
//
//	return 0;
//}


////方法2：
//#include <stdio.h>
//#include <assert.h>
//#include <string.h>
//
////定义一个逆序函数：
//void reverse(char* left, char* right)
//{
//	//断言：两指针不为空
//	assert(left && right);
//
//	//进行逆序：
//	while (left < right)
//		//两指针中间还有数就继续
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//void left_move(char* str, int k)
//{
//	int len = strlen(str);
//
//	k %= len;//防止重复操作
//
//	//求左旋字符进行逆序：
//	reverse(str, str + k - 1);
//
//	//对剩余字符进行逆序：
//	reverse(str + k, str + len - 1);
//
//	//整体逆序：
//	reverse(str, str + len - 1);
//}
//
//int main()
//{
//	char arr[] = "ABCD";
//	
//	//输入左旋次数：
//	int k = 0;
//	scanf("%d", &k);
//
//	//使用自定义函数进行左旋：
//	left_move(arr, k);
//	//参数： 字符串首地址，左旋次数
//	
//	//打印左旋后结果：
//	printf("%s\n", arr);
//
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//int is_left_move(char* str1, char* str2)
//{
//	//求字符串长度：
//	int len = strlen(str1);
//
//	int j = 0;
//	for (j = 0; j < len; j++)
//		//字符串长度为几，最多就旋转几次
//	{
//		//存放被左旋字符的地址，从第一个字符开始
//		char tmp = *str1;
//
//		//左旋后一个字符后，后面len-1个字符要往前挪一位
//		int i = 0;
//		for (i = 0; i < len - 1; i++)
//		{
//			//把后一个字符赋给前一个
//			*(str1 + i) = *(str1 + i + 1);
//			//这里就覆盖掉了前面一个字符，末尾空出一个字符
//		}
//
//		//将左旋的字符移动到尾部
//		*(str1 + len - 1) = tmp;
//
//		//到这里就旋转了1次，
//		//每旋转1次就使用strcmp判断是否和str2相同：
//		if (strcmp(str1, str2) == 0)
//		{
//			return 1;//找到返回1
//		}
//	}
//
//	//未找到就返回0
//	return 0;
//}
//
//int main()
//{
//	char arr1[] = "ABCDEF";
//	char arr2[] = "CDEFAB";
//
//	//使用自定义函数：
//	int ret = is_left_move(arr1, arr2);
//
//	//根据自定义函数返回结果进行打印：
//	if (ret == 1)
//	{
//		printf("Yes\n");
//	}
//	else
//	{
//		printf("No\n");
//	}
//
//	return 0;
//}



#include <stdio.h>
#include <string.h>
int is_left_move(char* str1, char* str2)
{
	int len1 = strlen(str1);
	int len2 = strlen(str2);

	//判断两字符串长度是否相同，不同的话肯定不能通过旋转得到：
	if (len1 != len2)
	{
		return 0;
	}

	//通过strncat在str1上再追加str1：
	strncat(str1, str1, len1);

	//判断追加后的str1中有没有str2，有则可以通过旋转得到：
	if (strstr(str1, str2) == NULL)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int main()
{
	char arr1[20] = "ABCDEF";
	char arr2[] = "CDEFAB";

	//使用自定义函数：
	int ret = is_left_move(arr1, arr2);

	//根据自定义函数返回结果进行打印：
	if (ret == 1)
	{
		printf("Yes\n");
	}
	else
	{
		printf("No\n");
	}

	return 0;
}