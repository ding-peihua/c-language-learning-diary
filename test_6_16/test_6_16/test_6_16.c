#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//编写函数：
int number_of_1(int m)
//函数返回值：int ； 函数形参：int m
{
	//设置计数器：
	int count = 0;

	//使用while循环
	while (m) 
	//如果m不为0，就说明二进制中还有1，则继续循环
	{
		//使用公式：
		m = m & (m - 1); //去掉最右边的1
		count++; //计数器++
	}
}

int main()
{
	//输入两个数：
	int m = 0;
	int n = 0;
	scanf("%d %d", &m, &n);

	//异或：相同为0，相异为1
	int ret = number_of_1(m ^ n);

	//打印：
	printf("%d", ret);

	return 0;
}