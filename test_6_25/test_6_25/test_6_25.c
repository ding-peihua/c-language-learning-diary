#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <math.h>
int main()
{
	//使用for循环产生 0~100000 的数：
	int i = 0;
	for (i = 0; i <= 100000; i++)
	{
		//使用变量 tmp 代替 i，防止后续操作改变循环变量：
		int tmp = i; //代替 i

		//计算 i 的位数：
		int n = 1; //位数，因为一个数至少也是一位数，所以n起始为1

		//思路：i/10 --> 可以去掉一位 --> n+1 ,直到 i/10==0
		while (tmp /= 10) //直到tem/10==0，停止循环
		{
			n++;//统计位数
		}
		
		//因为上面改变了 tmp /= 10 ,改变了 tmp 的值，
		//所以要再替换一次 i ：
		tmp = i;

		//创建变量 sum 存放该数每一位次方的和：
		int sum = 0;

		//只要 i 不等于0，就继续取出下一位：
		while (tmp)
		{
			//%10，取出一位后，求出该位的n次方，再求和
			sum += pow(tmp % 10, n);

			//计算下一位：
			tmp /= 10;
		}

		//判断sum是不是自幂数
		if (sum == i)
		{
			printf("%d ", i);//是则进行打印
		}

	}

	return 0;
}