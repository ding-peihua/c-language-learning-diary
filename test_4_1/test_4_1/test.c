#define  _CRT_SECURE_NO_WARNINGS

//写一个C语言的代码，在屏幕上打印：hello world
//#include <stdio.h>
//
//int main()
//{
//	printf("hello world\n");
//	return 0;
//}

//#include <stdio.h>:
//.h后缀 ---> 头文件
//

//#include <stdio.h> 
//
////main()函数:
////代码一般是从main函数开始写
////C语言中main函数是程序的入口
////程序是从main函数的第一行开始执行的
////进入到main函数后再一行一行按照顺序往后进行
////可以按键盘 F10 观察过程
////main函数有且仅有一个
//
//int main()	//int:整形  main():加()代表是一个函数
//{ //{}大括号： 函数体
//
//	printf("hehe\n");
//	printf("hehe\n");
//	printf("hehe\n");
//	printf("hehe\n");
//	printf("hehe\n");
//
//	//printf():
//	//printf是一个 库函数 --> C语言编译器提供的一个现成的函数
//	//直接可以使用
//	//但是在使用之前得包含头文件：stdio.h
//	//功能就是在屏幕上打印数据
//	//"hehe" --> 是一个字符串
//	//双引号引起来的就是字符串
//	printf("hehe\n"); // \n:换行
//	printf("呵呵\n");
//
//	return 0; //return 0:  int 和 return 0 是前后呼应的，int整形后面返回(return)0是一个整数
//}

//当一个文件有两个main方法时，会报错："int main()"已有主体  /  参见"main"的前一个定义
//int main() 
//{ 
//	printf("hehe\n");
//	printf("hehe\n");
//	printf("hehe\n");
//	printf("hehe\n");
//	printf("hehe\n");
//	printf("hehe\n");
//
//	return 0; 
//}


//这种写法非常古老 -- 不推荐
//void main()
//{
//
//}

//这种写法也有
//int main(void)	//void再这里是 main函数不接受任何参数
//{
//	return 0;
//}

//以后再研究，要搞清参数有什么意义
//int main(int argc, char* argv[])
//{
//	return 0;
//}





//数据类型
//#include <stdio.h>

//int main()
//{
//	//printf("%d\n", 20);	//%d：以十进制的形式来打印一个整型   20：打印20
//	// 
//	//sizeof：是一个操作符，用来计算 类型/变量所占内存空间的大小
//	printf("%d\n", sizeof(char));	//char --> 占 1 个字节
//	printf("%d\n", sizeof(short));	//short --> 占 2 个字节
//	printf("%d\n", sizeof(int));	//int --> 占 4 个字节
//	printf("%d\n", sizeof(long));	//long --> 占 4 个字节
//	printf("%d\n", sizeof(long long));	//long long --> 占 8 个字节
//	printf("%d\n", sizeof(float));	//float --> 占 4 个字节
//	printf("%d\n", sizeof(double));	//double --> 占 8 个字节
//
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	printf("%d\n %d\n", 10, 20);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	//类型 变量名; 
//	//类型 变量名 = 初始值;
//	
//	//int age;	//只初始化了，未赋值
//	//int age2 = 20;	//初始化顺便赋值
//
//	//55.5 这个小数直接写出来，编译器默认是double类型的
//	//55.5f 这个时候计算float类型
//	/*float weight = 55.5f;	
//	double weight2 = 88.5;*/
//
//	/*int _2b;
//	int _2B;*/
//	//int char;
//	//变量名要起得有意义
//	int num = 10;
//	printf("%d\n", num);	//10
//	num = 20;
//	printf("%d\n", num);	//20
//
//	return 0;
//}


//#include <stdio.h>

//首先原则上我们的变量名尽量不要冲突
//但是当全局变量和局部变量的名字冲突的情况下，局部变量是优先的

//int a = 100;	//全局变量
//
//int main()
//{
//	int a = 10;	//局部变量
//	printf("a=%d\n", a);
//
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	//写一个代码，求2个整数的和
//	int num1 = 0;
//	int num2 = 0;
//	int sum = 0;
//	//输入2个整数的值
//	//&num1 --> 取出num1的地址
//	//& --> 取地址操作符
//	//printf() --> 输出符 | scanf() --> 输入符（在控制台输入,输入时2个数之间留一个空格)
//	scanf("%d %d", &num1, &num2); 
//	sum = num1 + num2;
//	//打印
//	printf("%d\n", sum);
//	return 0;
//
//}

//#include <stdio.h>
//int main()
//{
//    printf("     **\n");
//    printf("     **\n");
//    printf("************\n");
//    printf("************\n");
//    printf("   *   *    \n");
//    printf("   *   *    \n");
//	return 0;
//}