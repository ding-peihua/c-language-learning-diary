﻿#define  _CRT_SECURE_NO_WARNINGS

//#include <stdio.h>

//在整个工程中，main函数有且仅有一个，不同.c文件中也仅有一个
//int main()
//{
//	return 0;
//}


//多组输入
//#include <stdio.h>
//
//int main()
//{
//	int iq = 0;
//	int eq = 0;
//	//输入
//	//int n = scanf("%d %d", &iq, &eq);
//	//scanf函数返回的是读取到数据的个数
//	//如果scanf函数读取失败会返回EOF
//	//EOF：end of file 文件结束标志
//	//#define EOF -1（所以失败后系统默认返回-1）
//	while (scanf("%d", &iq) == 1)
//		//等于1说明正常读取，则进入循环
//		//一直正常输入则能一直循环
//		//输入大写字母就能停止循环（Crtl + Z）
//	{
//		if (iq >= 140)
//			printf("Genius\n");
//	}
//
//	//printf("%d\n", n);
//
//	return 0;
//}


//函数
//#include <stdio.h>
//int Add(int x, int y)//定义一个名叫Add，带两个参数的函数
//{	//这里的两个参数是 形式参数（形参）
//	int z = x + y;//两个参数调进来后相加赋给z
//	return z;//返回z，因为z是int类型，所以Add()函数的返回类型定义为int
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//
//	//输入
//	scanf("%d %d", &a, &b);
//
//	int sum = Add(a, b);
//	//这里的两个参数是 实际参数（实参）
//	//这里调用上面的Add()函数,把输入的a和b代入函数的两个参数
//	// 两值相加后返回z，代入sum
//	//输出
//	printf("%d\n", sum);
//
//	return 0;
//}

//数组定义
//#include <stdio.h>
//
//int main()
//{
//	/*int a = 0;
//	int b = 1;
//	int c = 2;*/
//	//......(一个一个存)
//	//1-10（很麻烦）
//
//	//数组 - 可以保存一组相同类型的数
//	int arr[10] = {1,2,3,4,5,6,7,8,9,10};//整型数组
//	char ch[8];//字符数组
//
//	return 0;
//}

//数组下标
//#include <stdio.h>
//
//int main()
//{
//	/*int a = 0;
//	int b = 1;
//	int c = 2;*/
//	//......(一个一个存)
//	//1-10（很麻烦）
//
//	//数组 - 可以保存一组相同类型的数
//	//{}里的每个值为数组的 元素
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };//整型数组
//	//	数组下标：  0 1 2 3 4 5 6 7 8 9   （从0开始）
//	//char ch[8];//字符数组
//
//	//利用下标循环打印数组：
//	int i = 0;
//	while (i < 10)
//	{
//		printf("%d\n",arr[i]);
//		//先打印arr[0],之后i自增变成1，还是<10,再进循环，
//		//打印arr[1]......循环输出到arr[9],i自增成10，i=10,
//		//跳出循环
//		i++;
//	}
//
//	//printf("%d\n", arr[5]);
//	//通过数组下标来访问数组中的某个元素
//	//这里访问的是下标为5的元素，即6
//
//	return 0;
//}

//除法：
//#include <stdio.h>
//
//int main()
//{
//	//整数除法(两个整数相除)
//	int a = 7 / 2;
//	printf("%d\n", a);
//	
//	//浮点数除法(保证除数和被除数中至少有一个数是浮点数) 
//	printf("%lf\n", 7.0 / 2);
//	printf("%lf\n", 7 / 2.0);
//	printf("%lf\n", 7.0 / 2.0);
//	//%lf：double类型
//	return 0;
//}


//! - 逻辑反操作
//#include <stdio.h>
//
////C语言中表示真假：
////0 - 假
////非0 - 真（只要不是0就是真）
//
//int main()
//{
//	int a = 0;
//	if (a)//a = 5，非0，为真，进入if 
//	{
//		printf("hehe\n");
//	}
//	if (!a)//a本为真，逻辑反操作，变为假，无法进入if
//		   //真变假：0 假变真：1
//	{
//		printf("haha\n");
//	}
//	return 0;
//}

//++
//#include <stdio.h>
//
//int main()
//{
//	int a = 1;
//
//	int c = a++; //后置++，先使用，再++
//
//
//	int b = ++a; //前置++，先++，后使用
//
//	
//	printf("%d %d %d\n", c,b,a);
//	return 0;
//}



//--
//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//
//	int c = a--;//后置--：先使用，再--
// 
//	int b = --a;//前置--：先--，再使用
//
//	printf("%d %d %d\n", c, b, a);
//	return 0;
//}

//强制类型转换
//#include <stdio.h>
//
//int main()
//{
//	int a = (int)3.14;//（类型）：强制类型转换(不会四舍五入)
//	//这里如果不使用强制类型转换能打印但会警告： 
//	//“初始化”: 从“double”转换到“int”，可能丢失数据
//	printf("%d\n",a);
//	return 0;
//}