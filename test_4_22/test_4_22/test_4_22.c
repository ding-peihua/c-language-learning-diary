#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>

// strcpy函数的使用
//int main()
//{
//	char arr1[] = "hello friend"; // 源头
//	char arr2[20] = { 0 }; // 目的地
//
//	//对于数组：数组名其实是数组第一个元素的地址，也就是起始地址
//	strcpy(arr2, arr1);
//
//	printf("%s\n", arr2);
//
//	return 0;
//}
//
//

//// 写一个函数可以找出两个整数中的较大值：
//#include <stdio.h>
//
//// 自定义函数：
//int get_max(int x, int y)
//{
//	int z = (x > y ? x : y);
//	return z;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	//输入
//	scanf("%d %d", &a, &b);
//	//调用自定义函数
//	int m = get_max(a,b);
//	//输出
//	printf("较大值是： %d", m);
//
//	return 0;
//}

//写一个函数可以交换两个整型变量的内容：
//#include <stdio.h>
//
//void Swap(int x, int y) // 这里的 x 和 y 是形参（形式参数）
////当实参传递给实参时，形参是实参的一份临时拷贝，所以对形参的修改不会影响实参
//{
//	int tmp = x;
//	x = y;
//	y = tmp;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//
//	printf("交换前：a=%d,b=%d\n", a, b);
//
//	Swap(a, b); // 使用自定义函数调换两个整型变量的内容
//	// 这里的 a 和 b 是实参（实际参数）
//	printf("交换后：a=%d,b=%d\n", a, b);
//	return 0;
//}


//写一个函数可以交换两个整型变量的内容：
//#include <stdio.h>
//
//void Swap(int *pa, int *pb) // 把 a 和 b 的地址传进来，使用地址变量
//// int * 是类型 ，pa是变量名
//{
//	int tmp = *pa; 
//	*pa = *pb;
//	*pb = tmp;
//	// 变量 pa 或 pb 存放的是 地址
//	// *pa 或 *pb 则是通过地址获取里面的内容
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//
//	printf("交换前：a=%d,b=%d\n", a, b);
//
//	Swap(&a, &b); // 取 a 和 b 的地址
//
//	printf("交换后：a=%d,b=%d\n", a, b);
//	return 0;
//}


// 写一个函数可以判断一个数是不是素数。
// 是素数返回1，不是素数返回0：
//#include <stdio.h>
//#include <math.h>
//
////自定义判断素数的函数：
//int is_prime(int n)
//{
//	int j = 0;
//	for ( j = 2; j <= sqrt(n); j++ )
//	{
//		if (n % j == 0)
//		{
//			return 0; // 能被整除说明是素数，返回0
//		}
//	}
//	return 1; // 整个循环中都没有被整除，说明是素数 
//}
//
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for ( i = 100; i <= 200; i++)
//	{
//		// 判断i是否为素数
//		if (is_prime(i) == 1) // 调用函数返回值是1，说明是素数
//		{
//			printf("%d\n", i);
//			count++;
//		}
//	}
//
//	printf("\ncount = %d\n", count);
//	return 0;
//}

// 写一个函数判断一年是不是闰年
// 是闰年返回1，不是闰年返回0

// 2.实现函数
// 自定义判断是不是闰年的函数：
//int is_leap_year(int y)
//{
//	return ((y % 4 ==0) && (y % 100 != 0) || (y % 400 == 0));
//	// 因为判断结果是1或0，所以可以直接放在return后
//}
//
//int main()
//{
//	int y = 0;
//	int count = 0;
//	for ( y = 1000; y <= 2000; y++ )
//	{
//		// 1.函数怎么使用
//		//TDD - 测试驱动开发
//		//test drive development(测试驱动开发)：先做框架，再做细节
//		if (is_leap_year(y) == 1)
//		{
//			printf("%d ", y);
//			count++;
//		}
//	}
//
//	printf("\ncount = %d\n", count);
//
//	return 0;
//}


//写一个函数，实现一个整形有序数组的二分查找：
//#include <stdio.h>
//
//int binary_search(int arr[], int k, int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//
//	while (left <= right)
//	{
//		// 如果left和right过大超过4个字节，那就不是想要的结果了
//		// int mid = (left + right) / 2;
//
//		// left是小的一边，(right - left)是两者差值，
//		// (right - left) / 2，差值的一半赋给小的一边
//		// 两边一样，这时任意一边都是平均值（中间值）
//		int mid = left + (right - left) / 2;
//
//		if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else 
//		{
//			return mid; // 找到了
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 }; // 被查找数组
//	int k = 7; // 要查找的值
//	int sz = sizeof(arr) / sizeof(arr[0]); // 元素个数
//	
//	// 二分查找 (调用自定义函数)
//	int ret = binary_search(arr, k, sz); // 返回下标
//	// 自定义函数参数：数组，要找的值，元素个数
//
//	// 找到了，返回下标 ； 未找到，返回-1
//	if (ret == -1)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("找到了，下标是：%d\n", ret);
//	}
//
//	return 0;
//}