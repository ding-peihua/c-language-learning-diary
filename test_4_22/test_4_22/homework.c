#define _CRT_SECURE_NO_WARNINGS 1


//乘法口诀表
//#include <stdio.h>
//
//void multiplication(int num)
//{
//	int i = 0;
//	for (i = 1; i <= num; i++) // 1~n
//	{
//		printf("\n");
//		int j = 0;
//		int result = 0;
//		for (j = 1; j <= i; j++)
//		{
//			result = i * j;
//			printf("%d * %d = %d  ", i, j , result);
//		}
//
//	}
//}
//
//int main()
//{
//	int input = 0;
//	//输入：
//	scanf("%d", &input);
//
//	//调用自定义函数：
//	multiplication(input);
//
//	return 0;
//}

//交换两个整数
//#include <stdio.h>
//
//void Swap(int *pa, int *pb) // 把 a 和 b 的地址传进来，使用地址变量
//// int * 是类型 ，pa是变量名
//{
//	int tmp = *pa; 
//	*pa = *pb;
//	*pb = tmp;
//	// 变量 pa 或 pb 存放的是 地址
//	// *pa 或 *pb 则是通过地址获取里面的内容
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//
//	printf("交换前：a=%d,b=%d\n", a, b);
//
//	Swap(&a, &b); // 取 a 和 b 的地址
//
//	printf("交换后：a=%d,b=%d\n", a, b);
//	return 0;
//}


//实现函数判断year是不是润年。

// 是闰年返回1，不是闰年返回0
// 2.实现函数
// 自定义判断是不是闰年的函数：
//#include <stdio.h>
//int is_leap_year(int y)
//{
//	return ((y % 4 ==0) && (y % 100 != 0) || (y % 400 == 0));
//	// 因为判断结果是1或0，所以可以直接放在return后
//}
//
//int main()
//{
//	int y = 0;
//	int count = 0;
//	for ( y = 1000; y <= 2000; y++ )
//	{
//		// 1.函数怎么使用
//		//TDD - 测试驱动开发
//		//test drive development(测试驱动开发)：先做框架，再做细节
//		if (is_leap_year(y) == 1)
//		{
//			printf("%d ", y);
//			count++;
//		}
//	}
//
//	printf("\ncount = %d\n", count);
//
//	return 0;
//}



// 写一个函数可以判断一个数是不是素数。
// 是素数返回1，不是素数返回0：
#include <stdio.h>
#include <math.h>

//自定义判断素数的函数：
int is_prime(int n)
{
	int j = 0;
	for ( j = 2; j <= sqrt(n); j++ )
	{
		if (n % j == 0)
		{
			return 0; // 能被整除说明是素数，返回0
		}
	}
	return 1; // 整个循环中都没有被整除，说明是素数 
}

int main()
{
	int i = 0;
	int count = 0;
	for ( i = 100; i <= 200; i++)
	{
		// 判断i是否为素数
		if (is_prime(i) == 1) // 调用函数返回值是1，说明是素数
		{
			printf("%d\n", i);
			count++;
		}
	}

	printf("\ncount = %d\n", count);
	return 0;
}