#define _CRT_SECURE_NO_WARNINGS 1

////strlen()函数：
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	if (strlen("abc") - strlen("abcdef") > 0)
//		//strlen("abc") 长度为3，strlen("abcdef") 长度为6
//		// 3 - 6 == -3 < 0 ,
//	{
//		printf("大于\n");
//	}
//	else
//		//本来应该进入这个位置，但其实是进入上面
//	{
//		printf("小于等于\n");
//	}
//
//	return 0;
//}

//模拟strlen函数：


//#include <stdio.h>
////方法1：计数器方式
//size_t my_strlen(const char* str)
//	//和库函数的strlen函数一样
//	//返回值为无符号整型，参数为常量字符指针
//{
//	int count = 0; //计数器
//	while (*str != '\0')
//		//还没到结束符就继续统计
//	{
//		count++;//没到结束符就统计+1
//		str++;//判断下一位
//	}
//	return count;//返回计数器
//}

//#include <stdio.h>
////方法2：指针方式
//size_t my_strlen(const char* str)
////和库函数的strlen函数一样
////返回值为无符号整型，参数为常量字符指针
//{
//	char* p = str;
//	while (*p != '\0')
//		//还没到结束符就继续统计
//	{
//		p++;//没指向结束符就判断下一位
//	}
//
//	return p-str;//用移动后的指针 - 原来的指针 == 字符串长度
//}

//#include <stdio.h>
////方法3：迭代方式
//size_t my_strlen(const char* str)
////和库函数的strlen函数一样
////返回值为无符号整型，参数为常量字符指针
//{
//	if (*str == '\0')
//	{
//		return 0;//如果第一个就指向\0，说明长度是0
//	}
//	else
//	{
//		return 1 + my_strlen(str + 1);
//	}
//}
//
//int main()
//{
//	size_t sz = my_strlen("abc");
//	//使用模拟实现的strlen函数返回一个size_t（无符号整数）的数
//
//	//进行打印：
//	printf("%u\n", sz);
//	//%u：打印无符号的数
//
//	return 0;
//}


////strcpy函数：
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	//目标空间（字符数组）：
//	char arr1[20] = "xxxxxxxxxxxxxxxx";
//
//	//源字符串：
//	char arr2[] = "hello world";
//
//	strcpy(arr1, arr2);
//	//arr1为目标空间指针，arr2为源字符串指针
//	//将arr2指向的内容 拷贝到 目标空间指针中
//
//	//打印拷贝结果：
//	printf("%s\n", arr1);
//
//	return 0;
// }


////模拟strcpy函数：
//#include <stdio.h>
//#include <assert.h>
//char* my_strcpy(char* dest, const char* src)
////返回值类型 和 参数 与库函数strcpy相同
////返回值设置为char*，是为了能够使用链式访问，把返回值作为其它函数的参数
//{
//	//保存目标空间指针原位置，方便最后进行返回
//	char* ret = dest;
//
//	//进行断言，两个指针都不为空指针，需要头文件<assert.h>
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	//只要 源字符串 还没指向 \0 就继续循环拷贝
//	while (*src != '\0')
//	{
//		*dest = *src; //将源字符串的一位赋给目标空间
//		//下次赋值下一位，所以要移动两个指针：
//		dest++;
//		src++;
//	}
//
//	//循环中没有将 \0 赋给目标空间，所以要加上：
//	*dest = *src;
//	//循环完后，dest移向了新位置，src在\0位置，所以直接赋值即可
//
//	//返回拷贝后的目标指针原地址：
//	return ret;
//}

////模拟strcpy函数：
//#include <stdio.h>
//#include <assert.h>
//char* my_strcpy(char* dest, const char* src)
////返回值类型 和 参数 与库函数strcpy相同
////返回值设置为char*，是为了能够使用链式访问，把返回值作为其它函数的参数
//{
//	//保存目标空间指针原位置，方便最后进行返回
//	char* ret = dest;
//
//	//进行断言，两个指针都不为空指针，需要头文件<assert.h>
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	//只要 源字符串 还没指向 \0 就继续循环拷贝
//	while (*dest++ = *src++)
//	{
//		;
//	}
//
//	//返回拷贝后的目标指针原地址：
//	return ret;
//}
//
//int main()
//{
//	//目标空间（字符数组）：
//	char arr1[20] = "xxxxxxxxxxxxxxxx";
//	//源字符串：
//	char arr2[] = "hello world";
//
//	my_strcpy(arr1, arr2); //使用模拟的函数来拷贝
//	//arr1为目标空间指针，arr2为源字符串指针
//	//将arr2指向的内容 拷贝到 目标空间指针中
//
//	//打印拷贝结果：
//	printf("%s\n", arr1);
//
//	return 0;
//}


////strcat函数：
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	//目标空间（数组）：
//	char arr1[20] = "hello";
//
//	//源字符串：
//	char arr2[] = " world";
//
//	//使用strcat函数，将arr2连接到arr1后
//	strcat(arr1, arr1);
//
//	//打印连接后的新字符串:
//	printf("%s\n", arr1);
//
//	return 0;
//}


////模拟strcat函数：
//#include <stdio.h>
//#include <string.h>
//#include <assert.h>
//
//char* my_strcat(char* dest, const char* src)
//{
//	//进行断言，两个字符串都不为\0 (空)
//	assert(dest);
//	assert(src);
//
//	//保存目标地址的原位置
//	char* ret = dest;
//
//	//找到目标字符串的\0,作为连接的起点：
//	while (*dest)
//	{
//		dest++;
//	}
//
//	//开始连接字符串，和strcpy是一样的
//	while (*dest++ = *src++)
//	{
//		;
//	}
//
//	//返回连接后的目标空间指针：
//	return ret;
//}
//
//int main()
//{
//	//目标空间（数组）：
//	char arr1[20] = "hello";
//
//	//源字符串：
//	char arr2[] = " world";
//
//	//使用模拟的自定义函数，将arr2连接到arr1后
//	my_strcat(arr1, arr2);
//
//	//打印连接后的新字符串:
//	printf("%s\n", arr1);
//
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int ret = strcmp("abq", "abc");
//
//	printf("%d\n", ret);
//
//	return 0;
//}


////模拟strcmp函数：
//#include <stdio.h>
//int my_strcmp(const char* str1, const char* str2)
//{
//	while (*str1 == *str2)
//		//两字符串同位置上的值相同的情况
//	{
//		if (*str1 == '\0')
//			//同位置上都是\0说明两个字符串相同
//		{
//			return 0; //相同则返回 0
//		}
//		//不是\0，是其它值相同，则判断下一位
//		str1++;
//		str2++;
//	}
//
//	if (*str1 > *str2)
//		//当前位置，字符串1的字符大于字符串的字符
//	{
//		return 1; //大于则返回大于0的数
//	}
//	else
//		//当前位置，字符串1的字符小于字符串的字符
//	{
//		return -1; //小于则返回小于0的数
//	}
//}
//
//int main()
//{
//	//使用自定义函数进行比较：
//	int ret = my_strcmp("abq", "abc");
//
//	printf("%d\n", ret);
//
//	return 0;
//}


////strncpy()函数：
//#include <stdio.h>
//int main()
//{
//	char arr1[20] = "abcdef";
//	char arr2[] = "xxx";
//
//	strncpy(arr1, arr2, 5);
//	//从 arr2 中拷贝5个字符到 arr1 中
//
//	printf("%s\n", arr1);
//
//	return 0;
//}

////strncat()函数：
//#include <stdio.h>
//int main()
//{
//	char arr1[20] = "abcdef\0yyyyyyyyy";
//	char arr2[] = "xxx";
//
//	strncat(arr1, arr2, 6);
//	//源字符串的长度小于num，
//	//也只追加 源字符串 的字符串，
//	//不会像strncpy一样补0
//
//	printf("%s\n", arr1);
//
//	return 0;
//}



////strncmp
//#include <stdio.h>
//int main()
//{
//	char arr1[] = "abcqwer";
//	char arr2[] = "abcd";
//
//	int cmp = strncmp(arr1, arr2, 3);
//
//	printf("%d\n", cmp);
//
//	return 0;
//}

////strncmp:
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr1[] = "abcdefabcdef";
//	char arr2[] = "def";
//
//	char* ret = strstr(arr1, arr2);
//
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("未找到");
//	}
//
//	return 0;
//}


////模拟strncmp函数:
//#include <stdio.h>
//#include <string.h>
//char* my_strstr(char* str1, char* str2)
//{
//	char* cp = str1; //开始进行判断的初始位置指针
//	char* s1 = cp; //在arr1中的cp位置开始逐位进行匹配的指针
//	char* s2 = str2; //在arr2中逐位进行匹配的指针
//
//	如果要找的子字符串为空指针，则返回str1：
//	if (*str2 == '\0')
//	{
//		return str1;
//	}
//
//	while (*cp != '\0')
//		 \0之后不可能找到arr2的内容
//	{
//		开始匹配：
//		s1 = cp; //让s1在初始位置开始进行逐位判断
//		s2 = str2;
//
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//			两字符串都未到\0，如果当前位置的内容相同，则再循环判断下一位
//		{
//			s1++;
//			s2++;
//		}
//
//		if (*s2 == '\0')
//			如果子字符串已经到了\0，
//			说明arr1中有arr2，匹配成功
//		{
//			return cp; 
//			匹配成功，则返回子字符串的初始位置
//		}
//
//		cp++; //判断arr1下一个初始位置
//	}
//
//	return NULL; //未找到则返回空指针
//}
//
//int main()
//{
//	char arr1[] = "abbbcdef";
//	char arr2[] = "bbc";
//
//	使用自定义函数进行查找：
//	char* ret = my_strstr(arr1, arr2);
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("未找到");
//	}
//
//	return 0;
//}


//strtok函数：
//#include <stdio.h>
//int main()
//{
//	//被分割的字符串：
//	char arr[] = "ggdpz@csdn.com";
//
//	//因为strtok会改变字符串，所以要用被分割的字符串的拷贝进行操作：
//	char copy[30];
//	strcpy(copy, arr); //进行拷贝
//
//	//定义分隔符：
//	char sep[] = "@."; //分隔符为 @ 和 .
//
//	//接收函数返回值
//	char* ret = NULL;
//
//	//使用for循环取出被分割的字符串：
//	for (ret = strtok(copy, sep); ret != NULL; ret = strtok(NULL, sep))
//		// ret = strtok(copy, sep) -- 循环初始值，第一次调用函数后返回被分割后的首地址，
//		//							  将分隔符变为\0,下次再调用函数从\0后再分割查找
//		// ret != NULL -- 函数不是返回空指针就继续分割查找
//		// ret = strtok(NULL, sep) -- 之后再调用函数，参数为空指针
//	{
//		printf("%s\n", ret);
//	}
//
//	return 0;
//}



//strerror函数：
#include <stdio.h>
int main()
{
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d: %s\n", i, strerror(i));
	}
	  
	return 0;
}