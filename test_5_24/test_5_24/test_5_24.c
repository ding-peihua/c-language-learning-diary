﻿#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int cnt = 0;
//int fib(int n)
//{
//	cnt++;
//	if (n == 0)
//	{
//		return 1;
//	}
//	else if (n == 1)
//	{
//		return 2;
//	}
//	else
//	{
//		return fib(n - 1) + fib(n - 2);
//	}
//}
//void main()
//{
//	fib(8);
//	printf("%d", cnt);
//}

//在上下文和头文件均正常的情况下，以下程序的输出结果是（）
//#include <stdio.h>
//int main()
//{
//	int x = 1;
//	do {
//
//		printf("%d\n", x++);
//
//	} while (x--);
//
//	return 0;
//}

////求输入两个整数的最小公倍数
//#include <stdio.h>
////思路一：
////最小公倍数 肯定会 大于等于 较大值
////所以可以试试用 较大值 同时 整除两个数 ，能整除 最小公倍数就是 较大值
////不能整除就把 较大值++ ，直到能整除两个数
//
//int main()
//{
//	//输入两个值
//	long long a = 0;
//	long long b = 0;
//
//	scanf("%lld %lld", &a, &b);
//
//	//假设 a 和 b 的较大值是最小公倍数
//	//使用三目表达式，把 较大值 取出 
//	long long m = a > b ? a : b;
//
//	//
//	while (1)//死循环
//	{
//		if (m % a == 0 && m % b == 0)
//		{
//			break;
//			//如果较大值能同时整除两个数，说明是两值的最小公倍数，跳出循环
//		}
//		//不能整除则 较大值++，直到能同时整除两个值跳出循环
//		m++; //效率较低
//	}
//
//	printf("%lld\n", m);
//	return 0;
//}

////求输入两个整数的最小公倍数
//#include <stdio.h>
////思路二：
////假设有 a 和 b ，两值的最小公倍数是 k ，i = 1
////让        a * i % b        看能不能整除掉（结果等于0）
////不能则 i++，
////直到能整除为止，此时最小公倍数 k = a * i
//
//int main()
//{
//	//输入两个值
//	long long a = 0;
//	long long b = 0;
//
//	scanf("%lld %lld", &a, &b);
//
//	//求最小公倍数
//	int i = 1;
//	while (a*i % b != 0)//这里 !=0 可以省略
//	// a*i 不能整除 b 则改变 i ，直到可以整除退出循环 
//	{
//		i++;
//	}
//
//	//直到能整除为止，此时最小公倍数 k = a * i
//	printf("%lld\n", a*i);
//	return 0;
//}

//将一句话的单词进行倒置，标点不倒置。
//比如 I like beijing. 经过函数后变为：beijing. like I
//每个测试输入包含1个测试用例： I like beijing. 输入用例长度不超过100
#include <stdio.h>
#include <string.h>

//逆序 整个字符串 或 单词
void reverse(char* left, char* right)
{
	while (left < right)
	//左小于右，中间还有值且没有同时指向一个字符，继续逆序
	{
		char tmp = *left;
		*left = *right;
		*right = tmp;
		//调整指针
		left++;
		right--;
	}
}

int main()
{
	char arr[101];//数组长度不超过100，那就设置为101
	//之后要重新赋值数组的话，这里就不初始化了？

	//这里不能使用 scanf()函数 获取字符串，
	//因为当读到单词间的空格时，scanf()函数 就不再往后读了

	//这里可以使用 gets()函数 ，
	//读取一个 字符串 ，即使中间有空格
	gets(arr); //把读取到的字符串 放进 字符串数组arr中
	//但系统可能会觉得不安全，报警告

	//也可以写成：scanf("%[^\n]s",arr)

	//可以是 fgets()函数 再进行替代
	//fgets(arr, 100, stdin);
	//函数参数：
	//arr：把读取的值放进字符数组arr中
	//100：获取字符串最大字符数，这里设置为100
	//stdin：输入方式，stdin 为 键盘输入

	//求字符串长度：
	int len = strlen(arr);//strlen 不会把 \0 计算进去，找到 \0 就停了

	//逆序整个字符串：
	reverse(arr, arr + len - 1); //自定义一个逆序函数方法
	//方法参数：数组首地址（左指针） 、 最后一个字符的地址（右指针）
	//arr+len-1 ：首地址 + 数组长度 - 1 ---》 最后一个字符的地址

	//逆序每个单词：
	
	char* start = arr;//单词起始位置
	char* cur = arr;//单词尾部位置

	//逆序每个单词：
	while (*cur != '\0')//整个字符串还没结束，继续找单词
	{
		//逆序一个单词：

		while (*cur != ' ' && *cur != '\0')
		//单词尾部未找到空格且还没到结束符就继续找下一位
		{
			cur++;
		}//直到找到单词

		//找到单词后进行单词逆序
		reverse(start, cur - 1);
		//因为单词尾部，cur 此时是 空格位置，所以要在空格（或\0）位置前-1

		//找下一个单词：
		start = cur + 1;
		//cur+1，空格后的下一个位置就是下一个单词的首地址
		if (*cur == ' ') 
		//只有是空格时才能跳过，\0不能再跳过了，不然就跳不出循环了
		{
			cur++; //尾部跳过空格
		}

	}

	printf("%s", arr);

	return 0;
}