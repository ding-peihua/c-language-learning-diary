#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int main() 
{
	//设置数组：
	int arr[] = { 1,2,3,4,5,6,7,8,9 };

	//计算数组元素个数：
	int sz = sizeof(arr) / sizeof(arr[0]);

	//设置左右下标：
	int left = 0; //左下标，从左往右找 偶数
	int right = sz - 1; //右下标，从右往左找 奇数

	//进行调换，奇数放前，偶数放后：
	while (left < right)
	//当数组还有元素，进行判断调换
	{
		//从左往右找偶数：
		while ((left < right) && arr[left] % 2 == 1)
		{
			//找到奇数就跳过，left++，但不跳过right，防止越界
			left++;
		}

		//从右往左找奇数：
		while ((left < right) && arr[right] % 2 == 0)
		{
			//找到偶数就跳过，right--，但不跳过left，防止越界
			right--;
		}

		//此时left找到了偶数，right找到了奇数，开始进行调换：
		if (left < right)
		{
			//进行调换
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
			//调换后跳过这对值
			left++;
			right--;
		}
	}

	//调换后进行数组打印：
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}

	return 0;
}