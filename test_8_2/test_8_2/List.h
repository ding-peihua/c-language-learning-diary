#pragma once

//双向链表头文件：

//包含之后需要用到的头文件：
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

//定义双向链表数据域数据类型：
typedef int LTDataType;

//创建双向链表结点类型：
typedef struct ListNode
{
	//数据域：
	LTDataType data;

	//双向链表指针域：
	 //后继指针--指向后一个结点：
	struct ListNode* next;
	 //前驱指针--指向前一个结点：
	struct ListNode* prev;

}LTNode; //类型简称LTNode



//函数声明：

//创建链表结点--创建双向循环链表结点
//接收要插入创建结点数据域的数据
//返回创建结点的地址
LTNode* BuyLTNode(LTDataType x);

//双向链表初始化--带头双向循环链表初始化函数
//返回初始化结点的地址
LTNode* LTInit();

//打印链表--打印双向链表各结点数据域数据
//接收链表头指针（phead）
LTNode* LTPrint(LTNode* phead);

//双向链表尾插函数--向链表尾部插入一个结点（尾插）:
//接收链表头指针（phead）、要尾插进链表的值（x）
void LTPushBack(LTNode* phead, LTDataType x);

//双向链表尾删函数--删除链表尾部结点（尾删）
//接收链表头指针（phead）
void LTPopBack(LTNode* phead);

//双向链表头插函数--向链表头部插入一个结点（头插）:
//接收链表头指针（phead）、要头插进链表的值（x）
void LTPushFront(LTNode* phead, LTDataType x);

//双向链表头删函数--删除链表头部结点（头删）
//接收链表头指针（phead）
void LTPopFront(LTNode* phead);

//求链表结点个数函数--求链表有效结点个数（求链表长度）
//接收链表头指针（phead）
int LTSize(LTNode* phead);

//双向链表查找函数--在双向链表中查找数据域数据为x的结点地址
//接收链表头指针（phead）、要在链表中查找的值（x）
LTNode* LTFind(LTNode* phead, LTDataType x);

//双向链表插入函数--在pos结点之前插入数据域数据为x的结点
//接收插入位置(pos)、要插入链表的值（x）
void LTInsert(LTNode* pos, LTDataType x);

//双向链表删除函数--删除pos结点
//接收要删除结点地址（pos）
void LTErase(LTNode* pos);

//双向链表销毁函数--销毁链表
//接收要销毁链表头系欸但（phead）
void LTDestroy(LTNode* phead);