#define _CRT_SECURE_NO_WARNINGS 1

//力扣：138.随机链表的复制
/**
 * Definition for a Node.
 * struct Node {
 *     int val;
 *     struct Node *next;
 *     struct Node *random;
 * };
 */

struct Node* copyRandomList(struct Node* head) {
    //要求：时间复杂度为O(N)  空间复杂度为O(1)

        //暴力求解：
        //1.复制链表
        //2.算random在原链表的相对位置，再控制复制链表的random（O(N^2)）

        //巧妙求解：
        //1.将每个拷贝结点(copy)都插入到原结点(cur)的后面（都在同一链表上）

        //2.通过cur->random->next得到copy->random
        // 这里cur->random获得原结点的random指向的结点，
        //而cur->random指向结点的next结点后面就是第一步插入的该结点的复制结点，
        //那么就可以把这个复制结点给到拷贝结点(copy)的random
        //这样找到原结点(cur)的random结点就可以找到拷贝结点(copy)的random结点

        //3.最后将拷贝结点独立出来，尾插到一起成为一个新链表
        //再恢复原链表

        //创建用于遍历的cur指针：
    struct Node* cur = head;

    //1.将每个拷贝结点(copy)都插入到原结点(cur)的后面（都在同一链表上）
    //使用while循环遍历链表
    while (cur != NULL)
    {
        //遍历一次就复制一次cur结点
        //并插入到cur结点的后面

        //插入前先获得cur结点的后一个结点next：
        struct Node* next = cur->next;

        //再拷贝cur结点创建拷贝copy结点：
        struct Node* copy = (struct Node*)malloc(sizeof(struct Node));

        //将cur结点的数据域数据复制给copy结点：
        copy->val = cur->val;

        //完成copy结点的插入： 
        //原结点cur指向拷贝结点copy：
        cur->next = copy;
        //拷贝结点copy指向原本第二个结点next：
        copy->next = next;

        //完成一次拷贝结点copy的插入后，
        //调整指针再进行这套操作：
        cur = next;
    }

    //2.通过cur->random->next得到copy->random
    // 这里cur->random获得原结点的random指向的结点，
    //而cur->random指向结点的next结点后面就是第一步插入的该结点的复制结点，
    //那么就可以把这个复制结点给到拷贝结点(copy)的random
    //这样找到原结点(cur)的random结点就可以找到拷贝结点(copy)的random结点

    //复制完结点后，把cur指针重新移到head指针上：
    cur = head;

    while (cur != NULL)
    {
        //从头获得复制结点的的地址：
        struct Node* copy = cur->next; //cur->next就是第一个copy结点

        //开始设置拷贝结点copy的random指针：
        if (cur->random == NULL)
            //如果原结点cur的random指针指向NULL
        {
            //那copy的random指针也指向NULL：
            copy->random = NULL;
        }
        else
            //如果原结点cur的random指针不是指向NULL 
        {
            //拷贝结点的random 指向 原结点random指向结点的拷贝版结点：
            copy->random = cur->random->next;
        }

        //cur指向原链表的第二个结点：
        cur = copy->next;
    }

    //3.最后将拷贝结点独立出来，尾插到一起成为一个新链表
    //再恢复原链表

    //拷贝结点random设置好后，把cur指针重新移到head指针上：
    cur = head;

    //创建拷贝链表的头指针copyhead，
    //和在拷贝链表进行遍历的指针copytail：
    struct Node* copyhead = NULL;
    struct Node* copytail = NULL;

    //使用while循环把拷贝结点从原链表取出来组成拷贝链表：
    while (cur != NULL)
    {
        //创建copy指针指向原链表的拷贝结点：
        struct Node* copy = cur->next;

        //创建next指针指向原链表中原结点的下个原结点：
        struct Node* next = copy->next;

        //将copy结点尾插到新链表：
        if (copytail == NULL)
            //拷贝链表为空：
        {
            //将第一个拷贝结点存入：
            copyhead = copytail = copy;
        }
        else
            //拷贝链表不为空：
        {
            //copytail指向下个拷贝结点：
            copytail->next = copy;
            //指向完成后copytail移向下个拷贝结点：
            copytail = copytail->next;
        }

        //从原链表移了一个拷贝结点后，恢复原链表的连接：
        cur->next = next; //先指向下个结点
        cur = next; //再移向下个结点
    }

    //最后返回拷贝链表头指针：
    return copyhead;
}