#define _CRT_SECURE_NO_WARNINGS 1

//双向链表函数实现文件：

//包含双向链表头文件：
#include "List.h"

//函数实现：

//创建链表结点--创建双向循环链表结点
//接收要插入创建结点数据域的数据
LTNode* BuyLTNode(LTDataType x)
{
	//为创建结点开辟动态空间：
	LTNode* node = (LTNode*)malloc(sizeof(LTNode));
	//检查是否开辟失败：
	if (node == NULL)
		//返回NULL，开辟失败
	{
		//打印错误信息：
		perror("malloc fail");
		//终止程序：
		exit(-1);
	}

	//把x放入结点数据域：
	node->data = x;
	//设置双向链表指针域：
	node->next = NULL;
	node->prev = NULL;
	
	//开辟成功则返回开辟空间地址
	return node;
}



//链表初始化--带头双向循环链表初始化函数
//接收链表头指针（phead）
LTNode* LTInit()
{
	//初始化时先使用BuyLTNode函数创建哨兵位：
	LTNode* phead = BuyLTNode(-1); //返回哨兵位指针

	//因为要实现循环，
	//所以让哨兵位后继指针next和前驱指针prev都指向自己：
	phead->next = phead;
	phead->prev = phead;
	//这样即使链表为空，它也是有头有尾的，即哨兵位phead

	//初始化后返回链表哨兵位：
	return phead;
}



//打印链表--打印双向链表各结点数据域数据
//接收链表头指针（phead）
LTNode* LTPrint(LTNode* phead)
{
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);

	//创建结点指针cur进行遍历：
	//指针cur应该从哨兵位（头指针）下一个结点开始
	LTNode* cur = phead->next;
	
	printf("phead <==> ");
	//因为是循环链表，不是以NULL空指针结尾
	//所以应该是当指针cur遍历回到哨兵位就终止遍历：
	while (cur != phead)
	//如果只用哨兵位，链表为空，
	//phead->next还是phead，不会进行打印
	{
		//打印数据域内容：
		printf("%d <=> ", cur->data);

		//打印完当前结点数据域数据后cur移向下个结点：
		cur = cur->next;
	}

	//打印完一个链表后换行：
	printf("\n");
}



//向链表尾部插入一个结点（尾插）:
//接收结点头指针（phead）、要尾插进链表的值（x）
void LTPushBack(LTNode* phead, LTDataType x)
{
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);

	//通过哨兵位找到尾结点：
	//因为是循环链表：
	//所以哨兵位（带头链表）的前一个结点就是尾结点
	LTNode* tail = phead->prev; 
	//调用前驱指针获得尾结点

	//调用BuyLTNode函数为尾插创建尾插结点newnode：
	LTNode* newnode = BuyLTNode(x);

	//尾插结点前驱指针prev指向原本的尾结点：
	newnode->prev = tail;
	//原本尾结点后继指针next指向尾插结点：
	tail->next = newnode;

	//尾插结点后继指针next指向头结点：
	newnode->next = phead;
	//头结点前驱指针指向尾插结点：
	phead->prev = newnode;

	//带头+双向+循环 链表:
	//对比单链表，因为有哨兵位不用考虑链表为空的情况，
	//且不需要二级指针，通过操纵哨兵位这个结构体，
	//替换用二级指针操作头指针的操作


	/*
	//第二种方法：复用LTInsert函数
	//在哨兵位前插入一个值x就是尾插了：
	LTInsert(phead, x);
	*/
}



//双向链表尾删函数--删除链表尾部结点（尾删）
//接收链表头指针（phead）
void LTPopBack(LTNode* phead)
{
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);
	
	//assert断言双向链表不是空链表：
	assert(phead->next != phead);
	//如果哨兵位的下一个结点还是哨兵位说明是空链表

	//通过哨兵位的前驱指针prev获得尾结点tail：
	LTNode* tail = phead->prev;

	//再通过尾结点tail获得倒数第二个结点tailPrev：
	LTNode* tailPrev = tail->prev;

	//释放（删除）尾结点tail：
	free(tail);

	//这时就让倒数第二个结点tailPrev成为新的尾结点
	//为保持循环，把tailPrev和哨兵位连接起来：
	tailPrev->next = phead; //tailPrev后继指针指向哨兵位
	phead->prev = tailPrev; //哨兵位前驱指针指向tailPrev

	//带头+双向+循环 链表:
	//对比单链表，这里双向链表在尾删时因为有哨兵位的存在
	//即使链表只剩一个结点，也不用进行判断单独处理进行置空
	//这一个结点删掉后，还有哨兵位存在

	/*
	//第二种方法：复用LTErase函数
	//传尾结点地址给LTErase函数即可：
	LTErase(phead->prev);
	*/
}



//双向链表头插函数--向链表头部插入一个结点（头插）：
//接收链表头指针（phead）、要头插进链表的值（x）
void LTPushFront(LTNode* phead, LTDataType x)
{
	//第二种方法：多定义一个指针
	
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);

	//调用BuyLTNode函数为头插创建头插结点newnode：
	LTNode* newnode = BuyLTNode(x);

	//创建一个first指针保存原本第一个结点地址：
	LTNode* first = phead->next;

	//哨兵位后继指针next指向头插结点newnode：
	phead->next = newnode;

	//头插结点newnode前驱指针prev指向哨兵位：
	newnode->prev = phead;

	//头插结点newnode后继指针next指向原本头结点first:
	newnode->next = first;

	//原本头结点first前驱指针prev指向头插结点newnode：
	first->prev = newnode;


	/*
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);
	
	//第一种方法：需要注意连接的顺序
	
	//调用BuyLTNode函数为头插创建头插结点newnode：
	LTNode* newnode = BuyLTNode(x);
	
	//先将头插结点的后继节点next连接上原本头结点：
	newnode->next = phead->next;
	//哨兵位的后继指针指向的就是头结点
	
	//再将原本头结点的前驱指针prev指向头插结点newnode：
	phead->next->prev = newnode;
	
	//哨兵位连接上头插节点newnode：
	phead->next = newnode;
	
	//头插结点newnode的前驱指针指向哨兵位：
	newnode->prev = phead;
	*/


	/*
	//第三种方法：复用LTInsert函数
	//在哨兵位后一个结点前插入一个值x就是头插了：
	LTInsert(phead->next, x);
	*/
}



//双向链表头删函数--删除链表头部结点（头删）
//接收链表头指针（phead）
void LTPopFront(LTNode* phead)
{
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);

	//assert断言双向链表不是空链表：
	assert(phead->next != phead);
	//如果哨兵位的下一个结点还是哨兵位说明是空链表

	//通过哨兵位后继结点next获得头结点地址：
	LTNode* first = phead->next;

	//再通过first结点获得第二个结点：
	LTNode* second = first->next;

	//释放头结点first：
	free(first);

	//让哨兵位后继结点next指向第二个结点second：
	phead->next = second;

	//第二个结点的前驱指针prev指向哨兵位：
	second->prev = phead;

	//带头+双向+循环 链表:
	//对比单链表，这里双向链表在头删时因为有哨兵位的存在
	//即使链表只剩一个结点，也不用进行判断单独处理进行置空
	//这一个结点删掉后，还有哨兵位存在
	
	/*
	//第二种方法：复用LTErase函数
	//传第一个结点地址给LTErase函数即可：
	LTErase(phead->next);
	*/
}



//求链表结点个数函数
//接收链表头指针（phead）
int LTSize(LTNode* phead)
{
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);

	int size = 0; //存放链表长度

	//创建结点指针cur进行遍历：
	//指针cur应该从哨兵位（头指针）下一个结点开始
	LTNode* cur = phead->next;

	//因为是循环链表，不是以NULL空指针结尾
	//所以应该是当指针cur遍历回到哨兵位就终止遍历：
	while (cur != phead)
		//如果只有哨兵位，链表为空，
		//phead->next还是phead，不会进行打印
	{
		++size; //遍历一遍长度+1

		//cur移向下个结点：
		cur = cur->next;
	}

	//返回链表长度：
	return size;
}



//双向链表查找函数--在双向链表中查找数据域数据为x的结点地址
//接收链表头指针（phead）、要在链表中查找的值（x）
LTNode* LTFind(LTNode* phead, LTDataType x)
{
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);

	//创建遍历指针cur，从第一个结点开始：
	LTNode* cur = phead->next;

	//使用while循环进行遍历：
	while (cur != phead)
		//如果只有哨兵位，链表为空，
		//phead->next还是phead，不会进行打印
	{
		if (cur->data == x)
			//找到要找的值了：
		{
			return cur; //返回该值结点
		}

		//调整指针：
		cur = cur->next;
	}

	//未找到则返回空指针：
	return NULL;
}



//双向链表插入函数--在pos结点之前插入数据域数据为x的结点
//接收插入位置(pos)、要插入链表的值（x）
void LTInsert(LTNode* pos, LTDataType x)
{
	//assert断言插入位置pos不为空：
	assert(pos != NULL);

	//通过pos结点获得前一个结点posPrev地址：
	LTNode* posPrev = pos->prev;

	//使用BuyLTNode函数为插入结点开辟空间：
	LTNode* newnode = BuyLTNode(x);

	//posPrev结点的后继指针next指向newnode：
	posPrev->next = newnode;

	//newnode前驱指针prev指向posPrev:
	newnode->prev = posPrev;

	//newnode后继指针next指向pos：
	newnode->next = pos;

	//pos结点前驱指针prev指向newnode:
	pos->prev = newnode;
}



//双向链表删除函数--删除pos结点
//接收要删除结点地址（pos）
void LTErase(LTNode* pos)
{
	//assert断言删除位置结点pos不为空：
	assert(pos != NULL);

	//保存要删除结点pos的前一个结点posPrev地址：
	LTNode* posPrev = pos->prev;

	//保存要删除结点pos的后一个结点posNext地址：
	LTNode* posNext = pos->next;

	//释放掉pos结点：
	free(pos);

	//将pos前结点posPrev的后继指针指向posNext：
	posPrev->next = posNext;

	//将pos后结点posNext的前驱指针指向posPrev：
	posNext->prev = posPrev;
}



//双向链表销毁函数--销毁链表
//接收要销毁链表头系欸但（phead）
void LTDestroy(LTNode* phead)
{
	//assert断言头指针（哨兵位地址）不为空:
	assert(phead != NULL);

	//创建遍历指针cur，从第一个结点开始：
	LTNode* cur = phead->next;

	//使用while循环进行遍历释放：
	while (cur != phead)
	{
		//释放前先存储下一个结点地址：
		LTNode* next = cur->next;

		//释放当前结点：
		free(cur);

		//调整指针：
		cur = next;
	}
	
	//删除完有效结点后，最后再释放哨兵位：
	free(phead);
}