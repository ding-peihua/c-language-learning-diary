#define _CRT_SECURE_NO_WARNINGS 1

//双向链表函数测试文件：

//包含双向链表头文件：
#include "List.h"

//测试函数--
//LTInit、LTPushBack、LTPrintf函数
void TestList1()
{
	//初始化一个双向链表：
	LTNode* plist = LTInit();

	//初始化后使用尾插函数插入数据：
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	LTPushBack(plist, 4);
	LTPushBack(plist, 5);
	  
	//打印当前双向链表：
	LTPrint(plist);
}


//测试函数--LTPopBack函数
void TestList2()
{
	//初始化一个双向链表：
	LTNode* plist = LTInit();
	//初始化后使用尾插函数插入数据：
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	//打印当前双向链表：
	LTPrint(plist);

	//进行尾删：
	LTPopBack(plist);
	//打印当前双向链表：
	LTPrint(plist);

	//进行尾删：
	LTPopBack(plist);
	//打印当前双向链表：
	LTPrint(plist);

	//进行尾删：
	LTPopBack(plist);
	//打印当前双向链表：
	LTPrint(plist);
}


//测试函数--LTPushFront函数
void TestList3()
{
	//初始化一个双向链表：
	LTNode* plist = LTInit();
	//初始化后使用尾插函数插入数据：
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	//打印当前双向链表：
	LTPrint(plist);

	//进行头插：
	LTPushFront(plist, 1000);
	//打印当前双向链表：
	LTPrint(plist);
}


//测试函数--LTPopFront函数
void TestList4()
{
	//初始化一个双向链表：
	LTNode* plist = LTInit();
	//初始化后使用尾插函数插入数据：
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	//打印当前双向链表：
	LTPrint(plist);

	//进行头删：
	LTPopFront(plist);
	//打印当前双向链表：
	LTPrint(plist);
}


//测试函数--LTSize函数
void TestList5()
{
	//初始化一个双向链表：
	LTNode* plist = LTInit();
	//初始化后使用尾插函数插入数据：
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	//打印当前双向链表：
	LTPrint(plist);

	//计算链表长度：
	int size = LTSize(plist);
	//打印当前双向链表：
	printf("链表长度为：%d", size);
}


//测试函数--LTFind函数
void TestList6()
{
	//初始化一个双向链表：
	LTNode* plist = LTInit();
	//初始化后使用尾插函数插入数据：
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	//打印当前双向链表：
	LTPrint(plist);

	//使用查找函数：
	LTNode* find = LTFind(plist, 2);
	//打印找到的地址
	printf("0x%xn", find);
}


//测试函数--LTInsert函数
void TestList7()
{
	//初始化一个双向链表：
	LTNode* plist = LTInit();
	//初始化后使用尾插函数插入数据：
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	//打印当前双向链表：
	LTPrint(plist);

	//使用插入函数：
	LTInsert(plist->next->next, 100);
	//打印当前双向链表：
	LTPrint(plist);
}


//测试函数--LTErase函数
void TestList8()
{
	//初始化一个双向链表：
	LTNode* plist = LTInit();
	//初始化后使用尾插函数插入数据：
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	//打印当前双向链表：
	LTPrint(plist);

	//使用删除函数：
	LTErase(plist->next->next);
	//打印当前双向链表：
	LTPrint(plist);
}

//主函数：
int main()
{
	//调用测试函数：
	//TestList1();
	//TestList2();
	//TestList3();
	//TestList4();
	//TestList5();
	//TestList6();
	//TestList7();
	TestList8();

	return 0;
}

