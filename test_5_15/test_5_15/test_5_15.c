#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//	int a = 100; //在内存中开辟一块空间
//
//	int* pa = &a; //取出变量a的地址，使用&（取地址操作符）
//	//a变量占用4个字节的空间，这里是将a的4个字节的第一个字节的地址
//	//放在pa变量中，pa就是一个指针变量
//	// pa 是专门用来存放地址（指针）的，
//	// int * 可以分开理解
//	// * 告诉我们 pa 是 指针
//	// int 告诉我们 pa 指向的 a 是 int类型
//
//	printf("%p", pa);
//
//	return 0;
//}

//指针类型的意义：int*
//#include <stdio.h>
//int main()
//{
//	int a = 0x11223344; //0x开头的是16进制数字
//	int* pa = &a;
//	*pa = 0;
//
//	return 0;
//}

//指针类型的意义：char*
//#include <stdio.h>
//int main()
//{
//	int a = 0x11223344; //0x开头的是16进制数字
//	char* pa = &a;
//	*pa = 0;
//
//	return 0;
//}

////指针类型的意义2：
//#include <stdio.h>
//int main()
//{
//	int a = 0x11223344; //0x开头的是16进制数字
//
//	int* pa = &a;
//	char* pc = &a;
//
//	printf("%p\n", pa);
//	printf("%p\n", pc);
//
//	printf("%p\n", pa+1);
//	printf("%p\n", pc+1);
//
//	return 0;
//}

//指针未初始化
//#include <stdio.h>
//int main()
//{
//	int* p; //指针变量为初始化，默认为随机值
//
//	*p = 20;
//	printf("%p", *p);
//
//	return 0;
//}


//指针越界访问
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr; //相当于&arr[0]
//
//	int i = 0;
//	for (i = 0; i <= 11; i++)
//	{
//		//当这种指向的范围超出数字arr的范围时，p就是野指针
//		*(p++) = i; //先使用p，再++，移向数组的下一位
//	}
//
//	return 0;
//}

//指针指向的空间释放
//#include <stdio.h>
//
//int* test()
//{
//	int a = 110;
//	return &a;
//}
//
//int main()
//{
//	int *p = test();
//	printf("%d\n", *p);
//
//	return 0;
//}

//避免野指针：
//#include <stdio.h>
//int main()
//{
//	int a = 10;
//	int* p = &a;
//
//	int* ptr = NULL;  //本质就是0，ptr时一个空指针，没有指向任何
//	//有效的空间，这个指针不能直接使用
//
//	if (ptr != NULL)
//	{
//		//不为空指针，再使用
//	}
//
//	return 0;
//}

//指针运算
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	//不使用下标访问数组
//	int* p = &arr[0]; //相当于arr
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]); //数组长度
//
//	//使用指针运算循环赋值：
//	//第一种方法：
//	for (i = 0; i < sz; i++)
//	{
//		*p = i;
//		p++; //指针运算
//		// p = p + 1; 指针加一，移到数组中的下一位
//	}
//	
//	//第二种方法：
//	//for (i = 0; i < sz; i++)
//	//{
//	//	*(p + i) = i; 
//	//}
//
//	//使用指针打印
//	p = arr; //地址位置还原到数组第一个元素位置进行打印
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//
//	return 0;
//}

//补充：
//	int arr[10] = { 0 };
// 
//	int* p = arr;
// 
//	*(p + i) == arr[i]; //p存放首元素地址
// 
//	*(arr + i) == arr[i]; //编译器计算arr[i]时，就是按照 *(arr+i) 计算的
// 
//	arr[i] == *(arr + i) == *(i + arr) == i[arr]
// 
//	arr[i] --> i[arr] // 2+3 --> 3+2 
// 
//	加号只是一个操作符而已，[]也只是一个操作符，操作数顺序可以调换


//地址-地址（指针-指针）
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%d\n", &arr[9] - &arr[0]);
//	printf("%d\n", &arr[0] - &arr[9]);
//
//	// 指针-指针 得到的数值的绝对值：是指针和指针之间的元素个数
//	
//	// 指针-指针 运算的前提条件：指针和指针指向了同一块空间（同一数组）
//
//	return 0;
//}

//应用：
//#include <stdio.h>

//第一种版本：正常思路
//int my_strlen(char* s)
//{
//	int count = 0; //统计字符串个数
//	if (*s != '\0')//数组首元素不是结束标志
//	{
//		count++; //能进来说明有元素，个数++
//		s++; //判断下一位
//	}
//	return count;
//}

//第二种版本：递归
//int my_strlen(char* s)
//{
//	if (*s == '\0')
//	{
//		return 0;
//	}
//	else
//	{
//		return 1 + my_strlen(s + 1);
//	}
//}

//第三种版本：指针减指针求数组元素个数
//int my_strlen(char* s)
//{
//	char* start = s;
//	while (*s != '\0') //也可以写成 while(*s) ,\0的ASCII码值是0
//	{
//		s++;
//	} //循环到找到 \0 的地址
//	return s - start; //指针-指针：\0地址 - 首地址 = 元素个数
//}
//
//
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//
//	return 0;
//}

//指针的关系运算（比较大小）
//#include <stdio.h>
//
//#define N_VALUES 5 //定义宏：数组长度为5
//
//int main()
//{
//	float values[N_VALUES]; //浮点数数组
//	float* vp;
//
//	for (vp = &values[N_VALUES]; vp > &values[0];)
//	// vp 等于 数组第五个元素的地址；vp 大于 数组第0个元素的地址
//	{
//		*--vp = 0; // vp先 -- ，再解引用 vp，把第四个元素赋值为0，然后是第三个、第二个……
//	}
//	//实现倒着给数组赋值
//
//	int i = 0;
//	for (i = 0; i < N_VALUES; i++)
//	{
//		printf("%d ", *(vp + i));
//	}
//
//	return 0;
//}