#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//    int arr[] = { 1,2,3,4,5 };
//    short* p = (short*)arr;
//    int i = 0;
//    for (i = 0; i < 4; i++)
//    {
//        *(p + i) = 0;
//    }
//
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int a = 0x11223344;
//    char* pc = (char*)&a;
//    *pc = 0;
//    printf("%x\n", a);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//	unsigned long pulArray[] = { 6,7,8,9,10 };
//	unsigned long* pulPtr;
//	pulPtr = pulArray;
//	*(pulPtr + 3) += 3; //指针位置还是在首元素，9这个元素+=3
//	printf("%d,%d\n", *pulPtr, *( pulPtr + 3));
//	return 0;
//}
//
//


//写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//arr是一个整形一维数组。
//#include <stdio.h>
//
//void Print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p+i) );
//	}
//
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	int* p = arr;
//
//	//数组长度：
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	//自定义函数：
//	Print(p, sz); //使用指针
//	
//
//	return 0;
//}

//#include <stdio.h>
//
//void backPrint(char* pc, int sz)
//{
//    int i = 0;
//    for (i = sz - 1; i >= 0; i--)
//    {
//        if (*(pc + i) != 0)//数组为赋值元素存储的是0
//        {
//            printf("%c", *(pc + i));
//        }
//
//    }
//
//}
//
//int main() {
//    char input[10000] = { 0 };
//    char* pc = input;
//
//    //数组长度：
//    int sz = sizeof(input) / sizeof(input[0]);
//
//    //输入字符串：
//    int i = 0;
//    while ((input[i++] = getchar()) != '\n');
//    //i++：先使用i，再++，获取后不是回车，就继续循环
//    //使用getchar()函数输入字符串
//    input[i-1] = 0; //把回车改掉
//    //定义倒叙函数：
//    backPrint(pc, sz);
//
//    return 0;
//}


//打印菱形：
//#include <stdio.h>
//int main()
//{
//	char arr[13] = { '*','*','*','*','*','*','*','*','*','*','*','*','*' }; //13个
//
//	char* left = arr; //首指针
//	char* right = arr + sz - 1; //尾指针
//
//	//打印13行：
//		//从小到大: 7行
//	int i = 0;
//	for (i = 0; i < 7; i++)
//	{
//		int j = 0;
//		for ( j = 0; j < 7/2-i; j++)
//		{
//			*left = " ";
//			left++;
//			*right = " ";
//			right--;
//		}
//		printf("%c", arr);
//	}
//
//		//从大到小: 6行
//	int i = 0;
//	for (i = 0; i < 6; i++)
//	{
//		printf("%c", arr);
//		int j = 0;
//		for (j = 0; j < i; j++)
//		{
//			*left = " ";
//			left++;
//			*right = " ";
//			right--;
//		}
//	}
//
//
//	return 0;
//}

//打印菱形：
//#include <stdio.h>
//int main()
//{
//	printf("      *      \n");
//	printf("     ***     \n");
//	printf("    *****    \n");
//	printf("   *******   \n");
//	printf("  *********  \n");
//	printf(" *********** \n");
//	printf("*************\n");
//	printf(" *********** \n");
//	printf("  *********  \n");
//	printf("   *******   \n");
//	printf("    *****    \n");
//	printf("     ***     \n");
//	printf("      *      \n");
//	return 0;
//}

//打印菱形：
//#include <stdio.h>
//int main()
//{
//	int n = 0; //上半部分行数
//	scanf("%d", &n); 
//
//	//上半部分：
//	int i = 0;
//	for (i = 1; i <= n; i++)//行数做循环条件
//	{
//		//1.打印空格
//		int j = 0;
//		for ( j = 0; j < n - i; j++)//规律：n-i
//		{
//			printf(" ");
//		}
//		//2.打印*号
//		for ( j = 0;  j < 2*i-1;  j++)//规律：2*i-1
//		{
//			printf("*");
//		}
//		//每一行打印后换行
//		printf("\n");
//	}
//	
//	//下半部分
//	for ( i = 1; i <= n-1; i++)
//	{
//		int j = 0;
//		//打印空格
//		for ( j = 0; j < i; j++)//规律：i
//		{
//			printf(" ");
//		}
//		//打印*号
//		for ( j = 0; j < 2*(n-1-i)+1; j++)//规律：2*(n-1-i)+1
//		{
//			printf("*");
//		}
//		//换行
//		printf("\n");
//	}
//	return 0;
//}

//求出0～100000之间的所有“水仙花数”并输出。(5位数)
//#include <stdio.h>
//#include <math.h>
//int main()
//{
//	int i, a, b, c, d, e;
//
//	//求三位数的水仙花数：
//	for ( i = 100; i <= 999; i++)
//	{
//		a = i % 10;//个位
//		b = i / 10 % 10;//十位
//		c = i / 100;//百位
//
//		if (pow(a,3) + pow(b,3) + pow(c,3) == i)
//		{
//			printf("%d ", i);
//		}
//	}
//
//	printf("\n");
//
//	//求四位数的水仙花数：
//	for (i = 1000; i <= 9999; i++)
//	{
//		a = i % 10;//个位
//		b = i / 10 % 10;//十位
//		c = i / 100 % 10;//百位
//		d = i / 1000; //千位
//
//		if (pow(a, 4) + pow(b, 4) + pow(c, 4) + pow(d, 4) == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	
//	printf("\n");
//
//	//求五位数的水仙花数：
//	for (i = 10000; i <= 99999; i++)
//	{
//		a = i % 10;//个位
//		b = i / 10 % 10;//十位
//		c = i / 100 % 10;//百位
//		d = i / 1000 % 10;//千位
//		e = i / 10000;//万位
//
//		if (pow(a, 5) + pow(b, 5) + pow(c, 5) + pow(d, 5) + pow(e, 5) == i)
//		{
//			printf("%d ", i);
//		}
//	}
//
//	return 0;
//}

//求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和，其中a是一个数字，
#include <stdio.h>

int Count(int a)
{
	int a1 = a + 10 * a;
	int a2 = a + 10 * a + 100 * a;
	int a3 = a + 10 * a + 100 * a + 1000 * a;
	int a4 = a + 10 * a + 100 * a + 1000 * a + 10000 * a;


	int count = a + a1 + a2 + a3 + a4;

	return count;
}

int main()
{
	int a = 0;

	//输入要求和的整数：
	scanf("%d", &a);

	int count = Count(a);

	printf("%d", count);

	return 0;
}