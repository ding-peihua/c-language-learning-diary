#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
int main()
{
	//生成相关变量：
	int i = 0; //循环变量

	//生成存放输入数据的字符数组
	char password[20] = { 0 };

	//设置一个变量flag：
	int flag = 0;

	//使用for循环，循环输入三次数据：
	for (i = 0; i < 3; i++)
	{
		//输入前先打印提示信息：
		printf("请输入密码：");
		//输入数据：
		scanf("%s", password);
		//因为password是数组，数组名是数组首地址，所以不用取地址符&

		//设置if条件判断语句循环判断三次数据
		if (strcmp(password, "123456") == 0)
			//假设密码是 123456 ，如果相等会返回 0，说明输入的和密码相同
		{
			printf("登录成功\n");
			flag = 1;
			break; //成功则跳出循环
		}
		else
		{
			printf("密码错误\n");
		}
	}

	//跳出 for循环 后，看变量flag情况打印退出程序信息。
	if (flag == 0)
	{
		printf("退出程序");//密码错误 则 退出程序
	}

	return 0;
}