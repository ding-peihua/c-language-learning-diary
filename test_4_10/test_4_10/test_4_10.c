#define _CRT_SECURE_NO_WARNINGS 1

//关系操作符：
//#include <stdio.h>
//
//int main()
//{
//	int input1 = 0;
//	int input2 = 0;
//	while (scanf("%d %d", &input1, &input2) == 2) //运用scanf方法传参数要加取地址符&，不要老忘记！！！
//	{
//		if (input1 == input2) //==：等于
//		{
//			printf("%d 等于 %d\n",input1 ,input2);
//		}
//		else
//		{
//			if (input1 > input2)// >：大于
//			{
//				printf("%d 大于 %d\n", input1, input2);
//			}
//			else if (input1 < input2)// <: 小于
//			{
//				printf("%d 小于 %d\n", input1, input2);
//			}
//		}
//	}
//	return 0;
//}


//逻辑操作符：
//#include <stdio.h>
//
//int main()
//{
//	int a = 0;//默认：0为假
//	int b = 1;//	  非0为真
//
//	if (a && b)//&&：逻辑与
//	{
//		printf("两个操作数同时为真");
//	}
//	else if (a || b)//||：逻辑或
//	{
//		printf("两个操作数其中一个为真");
//	}
//	return 0;
//}


//逻辑操作符：
//#include <stdio.h>
//
//int main()
//{
//	int a = 0;
//	int b = 1;
//	if (a && b)//&&：逻辑与
//	{
//		printf("两个操作数同时为真");
//	}
//
//	if (a || b)//||：逻辑或
//	{
//		printf("两个操作数其中一个为真");
//	}
//	return 0;
//}

//条件操作符（三目操作符）
//#include <stdio.h>
//
//int main()
//{
//	int a = 0;//编程习惯：声明变量时随便赋值
//	int b = 0;
//	//局部变量不给初值的话，局部变量会是一个随机的值
//	//这时运用这个局部变量（加减乘除），结果就不确定 
//	
//	
//	//输入
//	scanf("%d %d",&a ,&b);
//	int m = 0;
//	//三目操作符：(逻辑类似下面的if...else...语句)
//	
//	// 写法一：
//	//(a > b) ? (m = a) : (m = b)
//	//  exp1	  exp2		exp3
//
//	//写法二：
//	m = (a > b ? a : b);
//	//表达式2(exp2)执行的话，整个表达式的结果就是表达式2的结果
//	//表达式3(exp3)执行的话，整个表达式的结果就是表达式3的结果
//
//	/*if (a > b)
//		m = a;
//	else
//		m = b;*/ //a = b的话，进入else赋值哪个都一样
//
//	printf("%d\n", m);
//
//	return 0;
//}

//逗号表达式：
//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = 2;
//	int c = 5;
//
//	//逗号表达式：exp1, exp2, exp3, ...expN
//	//从左向右依次计算
//	//逗号表达式的结果时最后一个表达式的结果
//	int d = (a+=3, b=5, c=a+b, c-4);
//	//	  a=a+3=6  b=5 c=6+5=11 c-4=7
//	//						  最后的结果=7
//
//	printf("%d\n",d);
//	return 0;
//}

// [] -- 下标引用操作符：
//#include <stdio.h>
//
//int main()
//{
//	int arr[10] = {1,2,3,4,5,6,7,8,9,10};
//	//下标：	   0 1 2 3 4 5 6 7 8  9 
//
//	printf("%d\n",arr[5]);//取数组中下标为5的值
//	// []是一个操作符，有两个操作数：这里一个是arr,一个是5
//	return 0;
//}

//（）：函数调用操作符：
//#include <stdio.h>
//
//int Add(int x, int y)//这里的括号是语法限定
//{
//	return x + y;
//} 
//
//
//int main()
//{
//	int z = Add(3,5);//调用Add（）函数，这里的（）就是函数调用操作符
//					 //这里有三个操作数： Add	3	5 
//	printf("%\d\n",z);
//
//	printf("hehe");//调用printf（）函数，这里的（）就是函数调用操作符
//				   //这里有两个操作数“	 printf	 "hehe"
//	return 0;
//}

//关键字：auto
//#include <stdio.h>
//
//int main()
//{
//	auto int num = 0; //局部变量本身都是有auto(自动创建，自动销毁)的，所有干脆都给省略掉了
//	//局部的变量是进入作用域创建，出了作用域销毁
//	//这里是 自动创建、自动销毁的 -- auto
//	return 0;
//}


//typedef -- 类型重命名（可以理解成给类型起个别名）
//#include <stdio.h>
//
//typedef unsigned int u_int;//将 unsigned int 重命名为 u_int，末尾加;(分号)
//
//int main()
//{
//	unsigned int num = 0;
//	//这里定义num的字符类型有点长，可以用typedef来重命名这个类型
//
//	u_int num2 = 0;
//	//运用重命名的字符类型
//
//	return 0;
//}

//static 修饰局部变量
//#include <stdio.h>
////本来一个局部变量是存放在栈区的，如果被static修饰就存储到静态区了
////static 修饰局部变量改变了变量的存储类型（位置），使得这个静态变量的生命周期变长了，直到程序结束才结束
//void test()
//{
//	static int a = 5;//加上static属性，变成静态变量，程序结束才销毁
//	//被static修饰的局部变量 
//	//出了作用域后，不再自动销毁，进入作用域也不再自动创建
//	//变量a在编译时就已经创建了
//	a++;
//	printf("%d\n",a);
//}
//
//int main()
//{
//	int i = 0;
//	while (i < 10)
//	{
//		test();
//		i++;
//	}
//	return 0;
//}

//static 修饰全局变量
//#include <stdio.h>
////声明外部符号
//extern int g_val;
//
//int main()
//{
//	printf("%d\n",g_val);
//	return 0;
//}


//static 修饰函数
//#include <stdio.h>
////声明外部符号
//extern int Add(int x, int y);
////声明是告诉 函数名 参数名 返回值
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int s = Add(a, b);
//	printf("%d\n", s);
//	return 0;
//}

//#define 定义标识符常量：
//#include <stdio.h>
//#define M 100 //后面不用加；（分号）
//			  //这里 M 是一个符号，是个常量，不用分配地址，没有地址
//			  // 在预处理时进行替换
////变量才需要存储起来，分配地址
//
//int main()
//{
//	int arr[M] = { 0 };//声明数组长度的值得时常量
//	int m = M;//使用符号进行赋值
//
//	printf("%d\n",sizeof(arr));//
//	//数组长度为100，有100个元素，一个元素占4个字节
//	//			100	* 4 = 400
//	printf("%d\n",M);
//	printf("%d\n",m);
//
//	return 0;
//}



//#define 定义宏：
//#include <stdio.h>

////使用#define定义一个宏：
//#define ADD(x, y) ((x)+(y))
////ADD-->宏的名字	(x, y)-->宏的两个参数	((x)+(y))-->宏的实现体 
//
////函数：
//int Add(int x, int y)
////int-->函数的返回类型	Add-->函数名	（int x, int y）-->函数参数
//{					//
//	return x + y;	//---->这三行是函数体
//}					//
//
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//
//	//调用宏，类似调用函数
//	int c = ADD(a, b);
//	//编译时进行替换：int c = ((a)+(b));
//	//宏 和 函数：写的形式不一样，调用方式非常类似
//	printf("%d\n", c);
//
//	//调用函数
//	int d = Add(a, b);
//	printf("%d\n", d);
//
//	return 0;
//}

//结构体--学生
#include <stdio.h>
#include <string.h>

struct Stu
{
	//学生的相关属性
	char name[20];//名字-字符串，一个汉字是2个字符
	int age;//年龄
	char sex[5];//“男”、“女”、“保密”
	//最长：“保密”两个汉字4个字符 加一个 “\0”一个字符，最多5个字符
};//大括号{}后加分号

struct Book
{
	char name[50];//书名
	char author[10];//作者
	float price;//价格
	//结构体里定义的内容称为结构体的成员
};

int main()
{
	struct Stu s1 = {"张三", 20, "男"};
//struct Stu整体是一个字符类型，s是一个变量

	struct Book b1 = {"《C语言从入门到入土》", "C大师", 66.6f};

	printf("%s %s %f\n",b1.name ,b1.author ,b1.price);
	//使用 .操作符 来访问 成员对象
	//浮点数在内存中无法精确地保存，会丢失一些精度
	return 0;
}

