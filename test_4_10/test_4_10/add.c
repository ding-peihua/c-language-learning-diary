#define _CRT_SECURE_NO_WARNINGS 1

//全局变量具有外部链接属性（可以被extern声明）
//使用在其他源文件内部依然可以使用（方法要正确）
	//static int g_val = 2023;
//static修饰全局变量，改变了这个全局变量的链接属性，
//由外部链接属性变成了内部链接属性
//内部链接属性：只能在源文件内部使用
//这个静态变量只能在自己所在的源文件内部使用，不能
//在其他源文件外部使用了

//static修饰函数和static修饰全局变量是一样的
//函数本身时具有外部链接属性的，但是被static修饰，
//就变成了内部链接属性
//使得这个函数只能在自己所在的源文件内部使用，
//不能再其他文件内部使用
static int Add(int x, int y)
{
	return (x + y);
}