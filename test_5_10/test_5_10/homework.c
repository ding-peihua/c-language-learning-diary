#define _CRT_SECURE_NO_WARNINGS 1

//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
#include <stdio.h>

int main()
{
	int input = 0;
	
	//输入
	scanf("%d", &input);

	int i = 0;
	
	printf("奇数位的数：");
	//循环要从1开始，不然判断奇偶数会出错
	for (i = 1; i <= 32; i++) //32位 循环32次
	{
		if (i % 2 == 1) //奇数位
		{
			int test = input & 1; //按位与1，取出当前位
			printf("%d",test);
			
		}
		input >>= 1; //判断下一位，还要再赋值回去
	}

	printf("\n");

	printf("偶数位的数：");
	for (i = 1; i <= 32; i++) //32位 循环32次
	{
		if (i % 2 != 1) //偶数位 
		{
			int test = input & 1; //按位与1，取出当前位
			printf("%d", test);

		}
		input >>= 1; //判断下一位
	}

	return 0;
}


//编程实现：两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？
//输入例子 :
//1999 2299
//输出例子 : 7
//#include <stdio.h>
//
//int main()
//{
//	int input1 = 0;
//	int input2 = 0;
//	int count = 0; //统计相同的情况
//
//	//输入
//	scanf("%d %d", &input1, &input2);
//
//	int i = 0;
//	for (i = 0; i < 32; i++)
//	{
//		int test1 = 0;
//		//把input1的最后一位取出来
//		test1 = input1 & 1;
//
//		int test2 = 0;
//		//把input2的最后一位取出来
//		test2 = input2 & 1;
//
//		//比较
//		if (test1 != test2)
//		{
//			count++;
//		}
//
//		//比较下一位：
//		input1 >>= 1; //要用 >>= ,还要再赋值回去
//		input2 >>= 1;
//	}
//
//	printf("%d", count);
//
//	return 0;
//}
