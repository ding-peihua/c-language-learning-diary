﻿#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>

//int main()
//{
//	//int r = 7 / 2;
//	//printf("%d\n", r);
//
//	//double d = 7 / 2;
//	//printf("%lf\n", d);
//
//	/*double d = 7.0 / 2;
//	printf("%lf\n", d); */
//
//	//int r = 15 % 8; //% 得到的是整除后的余数
//	//printf("%d\n", r);
//
//	//int r = 15.0 % 8.0;
//}

//#include <stdio.h>
////不管是正整数还是负整数都可以写出二进制原码
////根据正负直接写出的二进制序列就是原码
//int main()
//{
//	//1个整型是4个字节 = 32个bit位
//	//符号位是 1 表示负数
//	//符号位是 0 表示正数
//
//	int a = 15;
//	//正整数的原码、反码、补码是相同的
//	//00000000000000000000000000001111 -- 原码
//	//00000000000000000000000000001111 -- 反码
//	//00000000000000000000000000001111 -- 补码
//
//	int b = -15;
//	//负整数的原码、反码、补码是要计算的
//	//10000000000000000000000000001111 -- 原码
//	//11111111111111111111111111110000 -- 反码（原码的符号位不变，其它位按位取反得到的就是反码）
//	//11111111111111111111111111110001 -- 补码（反码 +1 就是补码）
//
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
////正整数：
//	int a = 15;
//	//00000000000000000000000000001111 -- 补码
//	int b = a >> 1;
//	// 0 0000000000000000000000000000111 1 -- 补码（移位后）
//
//	//C语言没有明确规定到底是算术右移还是逻辑右移，一般编译器上采用的是算术右移
//
//	printf("%d\n", b); //算术右移
//	// b 得到的是 a 移位后的结果
//	// 算术右移（右边丢弃，左边补原来的符号位）
//	// 从 二进制的1111 变成 二进制的111 ，从 十进制的 15 变成了 十进制的 7 
//	
//	printf("%d\n", a);
//	// a >> 1，进行移位后，a没有发生变化，
//	// 表达式不会影响 a 的 值
//
//
////负整数：
//	int c = -15;
//	//11111111111111111111111111110001 -- 补码 （符号位是 1 表示负数）
//	int d = c >> 1;
//	// 1 1111111111111111111111111111000 1 -- 补码 （移位后）
//	// 空出的位置补的是 1 ，说明是算术右移（右边丢弃，左边补原来的符号位）
//
//	
//
//	// 实际值是用 原码 表示的
//
//	printf("%d\n", d); //算术右移
//	// 反码 +1 就是补码 ，补码 -1 就是反码：
//	//  11111111111111111111111111110111 -- 反码
//
//	// 原码的符号位不变，反码按位取反得 原码
//	//  10000000000000000000000000001000 -- 原码 (-8)
//
//	printf("%d\n", c);
//	
//	return 0;
//}


//左移操作符：补码 左边丢弃，右边补0
//#include <stdio.h>
//
//int main()
//{
////正整数：
//	int a = 6;
//	//   00000000000000000000000000000110 -- 正整数的 原码、反码、补码 相同
//	//   00000000000000000000000000000110 -- 补码 （移位操作的是 补码）
//					//左移后：
//	// 0 0000000000000000000000000000110 0 -- 补码 (原码 、 反码)
//	//   因为正整数的 原码、反码、补码 相同，所以这也是原码
//
//	int b = a << 1;
//
//	printf("%d\n", b);
//
//	printf("%d\n", a);
//
//
////负整数：
//	int c = -6;
//	//   10000000000000000000000000000110 -- 原码
//	//   11111111111111111111111111111001 -- 反码 （原码按位取反）
//	//   11111111111111111111111111111010 -- 补码 （反码 +1 ）
//
//	int d = c << 1;
//				//左移后：
//	// 1 1111111111111111111111111111010 0 -- 补码
//	//   11111111111111111111111111110011 -- 反码 （补码 -1）
//	//   10000000000000000000000000001100 -- 原码 （符号位不变，反码按位取反）
//	// -12
//
//	printf("%d\n", d);
//
//	printf("%d\n", c);
//
//	a = a << 1;
//	// 也可以写成：
//	a <<= 1; //符合符号 <<= ：左移等于 ，类似 a += 1
//	
//	return 0;
//}

//位操作符
//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	// 00000000000000000000000000000011 -- 补码（正整数的原码、反码、补码相同）
//
//	int b = -5;
//	// 10000000000000000000000000000101 -- 原码
//	// 11111111111111111111111111111010 -- 反码
//	// 11111111111111111111111111111011 -- 补码
//
//	int c = a & b;
//	// & -- 对应二进制位有0则为0，两个同时为1，才为1
//	// 00000000000000000000000000000011 -- a补码
//	// 11111111111111111111111111111011 -- b补码
//	//				得：
//	// 00000000000000000000000000000011	-- 补码（两个补码计算出来的也是补码）
//	// 这里计算出来的结果符号位是 0 ，说明是 正整数，三个码相同，所以这也是原码
//	
//
//	printf("%d\n", c); // 所以结果是3
//
//	return 0;
//}

////位操作符
//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	// 00000000000000000000000000000011 -- 补码（正整数的原码、反码、补码相同）
//
//	int b = -5;
//	// 10000000000000000000000000000101 -- 原码
//	// 11111111111111111111111111111010 -- 反码
//	// 11111111111111111111111111111011 -- 补码
//
//	int c = a | b;
//	// | -- 按（二进制）位或 -- 对应二进制位有1则为1，两个同时为0，才为0
//	// 00000000000000000000000000000011 -- a补码
//	// 11111111111111111111111111111011 -- b补码
//	//				得：
//	// 11111111111111111111111111111011	-- 补码（两个补码计算出来的也是补码）
//	// 
//	// 这里计算出来的结果符号位是 1 ，说明是 负整数，还要计算 反码 和 补码
//	// 11111111111111111111111111111010 -- 反码
//	// 10000000000000000000000000000101 -- 原码
//
//	printf("%d\n", c); // 所以结果是-5
//
//	return 0;
//}

//位操作符
//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	// 00000000000000000000000000000011 -- 补码（正整数的原码、反码、补码相同）
//
//	int b = -5;
//	// 10000000000000000000000000000101 -- 原码
//	// 11111111111111111111111111111010 -- 反码
//	// 11111111111111111111111111111011 -- 补码
//
//	int c = a ^ b;
//	// ^ -- 按（二进制）位异或 -- 对应二进制位相同为0， 相异为1
//	// 00000000000000000000000000000011 -- a补码
//	// 11111111111111111111111111111011 -- b补码
//	//				得：
//	// 11111111111111111111111111111000	-- 补码（两个补码计算出来的也是补码）
//	// 
//	// 这里计算出来的结果符号位是 1 ，说明是 负整数，还要计算 反码 和 补码
//	// 10000000000000000000000000000111 -- 反码
//	// 10000000000000000000000000001000 -- 原码
//
//	printf("%d\n", c); // 所以结果是-8
//
//	return 0;
//}

//异或是支持交换律的
//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//
//	printf("%d\n", a ^ a); // 0 (每一位都是相同的，32位全是0)
//	printf("%d\n", a ^ 0); // a = 3
//	// 011 - 3 (a)
//	// 000 - 0
//	// 011 -- 异或结果 ：a = 3 
//
//	printf("%d\n", a ^ b ^ a); // b = 5
//	// 011 - 3 (a)
//	// 101 - 5 (b)
//	// 110 -- 异或结果
//	// 011 - 3 (a)
//	// 101 -- 异或结果 ： b = 5
//
//	printf("%d\n", a ^ a ^ b); // b = 5
//	// a ^ a --> 0
//	// 0 ^ b --> b = 5
//
//
//
//	return 0;
//}

//一道变态面试题：不使用临时变量，交换两个数的值
//#include <stdio.h>
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//
//	printf("交换前：a=%d b=%d\n", a, b);
//
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//
//	printf("交换后：a=%d b=%d\n", a, b);
//
//	return 0;
//}
//

//#include <stdio.h>
//
//int main() {
//    int input = 0;
//    int count = 0; //统计 1 的个数
//    //输入
//    scanf("%d", &input);
//
//    int i = 0;
//    for (i = 0; i < 32; i++)
//    { // 共32位，判断32次
//
//        int test = input & 1; //按位与1 取出32位最低位
//        //判断是不是1
//        if (test == 1) {
//            count++; //是的话，统计加1
//        }
//        //判断下一位：
//        input >>= 1; //向右移一位，继续判断，使用循环
//
//    }
//
//    //输出：
//    printf("%d", count);
//
//    return 0;
//}

//作业：
//#include <stdio.h>
//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;
//	b = ++c, c++, ++a, a++;
//	b += a++ + c;
//	printf("a = %d b = %d c = %d\n:", a, b, c);
//	return 0;
//}

//sizeof 和 数组
//#include <stdio.h>
//
//void test1(int arr[]) 
////传过来的首元素指针 ，一个 int*类型 大小是 4个字节
//{
//	printf("%d\n", sizeof(arr));
//}
//
//void test2(char ch[])
////传过来的首元素指针 ，一个 char*类型 大小是 4个字节
//{
//	printf("%d\n", sizeof(ch));
//}
//
//int main()
//{
////整型数组：
//	int arr[10] = { 0 };
//
//	printf("%d\n",sizeof(arr)); //使用 数组名
//	//40 - 计算整个数组的大小、单位字节
//	printf("%d\n", sizeof(int[10])); //使用数组的字符类型，包括[]和元素个数
//
//	test1(arr);
//
////字符数组
//	char ch[10] = { 0 };
//
//	printf("%d\n", sizeof(ch)); //使用 数组名
//	//10 -- 字符数组中 sizeof（数组名） 计算 的是 字符个数
//	printf("%d\n", sizeof(int[10])); //使用数组的字符类型，包括[]和元素个数
//	//40 -- 一个 char类型元素 是 4个字节，10个就是40
//
//	test2(ch);
//
//	return 0;
//}

//~按位取反的运用，结合 & (按位与)和  | (按位或)
//将 13 的 32bit位从右往左第5位，换为 1 ，再换为 0
//#include <stdio.h>
//
//int main()
//{
//	int a = 13;
//
//	// 00000000000000000000000000001101 - 13 的 原、反、补码
//	//			按位或：有1就是1
//	// 00000000000000000000000000010000 - 1 << 4
//	// 00000000000000000000000000011101 -- 29 的 原、反、补码
//	a |= (1 << 4);
//	// 这样就把第五位换成了 1 , 13 变成了 29 
//
//	printf("%d\n", a); 
//
//
//	// 再把第五位换成 0
//	// 00000000000000000000000000011101 - 29 的 原、反、补码
//	// 00000000000000000000000000010000 - 1 << 4
//	// 11111111111111111111111111101111 - ~(1 << 4) 进行按位取反
//	//			按位或：两个都是 1 才是 1
//	// 00000000000000000000000000001101 -- 13 的 原、反、补码
//	a &= (~(1 << 4));
//
//	printf("%d", a);
//
//	return 0;
//}

//运用 ~按位取反 实现 多组输入
//#include <stdio.h>
//
//int main()
//{
//	int a = 0; 
//
//	// scanf 读取失败返回的是EOF
//	// 假设 scanf 读取失败，返回EOF ，即 -1
//	// -1 的补码：
//	// 11111111111111111111111111111111
//	//			~ 按位取反后：
//	// 00000000000000000000000000000000
//	//			得0，条件即为假
//	
//	while (~scanf("%d", &a))
//	{
//		printf("%d\n", a);
//	}
//
//	return 0;
//}

//判断闰年
//#include <stdio.h>
//
//int main()
//{
//	int y = 0;
//	scanf("%d", &y);
//
//	//1. 能被4整除，并且不能被100整除
//	//2. 能被400整除是闰年
//	if ( ((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
//	{
//		printf("闰年\n");
//	}
//	else
//	{
//		printf("不是闰年");
//	}
//
//	return 0;
//}

//练习;
//#include <stdio.h>
//
//int main()
//{
//	int i = 0, a = 0, b = 2, c = 3, d = 4;
//
//	//i = a++ && ++b && d++;
//
//	i = a++ || ++b || d++;
//
//	printf("a = %d\nb = %d\nc = %d\nd = %d\n", a, b, c, d);
//
//	return 0;
//}