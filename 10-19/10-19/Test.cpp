#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <assert.h>
using namespace std;

//包含日期类头文件：
#include "Date.h"

//优化全缺省构造函数：
void TestDate1()
{
	Date d1;
	//打印d1日期：
	d1.Print();

	//全缺省构造函数的完善：

	//初始化的日期可能不合法（月份）：
	Date d2(2023, 13, 1);
	d2.Print();

	//初始化的日期可能不合法（日期）：
	Date d3(2010, 2, 29); //2月可能没有29天
	d3.Print();
}

//测试 “-” 和 “+”（日期-+整型）：
void TestDate2()
{
	//创建一个日期类对象：d1
	Date d1(2023, 10, 24);
	d1.Print();

	//测试“-”运算符重载函数：
	Date ret1 = d1 - 100; //隔几个月
	ret1.Print(); //打印结果

	Date ret2 = d1 - 10000; //跨越闰年
	ret2.Print(); //打印结果

	//测试“+”运算符重载函数：
	Date ret3 = d1 + 100; //隔几个月
	ret3.Print(); //打印结果

	Date ret4 = d1 + 10000; //跨越闰年
	ret4.Print(); //打印结果
}

//测试“前置++”和“后置++”：
void TestDate3()
{
	//创建一个日期类对象：d1
	Date d1(2023, 10, 24);
	d1.Print();

	++d1; 
	//d1.operator++() -- “前置++”
	d1.Print(); //2023/10/25


	Date d2 = d1++;
	//d1.operator++(int) -- “后置++”
	d2.Print(); //先取到d1：2023/10/25，再++
	d1.Print(); //2023/10/26
}

//测试“日期-日期”运算符重载函数：
void TestDate4()
{
	//创建一个日期类对象：d1
	Date d1(2023, 10, 24);
	d1.Print();

	//创建一个日期类对象：d2
	Date d2(2024, 2, 10);
	d2.Print();

	//打印两日期相差的日期：
	cout << (d2 - d1) << endl;
	cout << (d1 - d2) << endl;
} 

//测试“+=”的特殊情况：
void TestDate5()
{
	Date d1(2023, 10, 24);
	d1 += -100;

	d1.Print();
}

void TestDate6()
{
	//创建一个日期类对象 -- d1：
	Date d1(2023, 10, 31); //d1
	d1.Print(); //打印d1（通过函数）

	//创建一个日期类对象 -- d2：
	Date d2(2023, 10, 31); //d2
	d2.Print(); //打印d1（通过函数）

	//那能不能通过流插入打印d1呢:
	cout << d1;
	//operator<<(cout, d1)
	/*
	* << ：流插入运算符（输出流运算符）
	* 默认只支持内置类型的使用，
	* 自定义类型默认都是不支持运算符的，
	* 所以需要自定义该运算符的重载函数：
	* operator<<() ,<<有两个操作数，
	* 左操作数：cout，是ostream类（输出流）的对象，
	* 能够自动识别数据的类型，该功能是通过函数重载实现的，
	* 数据类型不同调用的重载函数不同，参数匹配不同
	*/
	//d1 << cout; //成员函数实现的话：“控制台流入d1对象”
	//d1.operator<<(&d1, cout);

	//还得支持以下写法：
	cout << d1 << d2 << endl; 
	/*
	* endl相当于'\n'，'\n'是char类型，
	* 等执行完cout << d1 << d2后，返回cout，
	* 再结合组成cout << endl; end为char类型，
	* 此时再执行时调用的就是char类型的“<<”的流插入重载函数了
	*/
}

void TestDate7()
{
	//创建一个日期类对象 -- d1：
	Date d1(2023, 10, 31); //d1
	d1.Print(); //打印d1（通过函数）

	//创建一个日期类对象 -- d2：
	Date d2(2023, 10, 31); //d2
	d2.Print(); //打印d1（通过函数）

	//通过输入流输入日期类对象：
	cin >> d1;
	cout << d1; 
	//使用“<<”运算符重载函数打印日期

	//连续输入：
	cin >> d1 >> d2;
	cout << d1 << d2; 
	//使用“<<”运算符重载函数连续打印日期
}

//int main()
//{
//	//TestDate1();
//	//TestDate2();
//	//TestDate3();
//	//TestDate4();
//	//TestDate5();
//	//TestDate6();
//	TestDate7();
//
//	return 0;
//}

int main()
{
	//使用const修饰d1所指向的变量：
	const Date d1(2023, 10, 31);

	/*
	*			隐藏的this指针传递问题：
	* 不加const的情况下（默认情况下），
	* Print成员函数隐藏的this指针的类型：
	* Date* const this -- const修饰的是this指针本身（指针常量）
	* 指针本身不能被改变
	* 
	* 加了const的情况下，Print成员函数隐藏的this指针的类型：
	* const Date* this -- const修饰的是指针所指向的变量（常量指针）
	* 指针指向对象的内容不能被改变
	* 
	*			导致了权限的放大问题：
	* 调用Print()函数时，会&d1（取d1的地址），d1会是const Date*，
	* 但Print()函数中的this指针是Date* const this类型的，
	* 即将常量指针传给了指针常量，导致了权限的放大
	* 
	* 所以为了解决该问题，需要让Print()函数的this指针也是常量指针，
	* 需要给隐藏的this也对应地使用const指针进行修饰
	*/

	d1.Print(); //给函数加上const进行修饰后
	//const Date* 传给 const Date* const (权限平移)

	//非const修饰的对象：
	Date d2(2022, 1, 1);

	d2.Print(); 
	/*
	* 此时可以发现非const对象也可以调用const修饰的Print()函数
	* 权限可以平移或缩小，唯独不能放大，
	* 这里是将 Date* 传给 const Date* const (权限缩小)
	*/

	/*
	* 给成员函数隐藏的this指针修饰const后，
	* const对象 和 非const对象 都可以调用
	* 该const成员函数，
	* 但也不是所有成员函数都要进行const修饰
	*/

	
	//那么哪些成员函数需要进行const修饰呢？
	d1 < d2; //d1--const ; d2--非const
	/*
	* 此时d1--const Date* d1 传给"<"重载函数的this指针：
	* Date* const this，同样导致了权限的放大，不允许执行
	*/

	d2 < d1; //d2--非const ; d1--const
	/*
	* 此时d2--Date* d1 传给"<"重载函数的this指针：
	* Date* const this，是权限的缩小，是允许的
	*/

	/*
	* 所以为了让 d1<d2 和 d2<d1 都能够支持，
	* 同样需要对“<”运算符重载函数进行const修饰
	* 
	* 对其它不需要改变对象的函数（比较运算符重载函数）同理，
	* 都需要加上const进行修饰，
	* 防止实参被const修饰后和this指针权限冲突的问题,
	* 让const对象和非const对象都能够正常调用该函数
	*/

	/*
	*				成员函数定义的原则：
	* 
	* 1、能定义成const的成员函数都应该使用const进行修饰，
	* 让隐藏的this指针的权限变小：const Date* this
	* 这样const对象（权限平移）和非const对象（权限缩小）
	* 都可以正常调用该成员函数
	* （如：比较运算符重载函数、“+”、“-”运算符重载函数）
	* 
	* 2、需要修改成员变量的成员函数，不能使用const进行修饰，
	* const对象不能调用（很合理），非const才能调用
	* （如：“+=”、“-=”、“++”、“--”运算符重载函数）
	*/


	//const对象取地址：
	cout << &d1 << endl;

	//非const对象取地址：
	cout << &d2 << endl;
	/*
	* 对于非const对象取地址，
	* 两个重载的“&”函数都可以被调用，
	* 但编译器会调用最匹配的一个重载函数，
	* 即返回值为 Date* const (this) 的“&”函数
	* 
	* 如果我们只实现了const版本的“&”函数，
	* 那该非const对象就会调用该版本的“&”函数
	*/


	return 0;
}
