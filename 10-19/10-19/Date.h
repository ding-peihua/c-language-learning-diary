#pragma once

//包含头文件：
#include <iostream>
#include <assert.h>

//完全展开std命名空间：
using namespace std;

//日期类：
class Date
{
public:
	//全缺省构造函数：
	Date(int year = 1, int month = 1, int day = 1);


	//打印日期函数：
	void Print() const;
	//“辅助”完成“+、+=”运算符重载的函数：
	/*
	* 因为考虑到各月份日期可能不一样，
	* 二月还需考虑闰年还是平年，
	* 所以可以单独写一个函数处理月份的情况：
	*/
	int GetMonthDay(int year, int month);


	//“==”运算符重载函数：
	//判断对象是否相等的函数（以日期类为例）：
	bool operator==(const Date& y) const;  //等于
	//“!=”运算符重载函数：
	bool operator!=(const Date& y) const;  //不等于
	//“>”运算符重载函数：
	//写一个函数判断自定义类型大小（以日期类对象为例）：
	bool operator>(const Date& y) const;  //大于	
	//“>=”运算符重载函数：
	bool operator>=(const Date& y) const;  //大于等于
	//“<”运算符重载函数：
	bool operator<(const Date& y) const;  //小于
	//“<=”运算符重载函数：
	bool operator<=(const Date& y) const;  //小于等于


	//“+=”运算符重载函数：
	Date& operator+=(int day);  // “+=” 才会改变d1对象
	//“+”运算符重载函数：
	Date operator+(int day) const;  // “+” 不会改变d1对象
	//“-=”运算符重载函数：
	Date& operator-=(int day);  // “-=” 会改变d1对象
	//“-”运算符重载函数 -- “日期 - 整型”：
	Date operator-(int day) const;  // “-” 不会改变d1对象
	//“-”运算符重载函数 -- “日期-日期”：
	int operator-(const Date& d) const;


	//“++”运算符重载函数 -- “前置++”：
	Date& operator++();
	//“++”运算符重载函数 -- “后置++”：
	Date operator++(int);
	//“--”运算符重载函数 -- “前置--”：
	Date& operator--();
	//“--”运算符重载函数 -- “后置--”：
	Date operator--(int);
	

	//“&”取地址运算符重载函数：
	//普通（非const）对象取地址：
	Date* operator&();
	//const对象取地址：
	const Date* operator&() const;


	//“<<”流运算符重载函数
	//void operator<<(ostream& out);

	/*
	* friend：友元函数，
	* 可以访问私有成员变量，不受制于隐藏的this指针
	*/
	//选择在全局中实现“<<”流运算符重载函数 -- cout输出流对象：
	friend ostream& operator<<(ostream& out, const Date& d);
	//选择在全局中实现“<<”流运算符重载函数 -- cin输入流对象：
	friend istream& operator>>(istream& in, Date& d);
	/*
	* “&”运算符重载函数的非const版本和const版本都是默认成员函数，
	* 如果我们不显式定义，编译器也会默认生成这两个运算符重载函数，
	* 
	* 虽然可以显式实现，但显式实现也没什么可以操作的，
	* 和编译器默认生成的功能接近，都是直接返回对象地址，
	* 所以这两个重载函数一般不会显式定义（对一般类来讲）
	*/

private:
	int _year;
	int _month;
	int _day;
};


//选择在全局中实现“<<”流运算符重载函数 -- cout输出流对象：
ostream& operator<<(ostream& out, const Date& d);
/*
* 这样就可以自己设置参数的位置了，
* 不受制于隐藏的this指针
* 
* 让输出流对象为第一个参数（左操作数），
* 该操作数需要接收数据，本身就需要被改变，
* 所以不能对其使用const修饰
* 
* 日期类对象为第二个参数（右操作数），
* 该对象只是将数据“流入”cout中，
* 不需要被改变，所以使用const修饰
*/
//选择在全局中实现“<<”流运算符重载函数 -- cin输入流对象：
istream& operator>>(istream& in, Date& d);