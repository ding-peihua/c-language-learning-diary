#define _CRT_SECURE_NO_WARNINGS 1

// s = x/(1*1) + x/(1*2) + x/(1*2*3) + …… + x/(1*2*3*……*n) 

#include <stdio.h>

double fun(double x, int n)
{
	double s = x; //接收最终计算结果
	double mul = 1.0; //接收阶乘结果（从1开始）
	double div = x; //接收x和阶乘相除的结果

	while (n != 1)
		//n还没到1就还需要继续算阶乘
	{
		double i = 0;
		//使用for循环计算当前阶乘结果：
		for (i = 1; i <= n; i++)
		{
			mul *= i; //当前阶乘结果
			//mul = mul * i
		}

		//计算每次x和阶乘相除的结果：
		div /= mul;
		//div = div / mul

		//当前阶乘mul使用完成后要重新置为1.0：
		mul = 1.0;

		//计算加法的部分：
		s = s + div;

		//div除法完成后也要重新置为x：
		div = x;

		//调整n值：
		n--;
	}

	return s;
}

//void main()
//{
//	printf("%.4f\n", fun(3.0, 10));
//}



