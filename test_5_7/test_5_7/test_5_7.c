#define _CRT_SECURE_NO_WARNINGS 1

//1. 游戏不退出，继续玩下一把（循环）
//2. 应用多文件的形式写代码

#include "game.h" //包含自己定义的头文件


void menu()
{
	printf("***********************************\n");
	printf("**********    1.play    ***********\n");
	printf("**********    0.exit    ***********\n");
	printf("***********************************\n");
}

void game()
{
/*
1. 游戏在走的过程中要进行数据的存储，三子棋可以使用3*3的二维数组
	即  char board[3][3];

2. 没下棋之前数组中存放空格，下棋后将空格替换成字符
*/
	char board[ROW][COL] = { 0 };

	InitBoard(board, ROW, COL); //传递 数组名，行数，列数
	// 用于初始化数组，将空格全部初始化为空格
	// 将游戏相关的自定义函数代码写到game.c中
	
	// 打印棋盘
	DisplayBoard(board, ROW, COL);

//下棋：
	char ret = 0; //接受判断输赢的符号
//让玩家一直下：
	while (1)
	{
		//玩家下棋：
		PlayerMove(board, ROW, COL);
		//落子后再打印棋盘看棋盘情况
		DisplayBoard(board, ROW, COL);
		//判断输赢（三子相连）
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}

		//电脑下棋：
		ComputerMove(board, ROW, COL);
		//落子后再打印棋盘看棋盘情况
		DisplayBoard(board, ROW, COL);
		//判断输赢
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}

	}

	//写在while循环外面的话，只用写一次
	if (ret == '*')
	{
		printf("玩家赢\n");
	}
	else if (ret == '#')
	{
		printf("电脑赢\n");
	}
	else
	{
		printf("平局");
	}

}

//int main()
//{
//	int input = 0;
//
//	//rand函数调用之前得调用srand，
//	//整个程序只用调一次，所以放在主函数中
//	srand((unsigned int)time(NULL));
//
////1. 游戏不退出，继续玩下一把（循环）	
//	do //不管三七二十一，先打印菜单
//	{
//		menu(); //打印游戏菜单
//		printf("请选择：>");
//		scanf("%d", &input); //1 0 其它
//
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误，重新选择：\n");
//			break;
//		}
//	} while (input); 
//	//当input为0时停止循环退出游戏
//	//当input非0时继续循环
//	
//	return 0;
//}