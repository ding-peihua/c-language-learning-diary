#define _CRT_SECURE_NO_WARNINGS 1

////打印9*9乘法口诀表（%-2d）
//int main()
//{
//	int i = 0;
//	//打印行：
//	for (i = 1; i <= 9; i++) //乘法表从1开始，所以i = 1
//	{
//		//每一行的打印：
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%-2d ", i, j, i * j);
//			//%-2d：拿右边的空格来提供两位
//		}
//		printf("\n");
//	}
//	return 0;
//
//}

//打印9*9乘法口诀表（\t：水平制表符）
//int main()
//{
//	int i = 0;
//	//打印行：
//	for (i = 1; i <= 9; i++) //乘法表从1开始，所以i = 1
//	{
//		//每一行的打印：
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%d\t", i, j, i * j);
//			//%-2d：拿右边的空格来提供两位
//		}
//		printf("\n");
//	}
//	return 0;
//
//}


//求10个整数中的最大值：
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//
//	//输入数组
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//
//	//求最大值
//	int max = arr[0]; //假设第一个就是最大值
//	for (i = 1; i < sz; i++)
//	{
//		if (arr[i] > max) 
//			//在数组中找到最大值，赋给max
//		{
//			max = arr[i];
//		}
//	}
//
//	//输出
//	printf("%d\n", max);
//
//	return 0;
//}

////计算1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99 - 1 / 100 的值，打印出结果
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	double sum = 0.0; //浮点数除法
//	int flag = 1; //用于改变符号
//
//	//使用循环进行计算
//	for (i = 1; i <= 100; i++) //让i充当分母
//	{
//		sum += flag * (1.0 / i);
//		flag = -flag; //循环之后改变符号，实现一加一减……
//	}
//
//	printf("%lf\n", sum);
//
//	return 0;
//}


//计算1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99 - 1 / 100 的值，打印出结果
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	double sum = 0.0; //浮点数除法
//	int flag = 1; //用于改变符号
//
//	//使用循环进行计算
//	for (i = 1; i <= 100; i++) //让i充当分母
//	{
//		if (i % 2 == 1)
//		{
//			sum += flag * (1.0 / i);
//		}
//		else
//		{
//			sum -= flag * (1.0 / i);
//		}
//	
//	}
//
//	printf("%lf\n", sum);
//
//	return 0;
//}


//编写程序数一下 1到 100 的所有整数中出现多少个数字9
#include <stdio.h>
int main()
{
	int i = 0;
	int count = 0;
	for (i = 1; i <= 100; i++)
	{
		if (i % 10 == 9) //个位是9
		{
			count++;
		}
		if (i / 10 == 9) //十位是9
		{
			count++;
		}
		//这里不能使用 else if，因为99个位和十位都算
		//如果使用 else if，99只会计算一次
	}

	printf("%d\n", count);

	return 0;
}