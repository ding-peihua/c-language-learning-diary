#pragma once
//游戏相关的自定义函数 声明 写到game.h中


#include <stdio.h> 
#include <stdlib.h> //rand函数需要的头文件
#include <time.h> //time函数需要的头文件
//直接把头文件包含在自定义头文件中，当别的文件包含本头文件时，
//这个头文件里的头文件也会被包含


//定义宏常量：行数ROW  列数COL 
//,之后想修改，直接修改头文件里的这两个宏常量就可以
#define ROW 3 
#define COL 3


// 1.  InitBoard(); 用于初始化数组，将空格全部初始化为空格
void InitBoard(char board[ROW][COL], int row, int col);


// 2.  DisplayBoard()：打印数组（棋盘）
void DisplayBoard(char board[ROW][COL], int row, int col);


// 3. 玩家下棋：
void PlayerMove(char board[ROW][COL], int row, int col);


// 4. 电脑下棋
void ComputerMove(char board[ROW][COL], int row, int col);


// 5. 判断输赢(返回对应的符号)：
//玩家赢 -- ‘*’
//电脑赢 -- ‘#’
//平局 -- ‘Q’
//继续 -- ‘C’
char IsWin(char board[ROW][COL], int row, int col);