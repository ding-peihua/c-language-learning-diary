#pragma once

////定义一个静态顺序表：使用定长数组存储元素
////因为静态顺序表使用定长数组存储元素，对空间的运用不够灵活
////所以在实际情况下，静态顺序表并不常用（不够实用）
//
//#define N 1000  //“表”的大小的值
//typedef int SLDataType;  //定义顺序表中存储的类型，这里是int类型
//
//struct SeqList
//{
//	SLDataType a[N]; //定义表的大小
//	int size; //记录表中存储了多少个有效数据
//};


//将需要的头文件都包含在 SeqList.h 头文件中
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


//定义一个动态顺序表：使用动态开辟的数组存储元素
typedef int SLDataType;  //定义顺序表中存储的类型，这里是int类型

typedef struct SeqList
{
	//定义一个顺序表类型的指针，指向动态开辟的数组
	SLDataType* a; 
	
	int size; //记录表中存储了多少个有效数据
	int capactity; //容量空间的大小
}SL;


//数据结构 --> 管理数据 --> 增删查改

//顺序表初始化  --  头文件中声明
void SLInit(SL* ps); 

//顺序表销毁 --  头文件中声明
//内存是动态开辟地，不销毁的话可能会导致内存泄漏)
void SLDestroy(SL* ps);

//写一个测试函数(声明)，方便检查各步骤有没有问题：
void SLPrint(SL* ps);

//尾插（声明） -- 将值插入顺序表尾部：
void SLPushBack(SL* ps, SLDataType x);

//尾删（声明） -- 将值从顺序表尾部删除：
void SLPopBack(SL* ps);

//头插（声明） -- 将值插入顺序表头部：
void SLPushFront(SL* ps, SLDataType x);

//头删（声明） -- 将值从顺序表头部删除：
void SLPopFront(SL* ps);

//在指定位置（pos）插入想要的值（x）
void SLInsert(SL* ps, int pos, SLDataType x);

//查找x这个值在顺序表中的下标是多少：
int SLFind(SL* ps, SLDataType x); //返回找到的下标

//删除指定位置（pos）的值：
void SLErase(SL* ps, int pos);

//把某个位置（pos）的值修改为某值（x）
void SLModify(SL* ps, int pos, SLDataType x);