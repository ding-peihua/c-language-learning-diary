#define _CRT_SECURE_NO_WARNINGS 1


//力扣：27. 移除元素
int removeElement(int* nums, int numsSize, int val) {
    //原地：不能额外开空间，即空间复杂度为O(1)

    //思路：有原数组a，有个src指针指向初始位置
    //      另外一个dst指针也指向初始位置
    //      src开始遍历，遇到不是val的值就付给dst，src和dst ++
    //      src遇到val后，就不付给dst了，src直接++，dst不动
    //      一直持续这套操作，直到遍历完数组
    //直接在原数组上操作，外加两个指针

    //先创建两个指针：
    int src = 0;
    int dst = 0;

    while (src < numsSize)
        //src遍历完元素为止：
    {
        if (nums[src] != val)
            //如果src位置元素不是val
        {
            //把该值赋给dst位置
            nums[dst++] = nums[src++];
            //后置++，赋值完后顺便++
        }
        else
            //如果src位置元素是val
        {
            //直接src++，判断下一位：
            src++;
        }
    }

    return dst; //dst就是移除后数组的新长度
}



//力扣：88. 合并两个有序数组
//非递减可以是这种情况：1 2 2 2 5 6 
//要求时间复杂度为 O(m + n)

//思路：两个数组从尾部依次比较，
//取大的值依次插入(m+n)数组尾部（相等顺便取哪个都行）
//比到其中一个数组结束就结束

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {

    int end1 = m - 1; //nums1数组末下标
    int end2 = n - 1; //nums2数组末下标
    int end = m + n - 1; //合并后数组末下标

    while (end1 >= 0 && end2 >= 0)
        //两个数组都有元素的话就继续比较归并
    {
        if (nums1[end1] > nums2[end2])
            //两个数组末位置元素进行比较
        {
            //如果第一个数组的末元素大于第二个的
            //则把第一个数组末元素放到自己数组末尾（相当于不变）
            nums1[end--] = nums1[end1--];
            //移动完成后两个对应末下标进行--
        }
        else
        {
            //如果第二个元素的末元素比较大，
            //则把该元素放到第一个元素末尾，
            //因为是把两个数组归并到第一个数组
            nums1[end--] = nums2[end2--];
        }
    }

    //这个循环结束后，如果第二个数组先遍历完，
    //则说明已经排好序了，
    //因为数组一本来就是有序的，数组二插入进去时也是有序插入的

    //如果第二个数组有元素比较小
    //则可能第一个数组会先遍历完，
    //这时要把第二个数组剩下元素移到第一个数组元素的前面
    while (end2 >= 0)
    {
        nums1[end--] = nums2[end2--];
    }
}