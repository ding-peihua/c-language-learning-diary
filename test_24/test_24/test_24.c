#define _CRT_SECURE_NO_WARNINGS 1

//包含我们写的 SeqList.h 头文件
#include "SeqList.h"

//分组测试，测试不同的函数--SLPushBack 和 SLPopBack 函数
void TestSeqList1()
{
	SL sl;  //创建顺序表类型变量

	//使用 SLInit函数 初始化顺序表类型变量
	//注意传递的是变量的地址，防止形参改变实参不改变
	SLInit(&sl);

	//使用尾插函数SLPushBack：
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	//此时再调用会进行扩容：
	SLPushBack(&sl, 5);
	SLPushBack(&sl, 6);
	//调用测试函数SPrint查看是否插入成功：
	SLPrint(&sl);

	//测试尾删函数SLPopBack：
	SLPopBack(&sl);
	SLPopBack(&sl);
	//调用测试函数SPrint查看是否“删除”成功：
	SLPrint(&sl);

	//测试完后使用SLDestroy函数销毁顺序表：
	SLDestroy(&sl);
}

//分组测试，测试不同的函数--SLPushFront函数
void TestSeqList2()
{
	SL sl;  //创建顺序表类型变量

	//使用 SLInit函数 初始化顺序表类型变量
	//注意传递的是变量的地址，防止形参改变实参不改变
	SLInit(&sl);

	//使用尾插函数SLPushBack：
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	//此时再调用会进行扩容：
	SLPushBack(&sl, 5);
	SLPushBack(&sl, 6);
	//调用测试函数SPrint查看是否插入成功：
	SLPrint(&sl);

	//测试头插函数SLPushFront：
	SLPushFront(&sl, 10);
	SLPushFront(&sl, 20);
	//调用测试函数SPrint查看是否“删除”成功：
	SLPrint(&sl);

	//测试完后使用SLDestroy函数销毁顺序表：
	SLDestroy(&sl);
}


//分组测试，测试不同的函数--SLPopFront函数
void TestSeqList3()
{
	SL sl;  //创建顺序表类型变量

	//使用 SLInit函数 初始化顺序表类型变量
	//注意传递的是变量的地址，防止形参改变实参不改变
	SLInit(&sl);

	//使用尾插函数SLPushBack：
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	//此时再调用会进行扩容：
	SLPushBack(&sl, 5);
	SLPushBack(&sl, 6);
	//调用测试函数SPrint查看是否插入成功：
	SLPrint(&sl);

	//测试头删函数SLPopFront：
	SLPopFront(&sl);
	SLPopFront(&sl);
	//调用测试函数SPrint查看是否“删除”成功：
	SLPrint(&sl);

	//测试完后使用SLDestroy函数销毁顺序表：
	SLDestroy(&sl);
}

//分组测试，测试不同的函数--SLInsert函数
void TestSeqList4()
{
	SL sl;  //创建顺序表类型变量

	//使用 SLInit函数 初始化顺序表类型变量
	//注意传递的是变量的地址，防止形参改变实参不改变
	SLInit(&sl);

	//使用尾插函数SLPushBack：
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	//此时再调用会进行扩容：
	SLPushBack(&sl, 5);
	SLPushBack(&sl, 6);
	//调用测试函数SPrint查看是否插入成功：
	SLPrint(&sl);

	//测试指定增加函数SLInsert：
	SLInsert(&sl, 2, 100);
	//调用测试函数SPrint查看是否“删除”成功：
	SLPrint(&sl);

	//int x;
	//scanf("%d", &x);
	//int pos = SLFind(&sl, x);
	//if (pos != -1)
	//{
	//	SLInsert(&sl, pos, x * 10);
	//}
	//SLPrint(&sl);

	//测试完后使用SLDestroy函数销毁顺序表：
	SLDestroy(&sl);
}


//分组测试，测试不同的函数--SLFind函数
void TestSeqList5()
{
	SL sl;  //创建顺序表类型变量

	//使用 SLInit函数 初始化顺序表类型变量
	//注意传递的是变量的地址，防止形参改变实参不改变
	SLInit(&sl);

	//使用尾插函数SLPushBack：
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	//此时再调用会进行扩容：
	SLPushBack(&sl, 5);
	SLPushBack(&sl, 6);
	//调用测试函数SPrint查看是否插入成功：
	SLPrint(&sl);

	//测试指定增加函数SLFind：
	int pos = SLFind(&sl, 2);
	
	printf("2在元素中是第%d个元素", pos+1);

	//测试完后使用SLDestroy函数销毁顺序表：
	SLDestroy(&sl);
}

//分组测试，测试不同的函数--SLErase函数
void TestSeqList6()
{
	SL sl;  //创建顺序表类型变量

	//使用 SLInit函数 初始化顺序表类型变量
	//注意传递的是变量的地址，防止形参改变实参不改变
	SLInit(&sl);

	//使用尾插函数SLPushBack：
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	//此时再调用会进行扩容：
	SLPushBack(&sl, 5);
	SLPushBack(&sl, 6);
	//调用测试函数SPrint查看是否插入成功：
	SLPrint(&sl);

	int x;
	scanf("%d", &x);
	//配合SLFind函数，找到顺序表中某个值的下标
	int pos = SLFind(&sl, x);
	//再使用SLErase函数通过下标删除该值
	if (pos != -1)
	{
		SLErase(&sl, pos);
	}
	SLPrint(&sl);

	//测试完后使用SLDestroy函数销毁顺序表：
	SLDestroy(&sl);
}


//分组测试，测试不同的函数--SLModify函数
void TestSeqList7()
{
	SL sl;  //创建顺序表类型变量

	//使用 SLInit函数 初始化顺序表类型变量
	//注意传递的是变量的地址，防止形参改变实参不改变
	SLInit(&sl);

	//使用尾插函数SLPushBack：
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	//此时再调用会进行扩容：
	SLPushBack(&sl, 5);
	SLPushBack(&sl, 6);
	//调用测试函数SPrint查看是否插入成功：
	SLPrint(&sl);
	
	//测试指定增加函数SLInsert：
	SLModify(&sl, 2, 100);
	//调用测试函数SPrint查看是否“删除”成功：
	SLPrint(&sl);

	//测试完后使用SLDestroy函数销毁顺序表：
	SLDestroy(&sl);
}

//菜单：
void menu()
{
	printf("********************************\n");
	printf("1、尾插			2、头插\n");
	printf("3、头删			4、尾删\n");
	printf("7、打印			-1、退出\n");
	printf("********************************\n");
}


int main()
{
	//先创建一个顺序表：
	SL sl;

	//再对其进行初始化：
	SLInit(&sl);

	//创建一个变量接收菜单选项：
	int option = 0;

	do
	{
		//使用菜单：
		menu();
		//打印提示信息：
		printf("请选择想要进行的操作的序号：>");
		//接收序号：
		scanf("%d", &option);
		
		printf("\n");

		if (option == 1)
		{
			printf("请依次输入你要插入的数据个数:>");
			int n = 0; //接收数据个数
			scanf("%d", &n); //接收数据个数
			
			printf("\n");
			printf("请依次输入你要插入的数据\n");

			//知道数据个数后，直接使用for循环循环接收数据
			int x = 0;
			for (int i = 0; i < n; i++)
			{
				scanf("%d", &x);
				SLPushBack(&sl, x);
			}
		}
		else if (option == 7)
		{
			SLPrint(&sl);
		}

	} while (option != -1);
	

	//最后销毁顺序表：
	SLDestroy(&sl);


	//TestSeqList1();
	//TestSeqList2();
	//TestSeqList3();
	//TestSeqList4();
	//TestSeqList5();
	//TestSeqList6();
	//TestSeqList7();

	return 0;
}