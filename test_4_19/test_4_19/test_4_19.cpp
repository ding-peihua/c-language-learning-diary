﻿#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//#include <stdlib.h>;
//#include <time.h>
//
//void menu() // 定义一个菜单函数，调用时打印一个菜单
//{
//	printf("*******************************************\n");
//	printf("************ 1.play	0.exit ************\n");
//	printf("*******************************************\n");
//}
//
//void game()// 自定义一个函数，完成 玩游戏代码
//{
//	//1. 生成随机数
//	// 通过rand()函数生成随机数，调用rand()函数之前要先调用srand()函数
//	// 而srand()函数设置随机数生成起点时，需要time函数返回一个时间戳
//	// time函数不想用，可以直接给time()函数传递空指针(NULL)
//	int ret = rand() % 100 + 1;
//	// 利用库函数rand()生成随机数
//	// 随机数的范围是：0~32767（7FFF）
//
//	// 随机数 % 100 后余数只能是 0~99的数，在加1 就是 1~100的数，把随机数控制在1~100之间
//	//printf("%d\n", ret);
//
//	//2, 猜数字
//	int guess = 0;
//	while (1) // 死循环，一直猜，直到猜大了break跳出循环
//	{
//		printf("请猜数字：>");
//		scanf("%d", &guess); //输入猜的数字
//		if (guess > ret)
//		{
//			printf("猜大了\n");
//		}
//		else if (guess < ret)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//	}
//	
//}
//
//int main()
//{
//	//打印菜单
//	//1.玩游戏
//	//0.退出游戏
//	int input = 0; // 打印菜单后让玩家选择1或0
//
//	srand((unsigned int)time(NULL));
//	// 使用rand()函数前最好使用srand()函数，用于设置随机数的生成起点
//	// 要给srand传送一个变化的值，值得是无符号整型（unsigned int），计算机上的时间是时刻发生变化的,可用其作为()中的值
//	// 
//	// time()函数可以返回时间戳，因为时间是一直在变化的，所以时间戳也是一直变化的
//	// time()的()中要写指针，可以用NULL空指针
//	// time()返回值是time_t类型的，本质是一个longlong整型，所以可以作为srand函数()中的值
//	//
//	// srand()函数最好不要频繁调用，会一直重新设置随机数得生成起点，
//	// 导致生成的随机数太接近不够随机，且调用得太快有可能会生成一样得随机数
//	// 所以srand() 只调用一次就好了，可以放在主函数中
//
//	do // 先玩一把再说，再看要不要再玩,使用do while循环
//	{
//		menu(); //自定义一个菜单函数
//		printf("请选择：>");
//		scanf("%d", &input); // 1 0
//		switch (input)
//		{
//
//		case 1: {
//			game(); // 自定义一个函数，完成 玩游戏代码
//			break;
//		}
//		case 0: {
//			printf("退出游戏\n");
//			break;
//		}
//		default: {
//			printf("选择错误\n");
//			break;
//		}
//
//		}
//
//	} while (input); // 选了1（非0），条件成立，再玩一次，选0则不玩退出游戏
//
//
//	return 0;
//}

//goto语句：
//#include <stdio.h>
//
//int main()
//{
//again://（顶格写）
//	printf("hehe\n");
//	goto again; //跳转到again的地方去
//	//随意跳转的goto语句
//	//标记跳转的标号：again
//
//	return 0;
//}

////关机程序
////1. 程序运行起来后，1分钟内电脑关机
////2. 如果输入：我是猪，就取消关机
//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h> // 类似java的import进入需要用的类
//
//int main()
//{
//	char input[20] = { 0 }; // 接收“我是猪”，哈哈哈
//	//程序倒计时关机
//	system("shutdown -s -t 60"); // 用system函数调用系统的倒计时关机命令
//again:
//	printf("请注意，你的电脑在1分钟内关机，如果输入：我是猪，就取消关机\n");
//	scanf("%s", input); // 数组名本来就是地址，不用加&
//
//	if (strcmp(input, "我是猪") == 0) // strcmp这里需要<string.h>头文件
//	{
//		system("shutdown -a"); // 调用系统的取消关机命令
//		// system这里需要<stdlib.h>头文件
//	}
//	else
//	{
//		goto again;
//	}
//	return 0;
//}


////关机程序
////1. 程序运行起来后，1分钟内电脑关机
////2. 如果输入：我是猪，就取消关机
//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h> // 类似java的import进入需要用的类
//
//int main()
//{
//	char input[20] = { 0 }; // 接收“我是猪”，哈哈哈
//	//程序倒计时关机
//	system("shutdown -s -t 60"); // 用system函数调用系统的倒计时关机命令
//
//	while (1) // 条件非0，一直为真，死循环
//	{
//		printf("请注意，你的电脑在1分钟内关机，如果输入：我是猪，就取消关机\n");
//		scanf("%s", input); // 数组名本来就是地址，不用加&
//
//		if (strcmp(input, "我是猪") == 0) // strcmp这里需要<string.h>头文件
//		{
//			system("shutdown -a"); // 调用系统的取消关机命令
//			 //system这里需要<stdlib.h>头文件
//			
//			break;
//		}
//	}
//
//
//	return 0;
//}

//ctrl + 先按k + 再按c = 注释 
//ctrl + 先按k + 再按u = 取消注释 

//练习1：（新思路）
//
//#include <stdio.h>
//
//int main()
//{
//	先产生1~100之间的数字
//	int i = 0;
//	for ( i = 3; i <= 100; i+=3)
//	{
//		printf("%d  ", i);
//	}
//	return 0;
//}

// 练习2：（新思路）
//#include <stdio.h>
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	// 输入
//	scanf("%d %d %d", &a, &b, &c);
//	// 计算（不用比较来进行排序） 
//	// 直接按打印的顺序把最大值赋给打印时的最大值，拍下去
//	if (a < b) 
//	{
//		int tmp = a; 
//	// 调换两个变量的值时，需要创建一个中间值存储其中一个变量
//
//		a = b;
//		b = tmp; 
//	// a已换成了b的值，所以用存储了a值的中间值tmp来进行赋值
//	}
//	if (a < c)
//	{
//		int tmp = a; // 局部变量tmp会自动销毁
//		a = c;
//		c = tmp;
//	}
//	//上面两个循环之后，保证最大值在a中，打印时就把a放最前就好了
//	if (b < c)
//	{
//		int tmp = b;
//		b = c;
//		c = tmp;
//	}
//	// 把第二大的值放在b中，最小值放c中
//
//	//输出
//	printf("%d %d %d\n", a, b, c); // 大 -> 小
//	return 0;
//}


// 打印100 - 200之间的素数
// 素数 - 质数
// 只能被1和它本身整除的数是质数
// 判断i是否为素数：用 2到i-1 之间的数字去试除 i，如果能整除则i不是素数
// 2到i-1 之间的数字 都不能整除i，则i是素数
//#include <stdio.h>
//#include <math.h>
//int main()
//{
//	int i = 0;
//	for ( i = 101; i <= 200; i+=2) //生成100 - 200之间的素数
//	{
//		// 判断i是否为素数
//		// 循环产生 2到i-1
//		int j = 0;
//		int flag = 1; // flag=1 则 i是素数
//		for ( j = 2; j <= sqrt(i); j++) // 这个循环产生的就是 2到i-1 的数
//			//    2  到   i - 1 (用<=) 
//			//sqrt 是库函数，是开平方的意思 ，需要头文件<math.h>
//			//sqrt(i)就是对i开平方
//		{
//			if (i % j == 0) // 用i模上一个j，看j能不能整除i，有余数则表示不能整除
//			{
//				flag = 0; // flag=0 则 i不是素数
//				break; 
//				// 有一个j把i整除了，说明i已经不是素数了，所以不用再循环了，break跳出循环
//			}
//		}
//		if (flag == 1) // 到这里说明i是素数，把它打印出来
//		{
//			printf("%d  ", i);
//		}
//	}
//	return 0;
//}

////给定两个数，求这两个数的最大公约数： 
//#include <stdio.h>
//
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n); // 18 24
//	//计算
//	int k = (m > n ? n : m); // 找出较小值
//	// 使用 条件操作符（三目操作符）条件成立把n赋给k，反之把m赋给k
//
//	while (1)
//	{
//		if (m % k == 0 && n % k == 0) // 使两个数都能被较小值整除
//		{
//			break; // 都能被整除则停止
//		}
//		k--; // 不能整除则较小值自减，再次循环，直到两个数都能被较小值整除
//	}
//	printf("%d\n", k);
//	return 0;
//}


//给定两个数，求这两个数的最大公约数： 
//#include <stdio.h>
//
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n); // 18 24
//	//计算
//	int k = 0;
//	while (k = m % n) // 求余数，当余数为0后停止循环，此时，n为最大公约数
//	{
//		m = n; // 除数赋给被除数
//		n = k; // 余数赋给除数
//
//		// 赋值后再求余数，直到余数为0
//	}
//	printf("%d\n", n);
//	return 0;
//}