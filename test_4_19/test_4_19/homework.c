#define _CRT_SECURE_NO_WARNINGS 1

//// homework7：
//// 
//#include <stdio.h>
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10}; // 升序（有序数组）
//	// 下标：     0 1 2 3 4 5 6 7 8 9
//
//	int find = 7; // 在数组中要找的值
//	int i = 0; // 数组下标
//
//	int sz = sizeof(arr) / sizeof(arr[0]); 
//	//		数组总大小 除以 数组中单个元素大小 = 元素个数
//
//	// 1.求左下标和右下标
//	int left = 0; // 左下标
//	int right = sz - 1; //右下标
//
//	int flag = 0; // 用来设置未找到情况下的处理
//
//	// 循环查找：
//	while (left <= right)
//		// 条件成立说明被左右下标包裹中的数组还有值，还有值就继续循环查找
//	{
//		// 2.确定中间元素下标：
//		int mid = (left + right) / 2;
//		//进行查找：
//		//找到：
//		if (arr[mid] == find)
//		{
//			printf("找到了，该值在数组中对应的下标使：%d\n", mid);
//			flag = 1;
//			break;
//		}
//		//未找到：
//		//中间值小于要找的值，排除mid和小于mid左边的值
//		else if (arr[mid] < find)
//		{
//			left = mid + 1; // 调整左下标
//		}
//		//中间值大于要找的值，排除mid和大于mid右边的值
//		else
//		{
//			right = mid - 1; // 调整右下标
//		}
//	}
//	if (flag == 0)
//	{
//		printf("没找到\n");
//	}
//
//	return 0;
//}

////求10 个整数中最大值:
//
//#include <stdio.h>
//
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	int num3 = 0;
//	int num4 = 0;
//	int num5 = 0;
//	int num6 = 0;
//	int num7 = 0;
//	int num8 = 0;
//	int num9 = 0;
//	int num10 = 0;
//
//	//输入
//	scanf("%d %d %d %d %d %d %d %d %d %d", &num1, &num2, &num3, &num4, &num5, &num6, &num7, &num8, &num9, &num10);
//
//	//把最大值赋给num1：
//	if (num1 < num2)
//	{
//		num1 = num2;
//	}
//
//	if (num1 < num3)
//	{
//		num1 = num3;
//	}
//
//	if (num1 < num4)
//	{
//		num1 = num4;
//	}
//
//	if (num1 < num5)
//	{
//		num1 = num5;
//	}
//
//	if (num1 < num6)
//	{
//		num1 = num6;
//	}
//
//	if (num1 < num7)
//	{
//		num1 = num7;
//	}
//
//	if (num1 < num8)
//	{
//		num1 = num8;
//	}
//
//	if (num1 < num9)
//	{
//		num1 = num9;
//	}
//
//	if (num1 < num10)
//	{
//		num1 = num10;
//	}
//
//	printf("这10个整数中的最大值为：%d", num1);
//
//	return 0;
//}

////计算1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99 - 1 / 100 的值，打印出结果
//
//#include <stdio.h>
//
//int main()
//{
//	int i = 1; // 分子
//	int j = 0; // 分母
//	double add = 0;
//	double sum = 0;
//
//	// 运用循环生成1-99的分母
//	for (j = 1; j <= 99; j++)
//	{
//		add = i / j;
//		sum += add;
//	}// 计算：1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99
//
//	double end = 1 / 100;
//	
//	double result = sum - end;
//
//	printf("%.2lf\n", result);
//
//	return 0;
//}

// 编写程序数一下 1到 100 的所有整数中出现多少个数字9

#include <stdio.h>

int main()
{
	int i = 0;
	int count = 0; // 统计出现多少个9

	//使用循环生成1-100
	for ( i = 1; i <= 100; i++)
	{
		if (i % 10 == 9)
		{
			count++;
		}
	}

	printf("9出现个数：%d", count);

	return 0;
}