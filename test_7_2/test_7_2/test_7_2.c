#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int main()
{
	//定义变量：
	char killer = 0; //凶手

	//依次假定每个人是凶手：
	for (killer = 'a'; killer < 'd'; killer++)
	//因为 a b c d 的ASCII码值是连着的，所以a+1==b，
	//以此类推，依次假定每个人是凶手，判断情况

	{
		//把4个情况列出来：
		if ((killer != 'a') + (killer == 'c') + (killer == 'd') + (killer != 'd') == 3)
		//把4句话，4个情况列出来，情况1假3真，真为1，假为0，4种情况“相加”==3,符合就是凶手进行打印
		{
			//符合则进行打印
			printf("凶手是：%c\n", killer);
			break;
		}
	}


	return 0;
}