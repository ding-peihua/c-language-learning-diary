#define _CRT_SECURE_NO_WARNINGS 1

//包含类头文件：
#include "Stack.h"

//类函数（方法）实现文件：

/*
* 通过命名空间（类名）和作用域限定符，
* 来指定实现类中对应的函数（方法）
* （类定义的也是一个域）
*/

//栈初始化函数（方法）-- 实现
void Stack::Init() 
//指定实现Stack类中的Init函数（方法）
{
	a = 0;
	top = 0;
	capacity = 0;

	//a、top、capacity都是栈的成员变量
}

//出栈函数（方法）-- 实现
void Stack::Push(int x) 
//指定实现Stack类中的Push函数（方法）
{
	//…… 
}