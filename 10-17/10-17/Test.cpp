#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <iostream>
using namespace std;

//使用C++实现一个栈：

//C语言中实现栈：
//struct Stack
//{
//	int* a;
//	int top;
//	int capacity;
//};
//
////C语言中栈相应的函数：
//void StackInit(struct Stack* ps);
//void StackPush(struct Stack* ps, int x);
//……

/*
* 由此可见：C语言中栈的数据和方法是分离的
* 数据 -- struct Stack
* 方法 -- StackInit、StackPush等等
*		（C语言 -- 面向过程）
* 
* C++中：兼容C语言struct的所有用法，
* 不仅兼容，而且还将struct升级成了“类”
*/

//C++中实现栈：
//struct Stack
//{
//	int* a;
//	int top;
//	int capacity;
//
//	/*
//	* 2、类中可以定义与该类相关的函数（方法）
//	* 
//	* （1）不需要像C语言中将数据和函数（方法）分离。
//	* 
//	* （2）因为函数（方法）被包含在类中，
//	* 所以不再需要像C语言中必须将函数定义在全局中，
//	* 所以函数名不需要定义得像 StackInit（堆的初始化函数）一样，
//	* 来特指是谁的初始化函数，直接在类中定义为 Init 即可，
//	* 该 Init函数 在Stack（堆）类的域中，
//	* 就是只属于Stack的Init函数
//	*/
//
//	void Init()
//	{
//		a = 0;
//		top = 0;
//		capacity = 0;
//	}
//
//	void Push(int x)
//	{
//		//……
//	}
//
//};


//int main()
//{
//	//C语言中调用struct“类型”：
//	struct Stack s1;
//
//	//C++中调用struct“类”：
//	Stack s2; 
//	/*
//	* 1、类名就是类型，Stack就是类型，不需要加struct
//	* struct后的名称就是类名，要调用类，直接调用类名即可，
//	* 不用加struct
//	*/
//	
//	//C++中：
//	//调用类中的变量或者函数的方法
//	//和调用结构体成员的方法一样：
//	s2.Init(); //调用栈类中的Init函数 （C++）
//
//	//调用栈类中的出栈Push函数（C++）：
//	s2.Push(1);
//	s2.Push(2);
//	s2.Push(3);
//	s2.Push(4);
//
//	//C语言中：
//	struct Stack s1; 
//	//声明时还需加struct(不加typedef)的话
//
//	//调用的是全局中的函数：
//	StackInit(&s1);
//	StackPush(&s1, 1);
//	StackPush(&s1, 2);
//	StackPush(&s1, 3);
//	
//	return 0;
//}

//假设定义一个链表结点类：
struct ListNode
{
	ListNode* next; 
	/*
	* 这里可以直接使用类名类型来定义变量了，
	* 而不是C语言中的：struct ListNode* next; 
	*/
	
	int val;
};


//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	/*
//	* C++中一般会在成员变量前加一个 “_” ，
//	* 表示该变量为类内部的成员变量，
//	* 防止在成员函数调用时和形参命名冲突
//	*/
//	
//	int _year;
//	int _month;
//	int _day;
//
//};

//int main()
//{
//	Date d; 
//	
//	d.Init(2023, 10, 17);
//
//	return 0;
//}



/*
* C++中，虽然可以使用struct定义类，
* 但还是更喜欢通过class来定义类，
* 
* class中由两部分构成：
* 变量（成员变量）和函数（成员函数），
* 两者统称类的成员
* 
* 类会通过访问限定符来实现封装，
* 访问限定符分为：
* public（共有）、protected（保护）、private（私有）
* 
* public（共有）：类中和类外都可以进行访问
* protected（保护）：类中可以访问，类外不能访问
* private（私有）：类中可以访问，类外不能访问
* 
* 在当前阶段，可以认为protected和private是没有区别的，
* 等后面了解了继承才能够知道两者的区别
* 
*			struct 和 class 的区别：
* 1、class的默认访问权限为private，但实践中建议还是明确写上限定符
*	 struct默认访问权限为public（为了兼容C语言）
* 2、（其它就没有什么大的区别）
* 
*			C++中设置访问限定符的目的：
* C语言中没有访问限定符的概念，数据和方法是分离的，
* 有时实现一个目标可以通过数据完成，也可以通过方法完成，
* 程序员素养比较高的话应该是使用方法（函数）完成，这是比较规范的
* 
* 所以C++中类的成员默认是private私有的，无法在外部调用数据，
* 在解决一个问题时就只能通过在类中定义方法（函数）来完成，
* 提高代码的规范性
*/

//class Stack
//{
//private: 
//	//私有：让以下三个成员变量的权限为私有
//	int* a;
//	int top;
//	int capacity;
//
//public:
//	//共有：让以下的两个成员函数的权限为共有
//	void Init()
//	{
//		a = 0;
//		top = 0;
//		capacity = 0;
//	}
//
//	void Push(int x)
//	{
//		//……	
//	}
//
//	bool Empty()
//	{
//		return top == 0;
//	}
//
///*
//* 一个访问限定符的作用范围为：
//* 如果后面还有限定符 -- 当前限定符到下个限定符
//* 如果后面没有限定符 -- 当前限定符到 “}” 
//*/
//};

//int main()
//{
//	Stack s1;
//	 
//	//权限为共有的成员可以在类外部进行调用：
//	s1.Init(); //Init函数为共有
//	s1.Push(1); //Push函数为共有
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//
//	//权限为私有或保护的成员不可以在类外部进行调用：
//	s1.a = 0; //成员变量a为私有
//}


//类的实例化：

//C++中 “{}” 定义的都是域

//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//	//这里这些成员变量只是声明，还没有开辟空间
//	/*
//	* 变量是否定义（实现）要看是否有开辟空间
//	* （在内存中开辟空间）
//	*/
//};

/*
* 类 和 对象  -->  一对多的关系
* 一个类可以有多个对象，可以想象类是设计图，
* 对象是通过设计图建出来的房子，一个设计图
* 可以设计建出多个类似的房子
*/


//class A
//{
//private:
//	char _ch;
//	int _a;
//}; //内存对齐问题


class B
{}; 
//没有成员变量的类（空类）的大小：1
/*
* 没有成员变量的类，说明该类不需要存储数据，
* 虽然该类没有成员变量，但还是可以创建该类的对象，
* 为了要表示一个空类的对象，证明空类B的对象存在，
* 就需要为这个空类对象开一个字节大小的空间，
* 这个字节不存储有效数据，仅标识定义的对象存在过
*/


class C
{
public:
	void f()
	{};
};
//有成员函数没有成员变量的类的大小：1
/*
* 该类还是没有成员变量，所以本质还是一个空类，
* 虽然有成员函数，但成员函数并不存放在该类中，
* 而是存储在公共代码区中，所以该类大小为1个字节
*/



//int main()
//{
//	Date d1; //定义（实例化）一个对象
//	Date d2; //再定义（实例化）一个对象
//	Date d3; //再再定义（实例化）一个对象
//
//	/*
//	* 定义一个对象后，对象中的成员变量
//	* 作为对象一部分，一起开辟了空间，
//	* 这时成员变量才被定义（实现）了
//	*/
//
//	d1.Init(2023, 10, 7);
//	d2.Init(2022, 10, 7);
//	/*
//	* （同个类）不同对象的成员函数是一样的：
//	* 这里d1和d2调用的是同一个函数
//	*（汇编指令call调用的是同一函数地址）
//	*/
//
//	//假设将成员变量的权限设置为public：
//	d1._year++;
//	d2._year++;
//	/*
//	* （同个类）不同对象的的成员变量是不一样的：
//	* 这里 d1的_year 和 d2的_year 不是同一个
//	*/
//
//	return 0;
//}


//int main()
//{
//	Date d1; //定义（实例化）一个对象
//	Date d2; //再定义（实例化）一个对象
//	Date d3; //再再定义（实例化）一个对象
//
//	//使用sizeof计算类的大小：
//	cout << sizeof(d1) << endl; //8个字节
//	/*
//	* sizeof计算类对象的大小和
//	* sizeof计算结构体大小是一样的，
//	* 所以要考虑内存对齐
//	* 
//	* sizeof计算类对象的大小时，
//	* 只会计算成员变量的大小，再考虑内存对齐，
//	* 不会考虑成员函数的大小
//	* 
//	* 成员函数不在对象里面，因为同一个类的不同对象
//	* 调用的是同一个成员函数（同名函数的情况下），
//	*（汇编指令call调用的是同一函数地址）
//	* 但同一个类的不同对象各自的成员变量是独立的，
//	* 类对象d1中的成员变量 和 类对象d2中的成员变量
//	* 是不同的，所以sizeof计算类对象时计算的是其成员变量
//	* 
//	* 成员变量存在对象中，而成员函数不存在对象中的原因：
//	* 成员变量各个对象不同（独立），
//	* 但成员函数调用的都是同一个，
//	* 所以没必要在所有对象中都存成员函数的地址，
//	* 将其放在一个公共的区域（公共代码区）是更适合的，
//	* 公共代码区在编译完成后是一堆指令，
//	* 编译链接时就可以确定函数的地址
//	*/
//	cout << sizeof(A) << endl;  //A类大小：8个字节
//	cout << sizeof(B) << endl;  //B类大小：1个字节
//	cout << sizeof(C) << endl;  //C类大小：1个字节
//
//	return 0;
//}


class Date
{
public:
	//Date类初始化函数：
	void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	//Data类打印日期函数：
	void Print()
	{
		cout << _year << "_" << _month << "_" << _day << endl;
	}
		/*
		* d1 和 d2 都调用该成员函数，但结果却不同的原因：
		* 
		* 我们写的 void Print()函数，编译器编译时实际上是这样的：
		* void Print(Date* const this)，有一个隐藏的参数--this指针
		* （所有我们定义的成员函数都默认有一个this指针参数）
		* 
		* d1调用函数时，访问的是: &d1->_year、&d1->month、&d1->_day
		* 此时 this指针 就是 d1的指针 ，&d1是实参，Date* this是形参
		* 访问的是d1对象的_year、_month、_day
		* 
		* d2调用函数时，访问的是: &d2->_year、&d2->month、&d2->_day
		* 此时 this指针 就是 d2的指针 ，&d2是实参，Date* this是形参
		* 访问的是d2对象的_year、_month、_day
		* 
		* this指针是形参，是个局部变量，是存放在栈帧上面的
		* （VS编译器中，this指针被存放在ecx寄存器中）
		*/
	//编译器编译时的Print函数：
	void Print(Date* const this)
	{
		cout << this->_year << "_" << this->_month << "_" << this->_day << endl;
		/*
		* 注：不能显式写出this相关实参和形参，但可以在成员函数中显式写上this指针
		*（之后有地方会需要显式地在成员函数中写出this指针）
		*/
	}

private:
	int _year;
	int _month;
	int _day;

};

class A
{
private:
	char _ch;
	int _a;
};

int main()
{
	Date d1; //定义（实例化）一个对象
	Date d2; //再定义（实例化）一个对象

	d1.Init(2023, 10, 7);
	d2.Init(2022, 10, 7);

	d1.Print();
	d2.Print();
	
	//编译器编译时的426和427代码：
	d1.Print(&d1); //传送“this”指针
	d2.Print(&d2); //传送“this”指针

	return 0;
}


//对象用 “.” 访问成员；指针用 “->” 访问成员


/*
*				1、
* C语言数据和函数（方法）是分离的，
* 而C++通过类将数据（成员变量）和成员函数（成员方法）绑定在一起
* 
*				2、
* C语言调用函数时，需要将类型变量传给函数，
* 而C++因为有this指针的存在，不需要传类对象给成员函数
*/