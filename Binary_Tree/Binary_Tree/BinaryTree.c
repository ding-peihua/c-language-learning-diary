#define _CRT_SECURE_NO_WARNINGS 1

//包含二叉树头文件：
#include "BinaryTree.h"


//创建节点函数 -- 创建链式二叉树节点并对其初始化
//接收要放入节点中的值（x）
BTNode* BuyNode(int x)
{
	//开辟动态空间：
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	//检查是否开辟成功：
	if (node == NULL)
	{
		//开辟失败则打印错误信息：
		perror("malloc fail");
		//终止程序：
		exit(-1);
	}

	//将要放入节点的值x放入节点：
	node->val = x;
	//将节点的左右指针初始化为NULL：
	node->left = NULL;
	node->right = NULL;

	//返回开辟的节点地址：
	return node;
}



//递归结构遍历 -- 前序（先序）遍历
//先访问根节点 - 再访问左子树 - 最后访问右子树
//接收二叉树根节点地址（root）
void PrevOrder(BTNode* root)
{
	//递归遍历到空指针NULL（或空树）：
	if (root == NULL)
	{
		//打印当前为空节点：
		printf("NULL  ");

		//返回结束当前递归：
		return;
	}

	//进行前序遍历：
	//先访问根节点 -- 打印当前节点的值：
	printf("%d  ", root->val);

	//再访问左子树 -- 使用递归打印左子树：
	PrevOrder(root->left);

	//最后访问右子树 -- 使用递归打印右子树：
	PrevOrder(root->right);
}



//递归结构遍历 -- 中序遍历
//先访问左子树 - 再访问根节点 - 最后访问右子树
//接收二叉树根节点地址（root）
void InOrder(BTNode* root)
{
	//递归遍历到空指针NULL：
	if (root == NULL)
	{
		//打印当前为空节点：
		printf("NULL  ");

		//返回结束当前递归：
		return;
	}

	//进行中序遍历：
	//先访问左子树 -- 使用递归打印左子树：
	InOrder(root->left);

	//再访问根节点 -- 打印当前节点的值：
	printf("%d  ", root->val);

	//最后访问右子树 -- 使用递归打印右子树：
	InOrder(root->right);
}



//递归结构遍历 -- 后序遍历
//先访问左子树 - 再访问右子树 - 最后访问根节点
//接收二叉树根节点地址（root）
void PostOrder(BTNode* root)
{
	//递归遍历到空指针NULL：
	if (root == NULL)
	{
		//打印当前为空节点：
		printf("NULL  ");

		//返回结束当前递归：
		return;
	}

	//进行后序遍历：
	//先访问左子树 -- 使用递归打印左子树：
	PostOrder(root->left);

	//再访问右子树 -- 使用递归打印右子树：
	PostOrder(root->right);

	//最后访问根节点 -- 打印当前节点的值：
	printf("%d  ", root->val);
}



//节点数函数 -- 计算二叉树中的节点个数
//接收二叉树根节点地址（root）
int TreeSize(BTNode* root)
{
	//第二种方法：“分治”思想
	return root == NULL ? 0 : TreeSize(root->left) + TreeSize(root->right) + 1;
	//使用三目操作符，如果根节点为空返回0（个节点）
	//如果不为空则返回 TreeSize(root->left) + TreeSize(root->right) + 1
	//即 左子树节点的个数 + 右子树节点的个数 + 1（根节点） --  后序遍历
}



//第一种方法：递归遍历统计二叉树节点个数
/*
//节点数函数 -- 计算二叉树中的节点个数
//接收二叉树根节点地址（root）
int TreeSize(BTNode* root)
{
	//递归时每递归一次就会创建一次栈帧，
	//如果有初始化普通变量，每递归一次就会初始化一次，
	//所以要赋予普通变量常属性，使其成为静态成员变量，
	//局部的静态成员变量只会被初始化一次：
	static int size = 0;
	//但是这样该函数就只能调用一次，因为调用一次后，
	//该变量不会再初始化为0，直到程序结束才被销毁

	//那么可以把size定义为全局变量，
	//在主函数调用该函数前就要先将size初始化为0


	if (root == NULL)
		//如果根节点为空：
	{
		return 0; //返回0表0个节点
	}
	else
	{
		++size; //root不为空则个数++
	}

	//使用递归遍历计算（类似前序遍历）：
	TreeSize(root->left); //先遍历左子树
	TreeSize(root->right); //再遍历右子树

	//返回二叉树中节点个数：
	return size;
	
}
*/




//叶子节点数函数 -- 计算二叉树中的叶子节点个数
//接收二叉树根节点地址（root）
int TreeLeafSize(BTNode* root)
{
	if (root == NULL)
		//如果当前根节点为空：
		//（当前为空树）
	{
		//返回0 -- 无叶子节点：
		return 0;
	}

	if (root->left == NULL && root->right == NULL)
		//如果当前节点的左子树(左孩子)和右子树(右孩子)都为空：
	{
		//说明当前节点为叶子节点，左右指针都指向NULL：
		return 1; //返回1
	}

	//能执行到这说明当前节点为非空树非叶子节点 -- 分支节点

	//如果递归后当前节点为分支节点（非空树非叶子）,
	//使用递归返回其左子树和右子树的叶子节点个数：
	return TreeLeafSize(root->left) + TreeLeafSize(root->right);
}




//第k层节点数函数 -- 计算二叉树中第k层的节点个数
//接收二叉树根节点地址（root）和层数（k）
int TreeKLevel(BTNode* root, int k)
{
	//层数默认从第1层开始（根节点所在层数）
	//assert断言二叉树的层数大于0:
	assert(k > 0);

	//（递归时）遍历过程遇到空节点：
	if (root == NULL)
	{
		//已经遇到空节点就返回上一层递归
		return 0;
	}

	//到达目标层数后如果节点不为空：
	if (k == 1)
	{
		//返回1个当层节点：
		return 1;
	}

	//需知：“相对层数”
	//我们说的第k层是以根节点（第1层）为准的
	// "第一层的第三层 等于 第二层的第二层 等于 第三层的第一层"
	// 爷爷找孙子，可以先找孙子的爸爸，再让爸爸找儿子（爷爷的孙子）
	//所以有：当前树的第k层 = 左子树的第k-1层 + 右子树的第k-1层 
	//					（递归的降级）
	
	//当前树的第k层 = 左子树的第k-1层 + 右子树的第k-1层 ：
	return TreeKLevel(root->left, k - 1)
		 + TreeKLevel(root->right, k - 1);
}



//二叉树销毁函数 -- 对二叉树类型进行销毁
//接收二叉树根节点地址（root）
void TreeDestory(BTNode* root)
{
	//分三部分进行销毁：
	//当前节点、左子树、右子树
	
	//使用后序销毁二叉树：
	//最后才销毁根节点，
	//防止先销毁根节点后找不到左右子树，
	//导致最后不能销毁左右1子树

	if (root == NULL)
		//如果遇到空树，
		//或者递归到叶子节点的“左右NULL节点”：
	{
		//直接返回：
		return;
	}
	
	//使用后序遍历：
	//先遍历销毁当前左子树：
	TreeDestory(root->left);
	//再遍历销毁当前右子树：
	TreeDestory(root->right);
	//最后销毁当前节点：
	free(root);

	//置空操作放在调用该函数后手动进行
}



//查找指定节点值函数 -- 在二叉树中查找值为x的节点
//接收二叉树根节点地址（root）和 要查找的节点值（x）
BTNode* TreeFind(BTNode* root, BTDataType x)
{
	//如果是空树（或“左右NULL子树”）：
	if (root == NULL)
	{
		//那就不用找了，直接返回空：
		return NULL;
	}
	
	//递归过程中找到对应节点了：
	if (root->val == x)
	{
		//返回该节点指针：
		return root;
	}

	//为了防止找到后往回递归又返回
	//覆盖掉了找到的节点指针，
	//所以要创建一个变量存储找到的节点指针
	//方便最终返回该指针：
	
	//创建二叉树节点类型指针变量：
	BTNode* ret = NULL;

	//进行递归并用变量ret存储：
	//（如果在左子树中找到）
	ret = TreeFind(root->left, x);
	//如果找到了就不用再递归遍历右子树了，
	//判断并返回在左子树中找到的对应节点地址：
	if (ret != NULL)
		//ret不为空说明已经在左子树中找到了对应节点：
	{
		//进行返回，不在进行下面的右子树遍历：
		return ret;
	}
	
	//执行到这里说明左子树中未找到相应节点，
	//需再遍历右子树进行查找：
	ret = TreeFind(root->right, x);
	//如果在右边找到了：
	if (ret != NULL)
		//此时ret不为空，
		//说明在右子树中找到了对应节点：
	{
		//返回找到的节点地址：
		return ret;
	}

	//如果能执行到这，说明二叉树中没有该节点：
	return NULL; //返回空
}



//层序遍历函数 -- 对二叉树进行层序遍历
//接收二叉树根节点地址（root）
void LevelOrder(BTNode* root)
{
	//使用 队列 存储二叉树：“上一层带下一层”
	//（从上到下、从左到右存储二叉树）

	//先创建一个队列类型：
	Que q;
		
	//对队列进行初始化：
	QueueInit(&q);

	//先将二叉树根节点root放入队列中：
	if (root != NULL)
		//二叉树根节点不为空才能放入队列：
	{
		//使用队列入队函数QueuePush将根节点（指针）录入：
		QueuePush(&q, root);
	}
	
	//之后再使用while循环，进行层序遍历：
	while (QueueEmpty(&q) != true)
		//只要当前队列不为空（队列中还有节点指针）就继续层序遍历：
	{
		//使用队列的队头函数QueueFront获取队头的节点指针：
		BTNode* front = QueueFront(&q);

		//打印当前队头节点值：
		printf("%d ", front->val);
		
		//先录入当前节点左孩子：
		//如果当前节点左孩子不为空，就将其左孩子录入队列：
		if (front->left != NULL)
		{
			//使用队列入队函数QueuePush将当前左孩子录入队列：
			QueuePush(&q, front->left);
		}

		//再录入当前节点右孩子：
		//如果当前节点右孩子不为空，就将其右孩子录入队列：
		if (front->right != NULL)
		{
			//使用队列入队函数QueuePush将当前右孩子录入队列：
			QueuePush(&q, front->right);
		}

		//打印当前节点值 且 录入新左后节点指针 后，
		//将当前节点出队(队列出队函数QueuePop)，
		//对下个队头二叉树节点指针进行相同操作：
		QueuePop(&q);

		//（完成“上一层带下一层”，从上到下、从左到右存储二叉树）
	}

	//层序遍历完成后换行：
	printf("\n");

	//使用队列对二叉树遍历完成后，销毁队列：
	QueueDestroy(&q);
}



//判断完全二叉树函数 -- 判断该树是不是完全二叉树
//接收二叉树根节点地址（root）
int TreeComplete(BTNode* root)
{
	/*
	思路：利用层序遍历的特征进行判断
	遍历时如果有空节点，将空节点也进行遍历，
	看最终空节点的分布情况就能判断是不是完全二叉树
	1、遍历时如果非空节点是连续的，就是完全二叉树
	2、遍历时如果非空节点不连续，遍历时中间右出现空节点，就是非完全二叉树
	*/
	//先创建一个队列类型：
	Que q;
	//对队列进行初始化：
	QueueInit(&q);

	//先将二叉树根节点root放入队列中：
	if (root != NULL)
		//二叉树根节点不为空才能放入队列：
	{
		//使用队列入队函数QueuePush将根节点（指针）录入：
		QueuePush(&q, root);
	}

	//之后再使用while循环，进行层序遍历：
	while (QueueEmpty(&q) != true)
		//只要当前队列不为空（队列中还有节点指针）就继续层序遍历：
	{
		//使用队列的队头函数QueueFront获取队头的节点指针：
		BTNode* front = QueueFront(&q);

		//循环遍历过程中如果遇到空节点就终止循环：
		if (front == NULL)
			//front为空节点：
		{
			break; //终止循环
		}

		//使用队列入队函数QueuePush将当前左孩子录入队列：
		QueuePush(&q, front->left);
		//使用队列入队函数QueuePush将当前右孩子录入队列：
		QueuePush(&q, front->right);
		//将当前队头的节点类型出队，判断下个节点：
		QueuePop(&q);
	}

	/*
	执行到这时，队列中队头节点即空节点（NULL）
	这时再看该空节点 后面的所有节点 还有没有非空节点，
	后面的所有节点还有 非空节点 的话 -- 说明该二叉树不是连续的，不是完全二叉树
	后面的所有节点没有 非空节点 的话 -- 说明该树非空节点都是连续的，是完全二叉树
	*/

	//同样使用while循环进行操作：
	while (QueueEmpty(&q) != true)
		//只要当前队列不为空（队列中还有节点指针）就继续循环判断：
	{
		//使用队列的队头函数QueueFront获取队头的节点指针：
		BTNode* front = QueueFront(&q);

		//使用出队函数QueuePop进行出队操作：
		//（第一次出队时将队头的空节点NULL出队）
		QueuePop(&q);
		/*
		出队后如果当前队头节点为非空节点的话，
		说明该二叉树不是连续的，不是完全二叉树，
		则销毁队列并返回false:
		*/
		if (front != NULL)
			//当前队头节点为非空节点：
		{
			//销毁队列：
			QueueDestroy(&q);

			//返回false：
			return false;
		}
	}

	/*
	执行到这，说明之后已经没有非空节点了
	说明该树非空节点都是连续的，是完全二叉树，
	则销毁队列并返回true:
	*/
	//销毁队列：
	QueueDestroy(&q);
	//返回true：
	return true;
	//（ true 以int类型返回 -- 1）
	//（ false 以int类型返回 -- 0）
}



//计算高度函数 -- 计算当前链式二叉树的高度
//接收二叉树根节点地址（root）
int TreeHeight(BTNode* root)
{
	//思路：树的高度 = 左右子树中较高树的高度 + 1(根节点)

	//如果该树是空树，返回 0（层）：
	if (root == NULL)
		//根节点为空：
	{
		//返回 0（层）：
		return 0;
	}

	//方法二：使用递归计算左右子树高度并记录，再返回树的高度
	//递归计算左子树高度：
	int leftHeight = TreeHeight(root->left);
	//递归计算右子树高度：
	int rightHeight = TreeHeight(root->right);

	//再使用三目操作符判断后返回树的高度：
	return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	//树的高度 = 左右子树中较高树的高度 + 1(根节点)


	/*
	//方法一：使用三目操作符返回树的高度
	return TreeHeight(root->left) > TreeHeight(root->right)
		? TreeHeight(root->left) + 1 : TreeHeight(root->right) + 1;
	//三目操作符选出较高树，再 计算其高度 + 1 = 树的高度
	*/
	/*
	这样可以计算出该树的高度，但是其中含有大量的重复计算，
	判断较高树时已经递归求了左右子树的高度，
	在返回树的高度时又重新计算了一遍左右子树的高度，
	是极其不好的代码
	*/
}