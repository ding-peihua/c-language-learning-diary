#pragma once

//包含之后所需的头文件：
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


//二叉树的增删查改操作并不重要，应该重点了解二叉树的结构
// 为后面了解高阶数据结构做铺垫
//（之后高阶数据结构的搜索二叉树、AVL和红黑树才是专业的）

//指定二叉树节点存储数据的类型：
typedef int BTDataType;

//创建二叉树节点类型：
typedef struct BinaryTreeNode
{
	//左指针：指向该节点的左孩子
	struct BinaryTreeNode* left;

	//该节点存储的值：
	BTDataType val;

	//右指针：指向该节点的右孩子
	struct BinaryTreeNode* right;

}BTNode; //从命名为BTNode

//包含之前写的队列头文件
//（层序遍历需要用到队列）
#include "Queue_BT.h"
//（要包含在二叉树节点类型BTNode下方）
//（这样队列头文件展开后才能
//找到要存储的链式二叉树节点类型）


//创建节点函数 -- 创建链式二叉树节点并对其初始化
//接收要放入节点中的值（x）
BTNode* BuyNode(int x);


//递归结构遍历 -- 前序（先序）遍历函数
//先访问根节点 - 再访问左子树 - 最后访问右子树
//接收二叉树根节点地址（root）
void PrevOrder(BTNode* root);


//递归结构遍历 -- 中序遍历函数
//先访问左子树 - 再访问根节点 - 最后访问右子树
//接收二叉树根节点地址（root）
void InOrder(BTNode* root);


//递归结构遍历 -- 后序遍历函数
//先访问左子树 - 再访问右子树 - 最后访问根节点
//接收二叉树根节点地址（root）
void PostOrder(BTNode* root);


//节点数函数 -- 计算二叉树中的节点个数
//接收二叉树根节点地址（root）
int TreeSize(BTNode* root);


//叶子节点数函数 -- 计算二叉树中的叶子节点个数
//接收二叉树根节点地址（root）
int TreeLeafSize(BTNode* root);


//第k层节点数函数 -- 计算二叉树中第k层的节点个数
//接收二叉树根节点地址（root）和层数（k）
int TreeKLevel(BTNode* root, int k);


//二叉树销毁函数 -- 对二叉树类型进行销毁
//接收二叉树根节点地址（root）
void TreeDestory(BTNode* root);


//查找指定节点值函数 -- 在二叉树中查找值为x的节点
//接收二叉树根节点地址（root）和 要查找的节点值（x）
BTNode* TreeFind(BTNode* root, BTDataType x);


//层序遍历函数 -- 对二叉树进行层序遍历
//接收二叉树根节点地址（root）
void LevelOrder(BTNode* root);


//判断完全二叉树函数 -- 
//判断该树是不是完全二叉树
//接收二叉树根节点地址（root）
int TreeComplete(BTNode* root);


//计算高度函数 -- 计算当前链式二叉树的高度
//接收二叉树根节点地址（root）
int TreeHeight(BTNode* root);
