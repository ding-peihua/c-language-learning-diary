#define _CRT_SECURE_NO_WARNINGS 1

//包含二叉树头文件：
#include "BinaryTree.h"



//测试函数 -- 链式二叉树：
void Test()
{
	//手动创建多个链式二叉树节点：
	BTNode* node1 = BuyNode(1);
	BTNode* node2 = BuyNode(2);
	BTNode* node3 = BuyNode(3);
	BTNode* node4 = BuyNode(4);
	BTNode* node5 = BuyNode(5);
	BTNode* node6 = BuyNode(6);

	//将多个链式二叉树节点按自己需要连接成链式二叉树：
	node1->left = node2; //节点1的左指针指向节点2
	node1->right = node3; //节点1的右指针指向节点3
	node2->left = node4; //节点2的左指针指向节点4
	node3->left = node5; //节点3的左指针指向节点5
	node3->right = node6; //节点3的右指针指向节点6

	//使用PrevOrder函数进行前序（先序）遍历：
	printf("对当前二叉树进行先序遍历:> ");
	PrevOrder(node1); //接收根节点
	printf("\n\n");

	//使用InOrder函数进行中序遍历：
	printf("对当前二叉树进行中序遍历:> ");
	InOrder(node1); //接收根节点
	printf("\n\n");

	//使用PostOrder函数进行后序遍历：
	printf("对当前二叉树进行后序遍历:> ");
	PostOrder(node1); //接收根节点
	printf("\n\n");
	

	//使用TreeSize计算当前二叉树节点数： 
	printf("当前二叉树节点个数为:> %d\n", TreeSize(node1));
	printf("\n");

	//使用TreeLeafSize计算当前二叉树节点数：
	printf("当前二叉树的叶子节点个数为:> %d\n", TreeLeafSize(node1));
	printf("\n");

	//使用TreeKLevel计算二叉树中第k层的节点个数：
	printf("当前二叉树中第3层的节点个数为:> %d\n", TreeKLevel(node1, 3));
	printf("\n");

	//使用LevelOrder使用队列进行层序遍历：
	printf("使用层序遍历遍历打印当前二叉树:> ");
	LevelOrder(node1);
	printf("\n");

	//使用TreeComplete判断当前链式二叉树是不是完全二叉树：
	printf("当前二叉树是不是完全二叉树:> %d\n", TreeComplete(node1));
	printf("\n");

	//使用TreeHeight函数计算当前二叉树的高度：
	printf("当前二叉树的高度为:> %d\n", TreeHeight(node1));
	printf("\n");

	//销毁二叉树类型：
	TreeDestory(node1);
	//销毁后将其置为空：
	node1 = NULL;
}


/*
//主函数：
int main()
{
	Test();

	return 0;
}
*/

