#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//	//定义相关变量：
//	int money = 0; //一元一瓶，所以也等于空瓶数
//	int total = 0; //喝的总瓶数
//	int empty = 0; //空瓶数
//
//	//输入money
//	scanf("%d", &money);
//
//	//一开始，有多少钱就有多少瓶：
//	total += money;
//
//	//钱换了多少瓶就有多少空瓶：
//	empty += money;
//
//	//进行空瓶兑换：
//	//使用while循环，当空瓶大于等于2就进行兑换
//	while (empty >= 2)
//	{
//		//把兑换后的瓶数加到总瓶数中：
//		total += empty / 2;
//
//		//计算换后喝完的总空瓶数：
//		empty = empty / 2 + empty % 2;
//		//empty / 2：换了多少瓶就又有多少个空瓶,
//		//empty % 2：再加上可能没到2瓶不够换的1瓶
//		//这两部分加起来才是总空瓶数
//
//		//之后再判断需不需要再循环
//	}
//
//	//退出循环后进行打印：
//	printf("%d", total);
//
//	return 0;
//}

#include <stdio.h>
int main()
{
	//定义相关变量：
	int money = 0; //一元一瓶，所以也等于空瓶数
	int total = 0; //喝的总瓶数
	int empty = 0; //空瓶数

	//输入money
	scanf("%d", &money);

	//一开始，有多少钱就有多少瓶：
	total += money;

	//钱换了多少瓶就有多少空瓶：
	empty += money;

	//利用方法一中发现的规律：
	if (money > 0)
		//防止0元计算出 负一瓶 的情况
	{
		//使用规律进行计算：
		total = money * 2 - 1;
	}

	//退出循环后进行打印：
	printf("%d", total);

	return 0;
}