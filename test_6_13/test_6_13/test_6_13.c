#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
//实现 函数print() -- 打印数组的每个元素
void print(int* arr, int sz)
{
	int j = 0;
	//使用for循环以及首元素地址，进行循环打印
	for (j = 0; j < sz; j++)
	{
		printf("%d ", arr[j]);
	}

	//进行换行
	printf("\n");
}

//实现 函数reverse() -- 函数完成数组元素的逆置
void reverse(int* arr, int sz)
{
	int i = 0;
	for (i = 0; i < (sz / 2); i++)
		//6个元素调3次，5个元素调2次
	{
		//使用指针移位，类似下标，易混淆，
		//让左右指针慢慢往中间靠
		int* left = arr + i;
		int* right = arr + sz - 1 - i;
	
		//进行逆置：
		int tmp = *left;
		*left = *right;
		*right = tmp;
		//获取指针值是用 取地址符：*
	}
}

//实现 函数init() -- 初始化数组为全0
void init(int* arr, int sz)
{
	//arr：首元素地址 ；sz为几就循环几次
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		int* tmp = arr + i; //创建指针变量存放指针
		*tmp = 0; //将指针中的值赋为0
	}
}

int main()
{
	//给出要进行操作的数组 -- arr
	int arr[5] = { 1,2,3,4,5 };

	//求出数组元素个数
	int sz = sizeof(arr) / sizeof(arr[0]);

	//测试三个函数
	print(arr, sz);//实现print(),打印数组的每个元素

	reverse(arr, sz);//实现reverse(),完成数组元素的逆置
	print(arr, sz);//打印查看逆置后结果

	init(arr, sz);//实现函数init(),初始化数组为全0
	print(arr, sz);//打印查看初始化后结果

	return 0;
}