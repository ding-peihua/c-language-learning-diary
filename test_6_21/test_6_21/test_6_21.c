#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
////写一个函数返回对应月份天数：
//int get_days_of_month(int y, int m)
//{
//	int d = 0; //该年该月天数
//	//使用 switch循环，根据月份返回该月天数：
//	switch (m)
//	{
//		//1 3 5 7 8 10 12 -- 返回31天
//		case 1:
//		case 3:
//		case 5:
//		case 7:
//		case 8:
//		case 10:
//		case 12:
//		{
//			d = 31;
//			break;
//		}
//		//4 6 9 11 -- 返回30天
//		case 4:
//		case 6:
//		case 9:
//		case 11:
//		{
//			d = 30;
//			break;
//		}
//		//2月看平年还是闰年返回天数：
//		case 2:
//		{
//			d = 28; //平年
//			if ((y%4==0 && y%100!=0) || (y%400==0))
//			{
//				d += 1; //闰年，28+1天，即29天
//			}
//		}
//	}
//	return d; //返回天数
//}
//
//int main()
//{
//	//定义变量：
//	int y = 0; //年
//	int m = 0; //月
//
//	//使用while循环，进行多组输入：
//	while (scanf("%d %d", &y, &m) == 2)
//		//输入了 年 和 月 两个变量后就进行获取天数
//	{
//		//调用函数 get_days_of_month 并获取返回天数：
//		int d = get_days_of_month(y, m);
//		
//		//获取天数后，进行打印：
//		printf("%d\n", d);
//	}
//
//	return 0;
//}

#include <stdio.h>

//写一个函数 get_days_of_month 返回对应月份天数：
int get_days_of_month(int y, int m)
{
	//定义天数变量：
	int d = 0;

	//定义一个数组（平年），存放一年12个月的天数：
	int days[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	// 下标（月份） 0  1  2  3  4  5  6  7  8  9 10 11 12

	//让传进来的月份对应数组中的该年份下标：
	d = days[m];

	//判断是不是闰年的二月：
	if (((y%4==0 && y%100!=0) || (y%400==0)) && m==2)
		//如果是 闰年，并且是 二月  
	{
		d += 1; //在数组中平年二月28天的基础上+1变成29天
	}

	//返回天数：
	return d;
}

int main()
{
	//定义变量：
	int y = 0; //年
	int m = 0; //月

	//使用while循环，进行多组输入：
	while (scanf("%d %d", &y, &m) == 2)
		//输入了 年 和 月 两个变量后就进行获取天数
	{
		//调用函数 get_days_of_month 并获取返回天数：
		int d = get_days_of_month(y, m);

		//获取天数后，进行打印：
		printf("%d\n", d);
	}

	return 0;
}