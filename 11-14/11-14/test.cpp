#define _CRT_SECURE_NO_WARNINGS 1

//包含IO流头文件：
#include<iostream>
//完全展开std命名空间：
using namespace std;

//包含string类头文件：
#include<string>

#include<vector>
#include<list>

//template<class T>
//T* func(int n)
//{
//	return new T[n];
//}
//
////主函数：
//int main()
//{
//	func<double> (10);
//	
//	return 0;
//}



////string类的默认成员函数：
////主函数：
//int main()
//{
//	//string：
//	/*
//	* string是一个类模板，
//	* typedef basic_string<char> string;
//	* 原名是叫 basic_string<char> 的类模板，
//	* 被typedef重命名为string
//	*/
//
//	//string类的默认成员函数 -- 构造函数：
//	//string类中有7种构造函数：
//	/*
//	*（最常用）1：无参的string -- string(); 
//	*（最常用）2：string (const char* s);
//	* 3：string (const string& str);
//	* 4：string (const string& str, size_t pos, size_t len = npos);
//	* 5：string (const char* s, size_t n);
//	* 6：string (size_t n, char c);
//	* 7：template <class InputIterator> string (InputIterator first, InputIterator last);
//	*/
//
//	//（最常用）1：无参的string -- string(); 
//	string s1;
//
//
//	//（最常用）2：string (const char* s);
//	string s2("hello world");
//	/*
//	* s：接收一个字符串
//	* 创建一个字符串类，
//	* 并使用C语言的常量字符串的地址进行初始化，
//	* s2就存储着这段常量字符串
//	*/
//
//
//	//赋值“=”，调用拷贝构造初始化：
//	string s3 = s2;
//	/*
//	* 该写法相当于4中的：string s3(s2);
//	*/
//	
//
//	//3：string (const string& str);
//	string s4(s3);
//	/*
//	* str：接收string字符串类对象
//	* 拷贝str字符串对象，
//	* 该写法相当于3中的：string s4 = s3;
//	* 这种方法也是调用拷贝构造函数进行初始化的
//	*/
//
//	/*
//	* 因为string类重载了 流插入/流提取：
//	*	operator << / operator >>
//	* 所以是可以直接使用 输出流/输入流 的
//	*/
//	cout << "构造函数1：" << s1 << endl;
//	cout << "构造函数2：" << s2 << endl;
//	cout << "赋值“=”：" << s3 << endl;
//	cout << "构造函数3：" << s4 << endl;
//
//
//	//4：string (const string& str, size_t pos, size_t len = npos);
//	/*
//	* str：接收string字符串类对象
//	* pos：下标
//	* len：长度
//	* npos：一个整型静态const无符号的变量，值为-1，
//	*		因为无符号，-1会是整型的最大值，
//	*		所以如果不对len初始化的话，len的缺省值就是很大的值，
//	*		所以会拷贝很长的字符串，即拷贝pos后的所有字符了
//	*		（len不给默认拷贝str中pos下标后的所有字符）
//	* 
//	* 这个string类函数的功能是拷贝字符串str的一部分，
//	* 从pos下标开始，拷贝len长度的字符串
//	*/
//	string s5(s2, 1, 5);
//	/*
//	* 从s2字符串的第1个字符开始，
//	* （s2：“hello world”）
//	* 往后拷贝5个字符长度的字符串，
//	* 拷贝结果存放在字符串s5中。
//	* （s5：“ello ”）
//	* 注：空格也算一个字符；
//	*     len如果超过str的长度，则str结尾为止
//	*/
//	cout << "构造函数4：" << s5 << endl;
//	
//
//	//5：string (const char* s, size_t n);
//	/*
//	* s：字符串指针
//	* n：在s字符串中拷贝字符长度
//	*/
//	string s6("hello world", 5);
//	/*
//	* 拷贝“hello world”中的前5个字符，
//	* 拷贝到s6中初始化s6
//	*/
//	cout << "构造函数5：" << s6 << endl;
//
//
//	//6：string (size_t n, char c);
//	/*
//	* n：填充的字符个数
//	* c：要填充的字符
//	* 使用n个c字符来填充字符串字符串
//	*/
//	string s7(10, 'x');
//	/*
//	* 使用10个‘x’来填充s7
//	*/
//	cout << "构造函数6：" << s7 << endl;
//
//
//	//7：template <class InputIterator> string (InputIterator first, InputIterator last);
//	/*
//	* 这个string类构造函数因为涉及迭代器的内容，
//	* 所以等了解了迭代器再来了解该类构造函数
//	*/
//
//
//
//
//
//	//string类的默认成员函数 -- 析构函数：
//	/*
//	* string类的析构函数：
//	* string字符串类为了支持扩容，
//	* 其字符数组是动态开辟的，
//	* 动态开辟的空间使用后要进行释放，
//	* 其释放工作就是由析构函数负责的，
//	* 而析构函数一般是自动调用的
//	*/
//	
//
//	//string类的默认成员函数 -- 赋值“=”运算符重载函数：
//	//string::operator= （string类赋值“=”运算符重载函数）
//	/*
//	* 第1种： string& operator= (const string& str);
//	* 第2种： string& operator= (const char* s);
//	* 第3种： string& operator= (char c);
//	* 
//	* 第1种是支持string字符串类对象进行赋值；
//	* 第2种是支持字符串（直接写出字符串）进行赋值；
//	* 第3种是支持单个字符进行赋值
//	*/
//	//string类第1种赋值方法：
//	s1 = s2; //string字符串类对象进行赋值
//	cout << "赋值=运算符重载函数1：" << s1 << endl;
//
//	//string类第2种赋值方法：
//	s1 = "world"; //字符串（直接写出字符串）进行赋值
//	cout << "赋值=运算符重载函数2：" << s1 << endl;
//
//	//string类第3种赋值方法：
//	s1 = 'x'; //单个字符进行赋值
//	cout << "赋值=运算符重载函数3：" << s1 << endl;
//	
//	
//	return 0;
//}


namespace ggdpz
//防止和std中的string命名冲突：
{
	//string类（自己的）：
	class string
	{
	private: //私有成员变量：

		char* _str; //字符数组（字符串）指针
		size_t _size; //字符数组大小（长度）
		size_t _capacity; //字符数组容量

		/*
		* 可以简单想象string类的私有成员变量
		* 就是这几个
		*/
	};
}


//string类的遍历和访问：
int main()
{
	string s1("hello world");
	/*
	* string本质是个字符数组，
	* 只不过通过类封装在一起，
	* 如果想要遍历string字符串的话有两种方法：
	* 
	* 遍历的第1种方法：下标 + []
	* 我们访问数组的时候会使用到方括号“[]”，
	* string的底层是数组实现的，所以会对“[]”进行重载，
	* 即operator[]，使用string类的operator[]后，
	* 就可以像访问数组一样访问字符串（字符数组）了
	*/

	//string类的成员函数 -- 下标“[]”运算符重载函数：
	//string::operator[]
	/*
	* 第1种： char& operator[] (size_t pos);
	* 第2种： const char& operator[] (size_t pos) const;
	* 
	* pos：访问string字符串对象pos下标的
	* char&：访问pos下标字符后返回该字符的引用（“别名”），
	*		 如果是普通对象则可以修改该字符
	* 
	* 第2种是第1种的重载版本
	*/

	//要获取string类对象的长度有两种方法：
	//第一种方法：size()
	cout << s1.size() << endl; 
	//第二种方法：length()
	cout << s1.length() << endl; 
	/*
	* size() 和 length() 都是返回字符串对象s1的长度，
	* 至于同个功能取两个名字，是因为历史发展的关系，
	* 对于字符串，长度其实使用length()会更合理，
	* 但由于string产生得比STL早，STL出来前string只有length()，
	* 当STL出来后，对像set(树)这种数据结构length(长度)就不太合适了，
	* STL设置接口的时候又需要一定的统一性，length又不能统一使用，
	* 所以又设置了size(大小)，所以string类中又有了size(),
	* 之后计算string类时使用size()即可，按照STL的标准来
	* 
	* size() 和 length() 计算string类时不会计算"\0"
	*/
	
	//第一种遍历方法：使用for循环遍历字符串：
	for (size_t i = 0; i < s1.size(); i++)
		//s1.size() 就可以返回string类对象s1的长度
	{
		/*
		* 第1种： char& operator[] (size_t pos);
		*/
		//遍历打印string类对象s1的字符：
		cout << s1[i] << " ";
		//cout << s1.operator[](i) << " "; //等于上面的代码
		/*
		* 这里遍历string类时使用了下标运算符"[]"，
		* 让string类可以像遍历数组一样被遍历，
		* 实际string字符串类对象s1调用了下标“[]”运算符重载函数，
		* s1[i] 即 s1.operator[](i)
		*/
	}
	cout << endl; //换行


	/*
	* 使用下标符[]可以读数据，
	* 还可以用它来写数据，跟数组类似，
	* 因为operator[]调用后返回的是char&，
	* 是一个引用，所以字符串使用[]可以直接写（修改）数据
	*/
	s1[0] = 'x';
	cout << s1 << endl;


	//逆置string字符串：
	int begin = 0; //字符串左边界（下标）
	int end = s1.size() - 1; //字符串右边界（下标）
	/*
	* 右边界即字符串最后一个字符下标，
	* 使用size()获得字符串长度，
	* 字符串长度 - 1，即最后一个字符下标
	*/

	while (begin < end) 
		/*
		* begin < end，
		* 说明字符串中还有字符能够逆置
		*/
	{
		//1、通过创建临时变量实现两值交换：
		/*
		* //创建临时变量存储左边界：
		* char tmp = s1[begin];
		*
		* //将右边界字符赋给左边界字符：
		* s1[begin] = s1[end];
		*
		* //将左边界字符赋给右边界字符：
		* s1[end] = tmp;
		*/

		//2、通过C++自带的swap交换函数实现两值交换：
		swap(s1[begin], s1[end]);
		/*
		* C++自带交换函数swap，
		* 因为C++中有了函数模板，
		* 所以不用考虑实际类型的问题，
		* 从而实现了通用的swap交换函数，
		* 使用swap函数需要包含<utility>头文件，
		* 但是这里该头文件间接包含了，
		* 所以不用再显式写出来
		*/

		//进行迭代：
		++begin; //调整左边界
		--end; //调整右边界
	}

	cout << s1 << endl;



	//第二种遍历方法：使用迭代器iterator遍历字符串：
	string::iterator it = s1.begin();
	/*
	* iterator定义在类域中，但它不是内部类，是一个类型，
	* 现在还不熟悉迭代器，它的用法类似指针，
	* 但迭代器不一定是指针
	* 
	* it可以理解成指向字符串（字符数组）首字符的指针，
	* begin() 和 end() 迭代器区间是“左闭右开”的，
	* begin()可以理解成指向字符串首字符的指针，
	* end()可以理解成指向最后一个有效字符的下一位字符（'/0'）的指针，
	*/
	while (it != s1.end())
	{
		*it += 1; //通过迭代器也可以写（修改）数据
		/*
		* it一开始指向字符串首字符，
		* *it解引用指针后就可以修改该位置的字符了，
		*/
		cout << *it << " "; //打印当前it指针的字符
		++it; //调整it指针位置

		/*
		* it实际可能是指针，也可能不是指针，
		* 可以发现iterator迭代器实际运作方式，
		* 就像是用指针的方式进行字符串遍历访问和修改
		*/

		/*
		* 下标运算符[]，只能底层有一定连续的情况下使用，
		* 所以不是所有容器都能够支持。
		* 真正访问容器最方便主流的就是迭代器
		* （链式结构、树形、哈希结构 只能使用迭代器）
		* 
		* 而且各类容器调用迭代器的方式都是相同，
		* 会调用一个容器的迭代器，
		* 其它容器的迭代器也就会使用了
		*/
	}

	cout << endl;


	//对于字符串的逆置，在C++算法中也有，
	//直接调用即可：
	reverse(s1.begin(), s1.end());
	/*
	* 该算法名叫reverse，
	* 使用时传 开始 和 结束 的位置即可,
	* 无论什么容器，只要传迭代器区间给reverse，
	* 就可以实现逆置
	* 
	* 所以迭代器还可以配合算法使用（nb）
	* C++算法也是泛型编程，
	* 不是针对某个容器的迭代器实现的，
	* 函数模板，针对各个容器的迭代器实现
	*/
	cout << s1 << endl;

	return 0;
}


//迭代器：
int main()
{
	//vector类：
	vector<int> v;

	//尾插入数据：
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	//vector可以使用类遍历，
	//也可以使用迭代器遍历：
	vector<int>::iterator vit = v.begin();
	/*
	* begin() 会获取第一个位置的迭代器（左闭）,
	* 迭代器的区间都是左闭右开的,
	* end() 会获取最后一个数据的下一个位置（右开）
	*/

	while (vit != v.end())
		//begin() 还未到 end() ：
	{
		//解引用vit获取当前位置数据：
		cout << *vit << " ";
		++vit; //调整指针
	}

	cout << endl; //换行
	
	//reverse 也可以实现vector的逆置：
	reverse(v.begin(), v.end());



	//list类：
	list<double> lt;

	//尾插入数据：
	lt.push_back(1.1);
	lt.push_back(2.1);
	lt.push_back(3.1);
	lt.push_back(4.1);

	//使用迭代器遍历list：
	list<double>::iterator lit = lt.begin();

	while (lit != lt.end())
		//begin() 还未到 end() ：
	{
		//解引用vit获取当前位置数据：
		cout << *lit << " ";
		++lit; //调整指针
	}

	cout << endl; //换行

	//reverse 也可以实现list的逆置：
	reverse(lt.begin(), lt.end());

	return 0;
}



int main()
{
	/*
	* 第2种： const char& operator[] (size_t pos) const;
	* 这个重载构造函数的隐藏this指针是用const修饰的，
	* 该函数主要是为了解决参数匹配的问题
	*/
	string s1{ "hello world" }; //非const对象
	const string s2{ "hello world" }; //const对象
	s1[0] = 'x'; //非const对象s1调用"[]"
	s2[1] = 'x'; //const对象s2调用"[]"
	/*
	* 非const对象s1调用的"[]"运算符重载函数：
	* 第1种： char& operator[] (size_t pos);
	* 
	* const对象s2调用的"[]"运算符重载函数：
	* 第2种： const char& operator[] (size_t pos) const;
	* (const：只读，不能修改)
	* 
	* s1也可以调用“第2种”，非const调用const，
	* “可读可写” 变成 “只读”，访问权限缩小是允许的，
	* 虽然都可以调用“第2种”，但是“第2种”的返回值是const char&，
	* 非const对象调用后返回“只读”的引用（别名）就不合适，
	* 所以还需要实现“第1种” 
	*/

	/*
	* 对于const的string对象s2，因为“只读”，
	* 所以不能像s1非const对象那样使用迭代器：
	*/
	//string::iterator it = s2.begin(); //编译错误

	//应该是const_iterator（“只读”）而不是iterator（“可读可写”）：
	string::const_iterator it = s2.begin();
	while (it != s2.end())
	{
		//*it += 1; //const对象数据无法被修改
		cout << *it << " ";
		++it; //迭代器it本身是可以修改的
	}
	cout << endl;
	/*
	* string类的迭代器中的 begin / end，
	* 其实现时也有两个版本：
	* 第1种：iterator begin();
	* 第2种：const_iterator begin() const;
	* 
	* 其中“第2种”const版本中，
	* 它是名字为：const_iterator，
	* 而不是在iterator前直接加const修饰，
	* 
	* 
	* 
	* const_iterator it 和 const iterator it：
	* 
	* const_iterator it 本质是修饰迭代器指向的数据，
	* 即 *it 不能被修改
	* 
	* const iterator it 修饰的是迭代器本身，
	* 迭代器本身不能被修改，即 it 不能被修改，
	* 要进行遍历的话，得调整it（it++），
	* 所以不能直接在iterator前加const进行修饰
	*/



	//容器都能支持范围for循环：
	for (auto e : s1)
		//依次取容器对象s1放入e中进行循环遍历：
	{
		cout << e << " ";
	}
	cout << endl;


	return 0;
}