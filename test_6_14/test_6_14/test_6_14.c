#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
////创建函数：
//int number_of_1(unsigned int m)
//{
//	//设置 计数器count ：
//	int count = 0;
//
//	//使用 while循环 ：
//	while (m) //如果 m 不为0，说明二进制中还有1
//	{
//		if (m % 2 == 1)
//		//使用 %2 判断最低位是否为1
//		{
//			count++; //是则 count++
//		}
//		m /= 2; //去掉二进制最低位
//	}
//
//	//放回 计数器 ：
//	return count;
//}
//
////编写主函数：
//int main()
//{
//	//输入要判断的数字：
//	int n = 0;
//	scanf("%d", &n);
//
//	//调用函数：
//	int ret = number_of_1(n);
//
//	//打印结果：
//	printf("%d\n", ret);
//
//	return 0;
//}

//
//#include <stdio.h>
//
////创建函数：
//int number_of_1(unsigned int m)
//{
//	//设置 计数器count ：
//	int count = 0;
//
//	//使用 for循环 ：
//	int i = 0;
//	for (i = 0; i < 32; i++)
//	//因为有32位二进制位，循环32次
//	{
//		//使用 移位操作符>> 和 按位与& 判断最低位：
//		if (((m >> i) & 1) == 1)
//		//移动 i位 后再 按位与1，判断最低位
//		//移动后 m 的值并没有变，所以可以一直移动
//		{
//			count++; //是 1 则计数++
//		}
//	}
//
//	//放回 计数器 ：
//	return count;
//}
//
////编写主函数：
//int main()
//{
//	//输入要判断的数字：
//	int n = 0;
//	scanf("%d", &n);
//
//	//调用函数：
//	int ret = number_of_1(n);
//
//	//打印结果：
//	printf("%d\n", ret);
//
//	return 0;
//}


#include <stdio.h>

//创建函数：
int number_of_1(unsigned int m)
{
	//设置 计数器count ：
	int count = 0;

	//使用 while循环：
	while (m) //如果m不为0，则m的二进制中还有1
	{
		//使用公式：
		m = m & (m - 1); //去掉最右边的1
		count++; //计数器++
	}

	//放回 计数器 ：
	return count;
}

//编写主函数：
int main()
{
	//输入要判断的数字：
	int n = 0;
	scanf("%d", &n);

	//调用函数：
	int ret = number_of_1(n);

	//打印结果：
	printf("%d\n", ret);

	return 0;
}