﻿#define _CRT_SECURE_NO_WARNINGS 1

//计算n的阶乘：
#include <stdio.h>

int main()
{
	//输入n：
	int n = 0;
	scanf("%d", &n);

	//创建一个变量 ret ，用来存放每次相乘后的值，因为 0 乘 任何数都为 0，所以应初始化为 1
	int ret = 1;

	//利用 for循环 和前面输入的 n 循环生成 1~n 个数，
	//再使用上一步的 ret变量 依次相乘生成的 1~n 个数，
	//这也是前面把 ret  初始化为 1 的原因，防止0乘任何数都为0，
	//再赋给 ret变量 本身。
	int i = 0;
	for (i = 1; i <= n; i++) //利用 for循环 和前面输入的 n 循环生成 1~n 个数
	{
		ret = ret * i;//再使用上一步的 ret变量 依次相乘生成的 1~n 个数,再赋给 ret变量 本身
	}

	printf("%d", ret);

	return 0;
}