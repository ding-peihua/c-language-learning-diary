﻿#define _CRT_SECURE_NO_WARNINGS 1

//contact.h文件 -- 相关函数和类型的声明：

//包含头文件：
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>


//定义全局变量 MAX 100 -- 设置通讯录最多人数
#define MAX 100
//通讯录信息也是同理：
#define MAX_NAME 20
#define MAX_SEX 5
#define MAX_TELE 12
#define MAX_ADDR 30
//通讯录默认初始最大值：
#define DEFAULT_SZ 3
//设置通讯录每次扩容的人数：
#define INC_SZ 2


//对测试中，用户输入的选项进行枚举，
//用对应的功能名字代表对应的选项:
enum OPTION
{
	//枚举中的内容默认0开始往后排：
	EXIT,	// 0 对应 退出
	ADD,	// 1 对应 增加
	DEL,	// 2 对应 删除
	SEARCH, // 3 对应 搜索
	MODIFY, // 4 对应 修改
	SHOW,	// 5 对应 显示所有
	SORT	// 6 对应 排序
};


//1.结构体 struct PeoInfo -- 存放保存人的信息：
//使用 typedef 重命名结构体，
typedef struct PeoInfo
{
	char name[MAX_NAME]; //名字
	int age; //年龄
	char sex[MAX_SEX]; //性别
	char tele[MAX_TELE]; //电话
	char addr[MAX_ADDR]; //地址
}PeoInfo;


//2.结构体 struct contact -- 通讯录类型：
//使用 typedef 重命名结构体，
//静态版：
//typedef struct contact
//{
//	//创建通讯信息录结构体变量数组：
//	PeoInfo data[MAX];
//
//	//创建一个变量来记录当前通讯录data的人数：
//	int sz;
//}Contact;

//动态版：
typedef struct contact
{
	//创建通讯信息录结构体类型指针，指向该类型：
	PeoInfo* data;

	//记录当前放的有效元素的个数：
	int sz;

	//通讯录当前可存放最多人数：
	int capacity;
}Contact;


//3.函数 InitContact() 的声明 -- 初始化通讯录类型变量
void InitContact(Contact* pc);

//4.函数 AddContact() 的声明 -- 增加通讯录成员：
void AddContact(Contact* pc);

//6.函数 ShowContact() 的声明 -- 打印通讯录所有成员信息：
void ShowContact(const Contact* pc);

//7.函数 FindByName() 的声明 -- 删除指定通讯录成员信息：
//该函数只在 contact.c文件 中支持其它函数，
//为了保密可以不在该文件声明
int FindByName(Contact* pc, char name[]);

//8.函数 DelContact() 的声明 -- 删除指定通讯录成员信息：
void DelContact(Contact* pc);

//9.函数 SearchContact() 的声明 -- 查找指定通讯录成员信息：
void SearchContact(const Contact* pc);

//10 . 函数 ModifyContact() 的声明 -- 查找指定通讯录成员信息：
void ModifyContact(Contact* pc);

//11. 函数DestroyContact() 的声明 -- 释放动态内存空间：
void DestroyContact(Contact* pc);

//保存通讯录信息到文件：
void SaveContact(Contact* pc);