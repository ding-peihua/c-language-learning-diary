﻿#define _CRT_SECURE_NO_WARNINGS 1

//test.c文件 -- 测试通讯录：

//包含<contact.h>头文件：
#include "contact.h"

//1 . 函数 void menu() -- 打印通讯录菜单：
void menu()
{
	// 选1：增加联系人		选2：删除联系人
	// 选3：搜索联系人		选4：修改联系人信息
	// 选5：显示所有联系人	选6：对联系人进行排序
	//				选0：退出通讯录
	printf("*****************************************************\n");
	printf("*****		1. add		2. del		*****\n");
	printf("*****		3. search	4. modify	*****\n");
	printf("*****		5. show		6. sort		*****\n");
	printf("*****			0. exit			*****\n");
	printf("*****************************************************\n");
}


//函数 void test() -- 完成通讯录测试：
void test()
{
	//创建通讯录类型变量：
	Contact con;
	//调用函数初始化通讯录类型变量：
	InitContact(&con);

	int input = 0;//接收输入的数据
	do
	{
		//调用菜单函数打印菜单：
		menu();

		//接收输入数据：
		printf("请选择：>");
		scanf("%d", &input);

		//使用switch语句进行筛选判断：
		switch (input)
		{
		case ADD:
			//如果用户输入1，
			//则调用AddContact()函数
			//添加联系人：
			AddContact(&con);
			//参数接收 &con ，对通讯录进行对应操作
			break;

		case DEL:
			//如果用户输入2，
			//则调用DelContact()函数
			//删除指定联系人：
			DelContact(&con);
			//参数接收 &con ，对通讯录进行对应操作
			break;

		case SEARCH:
			//如果用户输入3，
			//则调用SearchContact()函数
			//查找指定联系人：
			SearchContact(&con);
			//参数接收 &con ，对通讯录进行对应操作
			break;

		case MODIFY:
			//如果用户输入 4 ，
			//则调用ModifyContact()函数
			//修改指定联系人信息
			ModifyContact(&con);
			//参数接收 &con ，对通讯录进行对应操作
			break;

		case SHOW:
			//如果用户输入5，
			//则调用ShowContact()函数
			//打印所有联系人信息：
			ShowContact(&con);
			//参数接收 &con ，对通讯录进行对应操作
			break;

		case SORT:
			//待定
			break;

		case EXIT:
			//退出要销毁通讯录前使用文件将数据保存起来：
			SaveContact(&con); //改函数用于存放通讯录数据

			//退出时释放动态空间：
			DestroyContact(&con);
			//如果用户输入0，
			//打印对应信息并退出程序：
			printf("退出通讯录\n");
			break;

		default:
			//如果用户 输入非法，
			//打印对应信息并程序输入：
			printf("选择错误，重新选择\n");
			break;
		}

	} while (input);
	//只要输入数据不为0就继续进行
}

//主函数：
int main()
{
	//调用测试函数进行测试：
	test();

	return;
}