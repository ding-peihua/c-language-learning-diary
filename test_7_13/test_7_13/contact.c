﻿#define _CRT_SECURE_NO_WARNINGS 1

//contact.c文件 -- 自定义函数的实现：

//包含头文件：
#include "contact.h"


//1.函数 InitContact() --初始化通讯录类型变量
////静态版：
//void InitContact(Contact* pc)
//{
//	//断言：
//	assert(pc);
//
//	memset(pc->data, 0, sizeof(pc->data));
//	/*使用 memset系统函数，初始化data数组。
//	参数1：被设置的空间 -- data
//	参数2：要设置的指-- 0 （全部初始化为0）
//	参数3：要设置的空间大小-- sizeof(pc->data) ，
//		   直接计算出大小*/
//
//	//联系人个数初始化为0：
//	pc->sz = 0;
//}

int CheckCapacity(Contact* pc); //需要使用先声明
//加载通讯录文件中的信息：
LoadContact(Contact* pc)
{
	//先打开文件：
	FILE* pf = fopen("contact.dat", "rb");

	//检验：
	if (pf == NULL)
	{
		perror("LoadContact");
		return;
	}

	//读文件：
	PeoInfo tmp = { 0 };//用于暂时存放读取的信息
	while (fread(&tmp, sizeof(PeoInfo), 1, pf))
	{
		//先检测开辟的内存空间是否能继续存放文件中的信息：
		if (CheckCapacity(pc) == 0) //直接使用之前写的检验函数进行检验
		{
			return; //增容失败直接返回
		}
		//把从文件读取到的二进制信息放入InitContact初始化函数开辟的动态空间中
		pc->data[pc->sz] = tmp;
		pc->sz++;
	}

	//关闭文件：
	fclose(pf);
	pf = NULL;
}

//动态版：
void InitContact(Contact* pc)
{
	//断言：
	assert(pc);
	
	//根据默认初始人数最大值开辟对应动态内存空间：
	pc->data = (PeoInfo*)malloc(DEFAULT_SZ * sizeof(PeoInfo));
	//开辟的是联系人类型PeoInfo的空间

	//检验开辟情况：
	if (pc->data == NULL)
	{
		//返回空指针，开辟失败，打印错误信息：
		perror("InitContact");
		return;
	}

	//初始联系人个数：
	pc->sz = 0;

	//通讯录当前可存放最多人数(默认)：
	pc->capacity = DEFAULT_SZ;

	//保存通讯录信息文件中的信息加载到通讯录中：
	LoadContact(pc);
}

//创建一个函数，检验是否需要扩容通讯录人数，需要则扩容：
int CheckCapacity(Contact* pc)
{
	//如果当前人数到达最高人数：
	if (pc->sz == pc->capacity)
	{
		//进行扩容，开辟动态内存空间：
		PeoInfo* ptr = (PeoInfo*)realloc(pc->data, (pc->capacity + INC_SZ) * sizeof(PeoInfo));
		//空间是PeoInfo联系人信息类型的，
		//从 pc->data 开始开辟，
		//扩容 (pc->capacity + INC_SZ) * sizeof(PeoInfo) 的空间

		//检验开辟情况：
		if (ptr == NULL)
		{
			//失败打印错误信息：
			perror("CheckCapacity");
			//失败返回0：
			return 0;
		}
		else
		{
			//开辟成功则将扩容的空间进行赋值：
			pc->data = ptr;
			//修改扩容后最大人数：
			pc->capacity += INC_SZ;

			printf("增容成功\n");
			
			//扩容成功返回1：
			return 1;

		}
	}
	//不需要扩容也返回1
	return 1;
}


//2.函数 AddContact() 的声明 -- 增加通讯录成员：

////静态版：
//void AddContact(Contact* pc)
//{
//	//断言：
//	assert(pc);
//
//	//增加的前提是还没放满：
//	if (pc->sz == MAX)
//	{
//		printf("通讯录已满，无法添加\n");
//		//无法添加直接返回：
//		return;
//	}
//
//	//没满则开始增加信息：
//	// sz 和 data数组 的下标是对应的，
//	// 所以可以通过 sz 找到 data 的对应元素，
//	// 再通过对应元素找到对应元素的相应信息
//	// 如果对应的信息是数组。可以不加&
//
//	//名字：
//	printf("请输入名字：>");
//	//使用 scanf()函数 将信息放进去
//	scanf("%s", pc->data[pc->sz].name);
//
//	//年龄：
//	printf("请输入年龄：>");
//	//使用 scanf()函数 将信息放进去
//	scanf("%d", &pc->data[pc->sz].age);
//
//	//性别：
//	printf("请输入性别：>");
//	//使用 scanf()函数 将信息放进去
//	scanf("%s", pc->data[pc->sz].sex);
//
//	//电话：
//	printf("请输入电话：>");
//	//使用 scanf()函数 将信息放进去
//	scanf("%s", pc->data[pc->sz].tele);
//
//	//地址：
//	printf("请输入地址：>");
//	//使用 scanf()函数 将信息放进去
//	scanf("%s", pc->data[pc->sz].addr);
//
//	//添加完一个联系人后，将指针移向下一个联系人位置：
//	pc->sz++;
//
//	//打印添加成功信息：
//	printf("成功添加联系人\n");
//}

//动态版：
void AddContact(Contact* pc)
{
	//断言：
	assert(pc);

	//使用CheckCapacity函数看是否需要扩容：
	if (CheckCapacity(pc) == 0)
	{
		//CheckCapacity返回0，则开辟空间失败，无法添加：
		return;
	}

	//扩容成功则继续添加：

	//名字：
	printf("请输入名字：>");
	//使用 scanf()函数 将信息放进去
	scanf("%s", pc->data[pc->sz].name);

	//年龄：
	printf("请输入年龄：>");
	//使用 scanf()函数 将信息放进去
	scanf("%d", &pc->data[pc->sz].age);

	//性别：
	printf("请输入性别：>");
	//使用 scanf()函数 将信息放进去
	scanf("%s", pc->data[pc->sz].sex);

	//电话：
	printf("请输入电话：>");
	//使用 scanf()函数 将信息放进去
	scanf("%s", pc->data[pc->sz].tele);

	//地址：
	printf("请输入地址：>");
	//使用 scanf()函数 将信息放进去
	scanf("%s", pc->data[pc->sz].addr);

	//添加完一个联系人后，将指针移向下一个联系人位置：
	pc->sz++;

	//打印添加成功信息：
	printf("成功添加联系人\n");
}



//3 . 函数 ShowContact() -- 打印通讯录所有成员信息：
void ShowContact(const Contact* pc)
{
	//断言：
	assert(pc);

	printf("\n");

	//打印列标题：
	printf("%-10s\t%-4s\t%-5s\t%-12s\t%-30s\n", 
		   "名字", "年龄", "性别", "电话", "地址");

	//使用 for循环 循环打印信息：
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-10s\t%-4d\t%-5s\t%-12s\t%-30s\n", pc->data[i].name,
											   pc->data[i].age,
											   pc->data[i].sex,
											   pc->data[i].tele,
											   pc->data[i].addr
			  );
		// %20S：打印20个字符，这里名字是20个字符
		// \t： 使用制表符进行对齐
		// %4d：打印4个整型，这里是年龄
		// “-”号 ：左对齐
	}


	printf("\n");

}


//4 . 函数 FindByName() -- 删除指定通讯录成员信息：
int FindByName(const Contact* pc, char name[])
{
	//遍历查找该人坐标：
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (strcmp(pc->data[i].name, name) == 0)
			//如果 找到了一个下标元素的name 和 要找的name 一样
		{
			return i; //返回此时找到的下标
		}
	}

	return -1; //未找到则返回-1
}


//5 . 函数 DelContact() --删除指定通讯录成员信息：
void DelContact(Contact* pc)
{
	//断言：
	assert(pc);

	//先判断通讯录是否为空：
	if (pc->sz == 0)
	{
		//为空就没法删除了，直接返回
		printf("通讯录为空，无法删除\n");
		return;
	}

	//创建存放要删除联系人名字的字符数组：
	char name[MAX_NAME] = { 0 };

	//输入并接收删除联系人名字：
	printf("请输入要删除的人名字：>");
	scanf("%s", name);

	//因为在通讯录中查找某人是多个功能所需要的，
	//所以可以将其封装成函数，再调用：
	int del = FindByName(pc, name);

	//如果del为-1，说明未找到：
	if (del == -1)
	{
		printf("要删除的人不存在\n");
		return; //直接返回
	}

	//如果找到了则删除坐标为del的联系人：
	int i = 0;
	for (i = del; i < pc->sz-1; i++)
		//从del下标开始，到倒数第二个元素
	{
		pc->data[i] = pc->data[i + 1];
		//把del后1个元素赋给del，循环覆盖掉del的元素
		//倒数第二个元素+1 为最后一个元素为止
	}

	//删除一个后，将pc指针向前移一位：
	pc->sz--;

	//打印提示：
	printf("成功删除该联系人\n");
}


//6 . 函数 SearchContact() -- 查找指定通讯录成员信息：
void SearchContact(const Contact* pc)
{
	//断言：
	assert(pc);

	//创建存放要查找的联系人名字的字符数组：
	char name[MAX_NAME] = { 0 };

	//输入并接收要查找联系人名字：
	printf("请输入要查找的联系人名字：>");
	scanf("%s", name);

	//使用 FindByName() 函数查找该人在通讯录中的下标：
	int pos = FindByName(pc, name);

	//如果del为-1，说明未找到：
	if (pos == -1)
	{
		printf("要查找的人不存在\n");
		return; //直接返回
	}
	else //找到了则打印该人信息： 
	{
		//打印列标题：
		printf("%-10s\t%-4s\t%-5s\t%-12s\t%-30s\n",
			"名字", "年龄", "性别", "电话", "地址");
		//打印对应信息：
		printf("%-10s\t%-4d\t%-5s\t%-12s\t%-30s\n", pc->data[pos].name,
													pc->data[pos].age,
													pc->data[pos].sex,
													pc->data[pos].tele,
													pc->data[pos].addr);
	}

}


//7 . 函数 ModifyContact() -- 查找指定通讯录成员信息：
void ModifyContact(Contact* pc)
{
	//断言：
	assert(pc);

	//创建存放要修改的联系人名字的字符数组：
	char name[MAX_NAME] = { 0 };

	//输入并接收要修改联系人名字：
	printf("请输入要修改的联系人名字：>");
	scanf("%s", name);

	//使用 FindByName() 函数查找该人在通讯录中的下标：
	int pos = FindByName(pc, name);

	//如果del为-1，说明未找到：
	if (pos == -1)
	{
		printf("要修改的人不存在\n");
		return; //直接返回
	}
	else //找到了则修改该人信息： 
	{
		//名字：
		printf("请输入名字：>");
		//使用 scanf()函数 将信息放进去
		scanf("%s", pc->data[pos].name);

		//年龄：
		printf("请输入年龄：>");
		//使用 scanf()函数 将信息放进去
		scanf("%d", &pc->data[pos].age);

		//性别：
		printf("请输入性别：>");
		//使用 scanf()函数 将信息放进去
		scanf("%s", pc->data[pos].sex);

		//电话：
		printf("请输入电话：>");
		//使用 scanf()函数 将信息放进去
		scanf("%s", pc->data[pos].tele);

		//地址：
		printf("请输入地址：>");
		//使用 scanf()函数 将信息放进去
		scanf("%s", pc->data[pos].addr);

		printf("修改成功\n");
	}
}


//11. 函数DestroyContact() 的声明 -- 释放动态内存空间：
void DestroyContact(Contact* pc)
{
	//进行释放：
	free(pc->data);
	//置为空指针：
	pc->data = NULL;
	//修改当前人数和最大值：
	pc->sz = 0;
	pc->capacity = 0;
}

//保存通讯录信息到文件：
void SaveContact(Contact* pc)
{
	//打开（开辟）文件：
	FILE* pf = fopen("contact.dat", "wb");//以二进制形式写入文件

	//检验文件打开情况：
	if (pf == NULL)
	{
		perror("SaveContact");
		return;
	}

	//以二进制形式写入文件：
	int i = 0;
	for (i = 0; i < pc->sz; i++)
		//有sz个用户，存sz次
	{
		fwrite(pc->data+i, sizeof(PeoInfo), 1, pf);
		//pc->data+i -- 要写入的数据，是数组名，所以不加取地址符&
		//sizeof(PeoInfo) -- 每个为该结构体大小
		//1 -- 每次存入一个
		//pf -- 存入该文件指针指向的文件
	}

	//关闭文件：
	fclose(pf);
	pf = NULL;
}