#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//    char str1[] = "hello bit.";
//    char str2[] = "hello bit.";
//    char* str3 = "hello bit.";
//    char* str4 = "hello bit.";
//    if (str1 == str2)
//        printf("str1 and str2 are same\n");
//    else
//        printf("str1 and str2 are not same\n");
//
//    if (str3 == str4)
//        printf("str3 and str4 are same\n");
//    else
//        printf("str3 and str4 are not same\n");
//
//    return 0;
//}


//#include <stdio.h>
//int main()
//{
//	//定义变量：
//	char killer = 0; //凶手
//
//	//依次假定每个人是凶手：
//	for (killer = 'a'; killer <= 'd'; killer++)
//		//因为 a b c d 的ASCII码值是连着的，所以a+1==b，
//		//以此类推，依次假定每个人是凶手，判断情况
//
//	{
//		//把4个情况列出来：
//		if ((killer != 'a') + (killer == 'c') + (killer == 'd') + (killer != 'd') == 3)
//			//把4句话，4个情况列出来，情况1假3真，真为1，假为0，4种情况“相加”==3,符合就是凶手进行打印
//		{
//			//符合则进行打印
//			printf("凶手是：%c\n", killer);
//			break;
//		}
//	}
//
//
//	return 0;
//}

////计算器：
//#include <stdio.h>
//
////加法：
//int Add(int x, int y)
//{
//	return x + y;
//}
////减法：
//int Sub(int x, int y)
//{
//	return x - y;
//}
////乘法：
//int Mul(int x, int y)
//{
//	return x * y;
//}
////除法：
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//int main()
//{
//	//使用函数指针存储这四个函数的地址：
//	/*int (*pf1)(int, int) = Add;
//	int (*pf2)(int, int) = Sub;
//	int (*pf3)(int, int) = Mul;
//	int (*pf4)(int, int) = Div;*/
//
//	//上面可见四个函数的 返回类型 和 参数 都相同，
//	//所以可以用 函数指针数组存储它们。
//	int (*pfArr[4])(int, int) = { Add, Sub, Mul, Div };
//	//在函数指针的基础上加了个 [4]，初始化时 4 可以省略
//
//	return 0;
//}


////计算器：
//#include <stdio.h>
//
////加法：
//int Add(int x, int y)
//{
//	return x + y;
//}
////减法：
//int Sub(int x, int y)
//{
//	return x - y;
//}
////乘法：
//int Mul(int x, int y)
//{
//	return x * y;
//}
////除法：
//int Div(int x, int y)
//{
//	return x / y;
//}
//
////菜单：
//void menu()
//{
//	printf("***************************\n");
//	printf("*****  1.add   2.sub  *****\n");
//	printf("*****  3.mul   4.div  *****\n");
//	printf("*******   0.exit   ********\n");
//	printf("***************************\n");
//}
//
//int main()
//{
//	int input = 0; //菜单选择的数
//	int x = 0; //计算的第一个数
//	int y = 0; //计算的第二个数
//	int ret = 0; //接收函数返回值
//
//	do
//	{
//		menu();
//		//从键盘输入：
//		printf("请选择：>");
//		scanf("%d", &input);
//
//		//根据输入的数进行相应操作：
//		switch (input)
//		{ 
//		case 1:
//			//输入加法函数的两个参数：
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			//调用加法函数：
//			ret = Add(x, y);
//			printf("计算结果为：%d\n", ret);
//			break;
//		case 2:
//			//输入减法函数的两个参数：
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			//调用减法函数：
//			ret = Sub(x, y);
//			printf("计算结果为：%d\n", ret);
//			break;
//		case 3:
//			//输入乘法函数的两个参数：
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			//调用乘法函数：
//			ret = Mul(x, y);
//			printf("计算结果为：%d\n", ret);
//			break;
//		case 4:
//			//输入除法函数的两个参数：
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			//调用除法函数：
//			ret = Div(x, y);
//			printf("计算结果为：%d\n", ret);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//
//	} while (input);//如果input为0则退出停止循环
//
//	return 0;
//}


////计算器：
//#include <stdio.h>
//
////加法：
//int Add(int x, int y)
//{
//	return x + y;
//}
////减法：
//int Sub(int x, int y)
//{
//	return x - y;
//}
////乘法：
//int Mul(int x, int y)
//{
//	return x * y;
//}
////除法：
//int Div(int x, int y)
//{
//	return x / y;
//}
//
////菜单：
//void menu()
//{
//	printf("***************************\n");
//	printf("*****  1.add   2.sub  *****\n");
//	printf("*****  3.mul   4.div  *****\n");
//	printf("*******   0.exit   ********\n");
//	printf("***************************\n");
//}
//
//int main()
//{
//	int input = 0; //菜单选择的数
//	int x = 0; //计算的第一个数
//	int y = 0; //计算的第二个数
//	int ret = 0; //接收函数返回值
//	
//	//使用函数指针数组：
//	int (*pfArr[5])(int, int) = {NULL, Add, Sub, Mul, Div};
//	//“挤一位” 下标：				 0	   1	2    3    4
//
//	do
//	{
//		menu();
//		//从键盘输入：
//		printf("请选择：>");
//		scanf("%d", &input);
//
//		if (input >= 1 && input <= 4)
//			//输入有效，调用函数进行计算
//		{
//			//输入函数的两个参数：
//			printf("请输入两个操作数：");
//			scanf("%d %d", &x, &y);
//			//通过 函数指针数组 来调用函数：
//			ret = pfArr[input](x, y);
//			// pfArr[数组下标](函数参数1，函数参数2)
//
//			//打印计算结果：
//			printf("计算结果为：%d\n", ret);
//		}
//		else if(input == 0) //退出
//		{
//			printf("退出计算器\n");
//		}
//		else //输入错误
//		{
//			printf("选择错误，重新选择\n");
//		}
//
//	} while (input);//如果input为0则退出停止循环
//
//	return 0;
//}

//计算器：
//#include <stdio.h>
//
////加法：
//int Add(int x, int y)
//{
//	return x + y;
//}
////减法：
//int Sub(int x, int y)
//{
//	return x - y;
//}
////乘法：
//int Mul(int x, int y)
//{
//	return x * y;
//}
////除法：
//int Div(int x, int y)
//{
//	return x / y;
//}
//
////菜单：
//void menu()
//{
//	printf("***************************\n");
//	printf("*****  1.add   2.sub  *****\n");
//	printf("*****  3.mul   4.div  *****\n");
//	printf("*******   0.exit   ********\n");
//	printf("***************************\n");
//}
//
//void Calc(int (*pf)(int x, int y)) //使用函数指针作为形参
//{
//	//在该函数中调用相应函数继续计算
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	printf("请输入两个操作数：");
//	scanf("%d %d", &x, &y);
//
//	//通过形参的函数指针来调用函数
//	ret = pf(x, y);
//
//	printf("ret = %d\n", ret);
//}
//
//int main()
//{
//	int input = 0; //菜单选择的数
//	int x = 0; //计算的第一个数
//	int y = 0; //计算的第二个数
//	int ret = 0; //接收函数返回值
//
//	do
//	{
//		menu();
//		//从键盘输入：
//		printf("请选择：>");
//		scanf("%d", &input);
//
//		//根据输入的数进行相应操作：
//		switch (input)
//		{
//		case 1:
//			Calc(Add); //函数名就是函数地址
//			break;
//		case 2:
//			Calc(Sub); //函数名就是函数地址
//			break;
//		case 3:
//			Calc(Mul); //函数名就是函数地址
//			break;
//		case 4:
//			Calc(Div); //函数名就是函数地址
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//
//	} while (input);//如果input为0则退出停止循环
//
//	return 0;
//}


//#include <stdio.h>
//void bubble_sort(int arr[], int sz)
//{
//	//进行冒泡排序：
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//		//确定趟数：sz - 1 
//	{
//		//两两相邻元素比较，比较次数：sz-1-i
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1]);
//			//当前 > 后一位 ，进行调换
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//
//int main()
//{
//	//创建数组：
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	//元素个数：
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	bubble_sort(arr, sz);
//
//	//排完后进行打印：
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}


//#include <stdio.h>
//#include <stdlib.h>
//
//int cmp_int(const void* p1, const void* p2)
//{
//	//比较两个数的大小可以用这两个数做差，
//	//如果 返回结果大于0 ，则 p1的内容 > p2的内容
//	//如果 返回结果等于0 ，则 p1的内容 = p2的内容
//	//如果 返回结果小于0 ，则 p1的内容 < p2的内容
//	return (*(int*)p1 - *(int*)p2);
//	// 将 void* 转化为对应的指针类型，再解引用
//}
//
////打印函数
//void print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//test1()
//{
//	int arr[10] = { 9,8,7,6,5,4,3,2,1 };
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	//使用 qsort函数 ：
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	
//	//打印：
//	print(arr, sz);
//}

int main()
{
	test1();
	
	return 0;
}


#include <stdio.h>
#include <string.h>

//交换两个元素( 当前值 和 下个值 )的函数：
void Swap(char* buf1, char* buf2, int size)
{
	int i = 0;
	char tmp = 0;
	for (i = 0; i < size; i++)
	{
		tmp = *buf1;
		*buf1 = *buf2;
		*buf2 = tmp;
		buf1++;
		buf2++;
	}
}

//冒泡排序函数：
void bubble_sort(void* base, int num, int size, int (*cmp)(const void*, const void*))
{
	//比较的趟数：
	int i = 0;
	for (i = 0; i < num - 1; i++)
	{
		//内部一趟比较的对数：
		int j = 0;
		for (j = 0; j < num - 1; j++)
		{
			//使用回调函数进行两个元素比较,假设需要升序，如果 cmp返回值 > 0 ,则交换
			if (cmp((char*)base+j*size, (char*)base+(j+j)*size) > 0)
				//两个元素比较，需要将 当前值 和 下个值 进行比较
				//(char*)base+j*size：因为可能比较字符（char），所以要转化为最小的类型，
				//					  当前指针 + 数组下标 * 元素大小 == 当前值
				//					  当前指针 + (数组下标+1) * 元素大小 == 下个值
			{
				//自定义一个函数进行交换：
				Swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
			}
		}
	}
}

//测试结构体：
struct Stu
{
	char name[20];
	int age;
}; //结构体后要加分号

//对结构体的 年龄 进行排序函数：
int cmp_stu_by_age(const void* p1, const void* p2)
{
	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
}
//对结构体的 名字 进行排序函数：
int cmp_stu_by_name(const void* p1, const void* p2)
{
	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
}

void test2()
{
	struct Stu arr[] = { {"zhangsan", 20},{"lisi", 50},{"wangwu", 15} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz, sizeof(arr[0]), cmp_stu_by_age);
}
void test3()
{
	struct Stu arr[] = { {"zhangsan", 20},{"lisi", 50},{"wangwu", 15} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz, sizeof(arr[0]), cmp_stu_by_name);
}

//测试 bubble_sort 排序整型数据：
int cmp_int(const void* p1, const void* p2)
{
	//比较两个数的大小可以用这两个数做差，
	//如果 返回结果大于0 ，则 p1的内容 > p2的内容
	//如果 返回结果等于0 ，则 p1的内容 = p2的内容
	//如果 返回结果小于0 ，则 p1的内容 < p2的内容
	return (*(int*)p1 - *(int*)p2);
	// 将 void* 转化为对应的指针类型，再解引用
}
void test1()
{
	int arr[10] = { 3,1,2,5,4,6,3,8,7,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
	printf("%s", arr);
}

int main()
{
	//test1();
	//test2();
	//test3();

	return 0;
}
