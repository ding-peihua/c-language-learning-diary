#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//	//输入两个数：
//	long long a = 0;
//	long long b = 0;
//	//输入：
//	scanf("%lld %lld", &a, &b);
//
//	//使用 三目表达式 ，把较大值取出
//	//假设 a 和 b 的较大值是最小公倍数
//	long long m = a > b ? a : b;
//	//较大值赋给 m
//
//	//使用 while循环，
//	//循环判断 较大值 能否同时整除两个数
//	while (1) //死循环，等满足用break跳出循环
//	{
//		//使用 if条件判断 ：
//		if (m % a == 0 && m % b == 0)
//		//看 较大值 能否同时整除两个数
//		{
//			//能整除，跳出循环
//			break;
//		}
//		//不能整除,较大值++，直到能整除两个值
//		m++;
//	}
//
//	//打印：
//	printf("%lld\n", m);
//
//	return 0;
//}

#include <stdio.h>
int main()
{
	//输入两个数：
	long long a = 0;
	long long b = 0;
	//输入：
	scanf("%lld %lld", &a, &b);

	//求最小公倍数：
	int i = 1; //a的倍数
	while (a * i % b != 0)//这里 !=0 可以省略
	// a * i 不能整除 b，则改变 i，即a的倍数，
	//直到可以整除退出循环
	{
		i++;//不能整除则 i++
	}

	//直到能整除为止，此时最小公倍数 k = a * i
	printf("%lld\n", a * i);

	return 0;
}
