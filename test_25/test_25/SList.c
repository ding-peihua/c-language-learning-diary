//链表实现文件：
#define _CRT_SECURE_NO_WARNINGS 1

//包含链表头文件：
#include "SList.h"

//创建遍历单链表的函数：接收单链表结点的头指针
void PrintSList(SLTNode* phead)
{
	//phead头指针指向第一个结点，
	//之后还要多次使用phead头指针，
	//所以不要改变phead头指针，
	//所以可以创建一个和phead头指针一样类型的指针cur代替phead头指针
	SLTNode* cur = phead;

	//因为链表到最后结点的指针域为NULL才结束
	//所以只要cur不为NULL就继续遍历结点进行打印：
	while (cur != NULL)
	{
		//通过cur当前指向的结点打印cur里数据域的内容：
		printf("%d->", cur->data);

		//通过结点里指针域指向下一个结点，方便打印下个结点的数据域内容
		cur = cur->next;
	}

	//最后提示已指向NULL指针链表，遍历结束：
	printf("NULL\n");
}



//创建生成结点的函数 -- 接收要存入结点数据域的数据；返回该结点的指针
SLTNode* BuySListNode(SLTDataType x)
{
	//给结点开辟动态空间（注意开辟空间的大小是SLTNode结点的大小--8个字节）
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode)); 
	//返回该结点地址

	//检查是否开辟成功：
	if (newnode == NULL) //返回空指针，说明开辟失败
	{
		//打印错误信息：
		perror("malloc fail");
		//开辟失败直接退出程序：
		exit(-1);
	}

	//将接收的值x赋给该结点的数据域：
	newnode->data = x;

	//设置新创建结点的指针域：
	//因为是最新的结点，即最尾部的结点，
	//所以指针域的指针应是NULL(链表末尾结束)
	//之后通过指针域连接各个结点的操作要看情况操作，先都初始化为NULL
	newnode->next = NULL;

	//返回该新结点的指针（地址）
	return newnode;
}



//创建尾插函数
//使用二级指针接收单链表头指针的地址 和 接收要插入的值
void SLTPushBack(SLTNode** pphead, SLTDataType x)
{
	//改变结构体，要用结构体指针
	//改变结构体指针，要用结构体指针的指针（二级指针）
	   
	//因为pphead二级指针存储的是plist指针，
	//plist指针指向的值可能为空，但指针的地址不能为空
	//pphead是plist的地址，正常情况下一定不为空
	assert(pphead);

	//先使用newnode函数为要尾插的值创建一个结点
	//并返回该结点地址
	SLTNode* newnode = BuySListNode(x);

	//判断phead是否是NULL，如果是说明链表还没开辟过一个结点
	if (*pphead == NULL)
	{
		//如果为空，则将上面开辟的newnode结点地址赋给phead
		*pphead = newnode;
		//要改变结构体的指针，就要用二级指针

		//这里pphead是二级指针，存放链表头指针plist的地址
		//因为如果用一级指针SLTNode* phead的话，
		//phead形参只是plist实参的临时拷贝，两者空间相互独立
		//改变phead的话不会改变plist，plist还会是空指针
		//所以要用二级指针pphead存放plist指针的地址，
		//*pphead解引用就能得到一级指针plist,
		//即			*pphead = plist
		//所以实际上上面的代码就相当于：plist = newnode
		//这样就让本来指向空指针的头指针指向了新创建的结点指针
		//链表就能够连接起来
	}
	else
		//不为空则往尾部插入新结点：
	{
		//创建另一个指针存放单链表头指针
		SLTNode* tail = *pphead; //*pphead == plist

		//使用while循环获得最后一个结点的地址
		while (tail->next != NULL)
		//如果下个结点的next指针域不为NULL，
		//则继续往后找，直到tail等于最后结点的地址
		{
			//把当前结点指针域里下个结点的地址给到tail，
			//进行while循环下次判断：
			tail = tail->next;
		}

		//tail找到最后结点地址后，
		//把尾插的新结点地址给到tail的next指针域，
		//连接起链表：
		tail->next = newnode;
		//要改变结构体，用结构体的指针即可

		//因为newnode、tail、pphead都是临时(局部)变量，
		//所以函数运行结束后都会销毁，
		//但malloc函数开辟的空间(结点)都在堆上不会销毁
		//通过解引用二级指针pphead改变plist也没有问题
	}
}



//创建头插函数 -- 
//使用二级指针接收单链表头指针的地址 和 接收要插入的值
void SLTPushFront(SLTNode** pphead, SLTDataType x)
{
	//因为pphead二级指针存储的是plist指针，
	//plist指针指向的值可能为空，但指针的地址不能为空
	//pphead是plist的地址，正常情况下一定不为空
	assert(pphead);

	//先使用newnode函数为要头插的值创建一个结点
	//并返回该结点地址
	SLTNode* newnode = BuySListNode(x);

	//因为也要用到链表头指针本身，所以也要使用二级指针
	//因为plist指向原本头结点地址，
	//所以可以使用plist把原本头结点地址赋给新插入头结点指针域：
	newnode->next = *pphead;

	//再把头指针改为指向新插入头结点：
	*pphead = newnode;
}



//创建尾删函数 --
//使用二级指针接收单链表头指针的地址
void SLTPopBack(SLTNode** pphead)
{
	//尾删需要考虑三种情况：
	//注：*pphead 就是 plist
	
	//因为pphead二级指针存储的是plist指针，
	//plist指针指向的值可能为空，但指针的地址不能为空
	//pphead是plist的地址，正常情况下一定不为空
	assert(pphead);

	// 第一种情况：链表为空 -- 没有任何结点
	//没有结点那就删不了了，使用assert接收到空指针就报警告
	assert(*pphead);

	// 第二种情况：链表只有一个结点
	if ((*pphead)->next == NULL)
		// -> 也是解引用(结构体的)，优先级比 * 高
		//所以 *pphead 要用() 括起来
	{
		//只有一个结点，又要尾删，那就直接把这唯一的结点删掉：
		free(*pphead); //直接free释放掉plist指向的结点

		//再把释放掉的plist置为空指针，防止成为野指针：
		*pphead = NULL;
	}
	else
	// 第三种情况：链表有一个以上结点
	{
		//这种情况额外两个指针，
		//一个tail指针 -- 用来找到最后一个结点地址并将其释放，
		//还有一个tailPrev指针 -- 指向tail指针的前一个结点地址，
		//用来改变该结点的指针域，
		//防止原本指向tail结点的指针域变成野指针

		//先替代头指针plist：
		SLTNode* tail = *pphead;

		//创建tailPrev指针，
		//之后操作会指向tail结点的前一个结点，
		//即倒数第二个结点
		SLTNode* tailPrev = NULL;

		//再使用while循环找到尾结点：
		//和尾插的操作类似
		while (tail->next != NULL)
		{
			//tail查找之前先把当前指向结点地址给tailPrev
			//这样最后tail会指向尾结点，
			//tailPrev会指向倒数第二个结点
			tailPrev = tail;

			tail = tail->next;
		}

		//结束while循环后tail指向尾结点地址，
		//因为是尾删，所以free释放掉tail就可以“删掉”尾结点
		free(tail);
		//因为tail是局部（临时）变量，出了函数就销毁，
		//所以不置为指针也可以，不用担心成为野指针

		//删除原尾结点后，倒数第二个结点成为尾结点，
		//要把其指针域next置为空指针，成为链表新结尾
		tailPrev->next = NULL;
	}
}



//创建头删函数 --
//使用二级指针接收单链表头指针的地址
void SLTPopFront(SLTNode** pphead)
{
	//因为pphead二级指针存储的是plist指针，
	//plist指针指向的值可能为空，但指针的地址不能为空
	//pphead是plist的地址，正常情况下一定不为空
	assert(pphead);

	//分两种情况：
	
	//第一种情况：链表里没有结点（空链表）
	//没有结点可以删，直接assert断言pass掉：
	assert(*pphead);


	//第二种情况：链表有一个和多个结点（非空链表）
	//因为是删掉头结点，所以不用考虑找尾结点
	//所以不用细分一个或多个结点的情况：
	
	//这种情况要先通过plist拿到第二个结点的地址：
	SLTNode* newhead = (*pphead)->next;
	//使用newhead存储第二个结点地址

	//拿到第二个结点地址后，再释放掉头结点，
	//实现头删效果：
	free(*pphead);

	//这时要让第二个结点成为新的头结点：
	*pphead = newhead; 
	//头指针指向原本的第二个结点
}



//创建查找函数 --
//查找指定值(x)所在结点的地址
//接收 链表头指针phead、查找值x
SLTNode* SLTFind(SLTNode* phead, SLTDataType x)
{
	//像SLTFind和PrintSList函数只用头指针遍历
	//不改变指针本身就不需要用到二级指针

	//创建个变量代替头指针：
	SLTNode* cur = phead;

	//使用while循环对链表进行遍历查找：
	while (cur != NULL)
		//只要cur指针指向地址不为空就继续循环
//while遍历时，(cur->next != NULL) 和 (cur != NULL) 的区别：
//(cur->next != NULL)：这个条件最后cur会是最后结点的地址
//(cur != NULL)：这个条件最后cur会是NULL
//(cur->next != NULL) 会比 (cur != NULL) 少一次循环
	{
		//遍历过程中依次查找各结点数据域数据是否与x相同：
		if (cur->data == x)
		{
			//找到了一个结点数据域数据是x，返回该结点地址
			return cur;
		}
		//这里如果while循环的条件是(cur->next != NULL)
		//最后一个结点进不来，不能判断最后结点数据域数据是不是x

		cur = cur->next; //改变循环条件，指向下个结点
	}

	//如果能指向到这说明没找到，返回NULL：
	return  NULL;
}



//创建指定位置插入函数1 -- 
//在链表指定位置(pos)之前插入一个值(x)
//接收 链表头指针地址pphead、指定结点指针pos、插入值x
void SLTInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x)
{
	//因为pphead二级指针存储的是plist指针，
	//plist指针指向的值可能为空，但指针的地址不能为空
	//pphead是plist的地址，正常情况下一定不为空
	assert(pphead);

	//第一种情况：空指针
	//先排除空指针的情况：
	assert(pos);

	//第二种情况：头插
	if (pos == *pphead)
		// *pphead == plist
		//如果pos是pphead即第一个指针，
		//则在第一个指针前插入一个值，相当于头插
	{
		//直接复用头插函数SLTPustFront：
		SLTPushFront(pphead, x);
		//直接传pphead二级指针过去
	}
	else
	//第三种情况：非头插
	{
		//从头开始找pos结点的前一个结点：
		//先获得头指针
		SLTNode* prev = *pphead;

		//当前结点的指针域不是指向pos结点则继续循环
		while (prev->next != pos)
		{
			prev = prev->next;
		}

		//此时prev已指向pos结点的前一个结点

		//为要插入的结点创建一个结点newnode：
		//插入位置是pos结点之前，prev结点之后
		SLTNode* newnode = BuySListNode(x);

		//让prev结点指针域指向新插入结点地址：
		prev->next = newnode;

		//newnode结点指针域指向pos结点：
		newnode->next = pos;

		//此时newnode新结点就插入完成了
	}
}



//创建指定位置插入函数2 --
//在链表指定位置(pos)之后插入一个值(x)
//接收 指定结点指针pos、插入值x
void SLTInsertAfter(SLTNode* pos, SLTDataType x)
{
	//因为是在pos结点后插入一个值(结点)
	//所以不可能会有头插的情况，那就不需要头指针plist

	//pos指针存储结点地址，可能会接收到空指针
	//使用assert断言pass掉
	assert(pos);

	//先为插入值创建一个新结点newnode：
	SLTNode* newnode = BuySListNode(x);

	//先让newnode的指针域next指向后一个结点：
	//这里后一个结点就是pos结点指针域里的地址
	//因为未插入前pos就是指向后一个地址
	newnode->next = pos->next;

	//再让pos的指针域next指向newnode：
	pos->next = newnode;
}



//创建删除指定结点函数1 --
//在链表删除指定位置(pos)结点
///接收 链表头指针地址pphead、指定结点指针pos
void SLTErase(SLTNode** pphead, SLTNode* pos)
{
	//因为pphead二级指针存储的是plist指针，
	//plist指针指向的值可能为空，但指针的地址不能为空
	//pphead是plist的地址，正常情况下一定不为空
	assert(pphead);

	//防止要删的pos结点指针为空指针
	//使用assert断言：
	assert(pos);

	//分两种情况：

	// 1.头删：
	if (pos == *pphead)
		//pos结点 == 头结点 --> 头删
	{
		//直接复用SLTPopFront头删函数：
		SLTPopFront(pphead);
	}
	else
	// 2.非头删：
	//尾删不用单独分出一种情况，因为还得判断是不是尾结点
	//直接用通用逻辑删除也可以处理尾删的情况
	{
		//从头开始找pos结点的前一个结点：
		//先获得头指针
		SLTNode* prev = *pphead;

		//当前结点的指针域不是指向pos结点则继续循环
		while (prev->next != pos)
		{
			prev = prev->next;
		}

		//此时prev已指向pos结点的前一个结点

		//因为要删除pos结点，
		//所以要让pos前一个和后一个结点连接起来：
		prev->next = pos->next;

		//连接成功后再把pos结点释放，实现删除效果：
		free(pos);
	}
}



//创建删除指定结点函数2 --
//在链表删除指定位置(pos)之后一个结点
//接收 指定结点指针pos
void SLTEraseAfter(SLTNode* pos)
{
	//删除pos结点后的一个结点，
	//那么就不可能出现头删的情况，
	//pos结点是尾结点也没用，
	//因为尾结点后就没有结点可以删了
	//使用assert断言处理：
	assert(pos); //防止接收“空结点”
	assert(pos->next); //防止接收尾结点

	//将要删的pos结点的下个结点posNext先保存起来：
	SLTNode* posNext = pos->next;

	//再把pos结点和下下个结点连接起来：
	pos->next = posNext->next;
	//posNext的指针域next就是pos结点的下下个结点地址

	//最后释放(删除)posNext结点：
	free(posNext);
}



//创建销毁链表函数：
void SLTDestroy(SLTNode** pphead)
{
	//因为链表释放后，还要把头指针plist给置为空，
	//要操作到plist，所以要用到二级指针：

	//plist指向空链表，pphead也不能为空：
	assert(pphead);

	//创建一个指针进行遍历：
	SLTNode* cur = *pphead;//*pphead == plist
	//进行遍历释放：
	while (cur != NULL)
	{
		//创建一个指针保存下个结点地址：
		SLTNode* next = cur->next;
		//释放当前结点：
		free(cur);
		//cur指针移向下一个结点：
		cur = next;
	}

	//将链表头指针置为空：
	*pphead = NULL;
}