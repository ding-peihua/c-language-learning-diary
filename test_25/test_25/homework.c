#define _CRT_SECURE_NO_WARNINGS 1

//头文件：
#include <stdio.h>

/*
//力扣：26. 删除有序数组中的重复项
int removeDuplicates(int* nums, int numsSize) {
    //nums -- 数组指针； numsSize -- 数组长度

    //使用双指针完成该题：

    //先创建两个指针：
    int dst = 0; //数组第一个值下标，用来保留不同值
    int src = 1; //数组第二个值下标，用来寻找不同值

    while (src < numsSize)
        //使用src这个指针进行遍历
        //src 还小于 数组长度 就继续
    {
        if (nums[src] == nums[dst])
            //如果src和dst下标指向的元素相同
        {
            //则说明该值重复，src++判断下个元素是否相同
            src++;
        }
        else
            //如果src和dst下标指向的元素不同
        {
            //src找到和dst指向元素不同的值了
            //保留一个该值：
            dst++; //dst移到下一位
            nums[dst] = nums[src]; //在dst下标保留该值
            src++; //src继续判断下一位
        }
    }

    //返回修改后数组长度
    return dst + 1;
    //dst+1即是数组长度，因为数组下标从0开始，所以+1
}
*/



//力扣：206. 反转链表
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* reverseList(struct ListNode* head) {
    /*
    //第一种解法：使用多个指针进行调整
    //head -- 头指针

    //因为是单链表，
    //直接调整指针域next指向方向可能会丢失某个结点的指针域
    //所以可以创建变量将这些指针域next保存起来：
    struct ListNode* n1;
    struct ListNode* n2;
    struct ListNode* n3;

    //链表反向后，原本第一个结点变成尾结点，next指向NULL：
    n1 = NULL;
    //头结点地址：
    n2 = head;
    //n2不为空才能得到得到第二个结点：
    if(n2 != NULL)
    {
        //第二个结点地址：
         n3 = n2->next;
    }


    //反向时，调整的是结点里的指针域next
    while(n2 != NULL)
     //n2（头指针）不为空就继续，n2会一直调整
    {
        //后一个结点的指针域指向前一个指针地址：
        n2->next = n1;

        //迭代往后走，调整指针：
        n1 = n2; //n1变成后一个结点地址
        n2 = n3; //n2变成后一个结点地址

        //这样一直循环下去，
        //最后n1会从链表的前面移到后面
        //n2、n3最后会移到链表外面，因为n2不为空就循环调整
        //n1从反转前的头指针变成反转后的头指针，
        //所以最后反转完成后，n1还是头指针

        if(n3 != NULL)
            //n3不为空指针才进行调整：
        {
            n3 = n3->next; //n3也指向下一个结点地址
        }


    }

    //返回反转后的链表头指针：
    return n1;
    */

    //第二种解法：使用头插法
    //先创建指针：
    struct ListNode* cur = head; //用于在原链表中遍历
    struct ListNode* newhead = NULL; //新链表头指针

    while (cur != NULL)
        //cur指针在原链表没遍历结束就继续头插
    {
        //创建next指针保存cur结点的下一个结点：
        struct ListNode* next = cur->next;

        //进行头插：
        //cur结点的指针域先指向新链表newhead：
        cur->next = newhead;
        //再把新链表头节点设置为cur节点：
        newhead = cur;

        //调整cur结点指针：
        cur = next;
    }

    //最后返回头插后的新链表：
    return newhead;
}



//IO型：写完整代码（头文件、主函数、自定义函数）
//接口型：给个函数让你实现（不是完整的程序），参数接收，结果返回
//力扣：203. 移除链表元素

//Definition for singly-linked list.
 struct ListNode {
     int val;
     struct ListNode *next;
};

 //解法一：在原链表中进行操作
struct ListNode* removeElements(struct ListNode* head, int val)
{
    // head：链表头指针         val：要在链表中找到并删除的值

    //创建两个结点指针:
    //一个用来遍历排除val，一个用来排除后连接val前后结点
    struct ListNode* cur = head;
    struct ListNode* prev = NULL;

    //使用cur指针进行遍历：
    while (cur != NULL)
    {
        //判断cur所指结点的数据域是不是val
        if (cur->val == val)
            //cur结点存储的数据是val：
        {
            //相同则进行删除：
            //如果val结点是头结点（头删）：
            if (cur == head) //cur结点（val结点）为头结点
            {
                //头删时先让头指针指向第二个结点：
                head = cur->next;
                //再释放掉原头结点：
                free(cur);
                //cur指向新的头结点继续判断：
                cur = head;

                //如果新头结点还是val，
                //那就再进入这个判断
            }
            else
            {
                //不是头删的删除操作（删除中间的结点）：
                //因为是中间的结点，所以该结点前后都有结点
                //先让前后结点连接起来：
                //前结点prev指向cur结点的下个结点地址：
                prev->next = cur->next;
                //连接成功后，再释放cur结点：
                free(cur);
                //再通过prev结点调整cur结点位置到下个结点：
                cur = prev->next;
            }
        }
        else
            //如果不相同则将cur指针移向下一位判断下一个结点：
        {
            //cur结点不等于val则把当前结点的指针给到prev指针：
            //这样就能保存val结点的前一个结点，方便删除后连接链表
            prev = cur;
            //保存下当前结点后，cur指针指向下一个结点：
            cur = cur->next;
        }
    }

    //不通过二级指针改变头指针，就要返回头指针
    return head;
}

//链表IO型快速调试：
int main()
{
    //创建四个结点：
    struct ListNode* n1 = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* n2 = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* n3 = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* n4 = (struct ListNode*)malloc(sizeof(struct ListNode));

    //给结点数据域赋值：
    n1->val = 7;
    n2->val = 7;
    n3->val = 7;
    n4->val = 7;

    //将各结点指针域连接起来：
    n1->next = n2;
    n2->next = n3;
    n3->next = n4;
    n4->next = NULL;

    //调用我们写的函数：
    struct ListNode* head = removeElements(n1, 7);

    return 0;
}




//Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode* next;
};

//解法二：把原链表中不是val的值尾插放在一个新链表中：
struct ListNode* removeElements(struct ListNode* head, int val)
{
    // head：链表头指针         val：要在链表中找到并删除的值
    //定义cur结点指针在原链表中找val值：
    struct ListNode* cur = head;
    //定义newnode结点指针指向新链表：
    struct ListNode* newhead = NULL;
    //定义tail结点指针在newnode链表进行遍历：
    struct ListNode* tail = NULL;
    
    //使用cur指针在原链表中遍历：
    while (cur != NULL)
    {
        if (cur->val == val)
            //如果cur结点数据域值为val -- 删除该节点
        {
            //先保存该节点：
            struct ListNode* del = cur;
            //在让cur指针指向下一个结点：
            cur = cur->next;
            //这时就可以释放val结点了：
            free(del);
        }
        else
            //如果cur结点数据域不为val -- 尾插该结点
        {
            //不为val则把该结点尾插进新链表：
            //尾插分两种情况：
            
            if (tail == NULL)
                //第一种情况：被尾插链表为空
            {
                newhead = tail = cur;
                //newhead新链表指向cur结点（不为val的结点）
                //tail结点在newhead链表中进行遍历，从第一个结点开始
                //因为是链表为空，所以插入的cur结点就是该链表第一个结点
            }
            else
                //第二种情况：被尾插链表不为空
            {
                //tail结点指针域存储cur结点：
                tail->next = cur;
                //tail指针移向插入的结点：
                tail = tail->next;
            }
            
            //插入后cur指针在原链表上移到下个结点：
            cur = cur->next;
        }
    }

    if (tail != NULL)
        //不是空链表才设置结尾：
        //如果是空链表则tail结点指针为空指针
    {
        //空链表：空指针->next会出问题
        //最后将新链表结尾设置为null：
        tail->next = NULL;
    }

    //最后返回新链表newnode：
    return newhead;
}



//力扣：876.链表的中间结点
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* middleNode(struct ListNode* head) {
    //使用快(fast)慢(slow)指针：
    //快慢指针起点一样，
    //慢指针一次走一步，快指针一次走两步
    //快指针的速度是慢指针的二倍
    //所以快指针走到结尾后，慢指针刚好走一半
    //遍历一遍链表就能完成
    //奇数结点fast走到尾结点结束
    //偶数结点fast走到空指针结束

    //定义快慢结点：
    struct ListNode* slow = head;
    struct ListNode* fast = head;

    while (fast != NULL && fast->next != NULL)
        //fast == NULL --> 偶数结点
        //fast->next == NULL --> 奇数结点
        //只能是 奇数结点 或 偶数结点 的情况，所以条件用&&或者
    {
        //慢指针（slow）走一步：
        slow = slow->next;
        //快指针（fast）走两步：
        fast = fast->next->next;
    }

    //此时slow正好在链表中间位置：
    return slow;
}



//牛客：链表中倒数第k个结点
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 */

 /**
  *
  * @param pListHead ListNode类
  * @param k int整型
  * @return ListNode类
  */
struct ListNode* FindKthToTail(struct ListNode* pListHead, int k) {
    // write code here
    //使用“快(fast)慢(slow)指针法”：
    //快指针先走k步，然后快慢指针一起走，直到快指针为空指针
    //这时慢指针就指向链表的倒数第k个结点

    //先创建快慢指针：
    struct ListNode* fast; //快指针
    struct ListNode* slow; //慢指针

    //初始化快慢指针位置：
    slow = fast = pListHead; //先都指向链表头结点

    //走k步->k--   ; 走k-1步->--k
    //让快指针先走k步：
    while (k--)
    {
        //这里链表可能 为空 或者 没有k步长 ，
        //那么倒数就算是空：
        if (fast == NULL)
        {
            return NULL;
        }

        fast = fast->next;
    }

    //再让快慢指针一起走，直到快指针为空指针
    while (fast != NULL)
    {
        slow = slow->next;
        fast = fast->next;
    }

    //这时slow指针就指向倒数第k个数：
    return slow;
}



//力扣：21.合并两个有序链表
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
    /*
    //第一种方法：依次比较，取小的尾插
    //在list1和list2中，选小的尾插到新链表
    //一样大就随便取一个，取到其中一个链表结束
    //另一个链表剩下的结点直接搬到新链表尾部

    //如果list1为空，list2不为空：
    if(list1 == NULL)
    {
        return list2;//不用归并直接返回list2
    }

    //如果list1不为空，list2为空：
    if(list2 == NULL)
    {
        return list1;//不用归并直接返回list1
    }

    //创建结点指针：
    struct ListNode* head = NULL; //新链表头指针
    struct ListNode* tail = NULL; //用于在新链表中遍历

    //其中一个链表为空就结束：
    while(list1!=NULL && list2!=NULL)
    {
        //取小的尾插：
        if(list1->val < list2->val)
        // 如果list1的值比较小：
        {
            if(tail == NULL)
            //新链表初始化：
            {
                //把list1的值尾插进新链表：
                head = tail = list1;
            }
            else
            //咋初始化后的新链表中尾插：
            {
                tail->next = list1;
                //插入后调整tail位置：
                tail = tail->next;
            }
            //当前list1结点尾插完后，调整list1结点位置：
            list1 = list1->next;
        }
        else
        //list2的值比较小：
        {
            if(tail == NULL)
            //新链表初始化：
            {
                //把list2的值尾插进新链表：
                head = tail = list2;
            }
            else
            //咋初始化后的新链表中尾插：
            {
                tail->next = list2;
                 //插入后调整tail位置：
                tail = tail->next;
            }
            //当前list2结点尾插完后，调整list2结点位置：
            list2 = list2->next;
        }
    }

    //会有一个链表还有剩余的结点
    //两个都判断一下，把剩余结点插入新结点：
    if(list1 != NULL)
    {
        tail->next = list1;
    }
    if(list2 != NULL)
    {
        tail->next = list2;
    }

    //归并后返回新链表头结点：
    return head;
    */


    //第二种方法：哨兵位
    //在list1和list2中，选小的尾插到新链表
    //一样大就随便取一个，取到其中一个链表结束
    //另一个链表剩下的结点直接搬到新链表尾部

    //如果list1为空，list2不为空：
    if (list1 == NULL)
    {
        return list2;//不用归并直接返回list2
    }

    //如果list1不为空，list2为空：
    if (list2 == NULL)
    {
        return list1;//不用归并直接返回list1
    }

    //创建结点指针：
    struct ListNode* head = NULL; //新链表头指针
    struct ListNode* tail = NULL; //用于在新链表中遍历

    //创建哨兵位：
    head = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
    //有了哨兵位后，头结点就可以通过哨兵位来控制，
    //而不通过二级指针调用头指针进行操作，也可以省去空链表初始化操作

    //其中一个链表为空就结束：
    while (list1 != NULL && list2 != NULL)
    {
        //取小的尾插：
        if (list1->val < list2->val)
            // 如果list1的值比较小：
        {
            //新链表中尾插：
            tail->next = list1;
            //插入后调整tail位置：
            tail = tail->next;
            //当前list1结点尾插完后，调整list1结点位置：
            list1 = list1->next;
        }
        else
            //list2的值比较小：
        {
            tail->next = list2;
            //插入后调整tail位置：
            tail = tail->next;
            //当前list2结点尾插完后，调整list2结点位置：
            list2 = list2->next;
        }
    }

    //会有一个链表还有剩余的结点
    //两个都判断一下，把剩余结点插入新结点：
    if (list1 != NULL)
    {
        tail->next = list1;
    }
    if (list2 != NULL)
    {
        tail->next = list2;
    }

    //最后把哨兵位给释放掉：
    //释放前先调整头结点到哨兵位的后一个节点：
    struct ListNode* del = head; //要释放的哨兵位
    head = head->next; //移到头指针
    free(del); //释放

    //归并后返回新链表头结点：
    return head;
}


//牛客：CM11 链表分割（c++）
/*
struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};*/
class Partition {
public:
    ListNode* partition(ListNode* pHead, int x) {
        // write code here

        //接收 pHead--链表头结点  x--分割值

        //思路：小于x的尾插到第一条链表，大于等于x的尾插到第二条链表
        //最后两链表连接起来
        //带哨兵位的头结点会更简单：
        //1.进行尾插时不需要考虑链表为不为空的情况
        //2.两个链表进行连接时不需要考虑为不为空的

        struct ListNode* ghead; //较大值链表头结点
        struct ListNode* gtail; //较大值链表遍历指针
        struct ListNode* lhead; //较小值链表头结点
        struct ListNode* ltail; //较小值链表遍历指针

        //为较大值链表创建哨兵位：
        ghead = gtail = (struct ListNode*)malloc(sizeof(struct ListNode));
        //为较小值链表创建哨兵位：
        lhead = ltail = (struct ListNode*)malloc(sizeof(struct ListNode));

        //创建一个指针遍历原链表：
        struct ListNode* cur = pHead;

        //开始遍历原链表，较大值放较大值链表，较小值放较小值链表：
        while (cur != NULL)
        {
            if (cur->val < x)
                //较小值：
            {
                //将该值尾插进较小值链表：
                ltail->next = cur;
                //移动指针：
                ltail = ltail->next;
            }
            else
                //较大值：
            {
                //将该值尾插进较大值链表：
                gtail->next = cur;
                //移动指针：
                gtail = gtail->next;
            }

            //将原链表一个值尾插后，移动原链表指针：
            cur = cur->next;
        }

        //两个链表完成后，把较小值链表和较大值链表连接起来：
        ltail->next = ghead->next;
        //较小值遍历指针Itail最后在较小值链表尾结点。
        //这时next再指向ghead->next，即较大值链表哨兵位的下个结点，
        //即较大值链表头结点

        //这时还得找到连接后的链表的头结点：
        struct ListNode* head = lhead->next;
        //即较小值链表哨兵位下一个结点，即较小值链表头结点

        //合并链表后，链表结尾要置为空：
        gtail->next = NULL;

        //最后释放掉两个哨兵位：
        free(lhead);
        free(ghead);

        //返回合并链表：
        return head;
    }
};



//牛客：OR36 链表的回文结构
/*
struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};*/
class PalindromeList {
public:
    //这道题可以先找中间结点，再把后半部分逆置（反转），
    //再把前半部分和逆置的后半部分进行对比，
    //如果两部分各值都相同，则说明该链表是回文结构


    //找中间结点可以调用力扣：876.链表的中间结点
    struct ListNode* middleNode(struct ListNode* head) {
        //使用快(fast)慢(slow)指针：
        //快慢指针起点一样，
        //慢指针一次走一步，快指针一次走两步
        //快指针的速度是慢指针的二倍
        //所以快指针走到结尾后，慢指针刚好走一半
        //遍历一遍链表就能完成
        //奇数结点fast走到尾结点结束
        //偶数结点fast走到空指针结束


        //定义快慢结点：
        struct ListNode* slow = head;
        struct ListNode* fast = head;


        while (fast != NULL && fast->next != NULL)
            //fast == NULL --> 偶数结点
            //fast->next == NULL --> 奇数结点
            //只能是 奇数结点 或 偶数结点 的情况，所以条件用&&或者
        {
            //慢指针（slow）走一步：
            slow = slow->next;
            //快指针（fast）走两步：
            fast = fast->next->next;
        }


        //此时slow正好在链表中间位置：
        return slow;
    }


    //逆置（反转）可以调用力扣：206.反转链表
    //第二种解法：使用头插法
    //先创建指针：
    struct ListNode* reverseList(struct ListNode* head) {


        struct ListNode* cur = head; //用于在原链表中遍历
        struct ListNode* newhead = NULL; //新链表头指针


        while (cur != NULL)
            //cur指针在原链表没遍历结束就继续头插
        {
            //创建next指针保存cur结点的下一个结点：
            struct ListNode* next = cur->next;


            //进行头插：
            //cur结点的指针域先指向新链表newhead：
            cur->next = newhead;
            //再把新链表头节点设置为cur节点：
            newhead = cur;


            //调整cur结点指针：
            cur = next;
        }


        //最后返回头插后的新链表：
        return newhead;
    }


    bool chkPalindrome(ListNode* head) {
        //      head -- 链表头指针
        // write code here
    //有了找中间结点和逆置的函数后完成这道题：


    //调用函数找到链表中间元素：
        struct ListNode* mid = middleNode(head);
        //调用函数从mid位置开始逆置(将后半部分进行逆置)：
        struct ListNode* rmid = reverseList(mid);
        //逆置后rmid指向原链表最尾部的结点

        //开始进行对比：
        while (rmid != NULL && head != NULL)
        {
            //rmid逆置的后边部分 和 head原链表 都不为空就进行对比：
            //rmid因为只有一半，所以会先到空，rmid和head其中一个为空就会结束，
            //所以head链表不设置空也可以：
                //但凡有一个不同就返回错误：
            if (rmid->val != head->val)
                //对比后如果不一样
            {
                return false; //不是回文结构
            }


            //如果相同则调整两指针：
            rmid = rmid->next;
            head = head->next;
        }


        //能执行到这，说明是回文结构：
        return true;
    }
};



//力扣：160.相交链表
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
    //第一种：但时间复杂度为O(N^2)
    //思路：如果两个链表相交了，那么它们就会有共同的尾结点
    //所以只要判断两个链表的尾结点地址是否相等就可以判断是否相交：
    //（结点的地址是唯一的）
    //用一个链表的一个结点地址跟另一个链表的所有结点地址依次对比
    //然后轮流移动链表进行比对
    //直到第一次出现两个结点地址相同时，这个结点就是第一个相交结点

    //第二种思路：
    //1.先计算出A和B的长度--lenA、lenB
    //2.让长的链表先走差距步--abs(lenA-lenB)
    //  走完后长慢链表就一样长了
    //3.两链表一样长后，就可以将两链表对应位置的结点进行比较了
    //  这样就不用向上面一样去一个结点与多个结点依次比较了
    //  优化时间复杂度：遍历一遍知长度O(N)、在对应对比结点O(N)
    //  最终时间复杂度O(N)

    //先为两个链表分别创建遍历指针：
    struct ListNode* curA = headA;
    struct ListNode* curB = headB;

    //创建两个存储长度的变量：
    //因为后面判断长度时条件时是next不为空，所以会少算一个结点
    //那么初始化就要初始化为1
    int lenA = 1;
    int lenB = 1;

    //求差距步：
    //注：题目提示了两个链表都不为空
    //因为要判断相交，所以最后curA不能走到NULL结束，找不到尾
    //A链表长度：
    while (curA->next != NULL)
    {
        //移动一次就++lenA：
        curA = curA->next;
        ++lenA;
    }
    //B链表长度
    while (curB->next != NULL)
    {
        //移动一次就++lenB：
        curB = curB->next;
        ++lenB;
    }
    //长度计算完后两指针都指向尾结点：
    //这时可以判断两链表尾结点地址是否相等
    //不相等则两链表不相交：
    if (curA != curB)
    {
        //不相交则返回NULL
        return NULL;
    }

    //能执行到这说明两链表相交
    //开始寻找初始交点：

    //求差距步：
    //使用abs函数，求绝对值
    int gap = abs(lenA - lenB);

    //有了差距步后要知道哪个链表长，让它走差距步：
    //假设A链表长，B链表短：
    struct ListNode* longList = headA;
    struct ListNode* shortList = headB;
    //如果是A短B长，则调换一下：
    if (lenA < lenB)
    {
        longList = headB; //改成B长
        shortList = headA; //改成A短
    }

    //让长链表走差距步：
    while (gap--)
    {
        longList = longList->next;
    }

    //再两链表同时走，找初始交点：
    while (longList != shortList)
        //还没找到交点就继续：
    {
        //两链表向后移找交点：
        longList = longList->next;
        shortList = shortList->next;
    }

    //找到交点后，返回长链表或短链表都可以：
    return longList;
}



//力扣：141.环形链表
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
bool hasCycle(struct ListNode* head) {
    //定义快慢指针：
    struct ListNode* slow = head;
    struct ListNode* fast = head;

    //因为链表可能有奇数个结点或偶数个结点，
    //所以要保持fast不为空且fast->next不为空的情况下，
    //快指针走两步，慢指针走一步：
    while (fast != NULL && fast->next != NULL)
    {
        fast = fast->next->next; //快指针走两步
        slow = slow->next; //慢指针走一步

        //如果链表有环的话，
        //慢指针走一圈时快指针走了两圈，
        //这时快慢指针就会进入环，两指针可能会相遇，
        //如果两指针能够相遇，说明有环：
        if (slow == fast)
        {
            return true;
        }
    }

    //链表不带环的话就一定会有空NULL，
    //链表不带环：
    return false;
}



//力扣：142.环形链表||
            //第一种方法：公式法
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* detectCycle(struct ListNode* head) {
    //设置快(fast)慢(slow)指针：
    //当slow进环后，fast开始追击slow

    //假设slow进环时，它们之间的距离是N
    //快指针每次走两步，慢指针每次走一步
    //每追击一次，他们之间的距离就减少一
    //所以追击过程中，它们之间的距离变化是：
    //  N
    //  N-1
    //  N-2
    //  ……
    //  2
    //  1
    //  0 （相遇）

    //假设slow进环时，它们之间的距离是M
    //快指针每次走三步，慢指针每次走一步
    //每追击一次，他们之间的距离就减少二
    //所以追击过程中，它们之间的距离变化是：
    //  偶数结点：          奇数结点：
    //    M                   M
    //    M-2                 M-2
    //    M-4                 M-4
    //    ……                  …… 
    //    4                   3
    //    2                   1
    //    0                   -1 
    // （相遇）      （错过，进入新一轮追击）
    //（假设C是环的长度（周长），错过后两指针距离是C-1）
    //
    //这时如果C-1是偶数那还可以追上，
    //但如果还是奇数，那最后又会是C-1，就这样可能会陷入死循环，
    //永远不会追上，两指针无法相遇

    //假设slow进环时，它们之间的距离是K
    //快指针每次走四步，慢指针每次走一步
    //每追击一次，他们之间的距离就减少三
    //所以追击过程中，它们之间的距离变化是：
    //
    //     K%3 == 0:        K%3 == 2        K%3 == 1
    // （如果K是三的倍数）              
    //        K                 K               K
    //        K-3               K-3             K-3
    //        K-6               K-6             K-6
    //        ……                ……              ……
    //        6                 5               4
    //        3                 2               1
    //        0                 -1              -2
    //      (相遇)            （C-1）         （C-2）
    //
    //出现(C-1)和(C-2)，最后还是可能死循环一直错过

    //解题思路一：
    //假设快指针一次走两步，慢指针一次走一步（这种情况下不可能错过）
    //假设链表的起点到环的入口点的长度为：L
    //假设环的周长为：C
    //假设入口点到快慢相遇点的距离为：X
    //fast走的距离是slow的距离的两倍
    //slow进环后一圈之内，fast一定追上slow
    //slow走的距离：L+X (因为不会错过，所以不存在C+X的情况)

        //fast走的距离：L+C+X
        //所以有该等式：2(L+X) = L+C+X
        //化简得： L+X = C
        //移项得： L = C-X
        //但实际上这里fast走的距离是错的

    //当slow进环时，假设fast在环里面走了n圈，n>=1
    //实际fast走的距离：L + n*C + X
    //这时再得到等式：2(L+X) = L+n*C+X
    //化简：L+X = n*C
    //移项：L = n*C-X
    //  即：L = (n-1)*C + (C-X)
    //                     最后结论：
    //一个指针从起点走，一个从相遇点走，它们会在入口点相遇

    //创建快慢指针：
    struct ListNode* slow;
    struct ListNode* fast;

    //两指针都从起点（链表头结点）开始：
    slow = fast = head;

    while (fast != NULL && fast->next != NULL)
        //进行遍历，fast一次走两步且链表可能没有环
    {
        //慢指针一次走一步：
        slow = slow->next;
        //快指针一次走两步：
        fast = fast->next->next;

        if (slow == fast)
            //如果slow结点地址等于fast
            //说明两结点在环里相遇了
            //相遇是在环里得任意位置（结点）
        {
            //存储两指针相遇结点：
            struct ListNode* meet = slow;

            //使用公式，一个从相遇点走，一个从起点走
            //这样走下去，最后会在入口点（题目要求返回得结点）相遇
            while (head != meet)
                //还没走到入口点相遇就调整结点指针继续
            {
                head = head->next;
                meet = meet->next;
            }

            //到这说明在入口点相遇了：
            return meet; //返回此时结点
        }
    }

    //如果能出循环，说明这链表不带环：
    return NULL; //返回空指针
}


                //第二种方法：相交链表法
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

 //相交链表求交点函数：
 /**
  * Definition for singly-linked list.
  * struct ListNode {
  *     int val;
  *     struct ListNode *next;
  * };
  */
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
    //第一种：但时间复杂度为O(N^2)
    //思路：如果两个链表相交了，那么它们就会有共同的尾结点
    //所以只要判断两个链表的尾结点地址是否相等就可以判断是否相交：
    //（结点的地址是唯一的）
    //用一个链表的一个结点地址跟另一个链表的所有结点地址依次对比
    //然后轮流移动链表进行比对
    //直到第一次出现两个结点地址相同时，这个结点就是第一个相交结点

    //第二种思路：
    //1.先计算出A和B的长度--lenA、lenB
    //2.让长的链表先走差距步--abs(lenA-lenB)
    //  走完后长慢链表就一样长了
    //3.两链表一样长后，就可以将两链表对应位置的结点进行比较了
    //  这样就不用向上面一样去一个结点与多个结点依次比较了
    //  优化时间复杂度：遍历一遍知长度O(N)、在对应对比结点O(N)
    //  最终时间复杂度O(N)

    //先为两个链表分别创建遍历指针：
    struct ListNode* curA = headA;
    struct ListNode* curB = headB;

    //创建两个存储长度的变量：
    //因为后面判断长度时条件时是next不为空，所以会少算一个结点
    //那么初始化就要初始化为1
    int lenA = 1;
    int lenB = 1;

    //求差距步：
    //注：题目提示了两个链表都不为空
    //因为要判断相交，所以最后curA不能走到NULL结束，找不到尾
    //A链表长度：
    while (curA->next != NULL)
    {
        //移动一次就++lenA：
        curA = curA->next;
        ++lenA;
    }
    //B链表长度
    while (curB->next != NULL)
    {
        //移动一次就++lenB：
        curB = curB->next;
        ++lenB;
    }
    //长度计算完后两指针都指向尾结点：
    //这时可以判断两链表尾结点地址是否相等
    //不相等则两链表不相交：
    if (curA != curB)
    {
        //不相交则返回NULL
        return NULL;
    }

    //能执行到这说明两链表相交
    //开始寻找初始交点：

    //求差距步：
    //使用abs函数，求绝对值
    int gap = abs(lenA - lenB);

    //有了差距步后要知道哪个链表长，让它走差距步：
    //假设A链表长，B链表短：
    struct ListNode* longList = headA;
    struct ListNode* shortList = headB;
    //如果是A短B长，则调换一下：
    if (lenA < lenB)
    {
        longList = headB; //改成B长
        shortList = headA; //改成A短
    }

    //让长链表走差距步：
    while (gap--)
    {
        longList = longList->next;
    }

    //再两链表同时走，找初始交点：
    while (longList != shortList)
        //还没找到交点就继续：
    {
        //两链表向后移找交点：
        longList = longList->next;
        shortList = shortList->next;
    }

    //找到交点后，返回长链表或短链表都可以：
    return longList;
}

struct ListNode* detectCycle(struct ListNode* head) {
    /*
        //第一种方法：
        //设置快(fast)慢(slow)指针：
        //当slow进环后，fast开始追击slow

        //假设slow进环时，它们之间的距离是N
        //快指针每次走两步，慢指针每次走一步
        //每追击一次，他们之间的距离就减少一
        //所以追击过程中，它们之间的距离变化是：
        //  N
        //  N-1
        //  N-2
        //  ……
        //  2
        //  1
        //  0 （相遇）

        //假设slow进环时，它们之间的距离是M
        //快指针每次走三步，慢指针每次走一步
        //每追击一次，他们之间的距离就减少二
        //所以追击过程中，它们之间的距离变化是：
        //  偶数结点：          奇数结点：
        //    M                   M
        //    M-2                 M-2
        //    M-4                 M-4
        //    ……                  ……
        //    4                   3
        //    2                   1
        //    0                   -1
        // （相遇）      （错过，进入新一轮追击）
        //（假设C是环的长度（周长），错过后两指针距离是C-1）
        //
        //这时如果C-1是偶数那还可以追上，
        //但如果还是奇数，那最后又会是C-1，就这样可能会陷入死循环，
        //永远不会追上，两指针无法相遇

        //假设slow进环时，它们之间的距离是K
        //快指针每次走四步，慢指针每次走一步
        //每追击一次，他们之间的距离就减少三
        //所以追击过程中，它们之间的距离变化是：
        //
        //     K%3 == 0:        K%3 == 2        K%3 == 1
        // （如果K是三的倍数）
        //        K                 K               K
        //        K-3               K-3             K-3
        //        K-6               K-6             K-6
        //        ……                ……              ……
        //        6                 5               4
        //        3                 2               1
        //        0                 -1              -2
        //      (相遇)            （C-1）         （C-2）
        //
        //出现(C-1)和(C-2)，最后还是可能死循环一直错过

        //解题思路一：
        //假设快指针一次走两步，慢指针一次走一步（这种情况下不可能错过）
        //假设链表的起点到环的入口点的长度为：L
        //假设环的周长为：C
        //假设入口点到快慢相遇点的距离为：X
        //fast走的距离是slow的距离的两倍
        //slow进环后一圈之内，fast一定追上slow
        //slow走的距离：L+X (因为不会错过，所以不存在C+X的情况)

            //fast走的距离：L+C+X
            //所以有该等式：2(L+X) = L+C+X
            //化简得： L+X = C
            //移项得： L = C-X
            //但实际上这里fast走的距离是错的

        //当slow进环时，假设fast在环里面走了n圈，n>=1
        //实际fast走的距离：L + n*C + X
        //这时再得到等式：2(L+X) = L+n*C+X
        //化简：L+X = n*C
        //移项：L = n*C-X
        //  即：L = (n-1)*C + (C-X)
        //                     最后结论：
        //一个指针从起点走，一个从相遇点走，它们会在入口点相遇

        //创建快慢指针：
        struct ListNode* slow;
        struct ListNode* fast;

        //两指针都从起点（链表头结点）开始：
        slow = fast = head;

        while(fast!=NULL && fast->next!=NULL)
            //进行遍历，fast一次走两步且链表可能没有环
        {
            //慢指针一次走一步：
            slow = slow->next;
            //快指针一次走两步：
            fast = fast->next->next;

            if(slow == fast)
            //如果slow结点地址等于fast
            //说明两结点在环里相遇了
            //相遇是在环里得任意位置（结点）
            {
                //存储两指针相遇结点：
                struct ListNode* meet = slow;

                //使用公式，一个从相遇点走，一个从起点走
                //这样走下去，最后会在入口点（题目要求返回得结点）相遇
                while(head != meet)
                    //还没走到入口点相遇就调整结点指针继续
                {
                    head = head->next;
                    meet = meet->next;
                }

                //到这说明在入口点相遇了：
                return meet; //返回此时结点
            }
        }

        //如果能出循环，说明这链表不带环：
        return NULL; //返回空指针
    */


    //第二种方法：
    //在meet相遇结点断开
    //在meet结点的下个结点设置一个newhead结点
    //这时环断开后，就变成了两条链表最后相交与同个结点，
    //之后结点也相同，这就跟相交链表那道题一样了，求交点
    //让大的链表先走差距步，再让两链表一起走找交点

    //创建快慢指针：
    struct ListNode* slow;
    struct ListNode* fast;

    //两指针都从起点（链表头结点）开始：
    slow = fast = head;

    while (fast != NULL && fast->next != NULL)
        //进行遍历，fast一次走两步且链表可能没有环
    {
        //慢指针一次走一步：
        slow = slow->next;
        //快指针一次走两步：
        fast = fast->next->next;

        if (slow == fast)
            //如果slow结点地址等于fast
            //说明两结点在环里相遇了
            //相遇是在环里得任意位置（结点）
        {
            //存储两指针相遇结点：
            struct ListNode* meet = slow;

            //转换成链表相交：
            //设置断开后的新链表的头结点（为相遇点的下个结点）：
            struct ListNode* newhead = meet->next;

            //设置旧连表尾结点指针域指向NULL：
            meet->next = NULL;

            //旧链表 -- 头：head  尾：meet
            //新链表 -- 头：newhead  尾：meet

            //直接调用相交链表函数，将两个链表的头结点传过去
            //求得并返回相交结点地址：
            return getIntersectionNode(newhead, head);

        }
    }

    //如果能出循环，说明这链表不带环： 
    return NULL; //返回空指针
}
