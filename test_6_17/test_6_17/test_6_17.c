#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int main() {

    //输入 n 和 m
    int n = 0;
    int m = 0;
    scanf("%d %d", &n, &m);

    //定义变长数组：
    int arr1[n];
    int arr2[m];

    //对变长数组进行赋值
    int i = 0;
    for (i = 0; i < n; i++)
        //前面输入的n就是arr1的长度
    {
        scanf("%d", &arr1[i]);
    }

    i = 0;
    for (i = 0; i < m; i++)
        //前面输入的m就是arr1的长度
    {
        scanf("%d", &arr2[i]);
    }












    //开始合并：
    //定义第三个变长数组arr3，存放两arr1 和 arr2合并后的结果：
    int arr3[n + m]; //变长数组不能初始化
    //定义三个变长数组的下标：
    i = 0; //arr1下标
    int j = 0; //arr2下标
    int k = 0; //arr3下标

    //使用 while循环，
    while (i < n && j < m)
        //i<n：arr1下标小于arr1长度，说明arr1还有值
        //j<m：arr2下标小于arr2长度，说明arr2还有值
           //两个数组都有值才用进行比较赋值
    {
        if (arr1[i] < arr2[j]) //arr1的值小于arr2的值
        {
            arr3[k] = arr1[i]; //把大的值赋给arr3
            //给值 和 被给值 的数组下标都用往后移
            k++;
            i++;
        }
        else //等于时放谁都一样 或 arr2变得比较小了
        {
            arr3[k] = arr2[j]; //把大的值赋给arr3
            //给值 和 被给值 的数组下标都用往后移
            k++;
            j++;
        }
    }

    //跳出循环说明有一个数组已经被遍历完了
    //此时，i、j、k 都已经移动到了合适的位置
    //判断哪个数组遍历完了进行操作：
    if (i == n) //arr1下标等于arr1数组长度，说明arr1遍历完了
    {
        //将arr2中剩余的元素全部放入arr3中：
        while (j < m)//arr2下标小于arr2数组长度就继续放
        {
            arr3[k] = arr2[j];
            k++;
            j++;
        }
    }
    else //arr2遍历完
    {
        //将arr1中剩余的元素全部放入arr3中：
        while (i < n)//arr1下标小于arr1数组长度就继续放
        {
            arr3[k] = arr1[i];
            k++;
            i++;
        }
    }

    //进行打印：
    for (i = 0; i < n + m; i++)
    {
        printf("%d ", arr3[i]);
    }

    return 0;
}