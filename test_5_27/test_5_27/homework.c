#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//	unsigned char a = 200;
//	unsigned char b = 100;
//	unsigned char c = 0;
//	c = a + b;
//	printf("% d % d", a + b, c);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//    char a[1000] = { 0 };
//    int i = 0;
//    for (i = 0; i < 1000; i++)
//    {
//        a[i] = -1 - i;
//    }
//    printf("%d", strlen(a));
//    return 0;
//}


//#include <stdio.h>
//int main() 
//{
//	unsigned int a = 0x1234;
//	unsigned char b = *(unsigned char*)&a;
//	printf("%c", b);
//	return 0;
//}

//#include <stdio.h>
//int main() 
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	int d = 0;
//	int e = 0;
//	// 0为假，1为真
//	//每个变量都有5中可能：5个循环
//	for (a = 0; a <= 5; a++)
//	{
//		for (b = 0; b <= 5; b++)
//		{
//			for (c = 0; c <= 5; c++)
//			{
//				for (d = 0; d <= 5; d++)
//				{
//					for (e = 0; e <= 5; e++)
//					{			
//						if (   ((b == 2) + (a == 3) == 1)
//							//A: B第二，我第三 --》其中一个是对的（1+0=0）
//							&& ((b == 2) + (e == 4) == 1)
//							//B: 我第二，E第四 --》其中一个是对的（1+0=0）
//							&& ((c == 1) + (d == 2) == 1)
//							//C: 我第一，D第二 --》其中一个是对的（1+0=0）
//							&& ((c == 5) + (d == 3) == 1)
//							//D: C最后，我第三 --》其中一个是对的（1+0=0）
//							&& ((e == 4) + (a == 1) == 1)
//							//E: 我第四，A第一 --》其中一个是对的（1+0=0）
//							&& (a*b*c*d*e == 120)			)
//							//5人每次各不相同：1*2*3*4*5=120，去除相同排名的情况
//						{
//							printf("a=%d b=%d c=%d d=%d e=%d\n", a, b, c, d, e);
//						}
//					}
//				}
//			}
//		}
//	}
//
//	return 0;
//}

//#include <stdio.h>
//int main() 
//{
//	int A = 0, B = 0, C = 0, D = 0;
//	int FD = 1;//C和D的表述对立
//	int i;
//
//	for (i = 1; i <= 4; i++)
//	{
//		if (i == 1)//A是假的
//		{
//			A = 1;
//			C = 1;
//			FD = 1;//CD对立
//			D = 0;
//		}
//		if (i == 2)//B是假的
//		{
//			A = 0;
//			C = 0;
//			FD = 1;
//			D = 0;
//		}
//		if (i == 3)//C是假的
//		{
//			A = 0;
//			C = 1;
//			FD = 0;
//			D = 0;
//		}
//		if (i == 4)//D是假的
//		{
//			A = 0;
//			C = 1;
//			FD = 1;
//			D = 0;
//		}
//		if ((A + B + C + D + FD == 1) && FD == 0)
//		// (A + B + C + D + FD == 1)：只有一个凶手
//		// FD == 0：排除D是D不是的情况
//		{
//			printf("A=%d B=%d C=%d D=%d\n", A, B, C, D);
//		}
//	}
//
//	return 0;
//}

#include <stdio.h>
int main()
{
	int arr[30][30] = { 0 }, n = 0, row = 0, column = 0;

	while (scanf("%d", &n))//只要输入的数大于 0 均可进入循环
	{
		for (row = 0; row < n; row++)//数组中第一行的下标，即 row 为 0 ，所以得从 0 开始
		{
			int b = 0;
			for (b = 0; b < n - 1 - row; b++)//打印每行前面的空格
			{
				printf("  ");
			}
			for (column = 0; column <= row; column++) // 保证 行数=每行元素个数
			{

				if (column == 0 || column == row)//将每行第一个元素与最后一个元素初始化为 1
				{
					arr[row][column] = 1;
				}
				else
				{
					arr[row][column] = arr[row - 1][column] + arr[row - 1][column - 1];  //填充中间元素
				}
				printf("%3d ", arr[row][column]); //打印杨辉三角
			}
			printf("\n");//换行
		}
	}
	return 0;
}