//#define _CRT_SECURE_NO_WARNINGS 1
//
////力扣：225. 用队列实现栈
////思路：
////入队列：在不为空的队列进行入队
////出队列：把不为空队列的前N-1个元素放到空队列
////       此时不为空队列就只剩队尾元素，对其进行出队
////       实现栈的后进先出
//
//
////定义队列(链式结构)中数据域存储的数据类型：
//typedef int QDataType;
//
////定义队列(链式结构)结点类型：
//typedef struct QueueNode
//{
//	//队列指针域：
//	struct QueueNode* next;
//
//	//队列数据域：
//	QDataType data;
//
//}QNode; //将类型重命名为Qnode
//
//
////定义队列类型：
//typedef struct Queue
//{
//	//因为用链表尾插实现入队，
//	//用链表头删实现出队，
//	//那么就需要头结点和尾结点的指针，
//	//所以可以直接将这两个指针封装为一个类型，
//	//队列类型：
//
//	//头结点指针：
//	QNode* head;
//
//	//尾结点指针：
//	QNode* tail;
//
//	//记录队列结点(元素)个数：
//	int size;
//
//	//这样之后在出队和入队操作时，
//	//就不需要用到二级指针，
//	//直接接收这个结构体指针，
//	//通过结构体指针运用结构体里的头尾结点指针，
//	//再用头尾结点指针定义头尾结点
//	//来实现 二级指针、带哨兵位头结点 和 返回值 的作用
//	//所以现在已知的通过指针定义结点的方法就有4种：
//	//		1. 结构体包含结点指针
//	//		2. 二级指针调用结点指针
//	//		3. 哨兵位头结点指针域next指向结点地址
//	//		4. 返回值返回改变的结点指针
//
//}Que; //重命名为Que
//
//
////队列初始化函数 -- 将队列进行初始化
////接收队列类型指针(包含链表头尾结点) 
//void QueueInit(Que* pq);
//
////队列销毁函数 -- 将队列销毁
////接收队列类型指针(包含链表头尾结点) 
//void QueueDestroy(Que* pq);
//
////队列入队函数 -- 用链表的尾插操作实现入队
////接收队列类型指针(包含链表头尾结点) 、尾插值
//void QueuePush(Que* pq, QDataType x);
//
////队列出队函数 -- 用链表的头删操作实现出队
////接收队列类型指针(包含链表头尾结点) 
//void QueuePop(Que* pq);
//
////队头函数 -- 返回队头结点的数据域数据
////接收队列类型指针(包含链表头尾结点) 
//QDataType QueueFront(Que* pq);
//
////队尾函数 -- 返回队尾结点的数据域数据
////接收队列类型指针(包含链表头尾结点) 
//QDataType QueueBack(Que* pq);
//
////判空函数 -- 判断队列是否为空
////接收队列类型指针(包含链表头尾结点) 
//bool QueueEmpty(Que* pq);
//
////队列大小函数 -- 判断队列结点(元素)个数
////接收队列类型指针(包含链表头尾结点) 
//int QueueSize(Que* pq);
//
//
////队列初始化函数 -- 将队列进行初始化
////接收队列类型指针(包含链表头尾结点) 
//void QueueInit(Que* pq)
//{
//	//assert断言队列类型指针不为空：
//	assert(pq != NULL);
//
//	//将队头结点置为空：
//	pq->head = NULL;
//
//	//将队尾结点置为空：
//	pq->tail = NULL;
//
//	//队列结点(元素)个数置为0：
//	pq->size = 0;
//}
//
//
//
////队列销毁函数 -- 将队列销毁
////接收队列类型指针(包含链表头尾结点) 
//void QueueDestroy(Que* pq)
//{
//	//assert断言队列类型指针不为空：
//	assert(pq != NULL);
//
//	//释放队列跟单链表的释放一样
//	//先创建一个在队列进行遍历的指针：
//	QNode* cur = pq->head; //从队头结点开始
//
//	//使用while循环进行遍历释放队列结点：
//	while (cur != NULL)
//	{
//		//先保存下个结点：
//		QNode* next = cur->next;
//
//		//再释放当前结点：
//		free(cur);
//
//		//再指向下个结点：
//		cur = next;
//	}
//
//	//结点都释放后，把队头队尾指针都置空：
//	pq->head = NULL;
//	pq->tail = NULL;
//
//	//再把队列结点(元素)个数置为0：
//	pq->size = 0;
//}
//
//
//
////队列入队函数 -- 用链表的尾插操作实现入队
////接收队列类型指针(包含链表头尾结点) 、尾插值
//void QueuePush(Que* pq, QDataType x)
//{
//	//assert断言队列类型指针不为空：
//	assert(pq != NULL);
//
//	//入队放入元素需要空间，
//	//所以要先为队列结点开辟动态空间：
//	QNode* newnode = (QNode*)malloc(sizeof(QNode));
//	//检查是否开辟成功：
//	if (newnode == NULL)
//	{
//		//开辟失败则打印错误信息：
//		perror("malloc fail");
//		//终止程序：
//		exit(-1);
//	}
//
//	//队列结点完成后将尾插值(x)
//	//赋给队列结点数据域：
//	newnode->data = x;
//	//指针域指向空：
//	newnode->next = NULL;
//
//	//空间开辟后进行尾插：
//	if (pq->tail == NULL)
//		//如果队列刚初始化，队列为空，
//		//头结点指针和尾结点指针都为空：
//	{
//		//那么将刚开辟的结点newnode地址
//		//赋给头结点指针和尾结点指针
//		pq->head = newnode;
//		pq->tail = newnode;
//	}
//	else
//		//队列不为空，进行尾插：
//	{
//		//将目前队尾结点指针域next指向尾插结点：
//		pq->tail->next = newnode;
//		//然后再指向尾插结点，成为新队尾结点：
//		pq->tail = newnode;
//	}
//
//	//插入数据后队列结点(元素)个数++：
//	pq->size++;
//}
//
//
//
////队列出队函数 -- 用链表的头删操作实现出队
////接收队列类型指针(包含链表头尾结点) 
//void QueuePop(Que* pq)
//{
//	//assert断言队列类型指针不为空：
//	assert(pq != NULL);
//	//assert断言队列不为空，没数据不能删除：  
//	assert(QueueEmpty != true); //不为空就继续程序
//
//	//如果队列中只剩一个结点：
//	if (pq->head->next == NULL)
//		//队头指针指向空，说明只剩一个结点，
//		//只剩一个结点说明队头队尾指针都指向这一个结点，
//		//所以这时头删后头指针移动，尾指针也要移动
//	{
//		//先释放("删除")队列目前头结点：
//		free(pq->head);
//
//		//删除后将队头队尾指针都置为空：
//		pq->head = NULL;
//		pq->tail = NULL;
//	}
//	else
//		//队列不止一个结点，则头删后只需移动队头结点：
//	{
//		//用链表的头删操作实现出队,
//		//先保存第二个结点地址：
//		QNode* next = pq->head->next;
//
//		//释放("删除")队列目前头结点：
//		free(pq->head);
//
//		//再将队头结点指针指向原本第二个结点next，
//		//让其成为新的队头结点：
//		pq->head = next;
//	}
//
//	//“删除”后队列结点(元素)个数--：
//	pq->size--;
//}
//
//
//
////队头函数 -- 返回队头结点的数据域数据
////接收队列类型指针(包含链表头尾结点) 
//QDataType QueueFront(Que* pq)
//{
//	//assert断言队列类型指针不为空：
//	assert(pq != NULL);
//	//assert断言队列不为空，没数据不能查找：  
//	assert(QueueEmpty != true); //不为空就继续程序
//
//	//队列有数据，则直接返回队头结点数据域数据：
//	return pq->head->data;
//}
//
//
//
////队尾函数 -- 返回队尾结点的数据域数据
////接收队列类型指针(包含链表头尾结点) 
//QDataType QueueBack(Que* pq)
//{
//	//assert断言队列类型指针不为空：
//	assert(pq != NULL);
//	//assert断言队列不为空，没数据不能查找：  
//	assert(QueueEmpty != true); //不为空就继续程序
//
//	//队列有数据，则直接返回队尾结点数据域数据：
//	return pq->tail->data;
//}
//
//
//
////判空函数 -- 判断队列是否为空
////接收队列类型指针(包含链表头尾结点) 
//bool QueueEmpty(Que* pq)
//{
//	//assert断言队列类型指针不为空：
//	assert(pq != NULL);
//
//	//直接判断队头结点指向的下个结点是否为空：
//	return pq->head == NULL;
//	//是则返回true -- 队列为空
//	//是则返回false -- 队列不为空
//}
//
//
////队列大小函数 -- 判断队列结点(元素)个数
////接收队列类型指针(包含链表头尾结点) 
//int QueueSize(Que* pq)
//{
//	//assert断言队列类型指针不为空：
//	assert(pq != NULL);
//
//	//直接返回size队列结点(元素)个数：
//	return pq->size;
//}
//
////栈（队列实现）类型
//typedef struct {
//	//创建两个队列类型变量：
//	Que q1;
//	Que q2;
//	//MyStack结构体中包含q1、q2两个结构体
//} MyStack;
//
////创建栈（队列实现）类型
//MyStack* myStackCreate() {
//	//开辟空间：
//	MyStack* pst = (MyStack*)malloc(sizeof(MyStack));
//	//栈（队列实现）类型中的队列类型进行初始化：
//	QueueInit(&pst->q1);
//	QueueInit(&pst->q2);
//
//	//返回开辟的空间地址
//	return pst;
//}
//
////栈（队列实现）：入栈
//void myStackPush(MyStack* obj, int x) {
//	//思路：在不为空的队列进行入队(尾插) -- 入栈
//
//	if (QueueEmpty(&obj->q1) != true)
//		//q1队列不为空：
//	{
//		//在q1中入队：
//		QueuePush(&obj->q1, x);
//	}
//	else
//		//q2队列不为空：
//	{
//		//在q2中入队：
//		QueuePush(&obj->q2, x);
//	}
//}
//
////栈（队列实现）：出栈
//int myStackPop(MyStack* obj) {
//	//思路：把不为空队列的前N-1个元素放到空队列
//	//      此时不为空队列就只剩队尾元素，对其进行出队
//	//      实现栈的后进先出
//
//	//假设q1队列为空：
//	Que* empty = &obj->q1;
//	//假设q2队列不为空：
//	Que* nonEmpty = &obj->q2;
//
//	// obj->q1（q2）是得到q1（q2）结构体，
//	//这里empty和nonEmpty是结构体指针，
//	//所以需要队q1（q2）结构体取地址，得到指针
//	//即 &obj->q1（q2）
//
//	//假设错了的话，q1不为空：
//	if (QueueEmpty(&obj->q1) != true)
//	{
//		//则把q1队列的内容给到q2：
//		nonEmpty = &obj->q1;
//
//		//再把q1队列置为空：
//		empty = &obj->q2;
//	}
//
//	//把非空队列nonEmpty前size-1个元素导入空队列empty:
//	while (QueueSize(nonEmpty) > 1)
//		//非空队列元素大于1就继续把元素导入空队列
//	{
//		//使用入队函数，
//		//把非空队列的队头元素QueueFront(nonEmpty)
//		//入队到空队列empty中
//		QueuePush(empty, QueueFront(nonEmpty));
//
//		//导入后非空队列的队头元素出队，进行下次循环：
//		QueuePop(nonEmpty);
//	}
//
//	//导入完成后，获得非空队列仅剩的一个元素：
//	//该元素就是原队列的队尾元素，
//	//栈的后进先出就是栈尾(栈顶)元素
//	int top = QueueFront(nonEmpty);
//
//	//再进行出栈（非空队列仅剩元素出队）：
//	QueuePop(nonEmpty);
//
//	//返回出栈（非空队列仅剩元素出队）元素：
//	return top;
//}
//
////取栈顶元素：
//int myStackTop(MyStack* obj) {
//	//思路：使用QueueBack函数
//	//      取在非空队列的队尾元素
//	//      该队尾元素就是栈顶元素
//
//	if (QueueEmpty(&obj->q1) != true)
//		//p1不为空，取p1队列的队尾元素：
//	{
//		//使用QueueBack函数取队尾元素：
//		return QueueBack(&obj->q1);
//	}
//	else
//		//p2不为空，取p2队列的队尾元素：
//	{
//		//使用QueueBack函数取队尾元素：
//		return QueueBack(&obj->q2);
//	}
//}
//
////判断栈是否为空：
//bool myStackEmpty(MyStack* obj) {
//	//q1、q2两个队列都为空，栈才为空：
//	return QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2);
//	//条件成立返回true
//	//条件不成立返回false
//}
//
////释放栈空间：
//void myStackFree(MyStack* obj) {
//	//MyStack* obj类型里面还有两个队列（链式结构）结构体
//	//所以要先将这两个队列（链式结构）释放：
//	QueueDestroy(&obj->q1);
//	QueueDestroy(&obj->q1);
//
//	//再释放栈（队列实现）类型：
//	free(obj);
//}
//
///**
// * Your MyStack struct will be instantiated and called as such:
// * MyStack* obj = myStackCreate();
// * myStackPush(obj, x);
//
// * int param_2 = myStackPop(obj);
//
// * int param_3 = myStackTop(obj);
//
// * bool param_4 = myStackEmpty(obj);
//
// * myStackFree(obj);
//*/
//
//
//
//
////力扣：232. 用栈实现队列
////思路：
////和用队列实现栈的思路类似，
////要实现栈的先进先出，在栈中就是要让栈底元素先出，
////那么就把除栈底元素外的元素放入另一个栈中，
////此时就进行出栈操作（相当于出队）
////把栈底元素移出，实现队列的“先进先出”
//
////放入时是把栈顶元素(以向下的顺序)放入另一个栈中
////这时在另一个栈中的元素中，元素就是倒着的
////栈顶元素变成了栈顶元素，所以下次再进行出栈操作（相当于出队）时
////就不用再“倒元素”了
////栈中元素本来是“先进后出”的，“倒过来”后就变成了“先进先出”
////所以之后  入数据时就可以放到数据“没倒”的栈中
////          要出数据的话就可以出数据“倒过来”的栈的数据
////直到“倒过来”栈的数据出完了，
////出完了再把“没倒”栈数据移到“倒过来”栈
////这时数据又变成了“先进先出”
//
////可以把“没倒”栈定为 pushst(符合栈的“后进先出”)
////pushst -- 入栈（入队）就选这个
////可以把“倒过来”栈定为 popst(符合队的“先进先出”)
////popst -- 出栈（出队）就选这个
//
////动态顺序表实现栈：
//
////定义栈的栈顶数据元素：
//typedef int STDataType;
//
////定义栈类型：
//typedef struct Stack
//{
//	//为栈开辟动态空间时的头指针：
//	//控制栈内元素的指针
//	STDataType* a;
//
//	//栈顶值(用于定义栈顶)：
//	int top;
//	//类似于数组下标
//
//	//栈存放数据容量（栈的大小）：
//	int capacity;
//
//}ST; //重命名为ST
//
////初始化栈函数 -- 对栈类型进行初始化
////接收栈类型指针
//void STInit(ST* ps);
//
////销毁栈函数 -- 销毁栈类型
////接收栈类型指针
//void STDestroy(ST* ps);
//
////因为只有压栈和出栈操作
////只操作栈顶元素，所以没有
////头插（尾插）头删（头删）等其他操作
//
////压栈函数 -- 进行压栈
////接收栈类型指针（ps）、进行压栈的值（x）
//void STPush(ST* ps, STDataType x);
//
////出栈函数 -- 进行出栈
////接收栈类型指针
//void STPop(ST* ps);
//
////栈顶元素函数 -- 获取栈顶元素
////接收栈类型指针
//STDataType STTop(ST* ps);
//
////栈中元素函数 --计算栈中元素个数
////接收栈类型指针
//int STSize(ST* ps);
//
////判空函数 -- 判断栈是否为空
////接收栈类型指针
//bool STEmpty(ST* ps);
//
//
//
////初始化栈函数 -- 对栈类型进行初始化
////接收栈类型指针
//void STInit(ST* ps)
//{
//	//assert断言栈类型指针不为空：
//	assert(ps != NULL);
//
//	//将栈内元素控制指针置为空：
//	ps->a = NULL;
//	//将栈容量（大小）置为0：
//	ps->capacity = 0;
//	//将栈顶值定义为0：
//	ps->top = 0;
//}
//
//
//
////销毁栈函数 -- 销毁栈类型
////接收栈类型指针
//void STDestroy(ST* ps)
//{
//	//assert断言栈类型指针不为空：
//	assert(ps != NULL);
//
//	//释放栈内元素控制指针：
//	free(ps->a);
//	//并将其置为空：
//	ps->a = NULL;
//	//栈容量置为0：
//	ps->capacity = 0;
//	//栈顶值置为0：
//	ps->top = 0;
//}
//
//
//
////压栈函数 -- 进行压栈
////接收栈类型指针（ps）、进行压栈的值（x）
//void STPush(ST* ps, STDataType x)
//{
//	//assert断言栈类型指针不为空：
//	assert(ps != NULL);
//
//	//为栈开辟动态空间：
//	if (ps->top == ps->capacity)
//		//栈顶值 等于 栈大小
//		//说明空间不够，需要扩容
//	{
//		//只有压栈时容量会增大可能需要扩容
//		//只有这个函数会进行扩容操作，
//		//所以没必要单独写一个扩容函数
//
//		//进行扩容:
//		int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
//		//因为栈容量capacity初始化为0，
//		//所以可以使用三目操作符进行增容：
//		//ps->capacity == 0 ? 4 : ps->capacity * 2
//		//如果为0则直接增容到4，不为0则增容2倍
//
//		//开辟动态空间：
//		STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) * newCapacity);
//		//这里直接使用realloc进行动态空间开辟
//		//如果realloc函数接收头指针（第一个参数）为空，
//		//那么它的作用相当于malloc函数
//
//		//对开辟空间进行检查：
//		if (tmp == NULL)
//			//返回空指针，开辟失败：
//		{
//			//打印错误信息：
//			perror("realloc fail");
//			//终止程序：
//			exit(-1);
//		}
//
//		//开辟成功后，
//		//栈内元素控制指针指向开辟空间：
//		ps->a = tmp;
//
//		//重新设置capacity栈容量：
//		ps->capacity = newCapacity;
//	}
//
//	//将压栈的值（x）放入栈：
//	ps->a[ps->top] = x;
//	//上面以a为头开辟连续的空间，
//	//所以a可以看作一个数组名使用（？）
//	//通过数组下标放入值
//
//	//再调整栈顶值“下标”位置：
//	ps->top++;
//}
//
//
//
////出栈函数 -- 进行出栈
////接收栈类型指针
//void STPop(ST* ps)
//{
//	//assert断言栈类型指针不为空：
//	assert(ps != NULL);
//	//assert断言栈顶top到了栈底就不能继续出栈了
//	assert(ps->top > 0); //栈不为空
//
//	//出栈只要栈顶top--
//	//把栈顶向下移动一个单位即可实现”删除“（出栈）：
//	--ps->top;
//}
//
//
//
////栈顶元素函数 -- 获取栈顶元素
////接收栈类型指针
//STDataType STTop(ST* ps)
//{
//	//assert断言栈类型指针不为空：
//	assert(ps != NULL);
//	//assert断言栈为空的话就不能找栈顶元素了
//	assert(ps->top > 0);
//
//	//返回栈顶元素：
//	return ps->a[ps->top - 1];
//	//top即栈中元素个数：
//	//top从0开始，压栈后top++，先赋值再++
//	//top永远在栈顶元素的下一个位置
//	//所以要获得栈顶元素就要top-1到栈顶元素位置
//}
//
//
//
////栈中元素函数 --计算栈中元素个数
////接收栈类型指针
//int STSize(ST* ps)
//{
//	//assert断言栈类型指针不为空：
//	assert(ps != NULL);
//
//	//top即栈中元素个数：
//	//top从0开始，压栈后top++，先赋值再++
//	//top永远在栈顶元素的下一个位置
//	return ps->top;
//}
//
//
//
////判空函数 -- 判断栈是否为空
////接收栈类型指针
//bool STEmpty(ST* ps)
//{
//	//assert断言栈类型指针不为空：
//	assert(ps != NULL);
//
//	//如果top为0
//	//说明栈中没有元素
//	return ps->top == 0;
//	//top为0 -> 栈为空 -> 返回true
//	//top不为0 -> 栈不为空 -> 返回false
//}
//
////队列类型（用两个栈实现）
//typedef struct {
//	//创建一个pushst栈--用来入栈（实现入队）符合栈的“后进先出”
//	ST pushst;
//	//创建一个popst栈--用来出栈（实现出队）符合队的“先进先出”
//	ST popst;
//} MyQueue;
//
//
////创建队列函数：
//MyQueue* myQueueCreate() {
//	//开辟队列类型（用两个栈实现）空间：
//	MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));
//
//	//初始化obj中的两个结构体：
//	STInit(&obj->pushst);
//	STInit(&obj->popst);
//
//	//返回开辟的队列类型指针：
//	return obj;
//}
//
////队列入队函数（入栈操作实现）：
//void myQueuePush(MyQueue* obj, int x) {
//	//在pushst栈中执行入栈（队列入队）操作：
//	STPush(&obj->pushst, x); //使用入栈函数STPush
//}
//
////返回队头函数：
//int myQueuePeek(MyQueue* obj) {
//	//如果popst为空：
//	if (STEmpty(&obj->popst) == true)
//		//先将数据倒入popst栈：
//		//(将栈底元素（先进元素）移到栈顶再出栈获得队头元素)
//	{
//		while (STEmpty(&obj->pushst) != true)
//			//pushst栈还有元素，那就继续倒入：
//		{
//			//把pushst栈顶元素放到popst栈顶（一开始是栈底）
//			STPush(&obj->popst, STTop(&obj->pushst));
//			//倒入一个元素，pushst栈出栈，准备下个栈顶元素倒入：
//			STPop(&obj->pushst);
//		}
//	}
//
//	//返回popst栈的栈顶元素（队头元素）
//	return STTop(&obj->popst);
//}
//
////队列出队函数（出栈操作实现）：
//int myQueuePop(MyQueue* obj) {
//	//先使用myQueuePeek函数返回队头元素，
//	//myQueuePeek函数可以省去把数据从pushst倒入popst的操作
//	int front = myQueuePeek(obj);
//
//	//取到队头（栈头）元素后进行出栈：
//	STPop(&obj->popst);
//
//	//返回队头元素：
//	return front;
//}
//
////队列判空函数（栈判空函数实现）：
//bool myQueueEmpty(MyQueue* obj) {
//	//obj中两个栈为空，队列才为空；
//	return STEmpty(&obj->popst) && STEmpty(&obj->pushst);
//}
//
////队列销毁函数（栈销毁函数实现）：
//void myQueueFree(MyQueue* obj) {
//	//obj中两个栈销毁：
//	STDestroy(&obj->popst);
//	STDestroy(&obj->pushst);
//
//	//再释放队列类型obj：
//	free(obj);
//}
//
///**
// * Your MyQueue struct will be instantiated and called as such:
// * MyQueue* obj = myQueueCreate();
// * myQueuePush(obj, x);
//
// * int param_2 = myQueuePop(obj);
//
// * int param_3 = myQueuePeek(obj);
//
// * bool param_4 = myQueueEmpty(obj);
//
// * myQueueFree(obj);
//*/
//
//
//
//
//
//
////力扣：622. 设计循环队列
////思路：
////使用顺序表实现队列，
////（数组下标）队头--front ；队尾--rear
////创建k+1个空间，多一个空间不存放位置，
////用来区分循环队列满和不满两种状态
////如果有 (rear+1) % (k+1) == front 
////说明循环队列元素已满
//
////入队时（push），队尾下标（rear）++
////出队时（pop），队头下标（front）++
//
////front到rear中间的内容就是循环队列中的元素
////如果front==rear，那么循环队列元素为0
//
////创建循环队列类型（顺序表实现）
//typedef struct {
//	int* a; //数组首元素地址
//	int front; //队头元素下标
//	int rear; //队尾元素下标
//	int k; //队列长度（固定）
//} MyCircularQueue;
//
////循环队列创建（初始化）
//MyCircularQueue* myCircularQueueCreate(int k) {
//	//为循环队列类型开辟空间：
//	MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
//	//开辟动态空间创建动态顺序表：
//	//(多开一个方便区分空和满的情况)
//	obj->a = (int*)malloc(sizeof(int) * (k + 1));
//
//	//初始化队头队尾下标：
//	obj->front = obj->rear = 0;
//	//获取循环队列长度k：
//	obj->k = k;
//
//	//返回obj的地址：
//	return obj;
//}
//
////循环队列判空函数：
//bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
//	//队头队尾下标相等就说明队列为空：
//	return obj->front == obj->rear;
//}
//
////循环队列判满函数：
//bool myCircularQueueIsFull(MyCircularQueue* obj) {
//	//如果有 (rear+1) % (k+1) == front 
//	//说明循环队列元素已满
//	return (obj->rear + 1) % (obj->k + 1) == obj->front;
//	//取模%可以让队尾下标rear能够重新绕回队头 
//	//判满有两种情况：
//	//1.front在原队头，rear在原队尾
//	//（还未进行循环）
//	//2.front因为出队移到队列中间，rear在front后面
//	//（第二种就是循环了一遍队列）
//}
//
////循环队列入队函数：
//bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
//	//先使用判满函数myCircularQueueIsFull判断
//	//循环队列是否已满，满了则不能再入队:
//	if (myCircularQueueIsFull(obj) == true)
//	{
//		return false; //队列已满不能再入队
//	}
//
//	//未满则使用队尾下标入队：
//	obj->a[obj->rear] = value;
//	//放入后队尾下标++：
//	obj->rear++;
//
//	//队尾下标++可能会重新绕回队列起点：
//	obj->rear %= (obj->k + 1);
//	//rear = 5 % (4+1) = 1
//
//	return true; //入队成功
//}
//
////循环队列元素删除函数：
//bool myCircularQueueDeQueue(MyCircularQueue* obj) {
//	//先判断队列是否已空：
//	if (myCircularQueueIsEmpty(obj))
//	{
//		//已满则不能出队：
//		return false;
//	}
//
//	//要删除元素，直接把队头下标front往后移一位：
//	++obj->front; //之后这个删除元素会被入队函数替换掉
//
//	//队头下标++可能会重新绕回队列起点：
//	obj->front %= (obj->k + 1);
//	//rear = 5 % (4+1) = 1
//
//	return true; //删除成功
//}
//
////循环队列取队头元素：
//int myCircularQueueFront(MyCircularQueue* obj) {
//	//先判断队列是否已空：
//	if (myCircularQueueIsEmpty(obj))
//	{
//		//已满则不能取头：
//		return -1;
//	}
//	else
//	{
//		//有元素则直接返回队头元素：
//		return obj->a[obj->front];
//	}
//}
//
////循环队列取队尾元素：
//int myCircularQueueRear(MyCircularQueue* obj) {
//	//先判断队列是否已空：
//	if (myCircularQueueIsEmpty(obj))
//	{
//		//已满则不能取头：
//		return -1;
//	}
//	else
//	{
//		//有元素则直接返回队尾元素：
//		return obj->a[(obj->rear + (obj->k + 1) - 1) % (obj->k + 1)];
//		//(obj->rear+(obj->k+1)-1) % (obj->obj->k+1) 
//		//保证rear-1得到的队尾下标不小于0
//	}
//}
//
//
//void myCircularQueueFree(MyCircularQueue* obj) {
//	//先释放obj中的顺序表（数组）：
//	free(obj->a);
//	//再释放obj：
//	free(obj);
//}
//
///**
// * Your MyCircularQueue struct will be instantiated and called as such:
// * MyCircularQueue* obj = myCircularQueueCreate(k);
// * bool param_1 = myCircularQueueEnQueue(obj, value);
//
// * bool param_2 = myCircularQueueDeQueue(obj);
//
// * int param_3 = myCircularQueueFront(obj);
//
// * int param_4 = myCircularQueueRear(obj);
//
// * bool param_5 = myCircularQueueIsEmpty(obj);
//
// * bool param_6 = myCircularQueueIsFull(obj);
//
// * myCircularQueueFree(obj);
//*/