#define _CRT_SECURE_NO_WARNINGS 1

//包含栈头文件：
#include "Stack.h"

//测试函数：
void TestStack()
{
	//创建一个栈类型：
	ST st;
	//对其进行初始化：
	STInit(&st);
	//使用压栈往栈中增加元素：
	STPush(&st, 1);
	STPush(&st, 2);
	STPush(&st, 3);
	STPush(&st, 4);
	STPush(&st, 5);
	//元素大于4个再次进行扩容

	//打印当前栈中元素个数：
	printf("目前栈内元素个数为：%d\n", STSize(&st));
	//换行：
	printf("\n");

	//使用while循环：
	//打印栈顶元素再出栈，循环此操作：
	//证明栈的后进先出原则
	while (!STEmpty(&st))
		//链表不为空就继续操作：
	{
		//打印当前栈顶元素：
		printf("出栈前栈顶元素：%d\n", STTop(&st));

		//进行出栈：
		STPop(&st);
	}

	//换行：
	printf("\n");
	//打印当前栈中元素个数：
	printf("目前栈内元素个数为：%d", STSize(&st));

	//进行销毁：
	STDestroy(&st);
}

//主函数
int main()
{
	TestStack();

	return 0;
}
