#define _CRT_SECURE_NO_WARNINGS 1

//包含队列头文件：
#include "Queue.h"

//队列测试函数：
void TestQueue()
{
	//创建队列类型：
	Que q;

	//对队列类型进行初始化：
	QueueInit(&q);

	//进行入队操作：
	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);
	QueuePush(&q, 5);

	//当前队尾值：
	printf("当前队尾值：%d\n", QueueBack(&q));

	//当前队列元素个数：
	printf("当前队列元素个数：%d\n", QueueSize(&q));

	//换行：
	printf("\n");

	//使用while循环遍历进行出队：
	//（类似抽号机，当前号抽完就到下个号）
	while (!
		
		(&q))
		//队列不为空就继续出队：
	{
		//打印出队值：
		printf("当前出队值为：%d\n", QueueFront(&q));
		//进行出队：
		QueuePop(&q); //出队后打印下个出队值
	}

	//换行：
	printf("\n");

	//当前队列元素个数：
	printf("当前队列元素个数：%d", QueueSize(&q));

	//销毁队列：
	QueueDestroy(&q);
}

////主函数：
//int main()
//{
//	//调用队列测试函数：
//	TestQueue();
//
//	return 0;
//}