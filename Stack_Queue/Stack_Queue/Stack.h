#pragma once

//包含之后需要的头文件：
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>


//静态顺序表：
//define N 10
//struct Stack
//{
//	int a[N];
//	int top;
//};
 

//动态顺序表实现栈：

//定义栈的栈顶数据元素：
typedef int STDataType;
  
//定义栈类型：
typedef struct Stack
{
	//为栈开辟动态空间时的头指针：
	//控制栈内元素的指针
	STDataType* a;

	//栈顶值(用于定义栈顶)：
	int top;
	//类似于数组下标

	//栈存放数据容量（栈的大小）：
	int capacity;

}ST; //重命名为ST

//初始化栈函数 -- 对栈类型进行初始化
//接收栈类型指针
void STInit(ST* ps);

//销毁栈函数 -- 销毁栈类型
//接收栈类型指针
void STDestroy(ST* ps);

//因为只有压栈和出栈操作
//只操作栈顶元素，所以没有
//头插（尾插）头删（头删）等其他操作

//压栈函数 -- 进行压栈
//接收栈类型指针（ps）、进行压栈的值（x）
void STPush(ST* ps, STDataType x);

//出栈函数 -- 进行出栈
//接收栈类型指针
void STPop(ST* ps);

//栈顶元素函数 -- 获取栈顶元素
//接收栈类型指针
STDataType STTop(ST* ps);

//栈中元素函数 --计算栈中元素个数
//接收栈类型指针
int STSize(ST* ps);

//判空函数 -- 判断栈是否为空
//接收栈类型指针
bool STEmpty(ST* ps);