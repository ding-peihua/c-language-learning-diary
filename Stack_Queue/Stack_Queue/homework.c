#define _CRT_SECURE_NO_WARNINGS 1

/*
//力扣：20.有效的括号
//动态顺序表：

//定义栈的栈顶数据元素：
typedef char STDataType;

//定义栈类型：
typedef struct Stack
{
	//为栈开辟动态空间时的头指针：
	//控制栈内元素的指针
	STDataType* a;

	//栈顶值(用于定义栈顶)：
	int top;
	//类似于数组下标

	//栈存放数据容量（栈的大小）：
	int capacity;

}ST; //重命名为ST

//初始化栈函数 -- 对栈类型进行初始化
//接收栈类型指针
void STInit(ST* ps);

//销毁栈函数 -- 销毁栈类型
//接收栈类型指针
void STDestroy(ST* ps);

//因为只有压栈和出栈操作
//只操作栈顶元素，所以没有
//头插（尾插）头删（头删）等其他操作

//压栈函数 -- 进行压栈
//接收栈类型指针（ps）、进行压栈的值（x）
void STPush(ST* ps, STDataType x);

//出栈函数 -- 进行出栈
//接收栈类型指针
void STPop(ST* ps);

//栈顶元素函数 -- 获取栈顶元素
//接收栈类型指针
STDataType STTop(ST* ps);

//栈中元素函数 --计算栈中元素个数
//接收栈类型指针
int STSize(ST* ps);

//判空函数 -- 判断栈是否为空
//接收栈类型指针
bool STEmpty(ST* ps);



//初始化栈函数 -- 对栈类型进行初始化
//接收栈类型指针
void STInit(ST* ps)
{
	//assert断言栈类型指针不为空：
	assert(ps != NULL);

	//将栈内元素控制指针置为空：
	ps->a = NULL;
	//将栈容量（大小）置为0：
	ps->capacity = 0;
	//将栈顶值定义为0：
	ps->top = 0;
}



//销毁栈函数 -- 销毁栈类型
//接收栈类型指针
void STDestroy(ST* ps)
{
	//assert断言栈类型指针不为空：
	assert(ps != NULL);

	//释放栈内元素控制指针：
	free(ps->a);
	//并将其置为空：
	ps->a = NULL;
	//栈容量置为0：
	ps->capacity = 0;
	//栈顶值置为0：
	ps->top = 0;
}



//压栈函数 -- 进行压栈
//接收栈类型指针（ps）、进行压栈的值（x）
void STPush(ST* ps, STDataType x)
{
	//assert断言栈类型指针不为空：
	assert(ps != NULL);

	//为栈开辟动态空间：
	if (ps->top == ps->capacity)
		//栈顶值 等于 栈大小
		//说明空间不够，需要扩容
	{
		//只有压栈时容量会增大可能需要扩容
		//只有这个函数会进行扩容操作，
		//所以没必要单独写一个扩容函数

		//进行扩容:
		int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		//因为栈容量capacity初始化为0，
		//所以可以使用三目操作符进行增容：
		//ps->capacity == 0 ? 4 : ps->capacity * 2
		//如果为0则直接增容到4，不为0则增容2倍

		//开辟动态空间：
		STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) * newCapacity);
		//这里直接使用realloc进行动态空间开辟
		//如果realloc函数接收头指针（第一个参数）为空，
		//那么它的作用相当于malloc函数

		//对开辟空间进行检查：
		if (tmp == NULL)
			//返回空指针，开辟失败：
		{
			//打印错误信息：
			perror("realloc fail");
			//终止程序：
			exit(-1);
		}

		//开辟成功后，
		//栈内元素控制指针指向开辟空间：
		ps->a = tmp;

		//重新设置capacity栈容量：
		ps->capacity = newCapacity;
	}

	//将压栈的值（x）放入栈：
	ps->a[ps->top] = x;
	//上面以a为头开辟连续的空间，
	//所以a可以看作一个数组名使用（？）
	//通过数组下标放入值

	//再调整栈顶值“下标”位置：
	ps->top++;
}



//出栈函数 -- 进行出栈
//接收栈类型指针
void STPop(ST* ps)
{
	//assert断言栈类型指针不为空：
	assert(ps != NULL);
	//assert断言栈顶top到了栈底就不能继续出栈了
	assert(ps->top > 0); //栈不为空

	//出栈只要栈顶top--
	//把栈顶向下移动一个单位即可实现”删除“（出栈）：
	--ps->top;
}



//栈顶元素函数 -- 获取栈顶元素
//接收栈类型指针
STDataType STTop(ST* ps)
{
	//assert断言栈类型指针不为空：
	assert(ps != NULL);
	//assert断言栈为空的话就不能找栈顶元素了
	assert(ps->top > 0);

	//返回栈顶元素：
	return ps->a[ps->top - 1];
	//top即栈中元素个数：
	//top从0开始，压栈后top++，先赋值再++
	//top永远在栈顶元素的下一个位置
	//所以要获得栈顶元素就要top-1到栈顶元素位置
}



//栈中元素函数 --计算栈中元素个数
//接收栈类型指针
int STSize(ST* ps)
{
	//assert断言栈类型指针不为空：
	assert(ps != NULL);

	//top即栈中元素个数：
	//top从0开始，压栈后top++，先赋值再++
	//top永远在栈顶元素的下一个位置
	return ps->top;
}



//判空函数 -- 判断栈是否为空
//接收栈类型指针
bool STEmpty(ST* ps)
{
	//assert断言栈类型指针不为空：
	assert(ps != NULL);

	//如果top为0
	//说明栈中没有元素
	return ps->top == 0;
	//top为0 -> 栈为空 -> 返回true
	//top不为0 -> 栈不为空 -> 返回false
}


//创建栈类型和对应功能后，来完成这道题：

bool isValid(char* s) {
	//括号的类型、顺序和数量都需要匹配

	//创建栈类型：
	ST st;

	//初始化栈类型：
	STInit(&st);

	//栈的栈顶值：
	char topVal;

	//使用while循环
	while (*s != 0)
		//循环直到结束符\0
	{
		//判断的第二种方法：if else:
		if (*s == '(' || *s == '[' || *s == '{')
			//*s为各种左括号：
		{
			//将各种左括号进行压栈：
			STPush(&st, *s);
		}
		else
		{
			//现在栈中的都是左括号，现在x是右括号
			//从栈中拿出当前左括号与现在x右括号进行匹配：

			//判断右括号的前提是栈中已有左括号：
			if (STEmpty(&st))
			{
				//已经没有左括号，就没必要判断右括号了
				//括号数量不匹配：
				return false;
			}

			//起到当前栈顶元素（左括号）：
			topVal = STTop(&st);

			//取到栈顶元素后进行出栈：
			STPop(&st); //方便进行下次匹配

			//判断是否匹配：
			if ((*s == ']' && topVal != '[')
				|| (*s == ')' && topVal != '(')
				|| (*s == '}' && topVal != '{'))
			{
				//销毁栈：
				STDestroy(&st);
				//三种括号都进行判断
				//不匹配就返回false：
				return false;
			}
		}
		++s;
	}

	//栈不为空，返回false，说明 括号数量 不匹配
	//栈内元素出栈到无元素，说明都匹配，匹配成功：
	bool ret = STEmpty(&st);

	//销毁栈：
	STDestroy(&st);
	//空间是动态分配的，不销毁会造成内存泄漏

//循环中没返回false则到此匹配成功：
	return ret;
}
*/