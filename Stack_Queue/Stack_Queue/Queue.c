#define _CRT_SECURE_NO_WARNINGS 1

//包含队列头文件：
#include "Queue.h"

//队列初始化函数 -- 将队列进行初始化
//接收队列类型指针(包含链表头尾结点) 
void QueueInit(Que* pq)
{
	//assert断言队列类型指针不为空：
	assert(pq != NULL);

	//将队头结点置为空：
	pq->head = NULL;

	//将队尾结点置为空：
	pq->tail = NULL;

	//队列结点(元素)个数置为0：
	pq->size = 0;
}



//队列销毁函数 -- 将队列销毁
//接收队列类型指针(包含链表头尾结点) 
void QueueDestroy(Que* pq)
{
	//assert断言队列类型指针不为空：
	assert(pq != NULL);

	//释放队列跟单链表的释放一样
	//先创建一个在队列进行遍历的指针：
	QNode* cur = pq->head; //从队头结点开始

	//使用while循环进行遍历释放队列结点：
	while (cur != NULL)	
	{
		//先保存下个结点：
		QNode* next = cur->next;

		//再释放当前结点：
		free(cur);

		//再指向下个结点：
		cur = next;
	}

	//结点都释放后，把队头队尾指针都置空：
	pq->head = NULL;
	pq->tail = NULL;

	//再把队列结点(元素)个数置为0：
	pq->size = 0;
}



//队列入队函数 -- 用链表的尾插操作实现入队
//接收队列类型指针(包含链表头尾结点) 、尾插值
void QueuePush(Que* pq, QDataType x)
{
	//assert断言队列类型指针不为空：
	assert(pq != NULL);

	//入队放入元素需要空间，
	//所以要先为队列结点开辟动态空间：
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	//检查是否开辟成功：
	if (newnode == NULL)
	{
		//开辟失败则打印错误信息：
		perror("malloc fail");
		//终止程序：
		exit(-1);
	}

	//队列结点完成后将尾插值(x)
	//赋给队列结点数据域：
	newnode->data = x;
	//指针域指向空：
	newnode->next = NULL;

	//空间开辟后进行尾插：
	if (pq->tail == NULL)
		//如果队列刚初始化，队列为空，
		//头结点指针和尾结点指针都为空：
	{
		//那么将刚开辟的结点newnode地址
		//赋给头结点指针和尾结点指针
		pq->head = newnode;
		pq->tail = newnode;
	}
	else
		//队列不为空，进行尾插：
	{
		//将目前队尾结点指针域next指向尾插结点：
		pq->tail->next = newnode;
		//然后再指向尾插结点，成为新队尾结点：
		pq->tail = newnode;
	}

	//插入数据后队列结点(元素)个数++：
	pq->size++;
}



//队列出队函数 -- 用链表的头删操作实现出队
//接收队列类型指针(包含链表头尾结点) 
void QueuePop(Que* pq)
{
	//assert断言队列类型指针不为空：
	assert(pq != NULL);
	//assert断言队列不为空，没数据不能删除：  
	assert(QueueEmpty(pq) != true); //不为空就继续程序

	//如果队列中只剩一个结点：
	if (pq->head->next == NULL)
		//队头指针指向空，说明只剩一个结点，
		//只剩一个结点说明队头队尾指针都指向这一个结点，
		//所以这时头删后头指针移动，尾指针也要移动
	{
		//先释放("删除")队列目前头结点：
		free(pq->head);

		//删除后将队头队尾指针都置为空：
		pq->head = NULL;
		pq->tail = NULL;
	}
	else
		//队列不止一个结点，则头删后只需移动队头结点：
	{
		//用链表的头删操作实现出队,
		//先保存第二个结点地址：
		QNode* next = pq->head->next;

		//释放("删除")队列目前头结点：
		free(pq->head);

		//再将队头结点指针指向原本第二个结点next，
		//让其成为新的队头结点：
		pq->head = next;
	}

	//“删除”后队列结点(元素)个数--：
	pq->size--; 
}



//队头函数 -- 返回队头结点的数据域数据
//接收队列类型指针(包含链表头尾结点) 
QDataType QueueFront(Que* pq)
{
	//assert断言队列类型指针不为空：
	assert(pq != NULL);
	//assert断言队列不为空，没数据不能查找：  
	assert(QueueEmpty(pq) != true); //不为空就继续程序

	//队列有数据，则直接返回队头结点数据域数据：
	return pq->head->data;
}



//队尾函数 -- 返回队尾结点的数据域数据
//接收队列类型指针(包含链表头尾结点) 
QDataType QueueBack(Que* pq)
{
	//assert断言队列类型指针不为空：
	assert(pq != NULL);
	//assert断言队列不为空，没数据不能查找：  
	assert(QueueEmpty(pq) != true); //不为空就继续程序

	//队列有数据，则直接返回队尾结点数据域数据：
	return pq->tail->data;
}



//判空函数 -- 判断队列是否为空
//接收队列类型指针(包含链表头尾结点) 
bool QueueEmpty(Que* pq)
{
	//assert断言队列类型指针不为空：
	assert(pq != NULL);

	//直接判断队头结点指向的下个结点是否为空：
	return pq->head == NULL; 
	//是则返回true -- 队列为空
	//是则返回false -- 队列不为空
}


//队列大小函数 -- 判断队列结点(元素)个数
//接收队列类型指针(包含链表头尾结点) 
int QueueSize(Que* pq)
{
	//assert断言队列类型指针不为空：
	assert(pq != NULL);

	//直接返回size队列结点(元素)个数：
	return pq->size;
}