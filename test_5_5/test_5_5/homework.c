#define _CRT_SECURE_NO_WARNINGS 1


//定义了一维 int 型数组 a[10] 后，下面错误的引用是：
//#include <stdio.h>
//int main()
//{
//	int a[10] = { 0,1,2,3,4,5,6,7,8,9 };
//
//	a[0] = 1;
//	a[0] = 5 * 2;
//	a[0] = 2;
//	a[1] = a[2] * a[0];
//
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int arr[] = { 1,2,(3,4),5 };
//    printf("%d\n", sizeof(arr));
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    char str[] = "hello bit";
//    printf("%d %d\n", sizeof(str), strlen(str));
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int a[2][3] = { 1,2,3,4,5,6 };
//	printf("%d", a[2][1]);
//	return 0;
//}


//将数组A中的内容和数组B中的内容进行交换。（数组一样大）
//#include <stdio.h>
//#include <string.h>
//
//int main() 
//{
//	int A[5] = { 1,2,3,4,5 };
//	//下标：     0 1 2 3 4 
//	int B[5] = { 6,7,8,9,10 };
//	//下标：     0 1 2 3 4 
//	
//	int sz = sizeof(A) / sizeof(A[0]);
//
//	//进行交换：
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int tmp = A[i];
//
//		A[i] = B[sz -1 - i];
//
//		B[sz -1 - i] = tmp;
//	}
//
//
//	//输出：
//	int k = 0;
//	for (k = 0; k < 5; k++)
//	{
//		printf("%d ", A[k]);
//	}
//
//	printf("\n");
//
//	int j = 0;
//	for (j = 0; j < 5; j++)
//	{
//		printf("%d ", B[j]);
//	}
//	
//
//	return 0;
//}






//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。
#include <stdio.h>

//实现print()  打印数组的每个元素
void print(int* arr, int sz)
{
	//输出：
	int j = 0;
	for (j = 0; j < sz; j++)
	{
		printf("%d ", arr[j]);
	}

	//换行：
	printf("\n");
}


//实现reverse()  函数完成数组元素的逆置。
void reverse(int* arr, int sz)
{
	int i = 0;
	for (i = 0; i < (sz/2); i++) 
		//循环条件？？？
		//6个元素调3次，5个元素掉两次
		//sz/2 肯定够用
	{
		int* left = arr + i;
		int* right = arr + sz - 1 - i;
		// 使用指针移位，类似下标，易混
		// 让左右指针慢慢往中间靠

		int tmp = *left;
		*left = *right;
		*right = tmp;
		// 获取指针值是用 * 
	}
}


//实现函数init() 初始化数组为全0
void init(int* arr, int sz)
{
	//arr：首元素地址 ； sz为几就循环几次
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		int* tmp = arr + i; //创建指针变量存放指针
		*tmp = 0; //取指针中的值，并赋为0
	}

}

int main()
{
	int arr[5] = { 1,2,3,4,5 };

	int sz = sizeof(arr) / sizeof(arr[0]); //元素个数

	//实现print()  打印数组的每个元素
	print(arr, sz);


	//实现reverse()  函数完成数组元素的逆置。
	reverse(arr, sz);
	print(arr, sz);


	//实现函数init() 初始化数组为全0
	init(arr, sz);
	print(arr, sz);


	return 0;
}
