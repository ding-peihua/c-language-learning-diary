#define _CRT_SECURE_NO_WARNINGS 1

//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。
//#include <stdio.h>
//
//
////实现print()  打印数组的每个元素
//void print(int* arr, int sz)
//{
//	//输出：
//	int j = 0;
//	for (j = 0; j < sz; j++)
//	{
//		printf("%d ", arr[j]);
//	}
//
//
//	//换行：
//	printf("\n");
//}
//
////实现reverse()  函数完成数组元素的逆置。
//void reverse(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < (sz / 2); i++)
//		//循环条件？？？
//		//6个元素调3次，5个元素掉两次
//		//sz/2 肯定够用
//	{
//		int* left = arr + i;
//		int* right = arr + sz - 1 - i;
//		// 使用指针移位，类似下标，易混
//		// 让左右指针慢慢往中间靠
//
//
//		int tmp = *left;
//		*left = *right;
//		*right = tmp;
//		// 获取指针值是用 * 
//	}
//}
//
////实现函数init() 初始化数组为全0
//void init(int* arr, int sz)
//{
//	//arr：首元素地址 ； sz为几就循环几次
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int* tmp = arr + i; //创建指针变量存放指针
//		*tmp = 0; //取指针中的值，并赋为0
//	}
//}
//
//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//
//
//	int sz = sizeof(arr) / sizeof(arr[0]); //元素个数
//
//
//	//实现print()  打印数组的每个元素
//	print(arr, sz);
//
//	//实现reverse()  函数完成数组元素的逆置。
//	reverse(arr, sz);
//	print(arr, sz);
//
//	//实现函数init() 初始化数组为全0
//	init(arr, sz);
//	print(arr, sz);
//
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//
//int main() {
//
//    int n = 0; //第二行第一个升序序列中数字的个数
//    int m = 0; //第三行第二个升序序列中数字的个数
//    int i = 0; //用于循环
//
//    //输入
//    scanf("%d %d", &n, &m);
//
//    //生成两个足够大的数组 并 进行循环赋值
//    int N[3000] = { 0 };
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &N[i]);
//    }
//    int M[3000] = { 0 };
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &M[i]);
//    }
//
//    //循环排序
//    int arr[3000] = { 0 };//排序后放进新数组
//    for (i = 0; i < n + m; i++)
//    {
//        if (N[i] < M[i])
//        {
//            arr[i] = N[i];
//            arr[i + 1] = M[i];
//        }
//        n -= 1;
//        m -= 1;
//        while (n <= 0 && m <= 0)
//        {
//            break;
//        }
//    }
//
//    for (i = 0; i < strlen(arr); i++)
//    {
//        printf("%d ", arr[i]);
//    }
//
//
//    return 0;
//}

#include <stdio.h>

int main() {
    int input = 0;
    //输入
    scanf("%d", &input);

    int arr[1000] = { 0 };
    int i = 0;
    //输入两个升序序列
    for (i = 0; i < input; i++) {
        scanf("%d", &arr[i]);//注意加取地址符
    }

    //进行判断：
    int flag1 = 0;
    int flag2 = 0;
    for (i = 0; i < input; i++)
    {
        if ((arr[i] < arr[i + 1]))
        {
            flag1 = 1;
        }
        else if ((arr[i] > arr[i + 1]))
        {
            flag2 = 1;
        }
    }

    if (flag1 && flag2)
    {
        printf("unsorted\n");//只有当flag1和flag2都为1的时候序列无序
    }
    else {
        printf("sorted\n");
    }

    return 0;
}