#define _CRT_SECURE_NO_WARNINGS 1


////文件操作需要该头文件
//#include <stdio.h> 
//int main()
//{
//	//打开文件
//	FILE* fopen(const char* filename, const char* mode);
//
//
//	//关闭文件
//	int fclose(FILE * stream);
//
//	return 0;
//}


////文件操作需要该头文件
//#include <stdio.h> 
//int main()
//{
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "w");
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	
//	//关闭文件
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}


////文件操作需要该头文件
//#include <stdio.h> 
//int main()
//{
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "w");
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//顺序读写：
//	//写文件：
//	/*fputc('a', pf);
//	fputc('b', pf);
//	fputc('c', pf);*/
//	//也可以使用for循环写进文件：
//	int i = 0;
//	for (i = 0; i < 26; i++)
//	{
//		fputc('a' + i, pf);
//	}
//
//	//读文件：
//
//	//关闭文件
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}


////文件操作需要该头文件
//#include <stdio.h> 
//int main()
//{
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "r");
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//顺序读写：
//
//	//读文件：
//	int i = 0;
//	for ( i = 0; i < 26; i++)
//	{
//		int ch = fgetc(pf);
//		printf("%c", ch);
//
//	}
//
//	printf("\n");
//
//	//关闭文件
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}


////文件操作需要该头文件
//#include <stdio.h> 
//int main()
//{
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "w");
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//顺序读写：
//	//写文件 - 写一行
//	fputs("hello world\n", pf);
//	fputs("hello good good world\n", pf);
//	// 第一个参数：要输出的一行字符串，换行要自己加
//	// 第二个参数：输出流，填写要输出的目标
//
//
//	//关闭文件
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}


////文件操作需要该头文件
//#include <stdio.h> 
//int main()
//{
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "r");
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//顺序读写：
//	//读文件 - 读一行
//	
//	//读一行文件要先创建一个字符数组
//	//来存储待会从文件读到的数据：
//	char arr[10] = { 0 }; 
//	
//	//使用fgets函数读一行：
//	fgets(arr, 10, pf);
//
//	//打印读取到的数据：
//	printf("%s", arr); 
//	//直接打印用于存储的字符串即可
//
//
//	//关闭文件
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}


//#include <stdio.h>
//struct S
//{
//	int a; //整型数据
//	float s; //浮点型数据
//};
//
//int main()
//{
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "w");
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//顺序读写：
//
//	//写文件 - fprintf函数：
//	struct S s = { 100, 3.14f }; //创建结构体变量
//	//使用fprintf函数输出带格式的数据：
//	fprintf(pf, "%d %f", s.a, s.s);
//
//	//关闭文件d
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}

//#include <stdio.h>
//struct S
//{
//	int a; //整型数据
//	float s; //浮点型数据
//};
//
//int main()
//{
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "r");
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//顺序读写：
//
//	//读文件：
//	struct S s = { 0 };
//
//	fscanf(pf, "%d %f", &(s.a), &(s.s));
//	//读取后放在结构体变量s中
//
//	//打印查看读取效果：
//	printf("%d %f", s.a, s.s);
//
//	//关闭文件d
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}

//#include <stdio.h>
////fwrite:
//struct S
//{
//	int a;
//	float s;
//	char str[10];
//};
//
//int main()
//{
//	//创建一个结构体变量：
//	struct S s = { 99, 6.18f, "hello" };
//
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "wb"); 
//	//注：二级制写文件是“wb”
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//以二级制形式写文件：
//	fwrite(&s, sizeof(struct S), 1, pf);
//
//	//关闭文件d
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}


//#include <stdio.h>
////fwrite:
//struct S
//{
//	int a;
//	float s;
//	char str[10];
//};
//
//int main()
//{
//	//创建FILE类型指针，使用fopen函数进行文件操作
//	FILE* pf = fopen("data.txt", "rb"); 
//	//注：二级制读文件是“rb”
//	//第一个参数文件名可以是相对路径或绝对路径
//
//	//操作失败可能会返回空指针，进行检验：
//	if (pf == NULL)
//	{
//		//打印错误信息：
//		perror("fopen");
//		return 1;
//	}
//
//	//创建一个结构体变量：
//	//读取完二进制数据后存入改结构体变量中
//	struct S s = { 0 };
//
//	//以二级制形式读取文件：
//	fread(&s, sizeof(struct S), 1, pf);
//
//	//读取后打印查看读取情况：
//	printf("%d %f %s\n", s.a, s.s, s.str);
//
//	//关闭文件d
//	fclose(pf); //这样只是文件关闭了
//	//还需要把文件指针置为空指针：
//	pf = NULL;
//
//	return 0;
//}




//#include <stdio.h>
//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//b
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//c
//
//	//使用参数三的 SEEK CUR 进行演示：
//	fseek(pf, -3, SEEK_CUR);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//	
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//b
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//c
//
//	//使用ftell函数当前偏移量：
//	int pos = ftell(pf);
//	printf("%d\n", pos);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}



//#include <stdio.h>
//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//b
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//c
//
//	//使用rewind函数让文件指针回到初始位置：
//	rewind(pf);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}