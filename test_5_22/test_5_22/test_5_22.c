#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int arr[10] = { 0 };
//	
//	printf("%p\n",&i);
//	printf("%p\n",&arr[0]);
//	printf("%p\n",&arr[9]);
//
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//		printf("hehe\n");
//	}
//
//	return 0;
//}


//#include <stdio.h>
//模拟实现库函数：strcpy,把一个字符串赋给另一个字符串。
//形参 为 目标地址、被复制字符串地址
//返回值 为 char* ：返回目标地址的首元素地址
//复制包括 结束符：“\0”

//#include <assert.h>

//版本1：
//void my_strcpy(char* dest, char* src)
//{
//	//如果 目标地址 是 NULL空指针 就会出问题
//	//为防止出现空指针，可以设置 断言asset宏
//	
//	//断言(设置一个表达式，如果表达式为假则报错)
//	assert(dest != NULL); //使用时要包含头文件 <assert.h>
//	assert(src != NULL); 
//	//括号中存放我们期待的结果（表达式），一旦表达式为假就会报错
//
//	while (*src != '\0')
//	//可以直接写成（*src），因为 \0 的ASCII码值为0，判定到0就跳出循环
//	{
//		*dest = *src; //把对应位置的src的值赋给dest
//		//两指针移至下一位
//		dest++;
//		src++;
//	}
//	//当while循环到\0时，跳出循环，\0 还没有赋值给目标地址
//	//所以跳出循环后要再赋值一次，把 \0 赋给目标地址
//	*dest = *src;
//}

////版本2：
//#include <stdio.h>
//#include <assert.h>
////函数返回的是目标空间的起始地址
//char* my_strcpy(char* dest, const char* src) //这里const放在*左边，限制了*src，但还可以使用src
//// const：使具有 常属性 ，
////修饰指针时：
////当 const 放在 * 的左边时，限制的是 *p （指针指向内容），即 *p 不能被改变（不能通过指针变量改变指针的内容）
//// 但是 p 是可以改变的（指针变量的本身是可以改变的）
////当 const 放在 * 的右边时，限制的是 p （指针变量本身），即 p （指针变量本身）不能被改变
//// 但是 *p （指针内容）是可以通过指针来改变的
//{
//	char* ret = dest; //先把dest的首地址存起来，方便后面保存
//	//因为之后赋值后，dest会后移，就不是首地址了
//
//	//断言
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	while (*dest++ = *src++)
//		; //空语句
//	//把赋值操作直接放在while的判定条件中
//	//例如：把src中的 h 赋给 dest，此时表达式判定结果为 h
//	// h 的ASCII码值 不为0，所以循环继续
//	//当把 \0 赋给dest时，表达式结果为 0，停止循环
//	//此时即把 \0 赋值了过去，又停止了 循环
//	
//	//使用后置++，先使用地址上的内容，再++，判定下一位
//	
//	//*dest++：先执行 *dest 解引用，之后再执行 dest++
//
//	//直接把 while 的大括号给省了
//
//	return ret;
//}
//
//int main()
//{
//	char arr1[] = "hello bit";
//	char arr2[20] = "xxxxxxxxxxxxx";
//	//char* p = NULL; //断言测试
//
//	//my_strcpy(arr2, arr1);
//	//printf("%s\n", arr2);
//
//	printf("%s\n", my_strcpy(arr2, arr1)); 
//	//模仿的 strcpy 会返回目标地址的首元素地址 
//	//此时使用了 链式访问：一个函数的返回值 作为 另一个函数的返回值
//
//	return 0;
//}

//模拟写出 strlen 函数
//#include <stdio.h>
//#include <assert.h>
////原 strlen 的返回值是 size_t
////size_t 是专门为 sizeof 设计的一个类型
////size_t 本质是 unsigned int / unsigned long (无符号整型 / 无符号长整型)
////unsigned：保证没有负数，长度最小是0，不可能为负数
//size_t my_strlen(const char* str)
////使用 const 限制 *str，防止内容被修改
//{
//	//使用断言：指针不能为空
//	assert(str != NULL);
//	//计数器
//	size_t count = 0;
//	//开始计数
//	while (*str) //内容不为 \0 则继续数
//	{
//		count++;//能进循环说明此时数组有值
//		str++;//指针移向下一位
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abc";
//	int len = my_strlen(arr);
//	printf("%zd\n", len);
//	//%zd ：专门用来打印 size_t 类型的值
//	//%u ：打印无符号整数
//
//	return 0;
//}


//#include <stdio.h>
//int i;
//int main()
//{
//    i--;
//    if (i > sizeof(i))
//    {
//        printf(">\n");
//    }
//    else
//    {
//        printf("<\n");
//    }
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int n = 0;
//	int m = 0;
//	
//	scanf("%d %d", &n, &m);
//
//	//变长数组
//	int arr1[n];
//	int arr2[m];
//
//	return 0;
//}

//#include <stdio.h>
//
//int main() {
//    int n = 0;
//    int m = 0;
//    //牛客网是支持变长数组的
//    scanf("%d %d", &n, &m);
//
//    //变长数组
//    int arr1[n];
//    int arr2[m];
//
//    //数组赋值
//    //接受输入的第一行数据
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr1[i]);
//    }
//    //接受输入的第二行数据
//    i = 0;
//    for (i = 0; i < m; i++)
//    {
//        scanf("%d", &arr2[i]);
//    }
//
//    //进行合并：
//    int arr3[n + m];//变长数组不能初始化
//    i = 0; //arr1下标
//    int j = 0; //arr2下标
//    int k = 0; //arr3下标
//
//    while (i < n && j < m)
//        //i<n：arr1下标小于arr1长度，arr1还有值
//        //j<m：arr2下标小于arr2长度，arr2还有值
//        //两个数组都有值再进行比较赋值
//    {
//        if (arr1[i] < arr2[j])//arr1的值小于arr2的值
//        {
//            arr3[k] = arr1[i];//把大的值赋给arr3
//            //给值 和 被给值 的数组下标都往后移
//            k++;
//            i++;
//        }
//        else //等于时放谁都可以 + arr2比较小
//        {
//            arr3[k] = arr2[j];//把大的值赋给arr3
//            //给值 和 被给值 的数组下标都往后移
//            k++;
//            j++;
//        }
//    }
//    // 此时 i ，j ，k 下标都已经移到了合适的位置
//    //跳出数组说明有一个数组已经遍历完了
//    //判断是哪个数组遍历完了
//    if (i == n)//arr1下标等于数组长度
//    {
//        //arr1遍历完了，需要将arr2中剩余的元素全部放在arr3中
//        while (j < m)//arr2下标还小于arr2数组长度就继续
//        {
//            arr3[k] = arr2[j];
//            k++;
//            j++;
//        }
//    }
//    else
//    {
//        //arr2遍历完了，需要将arr1中剩余的元素全部放在arr3中
//        while (i < n)//arr2下标还小于arr2数组长度就继续
//        {
//            arr3[k] = arr1[i];
//            k++;
//            i++;
//        }
//    }
//
//    //输出
//    for (i = 0; i < n + m; i++)
//    {
//        printf("%d", arr3[i]);
//    }
//
//    return 0;
//}

//#include <stdio.h>
//
//int main() {
//    int n = 0;
//    int m = 0;
//    //牛客网是支持变长数组的
//    scanf("%d %d", &n, &m);
//
//    //变长数组
//    int arr1[n];
//    int arr2[m];
//
//    //数组赋值
//    //接受输入的第一行数据
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr1[i]);
//    }
//    //接受输入的第二行数据
//    i = 0;
//    for (i = 0; i < m; i++)
//    {
//        scanf("%d", &arr2[i]);
//    }
//
//    i = 0; //arr1下标
//    int j = 0; //arr2下标
//
//    while (i < n && j < m)
//        //i<n：arr1下标小于arr1长度，arr1还有值
//        //j<m：arr2下标小于arr2长度，arr2还有值
//        //两个数组都有值再进行比较赋值
//    {
//        if (arr1[i] < arr2[j])//arr1的值小于arr2的值
//        {
//            printf("%d ", arr1[i++]);//哪个小打印哪个
//        }
//        else //等于时放谁都可以 + arr2比较小
//        {
//            printf("%d ", arr2[j++]);//哪个小打印哪个
//        }
//    }
//    // 此时 i ，j 下标都已经移到了合适的位置
//    //跳出数组说明有一个数组已经遍历完了
//    //判断是哪个数组遍历完了
//    if (i == n)//arr1下标等于数组长度
//    {
//        //arr1遍历完了
//        while (j < m)//arr2下标还小于arr2数组长度就继续
//        {
//            printf("%d ", arr2[j++]);
//        }
//    }
//    else
//    {
//        //arr2遍历完了
//        while (i < n)//arr2下标还小于arr2数组长度就继续
//        {
//            printf("%d ", arr1[i++]);
//        }
//    }
//
//
//    return 0;
//}

