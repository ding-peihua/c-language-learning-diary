#pragma once
//Func函数声明头文件（C++版本）：


//包含IO流头文件：
#include <iostream>

//展开std命名空间：
using namespace std;

//定义重载函数：参数顺序不同
void Func(int a, char b);

//定义重载函数：参数顺序不同
void Func(char b, int a);