#define _CRT_SECURE_NO_WARNINGS 1
//Func函数实现文件（C++）:


//包含Func函数对应头文件：
#include "Func.h"

//定义重载函数：参数顺序不同
void Func(int a, char b)
{
	//打印当前参数值：
	cout << "f(int a, char b)" << endl;
}

//定义重载函数：参数顺序不同
void Func(char b, int a)
{
	//打印当前参数值：
	cout << "f(char b, int a)" << endl;
}