#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h" //包含栈头文件

//void f(int i)
//{
//	cout << i << endl;
//}
//
//void func()
//{
//	f(5);
//	/*
//	* 在函数实现（定义）文件下：
//	* 有了头文件，就有函数声明，
//	* 又是函数实现文件，有函数的定义，
//	* 所以可以调用内联函数f()，
//	* 虽然声明展开后找不到内联函数实现的地址，
//	* 但本文件本来就包含该内联函数的地址，
//	* 所以就不会报错
//	*/
//}
/*
* 所以如果内联函数的声明和实现分别分离在
* .h文件 和 .cpp文件 中的话，那就不能在其它文件中
* 使用该内联函数，只能在当前.cpp文件中使用。
* 所以内联函数不要将声明和定义（实现）分离是最好的选择
*/











////栈函数（C++版本）实现文件：
//
////包含栈头文件：
//#include "Stack.h"
//
////使用自己的命名空间实现栈，
////防止和别人写栈有命名冲突
//namespace ggdpz
//{
//	//栈初始化函数：
//	void StackInit(ST* ps)
//	{
//		ps->a = NULL;
//		ps->top = 0;
//		ps->capacity = 0;
//	}
//
//	//出栈函数：
//	void StackPush(ST* ps, int x)
//	{
//		//……
//	}
//}

