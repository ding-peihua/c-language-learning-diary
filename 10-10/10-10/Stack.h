#pragma once

//包含之后所需头文件：
#include <iostream> //包含IO流头文件
using namespace std; //展开std命名空间

//函数的声明：
void f(int i); //这样可以

//内联函数的声明和实现分离（不建议）：
inline void f(int i); 
//此时使用内联函数又不可以了

void func();

//内联函数的声明和实现不分离（建议）：
inline void f(int i)
{
	cout << i << endl;
}














////栈（C++版本）头文件：
//
////使用自己的命名空间实现栈，
////防止和别人写栈有命名冲突
//namespace ggdpz 
//{
//	//栈类型：
//	typedef struct Stack
//	{
//		int* a;
//		int top;
//		int capacity;
//	}ST;
//
//	//栈初始化函数：
//	void StackInit(ST* ps);
//
//	//出栈函数：
//	void StackPush(ST* ps, int x);
//}

