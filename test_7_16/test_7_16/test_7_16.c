#define _CRT_SECURE_NO_WARNINGS 1

////引入Add.c文件中的Add函数:
//extern int Add(int, int);
//
//#include <stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = Add(a, b);//该函数在add.c文件中
//
//	printf("%d\n", c);
//
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	printf("%s\n", __FILE__);
//	printf("%d\n", __LINE__);
//
//	return 0;
//}


//#include <stdio.h>
//
//#define MAX 1000
//#define reg register           //为 register这个关键字，创建一个简短的名字
//#define do_forever for(;;)     //用更形象的符号来替换一种实现
//#define CASE break;case        //在写case语句的时候自动把break写上,但最好还是不要这样操作
////定义一句话
//#define DEBUG_PRINT printf("file:%s\nline:%d\ndate:%s\ntime:%s\n" ,__FILE__,__LINE__ ,__DATE__,__TIME__ )
//
//int main()
//{
//	DEBUG_PRINT;
//
//	return 0;
//}




//#include <stdio.h>
//
//#define PRINT(n,format) printf("the value of " #n " is " format "\n", n)
//
//int main()
//{
//	int a = 20;
//	PRINT(a, "%d");
//
//	int b = 15;
//	PRINT(b, "%d");
//
//	float f = 4.5f;
//	PRINT(f, "%f");
//
//	return 0;
//}




//#include <stdio.h>
//
//#define CAT(x,y) x##y
//
//int main()
//{
//	int HelloWorld = 2024;
//
//	printf("%d\n", CAT(Hello, World));
//
//	return 0;
//}



//#include <stdio.h>
//
//#define MAX(x, y) ((x)>(y)?(x):(y))
//
//int main()
//{
//	int a = 5;
//	int b = 6;
//	int c = MAX(a++, b++);
//
//	//int c = ((a++) > (b++) ? (a++) : (b++));
//
//	
//
//	printf("a = %d\n", a);
//	printf("b = %d\n", b);
//	printf("c = %d\n", c);
//
//	return 0;
//}



#include <stdio.h>

#define MAX(x,y) ((x)>(y)?(x):(y))

int main()
{
	int c = MAX(3, 5);
	printf("%d\n", c);

#undef MAX
//进行移除宏，之后不能再使用该宏

	return 0;
}