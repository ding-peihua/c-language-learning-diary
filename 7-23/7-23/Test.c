#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
////计算一下Func1种 ++count 语句总共执行了多少次
//void Func1(int N)
//{
//	int count = 0;
//
//	//第一个for循环
//	for (int i = 0; i < N; ++i)
//	{
//		//第一个for循环的内嵌for循环
//		for (int j = 0; j < N; ++j)
//		{
//			++count;
//		}
//	}
//
//	//第二个for循环
//	for (int k = 0; k < 2*N; ++k)
//	{
//		++count;
//	}
//
//	int M = 10;
//	while (M--)
//	{
//		++count;
//	}
//
//	printf("%d\n", count);
//}
//
//int main()
//{
//
//	return 0;
//}






//示例一：
//计算Func2的时间复杂度：
void Func2(int N)
{
	int count = 0;
	for (int k = 0; k < 2*N; ++k)
	{
		++count;
	}

	int M = 10;
	while (M--)
	{
		++count;
	}

	printf("%d\n", count);
}












//示例二：
//计算Func3的时间复杂度：
void Func3(int N, int M)
{
	int count = 0;
	for (int k = 0; k < M; ++k)
	{
		++count;
	}
	
	for (int k = 0; k < N; k++)
	{
		++count;
	}

	printf("%d\n", count);
}






//示例三：
//计算Func4的时间复杂度：
void Func4(int N)
{
	int count = 0;
	for (int k = 0; k < 100; ++k)
	{
		++count;
	}

	printf("%d\n", count + N);
}












//示例四：
//计算strchr的时间复杂度：
const char* strchr(const char* str, int character);
//strchr库函数：在str字符数组中查找一个字符
















//示例五：
#include <stdio.h>
//计算BubbleSort的时间复杂度：
void BubbleSort(int* a, int n)
{
	assert(a);

	for (size_t end = n; end > 0; --end)
	{
		int exchange = 0;
		for (size_t i = 1; i < end; ++i)
		{
			if (a[i - 1] > a[i])
			{
				Swap(&a[i - 1], &a[i]);
				exchange = 1;
			}
		}

		if (exchange == 0)
		{
			break;
		}
	}
}





//示例六：
//计算BinarySearch的时间复杂度：
int BinarySearch(int* a, int n, int x)
{
	assert(a);

	int begin = 0;
	int end = n - 1;
	// [begin, end]：begin和end是左闭右闭区间，因此有=号

	while (begin <= end)
	{
		int mid = begin + ((end - begin) >> 1);
		if (a[mid] < x)
		{
			begin = mid + 1;
		}
		else if (a[mid] > x)
		{
			end = mid - 1;
		}
		else
		{
			return mid;
		}
	}

	return -1;
}











//示例七：
//计算阶乘递归Fac的时间复杂度：
long long Fac(size_t N)
{
	if (0 == N)
	{
		return 1;
	}

	return Fac(N - 1) * N;
}













//示例八：
//计算斐波那契递归Fib的时间复杂度：
long long Fib(size_t N)
{
	if (N < 3)
	{
		return 1;
	}
	
	return Fib(N - 1) + Fib(N - 2);
}













//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char a;
//	char* str = &a;
//	strcpy(str, "hello");
//	printf(str);
//	
//	return 0;
//}





//计算BubbleSort的空间复杂度：
void BubbleSort(int* a, int n)
{
	assert(a);

	for (size_t end = n; end > 0; --end)
	{
		int exchange = 0;
		for (size_t i = 1; i < end; ++i)
		{
			if (a[i - 1] > a[i])
			{
				Swap(&a[i-1], &a[i]);
				exchange = 1;
			}
		}

		if (exchange == 0)
		{
			break;
		}
	}
}




//计算Fibonacci的空间复杂度：
//返回斐波那契数列的前n项
long long* Fibonacci(size_t n)
{
	if (n==0)
	{
		return NULL;
	}
	
	long long* fibArray = (long long*)malloc((n + 1) * sizeof(long long));
	
	fibArray[0] = 0;
	fibArray[1] = 1;

	for (int i = 2; i <= n; ++i)
	{
		fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
	}

	return fibArray;
}













//计算阶乘递归Fac的空间复杂度：
long long Fac(size_t N)
{
	if (N == 0)
	{
		return 1;
	}

	return Fac(N-1)*N;
}