#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int main()
{
	//生成变量：
	int a = 0; //题目中的a
	int n = 0; //a的前n项

	//输入 a 和 n -- scanf()函数
	scanf("%d %d", &a, &n);

	//创建相关变量：
	int sum = 0; //算出每一项后相加
	int k = 0; //用于算出每一项

	//使用 for循环 循环n次，算n次：
	int i = 0;
	for (i = 0; i < n; i++) //前n项，算n次
	{
		k = k * 10 + a; //利用规律算出每一项
		sum += k; //每一项相加
	}

	//输出结果：
	printf("%d\n", sum);

	return 0;
}