#define _CRT_SECURE_NO_WARNINGS 1

//表达式
//#include <stdio.h>
//
//int main()
//{	//一个大括号括起来的几条语句就是复合语句
//
//	3 + 5; //这里的 3 + 5 就是表达式语句
//
//	printf("hehe"); //这里的 printf() 就是函数调用语句
//
//	; //只有一个 ; 就是空语句
//
//	return 0;
//}


//
//#include <stdio.h>
//
//int main()
//{
//
//	return 0;
//}

//if
//#include <stdio.h>
//
//int main()
//{
//	int age = 0;
//	//输入
//	scanf("%d", &age);

	////单分支
	//if (age < 18)
	//{
	//	printf("未成年");
	//	//如果符合 表达式 ，则打印相应内容
	//	//如果不符合 表达式 ， 什么都不做
	//}

	////双分支
	//if (age < 18)
	//{
	//	printf("未成年");
	//	//如果符合 表达式 ，则打印相应内容
	//	
	//}
	//else
	//{
	//	//如果不符合 表达式 ，则执行else里的语句
	//	printf("已成年");
	//}

	//多分支
	//if (age < 18)
	//{//<18 青少年
	//	printf("青少年\n");
	//}
	//else if (age >= 18 && age <=30)//&&:条件与
	//{//18~30 青年
	//	printf("青年\n");
	//}
	//else if (age >= 31 && age <= 50)//&&:条件与
	//{//31~50 中年
	//	printf("中年\n");
	//}
	//else if (age >= 51 && age <= 80)//&&:条件与
	//{//51~800 中老年
	//	printf("中老年\n");
	//}
	//else if (age >= 81 && age <= 100)//&&:条件与
	//{//81~100 老年
	//	printf("老年\n");
	//}
	//else
	//{//101+ 老寿星
	//	printf("老寿星\n");
	//}

	//嵌套
	//if (age < 18)
	//{//<18 青少年
	//	printf("青少年\n");
	//}
	//else { //if语句里再嵌套一个if语句

	//	if (age >= 18 && age <= 30)//&&:条件与
	//	{//18~30 青年
	//		printf("青年\n");
	//	}
	//	else if (age >= 31 && age <= 50)//&&:条件与
	//	{//31~50 中年
	//		printf("中年\n");
	//	}
	//	else if (age >= 51 && age <= 80)//&&:条件与
	//	{//51~800 中老年
	//		printf("中老年\n");
	//	}
	//	else if (age >= 81 && age <= 100)//&&:条件与
	//	{//81~100 老年
	//		printf("老年\n");
	//	}
	//	else
	//	{//101+ 老寿星
	//		printf("老寿星\n");
	//	}

	//}
	//
//
//	return 0;
//}

////补充1
//#include <stdio.h>
//
//int main()
//{
//	int age = 0;
//
//	scanf( "%d", &age );
//
//	if ( age > 18 )
//		printf("已成年");
//		printf("谈恋爱");
//
//	return 0;
//}

//补充2
//#include <stdio.h>
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//
//	if ( a == 1 )
//		if ( b == 2 )
//			printf("hh");
//	else
//		printf("ll");
//
//	return 0;
//}

//补充3
//#include <stdio.h>
//
//int test1()
//{
//	//int n = 20;
//	int n = 2;
//	if ( n > 5 )
//		return 1;
//	return 2;
//	printf("hehe\n");
//}
//
//int test2()
//{
//	//int n = 20;
//	int n = 2;
//	if (n > 5)
//	{
//		return 1;
//	}
//	else
//	{
//		return 2;
//	}
//
//}

//int main()
//{
//	int m = test1();
//	printf("%d\n", m);
//
//	int n = test2();
//	printf("%d\n", n);
//	return 0;
//}


//补充4

//#include <stdio.h>
//
//int main()
//{
//	int n = 5;
//	if ( n = 3 )
//	{
//		printf("hehe\n");
//	}
//	return 0;
//}

//switch语句


//switch语句
//#include <stdio.h>
//
//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//
//	switch (day) // 得输入整型表达式
//	{
//	case 1: // case后写整形常量表达式
//		printf("星期一\n"); //语句
//		break; // 如果进入对应的case语句的话，退出switch语句，
//			   // 否则之后的case语句也会被执行
//	case 2:
//		printf("星期二\n");
//		break;
//	case 3:
//		printf("星期三\n");
//		break;
//	case 4:
//		printf("星期四\n");
//		break;
//	case 5:
//		printf("星期五\n");
//		break;
//	case 6:
//		printf("星期六\n");
//		break;
//	case 7:
//		printf("星期七\n");
//		break;
//
//	default:
//		printf("输入错误");
//		break;
//	}
//
//	return 0;
//}

//巧用break
//#include <stdio.h>
//
//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//
//	switch (day)
//	{
//	case 1: 
//	case 2:
//	case 3:
//	case 4:
//	case 5:
//		printf("weekday\n");
//		break;
//	case 6:
//	case 7:
//		printf("weekend\n");
//		break;
//	}
//
//
//	return 0;
//}


//while循环
//#include <stdio.h>
//
//int main()
//{
//	int n = 1;
//	//使用while循环打印 1~10
//	while ( n <= 10 )
//	{
//		printf("%d ", n);
//		n++;
//	}
//	return 0;
//}

////while循环
//#include <stdio.h>
//
//int main()
//{
//	int n = 1;
//	//使用break终止while循环
//	while (n <= 10)
//	{
//		if (n == 5)
//		{
//			break;
//		}
//		printf("%d ", n);
//		n++;
//	}
//	return 0;
//}

//while循环
//#include <stdio.h>
//
//int main()
//{
//	int n = 1;
//	//使用continue跳过循环中continue后面的语句
//	while (n <= 10)
//	{
//		if (n == 5)
//		{
//			continue;
//		}
//		printf("%d ", n);
//		n++;
//	}
//	return 0;
//}

//getchar() 和 putchar()
//#include <stdio.h>
//
//int main()
//{
//	// getchar()函数：接收一个字符
//	// 如果接收失败的话，会返回EOF(即-1)，
//	// 所以，该函数的返回类型是 int，
//	// 接收该函数返回值的变量也应该是int
//	// 因为字符本质是ASCii码，所以也可以接收字符
//	int ch = getchar();
//
//	// 跟getchar对应的还有putchar
//	// putchar()函数：打印字符，只能打印字符
//	putchar(ch);
//
//	return 0;
//}

//getchar()实现多组输入;
//#include <stdio.h>
//
//int main()
//{
//	int ch = 0;
//
//	// EOF - end of file - 文件结束标志
//	// #define EOF    (-1)
//	// 在函数读取失败的时候返回了 EOF
//	// 之前介绍过利用 scanf函数 来实现多组输入 
//	// scanf 函数读取成功，返回的是读取到的数据的个数
//	// 读取失败返回 EOF
//	// 
//	// getchar 读取成功返回字符的ASCII码值
//	// 读取失败返回EOF
//	// 回车也会读取,先读输入的字符，在读回车
//	// 
//	// ctrl + Z - 会让 scanf 或者 getchar 返回 EOF
//	while ( (ch = getchar()) != EOF )
//	{
//		putchar( ch );
//	}
//
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	//密码：123456
//	char password[20] = { 0 };
//
//	printf("请输入密码：>");
//	scanf("%s", password); // 数组本身就是地址，所以这里不用加&（取地址符）重点！！
//	
//	printf("请确认密码(Y/N)：");
//	char input = 0;
//
//	//getchar(); // 把键盘输入的 \n(回车) 取走 
//
//	//清理掉缓冲区中剩余的数据
//	while ( getchar() != '\n' )//循环直到取走\n（回车）为止
//	{
//		;
//	}
//
//	scanf("%c", &input); // 因为input是变量，所以要加&（取地址符）
//
//	if ('Y' == input)
//	{
//		printf("确认成功\n");
//	}
//	else 
//	{
//		printf("确认失败\n");
//	}
//
//	return 0;
//}