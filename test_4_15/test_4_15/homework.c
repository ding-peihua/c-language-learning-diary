#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
//
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		if (i = 5)// 不是 == 老六
//			printf("%d ", i);
//	}
//	return 0;
//}

//int func(int a)
//{
//    int b;
//    switch (a)
//    {
//    case 1: b = 30;
//    case 2: b = 20;
//    case 3: b = 16;
//    default: b = 0;
//    }
//    return b;   //没有break，一直留到default
//}
//
//int main()
//{
//    int b =func(1);
//    printf("%d", b);
//
//    return 0;
//}

//int main()
//{
//    float a = 0;
//
//	switch (a)
//	{
//	default:
//		break;
//	}
//
//    return 0;
//}

//#include <stdio.h>
//int main() {
//	int x = 3;
//	int y = 3;
//	switch (x % 2) {
//	case 1:
//		switch (y)
//		{
//		case 0:
//			printf("first");
//		case 1:
//			printf("second");
//			break;
//		default: printf("hello");
//		}
//	case 2:
//		printf("third");
//	}
//	return 0;
//}

//打印1-100之间所有3的倍数的数字
//#include <stdio.h>
//
//int main()
//{
//	for (int i = 1; i < 101; i++)
//	{
//
//		if (i % 3 == 0)
//		{
//			printf( "%d\n", i );
//		}
//
//	}
//	return 0;
//}

//将三个整数数按从大到小输出
//#include <stdio.h>
//
//int main()
//{
//	int input1 = 0;
//	int input2 = 0;
//	int input3 = 0;
//
//	//输入
//	printf( "请输入各不相等的三个整数：>" );
//	scanf("%d %d %d", &input1, &input2, &input3);
//
//	//判断输出
//	if ( input1 >= input2 && input1 >= input3 )
//	{
//		if (input2 > input3)
//		{
//			printf("%d %d %d", input1, input2, input3);
//		} 
//		else
//		{
//			printf("%d %d %d", input1, input3, input2);
//		}
//	}
//	else if (input2 >= input1 && input2 >= input3)
//	{
//		if (input1 > input3)
//		{
//			printf("%d %d %d", input2, input1, input3);
//		}
//		else
//		{
//			printf("%d %d %d", input2, input3, input1);
//		}
//	}
//	else if (input3 >= input1 && input3 >= input2)
//	{
//		if (input2 > input1)
//		{
//			printf("%d %d %d", input3, input2, input1);
//		}
//		else
//		{
//			printf("%d %d %d", input3, input1, input2);
//		}
//	}
//
//	return 0;
//}

//打印100~200之间的素数
//#include <stdio.h>
//
//int main()
//{
//	int num = 100;
//
//	//先生成100-200
//	while (num < 201)
//	{
//		//判断素数：
//		if ( num % 2 != 0  &&  num % 3 != 0 && num % 5 != 0 && num % 7 != 0 )
//		{
//			printf("%d ", num);
//		}
//
//		num ++ ;
//	}
//
//	return 0;
//}

//打印1000年到2000年之间的闰年
//#include <stdio.h>
//
//int main()
//{
//	int year = 1000;
//
//	//先生成1000-2000年
//	while (year < 2001)
//	{
//		//判断闰年：
//		//普通闰年：公历年份是4的倍数且不是100的倍数为普通闰年
//		//世纪闰年：公历年份是整百数的，必须是400的倍数才是世界闰年（如2000是世纪闰年，1900不是世纪闰年）。
//		//能 被4整除却不能被100整除 或能 被400整除的年份 就是闰年！
//		if ( (year % 4 == 0  &&  year % 100 != 0) || year % 400 == 0 )
//		{
//			printf("%d ", year);
//		}
//
//		year++;
//	}
//
//	return 0;
//}

//给定两个数，求这两个数的最大公约数
//#include <stdio.h>
//
//int main()
//{
//	
//	int num1 = 0;
//	int num2 = 0;
//	int divisor = 0; // 除数
//	int remainder = 0; //余数
//
//	//输入：
//	scanf("%d %d", &num1, &num2);
//
//	//判断输出：
//	if ( num1 == num2 )
//	{
//		printf("%d\n", num1);
//	}
//	else if ( num1 > num2 )
//	{
//		divisor = num1 / num2;
//		remainder = num1 % num2;
//		while ( remainder != 0 )
//		{
//			num2 = divisor;
//			num1 = remainder;
//			divisor = num1 / num2;
//			remainder = num1 % num2;
//		}
//	}
//	else
//	{
//		divisor = num2 / num1;
//		remainder = num2 % num1;
//		while (remainder != 0)
//		{
//			num1 = divisor;
//			num2 = remainder;
//			divisor = num2 / num1;
//			remainder = num2 % num1;
//		}
//	}
//
//	printf("%d", divisor);
//
//	return 0;
//}

//给定两个数，求这两个数的最大公约数

#include <stdio.h>

int main()
{
	int num1 = 0;
	int num2 = 0;
	int remainder = 0; //余数

	printf( "请输入两个整数: \n" );
	scanf( "%d %d", &num1, &num2 );

	while ( remainder = num1 % num2 ) // 余数不为0则一直循环
	{
		num1 = num2; // 被除数赋给除数
		num2 = remainder; // 余数赋给被除数
	} // 余数为0退出循环，此时num2(余数)为最大公约数

	printf("%d\n", num2);

	return 0;
}