#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int main()
{
	//创建变量：
	int n = 0; //输出的行数、正斜线、反斜线 的长度

	//多组输入，打印 x图案：
	while (scanf("%d", &n) == 1)
		//使用 while循环 进行多组输入
	{
		//定义 行和列：
		int i = 0; //行
		int j = 0; //列

		//使用 for循环 循环打印行：
		for (i = 0; i < n; i++)
		{
			//内嵌for循环 循环打印列：
			for (j = 0; j < n; j++)
			{
				//当 行等于列 或 行+列=行数-1 时打印 *号：
				if (i == j || (i+j == n-1))
					//i == j：x图像的正斜线
					//i+j == n-1：x图像的反斜线
				{
					printf("*"); //打印 *号
				}
				else
				{
					printf(" "); //打印空格
				}
			}

			//打印完一列后换行：
			printf("\n");
		}
	}

	return 0;
}